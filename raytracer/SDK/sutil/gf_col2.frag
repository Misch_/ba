#version 150 core

in vec2 texCoord;

uniform sampler2D I;
uniform sampler2D a_r_sat;
uniform sampler2D a_g_sat;
uniform sampler2D a_b_sat;
uniform sampler2D b_sat;
uniform sampler2D boxsize;
uniform int fltRadius;
uniform int width; // window Size, e.g. 512x512px
uniform int height;

vec3 getRGBat(sampler2D img, int i, int j)
{
	return texture2D(img, vec2(texCoord.x + float(i)/float(width),texCoord.y + float(j)/float(height))).xyz;
}

vec3 boxfilter(sampler2D sat, int fltRadius)
{
	float boxSize = getRGBat(boxsize,0,0).x;

	vec3 upRight	= getRGBat(sat,	fltRadius,		fltRadius);
	vec3 upLeft		= getRGBat(sat, -(fltRadius+1),	fltRadius);
	vec3 lowRight	= getRGBat(sat, fltRadius,		-(fltRadius+1));
	vec3 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec3 mean = upRight - upLeft - lowRight + lowLeft;
	
	mean /= boxSize;

	return mean;
}

void main(){	
	
	vec3 a_i_r = boxfilter(a_r_sat,fltRadius);
	vec3 a_i_g = boxfilter(a_g_sat,fltRadius);
	vec3 a_i_b = boxfilter(a_b_sat,fltRadius);

	vec3 currentI = getRGBat(I,0,0);
	float aI_r = dot(a_i_r,currentI);
	float aI_g = dot(a_i_g,currentI);
	float aI_b = dot(a_i_b,currentI);
	
	vec3 b_i = boxfilter(b_sat,fltRadius);

	vec3 q = vec3(aI_r,aI_g,aI_b) + b_i;

	gl_FragData[0].xyz = q;
}