#version 150 core

out vec4 fragcolor;

in vec2 texCoord;


uniform sampler2D img;
uniform sampler2D img2;

void main(){


	float v = texture2D(img, texCoord).x;
	
	vec4 c = vec4(v/341, 0, 0, 0);
		
		if(v>341)
			c.y = (v-341)/682;
		
		if(v>682)
			c.z = (v-682)/1024;
		
		fragcolor = c;
}