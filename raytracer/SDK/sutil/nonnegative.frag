#version 150 core

in vec2 texCoord;

uniform sampler2D tex;
	
void main()
{
	vec4 inVec = texture2D(tex, vec2(texCoord.x,texCoord.y));

	inVec = abs(inVec);
	//inVec = (inVec + 1)/2;

	gl_FragData[0].xyz = inVec.xyz;
}