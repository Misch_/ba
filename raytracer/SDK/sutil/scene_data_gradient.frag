#version 150 core

out vec4 fragcolor;

in vec2 texCoord;


uniform sampler2D brdf;
uniform sampler2D pos;
uniform sampler2D normal;

uniform int width;
uniform int height;

uniform float scale_brdf;
uniform float scale_pos;
uniform float scale_normal;

mat3 sobelX = mat3(	1, 0, -1,
					2, 0, -2,
					1, 0, -1);
					
mat3 sobelY = mat3( 1, 2, 1,
					0, 0, 0,
					-1, -2, -1);
					
void sobelize(inout vec3 oX, inout vec3 oY, in sampler2D v)
{
	vec3 f00 = texture2D(v, vec2(texCoord.x-1.f/width, texCoord.y-1.f/height)).xyz;
	vec3 f01 = texture2D(v, vec2(texCoord.x-1.f/width, texCoord.y)).xyz;
	vec3 f02 = texture2D(v, vec2(texCoord.x-1.f/width, texCoord.y+1.f/height)).xyz;
	
	vec3 f10 = texture2D(v, vec2(texCoord.x, texCoord.y-1.f/height)).xyz;
	vec3 f12 = texture2D(v, vec2(texCoord.x, texCoord.y+1.f/height)).xyz;
	
	vec3 f20 = texture2D(v, vec2(texCoord.x+1.f/width, texCoord.y-1.f/height)).xyz;
	vec3 f21 = texture2D(v, vec2(texCoord.x+1.f/width, texCoord.y)).xyz;
	vec3 f22 = texture2D(v, vec2(texCoord.x+1.f/width, texCoord.y+1.f/height)).xyz;
	
	oX =	sobelX[0][0] * f00 + sobelX[0][1]*f01 + sobelX[0][2]*f02 +
			sobelX[1][0] * f10 /*+ sobelX[1][1]*f11*/ + sobelX[1][2]*f12 +
			sobelX[2][0] * f20 + sobelX[2][1]*f21 + sobelX[2][2]*f22;
	
	oY =	sobelY[0][0] * f00 + sobelY[0][1]*f01 + sobelY[0][2]*f02 +
			sobelY[1][0] * f10 /*+ sobelY[1][1]*f11*/ + sobelY[1][2]*f12 +
			sobelY[2][0] * f20 + sobelY[2][1]*f21 + sobelY[2][2]*f22;
	
}

void main(){
	vec3 flt_brdf_x, flt_brdf_y; 
	sobelize(flt_brdf_x, flt_brdf_y, brdf);

	vec3 flt_pos_x, flt_pos_y; 
	sobelize(flt_pos_x, flt_pos_y, pos);

	vec3 flt_normal_x, flt_normal_y; 
	sobelize(flt_normal_x, flt_normal_y, normal);

	float brdf_gmag = sqrt(flt_brdf_x.x*flt_brdf_x.x + flt_brdf_y.x*flt_brdf_y.x) 
					+ sqrt(flt_brdf_x.y*flt_brdf_x.y + flt_brdf_y.y*flt_brdf_y.y) 
					+ sqrt(flt_brdf_x.z*flt_brdf_x.z + flt_brdf_y.z*flt_brdf_y.z);
					
	float pos_gmag =  sqrt(flt_pos_x.x*flt_pos_x.x + flt_pos_y.x*flt_pos_y.x)
					+ sqrt(flt_pos_x.y*flt_pos_x.y + flt_pos_y.y*flt_pos_y.y) 
					+ sqrt(flt_pos_x.z*flt_pos_x.z + flt_pos_y.z*flt_pos_y.z);
					

	
	float norm_gmag = sqrt(flt_normal_x.x*flt_normal_x.x + flt_normal_y.x*flt_normal_y.x) 
					+ sqrt(flt_normal_x.y*flt_normal_x.y + flt_normal_y.y*flt_normal_y.y) 
					+ sqrt(flt_normal_x.z*flt_normal_x.z + flt_normal_y.z*flt_normal_y.z);

//binarize data
	pos_gmag = pos_gmag>50 ? 1 : 0;
	brdf_gmag = brdf_gmag>0.5 ? 1 : 0;
	norm_gmag = norm_gmag>1 ? 1 : 0; 
	
gl_FragData[0].x = 1 - (/*brdf_gmag +*/ pos_gmag + norm_gmag)/3.f; 
}