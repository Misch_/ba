#version 150 core

in vec2 texCoord;

uniform sampler2D tex1;
uniform sampler2D tex2;
	
vec3 currentRGB(sampler2D img)
{
	return texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
}

void main()
{
	float mult_rr = currentRGB(tex1).x * currentRGB(tex2).x;
	float mult_rg = currentRGB(tex1).x * currentRGB(tex2).y;
	float mult_rb = currentRGB(tex1).x * currentRGB(tex2).z;

	float mult_gr = currentRGB(tex1).y * currentRGB(tex2).x;
	float mult_gg = currentRGB(tex1).y * currentRGB(tex2).y;
	float mult_gb = currentRGB(tex1).y * currentRGB(tex2).z;

	float mult_br = currentRGB(tex1).z * currentRGB(tex2).x;
	float mult_bg = currentRGB(tex1).z * currentRGB(tex2).y;
	float mult_bb = currentRGB(tex1).z * currentRGB(tex2).z;

	vec3 mult_rx = vec3(mult_rr, mult_rg, mult_rb);
	vec3 mult_gx = vec3(mult_gr, mult_gg, mult_gb);
	vec3 mult_bx = vec3(mult_br, mult_bg, mult_bb);

	gl_FragData[0].xyz = mult_rx;
	gl_FragData[1].xyz = mult_gx;
	gl_FragData[2].xyz = mult_bx;
}