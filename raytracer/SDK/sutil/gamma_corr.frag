#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform float gamma;

void main()
{
	vec3 q_old = texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
	float exp = float(1)/float(gamma);
	
	vec3 q_new = pow(q_old, vec3(exp));

	gl_FragData[0].xyz = q_new;
}