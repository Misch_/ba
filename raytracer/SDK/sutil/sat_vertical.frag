#version 150 core

in vec2 texCoord;

uniform int size;
uniform sampler2D img;
uniform int pass;

vec4 getRGBat(sampler2D img, int i, int j)
{
	return texture2D(img, vec2(texCoord.x + float(i)/float(size),texCoord.y + float(j)/float(size))).xyzw;
}

vec4 verticalSAT(int i){
	float offset = (-1) * pow(2.0,float(i));
	vec4 sum;

	if (texCoord.y + offset/float(size) < 0.0){
		sum = getRGBat(img,0,0);
	}
	else{
		sum = getRGBat(img,0,0) + getRGBat(img,0,int(offset));
	}

	return sum;
}

void main(){
	vec4 sat = verticalSAT(pass);
	gl_FragData[0].xyzw = sat;
}