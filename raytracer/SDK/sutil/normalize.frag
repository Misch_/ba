#version 150 core

in vec2 texCoord;

uniform sampler2D tex;
uniform float factor;

void main()
{
	vec3 normalized = texture2D(tex, vec2(texCoord.x,texCoord.y)).xyz;
	normalized /= float(factor);
	gl_FragData[0].xyz = normalized;
}