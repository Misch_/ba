#version 150 core

in vec2 texCoord;

uniform sampler2D I_sat;
uniform sampler2D p_sat;
uniform sampler2D Ip_sat;
uniform sampler2D II_sat;
uniform sampler2D boxsize;
uniform int fltRadius;
uniform float epsilon;
uniform int width; // window Size, e.g. 512x512px
uniform int height;

vec3 getRGBat(sampler2D img, int i, int j)
{
	return texture2D(img, vec2(texCoord.x + float(i)/float(width),texCoord.y + float(j)/float(height))).xyz;
}

vec3 boxfilter(sampler2D sat, int fltRadius)
{
	float boxSize = getRGBat(boxsize,0,0).x;

	vec3 upRight	= getRGBat(sat,	fltRadius,		fltRadius);
	vec3 upLeft		= getRGBat(sat, -(fltRadius+1),	fltRadius);
	vec3 lowRight	= getRGBat(sat, fltRadius,		-(fltRadius+1));
	vec3 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec3 mean = upRight - upLeft - lowRight + lowLeft;
	
	mean /= boxSize;

	return mean;
}

void main()
{	
	vec3 mean_I = boxfilter(I_sat, fltRadius);
	vec3 mean_p = boxfilter(p_sat, fltRadius);

	vec3 mean_Ip = boxfilter(Ip_sat,fltRadius);
	vec3 cov_Ip = mean_Ip - (mean_I * mean_p);

	vec3 mean_II = boxfilter(II_sat,fltRadius);
	vec3 var_I = mean_II - (mean_I * mean_I);
	
	vec3 a = cov_Ip/(var_I + vec3(epsilon));
	vec3 b = mean_p - a * mean_I;

	gl_FragData[0].xyz = a;
	gl_FragData[1].xyz = b;
}