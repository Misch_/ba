#pragma once
#if defined(__APPLE__)
#  include <GLUT/glut.h>
#  define GL_FRAMEBUFFER_SRGB_EXT           0x8DB9
#  define GL_FRAMEBUFFER_SRGB_CAPABLE_EXT   0x8DBA
#else
#  include <GL/glew.h>
#  if defined(_WIN32)
#    include <GL/wglew.h>
#  endif
#  include <GL/glut.h>
#endif
#include "vertexData.h"
#include <stdio.h>

VertexData::~VertexData(){
		delete [] VertexData::attrib;
}

GLuint VertexData::create(int numOfAttributes){
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);

	attrib = new Attribute[numOfAttributes];
	attNum = numOfAttributes;

	return vao;
}

void VertexData::storeAttribute(int index, float *data, int elemSize, int elem, const char *name){

	attrib[index].name = name;
	attrib[index].elemSize = elemSize;
	attrib[index].numOfElem = elem;

	GLuint bon; // buffer object name
	glGenBuffers(1, &bon);
	glBindBuffer(GL_ARRAY_BUFFER, bon);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*elemSize*elem, data, GL_STATIC_DRAW);

	attrib[index].vbo = bon;

	//actualIndex++;
}

void VertexData::storeIndices(GLuint *data, int size){
	glBindVertexArray(vao);
	GLuint bon;
	glGenBuffers(1, &bon);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bon);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}

void VertexData::bindToShader(GLuint id){
	
	glBindVertexArray(vao);

	for(int i=0; i<attNum; i++){
		glBindBuffer(GL_ARRAY_BUFFER,attrib[i].vbo);
		const GLint loc(glGetAttribLocation(id, attrib[i].name));
		if(loc<0)
			printf("The name %s is not an active Attribute in the Shader with the id %i\n", attrib[i].name, id);
		else{
			glVertexAttribPointer(loc, attrib[i].elemSize, GL_FLOAT, GL_TRUE, 0, NULL);
			glEnableVertexAttribArray(loc);
		}
	}
}



