#version 150 core

in vec2 texCoord;

uniform int size;
uniform sampler2D img;
uniform int pass;

vec4 getRGBat(sampler2D img, int i, int j)
{
	return texture2D(img, vec2(texCoord.x + float(i)/float(size),texCoord.y + float(j)/float(size))).xyzw;
}

vec4 horizontalSAT(int i){
	float offset = (-1) * pow(2.0,float(i));
	vec4 sum;

	if (texCoord.x + offset/float(size) < 0.0){
		sum = getRGBat(img,0,0);
	}
	else{
		sum = getRGBat(img,0,0) + getRGBat(img,int(offset),0);
	}

	return sum;
}

void main(){
	vec4 sat = horizontalSAT(pass);
	gl_FragData[0].xyzw = sat;
}