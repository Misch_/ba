#version 150 core

in vec2 texCoord;

uniform sampler2D guide1;
uniform sampler2D guide2;

uniform sampler2D a_r1_sat;
uniform sampler2D a_r2_sat;
uniform sampler2D a_g1_sat;
uniform sampler2D a_g2_sat;
uniform sampler2D a_b1_sat;
uniform sampler2D a_b2_sat;
uniform sampler2D b_sat;
uniform sampler2D boxsize;
uniform int fltRadius;
uniform int width; // window Size, e.g. 512x512px
uniform int height;

vec3 getRGBat(sampler2D img, int i, int j)
{
	return texture2D(img, vec2(texCoord.x + float(i)/float(width),texCoord.y + float(j)/float(height))).xyz;
}

vec3 boxfilter(sampler2D sat, int fltRadius)
{
	float boxSize = getRGBat(boxsize,0,0).x;

	vec3 upRight	= getRGBat(sat,	fltRadius,		fltRadius);
	vec3 upLeft		= getRGBat(sat, -(fltRadius+1),	fltRadius);
	vec3 lowRight	= getRGBat(sat, fltRadius,		-(fltRadius+1));
	vec3 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec3 mean = upRight - upLeft - lowRight + lowLeft;
	
	mean /= boxSize;

	return mean;
}

void main(){	
	
	vec3 a_i_r1 = boxfilter(a_r1_sat,fltRadius);
	vec3 a_i_r2 = boxfilter(a_r2_sat,fltRadius);
	float[6] a_i_r = float[6](a_i_r1.x, a_i_r1.y, a_i_r1.z, a_i_r2.x, a_i_r2.y, a_i_r2.z);

	vec3 a_i_g1 = boxfilter(a_g1_sat,fltRadius);
	vec3 a_i_g2 = boxfilter(a_g2_sat,fltRadius);
	float[6] a_i_g = float[6](a_i_g1.x, a_i_g1.y, a_i_g1.z, a_i_g2.x, a_i_g2.y, a_i_g2.z);

	vec3 a_i_b1 = boxfilter(a_b1_sat,fltRadius);
	vec3 a_i_b2 = boxfilter(a_b2_sat,fltRadius);
	float[6] a_i_b = float[6](a_i_b1.x, a_i_b1.y, a_i_b1.z, a_i_b2.x, a_i_b2.y, a_i_b2.z);


	//vec3 currentI = getRGBat(I,0,0);
	vec3 tmp1 = getRGBat(guide1,0,0);
	vec3 tmp2 = getRGBat(guide2,0,0);
	float[6] current_I = float[6](tmp1.x,tmp1.y,tmp1.z,tmp2.x,tmp2.y,tmp2.z);

	float aI_r = 0.f;
	float aI_g = 0.f;
	float aI_b = 0.f;
	
	for (int i = 0; i<6; i++){
		float mu = current_I[i];
		aI_r += a_i_r[i]*mu;
		aI_g += a_i_g[i]*mu;
		aI_b += a_i_b[i]*mu;
	}

	vec3 b_i = boxfilter(b_sat,fltRadius);

	vec3 q = vec3(aI_r,aI_g,aI_b) + b_i;

	gl_FragData[0].xyz = q;
}