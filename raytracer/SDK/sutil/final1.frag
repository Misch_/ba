#version 150 core

in vec2 texCoord;

uniform sampler2D smap;

uniform sampler2D smin_base;
uniform sampler2D smin;

uniform sampler2D base;
uniform sampler2D img0;
uniform sampler2D img1;
uniform sampler2D img2;
uniform sampler2D img3;

uniform float gamma;

void main()
{

vec4 sigma = round(4*texture2D(smap, texCoord));

	vec4 c[5];
	//glsl seems not to like dynamic indexing on sampler2D arrays... so lets make the code overly complicated!
	c[0] = texture2D(base, texCoord);
	c[1] = texture2D(img0, texCoord);
	c[2] = texture2D(img1, texCoord);
	c[3] = texture2D(img2, texCoord);
	c[4] = texture2D(img3, texCoord);
	
	float t, m;
	int s = int(sigma.x);	
	if(sigma.x>0)
		m = texture2D(smin,texCoord)[s-1];
	else
		m = texture2D(smin_base, texCoord).x;
			
	t = m - sigma.x;
		
	if(t>0)
		gl_FragColor = (1.f-t)*c[s] + t*c[s+1];
	else
		gl_FragColor = (-t)*c[s-1] + (1+t)*c[s];

	/*
	if(sigma.x==0)
		gl_FragColor = texture2D(base, texCoord);
		
	if(sigma.x==1)
		gl_FragColor = texture2D(img0, texCoord);
			
	if(sigma.x==2)
		gl_FragColor = texture2D(img1, texCoord);
			
	if(sigma.x==3)
		gl_FragColor = texture2D(img2, texCoord);
		
	if(sigma.x==4)
		gl_FragColor = texture2D(img3, texCoord);
	*/	
		
	gl_FragColor = pow(max(vec4(0),gl_FragColor),vec4(1/gamma)); //DO NOT PERFORM GAMMACORRECTION WHEN COMPUTING MSE!!!!!!!
}