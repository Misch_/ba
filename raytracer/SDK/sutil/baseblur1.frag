#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform int size; //width of img eg. 4 x screen resolution

int fsize	= 8;
//gaussian with sigma=0.5 with evaluation-steps of size 0.25
float gauss[8]	= float[](	0.045089598, 0.09545468, 0.157378162, 0.20207756, 0.20207756, 0.157378162, 
							0.09545468, 0.045089598);
			
//indexing: fsize/(2*size) to reach the beginning of the filteregion.
//-0.5/size to fall on the center of pixels at 4times higher resolution
//+1/size because we already are on the filters fsize-1 th element
// => -(fsize/2-0.5)/size 
					
// ' center  of pixel
// | pixelborder

// |-------'-------|-------'-------|
// |-'-|-'-|-'-|-'-|-'-|-'-|-'-|-'-|
//       --0.5/size
//   ------1.5/size

void main()
{
	gl_FragData[0] = vec4(0,0,0,0);
	float s = fsize/2-0.5;
	for(int i= 0; i<fsize; i++)
		gl_FragData[0].xyz += gauss[i]*texture2D(img, vec2(texCoord.x - (s-i)/size, texCoord.y)).xyz;
}