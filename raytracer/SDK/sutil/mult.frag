#version 150 core

in vec2 texCoord;

uniform sampler2D tex1;
uniform sampler2D tex2;
	
vec3 currentRGB(sampler2D img)
{
	return texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
}

void main()
{
	vec3 mult = currentRGB(tex1) * currentRGB(tex2);
	gl_FragData[0].xyz = mult;
}