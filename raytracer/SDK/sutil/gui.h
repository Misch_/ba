#pragma once

#include <AntTweakBar.h>
#include "sutil.h"

/******************************************************************************************
 * The GUI using AntTweakBar ( http://www.antisphere.com/Wiki/tools:anttweakbar). 
 * Some parameters can directly be modified in it while others are readable only.
 * Most params of the scene can be accessed from here, therefore another important role 
 * of this class is to provide the parameters to other application-parts.
 * Careful: Some parameters are deprecated! 
 ******************************************************************************************/

class GUI{
private:
	SUTILAPI GUI(); //its a sinlgeton so no public constructor 
	static TwBar* menuBar;
	//gamma weight for stopmap
	static float gamma_weight;
	static float gamma_weight_d;
	//which texture should be shown when using IC (direct-/ indirect light, both..)
	static int ic_mode;
	//maximum number of indirect bounces
	static int max_bounces;
	//samples per frame
	static float samples_per_frame;
	//max samples per pixel per frame
	static int max_ssp_frame;
	//initial number of smaples
	static int init_samples;
	//threshold for outlier detection
	static float outlier_treshold;

	//debug texture we want to see
	static int debug_textue_to_show;
	//deguber multiplicator 
	static int debug_mult;
	//debug channel
	static int debug_channel;
	//self explaining...
	static bool debug_enabled;
	//negate vlaues yes/no
	static bool debug_negate;
	//use irradiance caching
	static bool use_ic;
	//ratio if direct / indirect MSE reduction
	static float mseRatioIC;
	
	//frame at which we want to stop the iteration
	static int stop_frame;

	static bool show_direct;
	static int show_component;
	static bool filter_changed;
	//filter params
	static float minFilter_ind;
	static float fScale_ind;
	static float minFilter_dir;
	static float fScale_dir;
	static float epsilon;
	static int	fltRadius;

	//depth of field params
	static float dof_focal_plane;
	static float dof_lens_radius;

	static float gamma_correction;

	static float avgNS;

	static int denoiser_type;

//	static void TW_CALL setIC_options(const void* val, void *clientData);
//	static void TW_CALL getIC_options(void* val,void *clientData);
	static void TW_CALL setDebug_options(const void* val, void *clientData);
	static void TW_CALL getDebug_options(void* val,void *clientData);
	static void TW_CALL setFilterParam(const void* val, void *clientData);
	static void TW_CALL getFilterParam(void* val,void *clientData);
	
public:
	 
	 SUTILAPI static void load();
	 SUTILAPI static void draw() { TwDraw(); }
	 SUTILAPI static void resize(int w, int h){ TwWindowSize(w, h); }

	 //getters
	 SUTILAPI static int getInitSamples(){ return init_samples; }
	 SUTILAPI static float getGamma(){ return gamma_weight;}
	 SUTILAPI static float getGammaCorr(){ return gamma_correction;}
	 SUTILAPI static float getGammaDirect(){ return gamma_weight_d;}
	 SUTILAPI static float getOutlierThreshold(){return outlier_treshold;}
	 SUTILAPI static int getICMode(){ return ic_mode;}
	 SUTILAPI static int getMaxBounces(){ return max_bounces;}
	 SUTILAPI static float getSamplesPerFrame(){ return samples_per_frame;}
	 SUTILAPI static int getMaxSSPFrame(){ return max_ssp_frame;}
	 SUTILAPI static bool isDebugEnabled(){ return debug_enabled; }
	 SUTILAPI static int getDebugChannel(){ return debug_channel; }
	 SUTILAPI static int getDebugMult(){ return debug_mult; }
	 SUTILAPI static int getDebugTexture(){ return debug_textue_to_show; }
	 SUTILAPI static int isNegateActive(){ return debug_negate; }
	 SUTILAPI static bool isIrradianceCachingEnabled(){return use_ic; }
	 SUTILAPI static bool showDirectLight(){ return show_direct; }
	 SUTILAPI static int getStopFrame(){return stop_frame; }
	 SUTILAPI static float getGammaCorrection(){return gamma_correction; }
	 SUTILAPI static float getFilterScaleDirect(){ return fScale_dir; }
	 SUTILAPI static float getFilterScaleIndirect(){ return fScale_ind; }
	 SUTILAPI static float getFilterMinDirect(){ return minFilter_dir; }
	 SUTILAPI static float getFilterMinIndirect(){ return minFilter_ind; }
	 SUTILAPI static float getTotalMSE(){ return mseRatioIC; }	//careful, this only return the *estimated* MSE, not the true one...
	 SUTILAPI static int getDenoiserType(){ return denoiser_type; }
	 SUTILAPI static float getEpsilon(){ return epsilon;};
	 SUTILAPI static int getFltRadius(){ return fltRadius;};


	 //filter param functions
	 SUTILAPI static bool filterChanged(){ return filter_changed;}
	 SUTILAPI static void toggleFilterChanged(){ filter_changed = !filter_changed;}

	//direct setters
	 SUTILAPI static void setDOFfocalplane(float fp){dof_focal_plane = fp;}
	 SUTILAPI static float getDOFfocalplane(){return dof_focal_plane;}
	 SUTILAPI static void setDOFlensradius(float lr){dof_lens_radius = lr;}
	 SUTILAPI static float getDOFlensradius(){return dof_lens_radius;}
	 SUTILAPI static int getICDebugComponent(){ return show_component; }
	 SUTILAPI static void setAvgNS( float ans ){ avgNS = ans; }
	 SUTILAPI static float getAvgNS(){ return avgNS; }
	 SUTILAPI static void setTotalMSE( float mse ){ mseRatioIC = mse; } 
	 SUTILAPI static void setDenoiserType(int type){denoiser_type=type;}


};