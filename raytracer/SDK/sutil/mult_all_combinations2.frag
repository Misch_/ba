#version 150 core

in vec2 texCoord;

uniform sampler2D guide1;	// The rgb-values of the 1st guide.
uniform sampler2D guide2;	// The rgb-values of the 2nd guide.
							// In fact the values of guide1 and guide2 at a pixel
							// should be treated as one 6-dim vector instead of two 3-dim vectors
	
vec3 currentRGB(sampler2D img)
{
	return texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
}

void main()
{
	int dim = 6;
	vec3 v1 = currentRGB(guide1);
	vec3 v2 = currentRGB(guide2);

	float features[6] = float[6](v1.x,v1.y,v1.z,v2.x,v2.y,v2.z);
	float mult[36];

	
	// mult:
	//	c1*c1, c1*c2, c1*c3, c1*c4, c1*c5, c1*c6,
	//	c2*c1, c2*c2, c2*c3, c2*c4, c2*c5, ...
	//	...
	for (int i = 0; i < dim; i++){
		for (int j = 0; j < dim; j++){
			mult[j + i*dim] = features[i] * features[j];
		}
	}

	// only give the values that are really necessary
	//	[0]		[1]		[2]		[3]		[4]		[5]
	//	symm	[7]		[8]		[9]		[10]	[11]
	//	symm	symm	[14]	[15]	[16]	[17]
	//	symm	symm	symm	[21]	[22]	[23]
	//	symm	symm	symm	symm	[28]	[29]
	//	symm	symm	symm	symm	symm	[35]
	gl_FragData[0].xyzw =	vec4(mult[0],	mult[1],	mult[2],	mult[3]);
	gl_FragData[1].xyzw =	vec4(mult[4],	mult[5],	mult[7],	mult[8]);
	gl_FragData[2].xyzw =	vec4(mult[9],	mult[10],	mult[11],	mult[14]);
	gl_FragData[3].xyzw =	vec4(mult[15],	mult[16],	mult[17],	mult[21]);
	gl_FragData[4].xyzw =	vec4(mult[22],	mult[23],	mult[28],	mult[29]);
	gl_FragData[5].x =			 mult[35];
}