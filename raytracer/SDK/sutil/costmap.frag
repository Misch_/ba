/**
*	Computes the estimated MSE reduction per pixel when one additional pixel is shot on the "best" scale.
*	In contrast to the method in the paper, we don't know a priori how many pixel will be shot in a pixelregion. 
*	Because of this we assume that only one additional smaple per pixel is shot, and based on this assumption we compute the MSE reduction.
*	We assume then that additional samples reduce the MSE by the same amount. This is not correct, but as approximation it should be good enough.
*/


#version 150 core

in vec2 texCoord;


uniform sampler2D basens;
uniform sampler2D base_bright;
uniform sampler2D bright;

uniform sampler2D basevar;
uniform sampler2D var;

uniform sampler2D cost0;
uniform sampler2D stop0;

uniform sampler2D ns;

uniform sampler2D debug;

uniform samplerBuffer nsNorm;

uniform float s_imp;

float minVal = 0.0;

/*
* Computed the a,b and c coefficients of a quadratic function f(x) = a*x*x + b*x + c given three points.
*/
void findQuadraticFunc(out vec3 coef, in float y0, in float y1, in float y2, in float x0, in float x1, in float x2){
	coef.x = (x0*(y1-y2)+x1*(y2-y0)+x2*(y0-y1))/((x0-x1)*(x0-x2)*(x2-x1));
	coef.y = (x0*x0*(y1-y2)+x1*x1*(y2-y0)+x2*x2*(y0-y1))/((x0-x1)*(x0-x2)*(x1-x2));
	coef.z = (x0*x0*(x1*y2-x2*y1)+x0*(x2*x2*y1-x1*x1*y2)+x1*x2*y0*(x1-x2))/((x0-x1)*(x0-x2)*(x1-x2));
}

void main(){

	gl_FragData[1] = vec4(0,0,0,0);
	float baseNS = texture2D(basens, texCoord).x;	//scale 0
	vec4 nSamples = texture2D(ns, texCoord);		//scale 1 to 4
	
	vec4 sm = texture2D(stop0, texCoord);
	
	vec4 scaleMSE = texture2D(cost0, texCoord);
	//vec4 variance = texture2D(var, texCoord);
	
	//The brightness-values
	float base_brightness = texture2D(base_bright, texCoord).x; //scale 0
	vec4 brightness = texture2D(bright, texCoord);				//scale 1 to 4

	//compute the variance of the base-scale (eg the pixelsized boxfilter)
	//stored is the variance ofthe smpales but we want the variance of the mean estimatro, which means division by nSamples
	float baseMSE = texture2D(basevar, texCoord).x/baseNS;
	
	float norm = texelFetch(nsNorm, 0).x;
	//for each scale do:
	//if we stop at this scale divide by brightness (lum^2 +alpha) to get relative mse
	//   and divide by (1+nSamples) of this scale to get the relative gain
	//else add MSE difference of the two subsequent scales (this vlaue is negativ!) to the old MSE and continu
	
	
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	//compute interpolated scale
	
	int scale = 4;
	if(sm.x == 1)
		scale = 0;
	else if(sm.y == 1)
		scale = 1;	
	else if(sm.z == 1)
		scale = 2;	
	else if(sm.w == 1)
		scale = 3;
	
	vec3 funcCoef;
	if(scale<=1){
		findQuadraticFunc(funcCoef, baseMSE, scaleMSE.x, scaleMSE.y, 0, 1, 2);		
	} else if(scale==2){
		findQuadraticFunc(funcCoef, scaleMSE.x, scaleMSE.y, scaleMSE.z, 1, 2, 3);

	} else{
		findQuadraticFunc(funcCoef, scaleMSE.y, scaleMSE.z, scaleMSE.w, 2, 3, 4);
	}
	
	float minVal = -funcCoef.y/(2.f*funcCoef.x);
	
	//stores location of the minimum of the quadratic MSE function... 
	//this vlaue is used later for interpolation purposes
	//be carefull due to noisy data, vlaues can be totally wrong therefore ignore obviously wrong results...
	if(minVal >= max(0, scale-2) && minVal <= min(4, scale))
		gl_FragData[2] = vec4(minVal);
	else
		gl_FragData[2] = vec4(scale);
	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////
	
	gl_FragData[0].x = baseMSE; 
	if(sm.x==1 ){	
		gl_FragData[0].x = gl_FragData[0].x/base_brightness/(1+baseNS);
		gl_FragData[1].x = 0.f;
		return;
	}	
	
	gl_FragData[0].x = scaleMSE.x; 	
	if(sm.y==1 ){
		gl_FragData[0].x = norm * gl_FragData[0].x/brightness.x/(1+nSamples.x);
		gl_FragData[1].x = 1.f/4.f;
		return;
	}		
	
	gl_FragData[0].x = scaleMSE.y;	
	if(sm.z==1){
		gl_FragData[0].x = s_imp * norm * gl_FragData[0].x/brightness.y/(1+nSamples.y);
		gl_FragData[1].x = 2.f/4.f;
		return;
	}
		
	gl_FragData[0].x = scaleMSE.z;		
	if(sm.w==1){
		gl_FragData[0].x = s_imp * s_imp * norm * gl_FragData[0].x/brightness.z/(1+nSamples.z);
		gl_FragData[1].x = 3.f/4.f;
		return;
	}
	
	gl_FragData[0].x = scaleMSE.w;
	gl_FragData[0].x = s_imp * s_imp * s_imp * norm * gl_FragData[0].x/brightness.w/(1+nSamples.w);
	gl_FragData[1].x = 4.f/4.f;		
}