#version 150 core

in vec2 texCoord;

uniform sampler2D sat;
uniform sampler2D boxSize;
uniform int size; // window Size, e.g. 512x512px
uniform int fltRadius; // filter radius: a "box" will have the size of 2*fltRadius+1

vec4 getRGBat(sampler2D img, int i, int j)
{
	return texture2D(img, vec2(texCoord.x + float(i)/float(size),texCoord.y + float(j)/float(size))).xyzw;
}

void main()
{
	float boxSize = getRGBat(boxSize,0,0).x;
	vec4 upRight	= getRGBat(sat,fltRadius,fltRadius);
	vec4 upLeft		= getRGBat(sat, -(fltRadius+1),fltRadius);
	vec4 lowRight	= getRGBat(sat, fltRadius, -(fltRadius+1));
	vec4 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec4 mean = upRight - upLeft - lowRight + lowLeft;
	
	mean /= boxSize;

	gl_FragData[0].xyzw = mean;

}