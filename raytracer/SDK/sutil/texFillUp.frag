#version 150 core

in vec2 texCoord;


vec4 color;

//a trivial shader
//the magic happens when the viewport is smaller than the output texture

void main()
{	
	gl_FragData[0] = color;
}