#version 150 core


in vec2 texCoord;;

uniform sampler2D img;

void main(){
	gl_FragColor = texture2D(img, texCoord);
}