#version 150 core

out vec4 fragcolor;

in vec2 texCoord;

uniform float mult;

//channels encodes what color-channels of img should be showed:
// 0 = RGBA, 1 = R, 2 = G, 3 = B, 4 = A;
uniform int channel;

uniform bool negate;

uniform float gamma;

uniform sampler2D img;

void main()
{
	vec4 color = mult * texture2D(img, texCoord);
	if(negate)
		color = -color;
		
	vec4 zero = vec4(0.f);
	
	if(channel == 0 ){
	gl_FragColor = color;
/*	if (color.x==1)
	gl_FragColor = vec4(1,0,0,0);
	else if (color.y==1)
	gl_FragColor = vec4(0,1,0,0);
	else if (color.z==1)
	gl_FragColor = vec4(0,0,1,0);
	else if (color.w==1)
	gl_FragColor = vec4(1,1,0,0);
*/
		}
	else if( channel ==4)
		gl_FragColor = vec4(color.w);
	else{
		zero[channel-1] = color[channel-1];
		gl_FragColor = vec4(color[channel-1]);//zero;
		}
		
	gl_FragColor = pow(gl_FragColor, vec4(1/gamma));
}