#version 150 core

in vec2 texCoord;

uniform sampler2D guide1;	// The rgb-values of the 1st guide.
uniform sampler2D guide2;	// The rgb-values of the 2nd guide.
uniform sampler2D guide3;	// The rgb-values of the 3rd guide.
							// In fact the values of guide1, 2 and 3 at a pixel
							// should be treated as one 9-dim vector instead of three 3-dim vectors
	
vec3 currentRGB(sampler2D img)
{
	return texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
}

void main()
{
	int dim = 9;
	vec3 v1 = currentRGB(guide1);
	vec3 v2 = currentRGB(guide2);
	vec3 v3 = currentRGB(guide3);

	float features[9] = float[9](v1.x,v1.y,v1.z,v2.x,v2.y,v2.z,v3.x,v3.y,v3.z);
	float mult[81];

	
	// mult:
	//	c1*c1, c1*c2, c1*c3, c1*c4, c1*c5, c1*c6, c1*c7, c1*c8, c1*c9
	//	c2*c1, c2*c2, c2*c3, c2*c4, c2*c5, ...
	//	...
	for (int i = 0; i < dim; i++){
		for (int j = 0; j < dim; j++){
			mult[j + i*dim] = features[i] * features[j];
		}
	}

	// only give the values that are really necessary
	//	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]
	//	symm	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]
	//	symm	symm	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]
	//	symm	symm	symm	[* ]	[* ]	[* ]	[* ]	[* ]	[* ]
	//	symm	symm	symm	symm	[* ]	[* ]	[42]	[43]	[44]
	//	symm	symm	symm	symm	symm	[50]	[51]	[52]	[53]
	//	symm	symm	symm	symm	symm	symm	[60]	[61]	[62]
	//	symm	symm	symm	symm	symm	symm	symm	[70]	[71]
	//	symm	symm	symm	symm	symm	symm	symm	symm	[80]
	//
	// [*]	this numbers have been given out from the shader mult_all_combinations3_1.frag because
	//		it's not possible to write to more than 8 buffers from one shader.

	gl_FragData[0].xyzw =	vec4(mult[42],	mult[43],	mult[44],	mult[50]);
	gl_FragData[1].xyzw =	vec4(mult[51],	mult[52],	mult[53],	mult[60]);
	gl_FragData[2].xyzw =	vec4(mult[61],	mult[62],	mult[70],	mult[71]);
	gl_FragData[3].x	=	mult[80];
}