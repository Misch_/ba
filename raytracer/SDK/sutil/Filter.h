#pragma once

#include <vector>
#include <math.h>
#include "sutil.h"
#define M_PI 3.14159265358979323846

using namespace std;

class  Filter{
private:
	float minScale, scaleFactor;
	int nScales;
	vector< vector<float> > gauss;
	vector< vector<float> > var;
	vector< vector<float> > img;
	vector< vector<float> > centerOne;

	//TIMES_SIGMA has a HUGE impact on performance!!!!
	static const int TIMES_SIGMA = 3; 
	//the higher SUB_RES better the filter-approx, but also the higher the Filter-computation time (but this is done once at startup so no biggie...)
	static const int SUB_RES = 8;	

	void createFilters();
	void gaussian(float ms, vector<float> &g );
	void sub2pix(float ms, vector<float> &subFlt, vector<float> &pixFlt );
	void sqrtVec( vector<float> &flt, vector<float> &result );
	void conv( vector<float> &flt1, vector<float> &flt2, vector<float> &result );
	void sub2pixHalf(float sigma, vector<float> &subFlt, vector<float> &pixFlt, float f );
	void convertTo1DArray( vector< vector<float> > &flt, vector<float> &result );
	void centerToOne(vector<float> &flt, vector<float> &result);
public:
	SUTILAPI Filter(float ms, float sf, int ns);
	SUTILAPI void getGaussianFilterbank(vector<float> &result);
	void getVarFilterbank(vector<float> &result);
	void getGaussianBoxFilterbank(vector<float> &result);
	void getGaussianBoxCenterOneFilterBank(vector<float> &result);
	void getGaussianBoxCenterOneNorm(vector<float> &norm);
};