#version 150 core

in vec2 texCoord;

uniform sampler2D guide1;	// The rgb-values of the 1st guide.
uniform sampler2D guide2;	// The rgb-values of the 2nd guide.
uniform sampler2D guide3;	// The rgb-values of the 3rd guide.
							// In fact the values of guide1, 2 and 3 at a pixel
							// should be treated as one 9-dim vector instead of three 3-dim vectors
	
vec3 currentRGB(sampler2D img)
{
	return texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
}

void main()
{
	int dim = 9;
	vec3 v1 = currentRGB(guide1);
	vec3 v2 = currentRGB(guide2);
	vec3 v3 = currentRGB(guide3);

	float features[9] = float[9](v1.x,v1.y,v1.z,v2.x,v2.y,v2.z,v3.x,v3.y,v3.z);
	float mult[81];

	
	// mult:
	//	c1*c1, c1*c2, c1*c3, c1*c4, c1*c5, c1*c6, c1*c7, c1*c8, c1*c9
	//	c2*c1, c2*c2, c2*c3, c2*c4, c2*c5, ...
	//	...
	for (int i = 0; i < dim; i++){
		for (int j = 0; j < dim; j++){
			mult[j + i*dim] = features[i] * features[j];
		}
	}

	// only give the values that are really necessary
	//	[0]		[1]		[2]		[3]		[4]		[5]		[6]		[7]		[8]
	//	symm	[10]	[11]	[12]	[13]	[14]	[15]	[16]	[17]
	//	symm	symm	[20]	[21]	[22]	[23]	[24]	[25]	[26]
	//	symm	symm	symm	[30]	[31]	[32]	[33]	[34]	[35]
	//	symm	symm	symm	symm	[40]	[41]	[* ]	[* ]	[* ]
	//	symm	symm	symm	symm	symm	[* ]	[* ]	[* ]	[* ]
	//	symm	symm	symm	symm	symm	symm	[* ]	[* ]	[* ]
	//	symm	symm	symm	symm	symm	symm	symm	[* ]	[* ]
	//	symm	symm	symm	symm	symm	symm	symm	symm	[* ]
	//
	// [*]	this numbers have to be given out from the shader mult_all_combinations3_2.frag because
	//		it's not possible to write to more than 8 buffers from one shader.

	gl_FragData[0].xyzw =	vec4(mult[0],	mult[1],	mult[2],	mult[3]);
	gl_FragData[1].xyzw =	vec4(mult[4],	mult[5],	mult[6],	mult[7]);
	gl_FragData[2].xyzw =	vec4(mult[8],	mult[10],	mult[11],	mult[12]);
	gl_FragData[3].xyzw =	vec4(mult[13],	mult[14],	mult[15],	mult[16]);
	gl_FragData[4].xyzw =	vec4(mult[17],	mult[20],	mult[21],	mult[22]);
	gl_FragData[5].xyzw =	vec4(mult[23],	mult[24],	mult[25],	mult[26]);
	gl_FragData[6].xyzw =	vec4(mult[30],	mult[31],	mult[32],	mult[33]);
	gl_FragData[7].xyzw =	vec4(mult[34],	mult[35],	mult[40],	mult[41]);
}