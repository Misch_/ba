#include "FlowFile.h"
#include <fstream>
#include <cassert>

FlowFile::FlowFile(void)
	: w(-1), h(-1), nFrames(-1), data(0)
{
}


FlowFile::~FlowFile(void)
{
	delete [] data;
}


bool FlowFile::init(const QString & fileName)
{
	std::ifstream ifs(fileName.toAscii().constData(), std::ios::binary | std::ios::in);
	if(!ifs.good())
		return false;

	ifs.read((char*)&h,12);
	data = new float[w*h*2*nFrames];
	ifs.read((char*)data,sizeof(float)*w*h*2*nFrames);
	return true;
}


void FlowFile::getFrame(FlowField & ff, int frame) const
{
#ifdef _DEBUG
	assert(ff.h == h);
	assert(ff.w == w);
#endif
	const float * in = this->data + frame*2*w*h;
	float * out = ff.getBuffer();
	for(int y = 0; y < h; y++) {
		for(int x = 0; x < w; x++) {
			out[(y*w + x)*2] = in[x*h + y];
			out[(y*w + x)*2+1] = in[x*h + y + w*h];
		}
	}
}


void FlowFile::clear()
{
	w = h = nFrames = -1;
	delete [] data;
	data = 0;
}
