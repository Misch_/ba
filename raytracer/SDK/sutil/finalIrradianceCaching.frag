#version 150 core

in vec2 texCoord;

uniform sampler2D d_base;
uniform sampler2D d_map;
uniform sampler2D direct0;
uniform sampler2D direct1;
uniform sampler2D direct2;
uniform sampler2D direct3;
uniform sampler2D smin_base;
uniform sampler2D smin;

uniform sampler2D id_base;
uniform sampler2D id_map;
uniform sampler2D indirect0;
uniform sampler2D indirect1;
uniform sampler2D indirect2;
uniform sampler2D indirect3;
uniform sampler2D id_smin_base;
uniform sampler2D id_smin;

uniform sampler2D brdf_base;
uniform sampler2D brdf_map;
uniform sampler2D brdf0;
uniform sampler2D brdf1;
uniform sampler2D brdf2;
uniform sampler2D brdf3;
uniform sampler2D brdf_smin_base;
uniform sampler2D brdf_smin;


uniform float gamma;

void main()
{
	
	vec4 dir, idir, brdf_flt;
	
	float sigma = round(4*texture2D(d_map, texCoord).x);	
	
	vec4 c[5];
	//glsl seems not to like dynamic indexing on sampler2D arrays... so lets make the code overly complicated!
	c[0] = texture2D(d_base, texCoord);
	c[1] = texture2D(direct0, texCoord);
	c[2] = texture2D(direct1, texCoord);
	c[3] = texture2D(direct2, texCoord);
	c[4] = texture2D(direct3, texCoord);
	
	float t, m;
	int s = int(sigma);	
	if(s>0)
		m = texture2D(smin,texCoord)[s-1];
	else
		m = texture2D(smin_base, texCoord).x;
			
	t = m - s;
		
	if(t>0)
		dir = (1.f-t)*c[s] + t*c[s+1];
	else
		dir = (-t)*c[s-1] + (1+t)*c[s];
		
	/*
	if(sigma==0)
		dir = texture(d_base, texCoord);
	if(sigma==1)
		dir = texture2D(direct0, texCoord);			
	if(sigma==2)
		dir = texture2D(direct1, texCoord);			
	if(sigma==3)
		dir = texture2D(direct2, texCoord);		
	if(sigma==4)
		dir = texture2D(direct3, texCoord);
		*/
	
		
		
		
	sigma = round(4*texture2D(id_map, texCoord).x);
		
		
	//vec4 c[5];
	//glsl seems not to like dynamic indexing on sampler2D arrays... so lets make the code overly complicated!
	c[0] = texture2D(id_base, texCoord);
	c[1] = texture2D(indirect0, texCoord);
	c[2] = texture2D(indirect1, texCoord);
	c[3] = texture2D(indirect2, texCoord);
	c[4] = texture2D(indirect3, texCoord);
	
	//float t, m;
	s = int(sigma);	
	if(s>0)
		m = texture2D(id_smin,texCoord)[s-1];
	else
		m = texture2D(id_smin_base, texCoord).x;
			
	t = m - s;
		
	if(t>0)
		idir = (1.f-t)*c[s] + t*c[s+1];
	else
		idir = (-t)*c[s-1] + (1+t)*c[s];
		
		
	/*
	if(sigma==0)
		idir = texture(id_base, texCoord);
	if(sigma==1)
		idir = texture2D(indirect0, texCoord);			
	if(sigma==2)
		idir = texture2D(indirect1, texCoord);			
	if(sigma==3)
		idir = texture2D(indirect2, texCoord);		
	if(sigma==4)
		idir = texture2D(indirect3, texCoord);*/


	sigma = round(4*texture2D(brdf_map, texCoord).x);

	c[0] = texture2D(brdf_base, texCoord);
	c[1] = texture2D(brdf0, texCoord);
	c[2] = texture2D(brdf1, texCoord);
	c[3] = texture2D(brdf2, texCoord);
	c[4] = texture2D(brdf3, texCoord);

//float t, m;
	s = int(sigma);	
	if(s>0)
		m = texture2D(brdf_smin,texCoord)[s-1];
	else
		m = texture2D(brdf_smin_base, texCoord).x;
			
	t = m - s;
		
	if(t>0)
		brdf_flt = (1.f-t)*c[s] + t*c[s+1];
	else
		brdf_flt = (-t)*c[s-1] + (1+t)*c[s];


	gl_FragColor = dir + brdf_flt * idir;
	
	gl_FragColor = pow(gl_FragColor, vec4(1/gamma));
}