#pragma once
#include <string>
#include <system_error>

#define MAX_SHADER_LENGTH 16384 

class Shader {
private:
	
	static std::string _path;
	int id;
	GLuint vao;
	static void setupShader(unsigned int& id, std::string vert, std::string frag);
	bool loadShaderFile(const char *, GLuint);
public:

	static unsigned int getShaderID(int n);
	Shader(): id(0), vao(0){};
	static void loadAllShaders(std::string& path);
	GLuint create(const char *, const char *);

	//not used!
	void bindAttribute(float *, int, const char *);

	GLuint getVAO();
	void enable();
	void disable();
	
};