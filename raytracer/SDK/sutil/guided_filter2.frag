#version 150 core

in vec2 texCoord;

uniform sampler2D a_sat;
uniform sampler2D b_sat;
uniform sampler2D I;
uniform sampler2D boxsize;
uniform int fltRadius;
uniform int width; // window Size, e.g. 512x512px
uniform int height;

vec3 getRGBat(sampler2D img, int i, int j)
{
	return texture2D(img, vec2(texCoord.x + float(i)/float(width),texCoord.y + float(j)/float(height))).xyz;
}

vec3 boxfilter(sampler2D sat, int fltRadius)
{
	float boxSize = getRGBat(boxsize,0,0).x;

	vec3 upRight	= getRGBat(sat,	fltRadius,		fltRadius);
	vec3 upLeft		= getRGBat(sat, -(fltRadius+1),	fltRadius);
	vec3 lowRight	= getRGBat(sat, fltRadius,		-(fltRadius+1));
	vec3 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec3 mean = upRight - upLeft - lowRight + lowLeft;
	
	mean /= boxSize;

	return mean;
}

void main()
{	
	vec3 mean_a = boxfilter(a_sat, fltRadius);
	vec3 mean_b = boxfilter(b_sat, fltRadius);

	vec3 q = getRGBat(I,0,0) * mean_a + mean_b;
	
	gl_FragData[0].xyz = q;
}