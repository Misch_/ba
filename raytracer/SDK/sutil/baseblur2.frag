#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform int size; //height of img

int fsize	= 8;
float gauss[8]	= float[](	0.045089598, 0.09545468, 0.157378162, 0.20207756, 0.20207756, 0.157378162, 
							0.09545468, 0.045089598);
//see baseblur1 for explanation								
void main()
{
	gl_FragData[0] = vec4(0,0,0,0);
	float s = fsize/2-0.5;
	for(int i= 0; i<fsize; i++)
		gl_FragData[0].xyz += gauss[i]*texture2D(img, vec2(texCoord.x, texCoord.y - (s-i)/size)).xyz;
}