#version 150 core

in vec2 texCoord;

uniform sampler2D tex1;
uniform sampler2D tex2;
	
vec3 currentRGB(sampler2D img)
{
	return texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
}

void main()
{
	vec3 mult_r = currentRGB(tex1).x * currentRGB(tex2);
	vec3 mult_g = currentRGB(tex1).y * currentRGB(tex2);
	vec3 mult_b = currentRGB(tex1).z * currentRGB(tex2);

	gl_FragData[0].xyz = mult_r;
	gl_FragData[1].xyz = mult_g;
	gl_FragData[2].xyz = mult_b;
}