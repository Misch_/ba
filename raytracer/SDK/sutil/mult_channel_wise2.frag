#version 150 core

in vec2 texCoord;

uniform sampler2D guide1;
uniform sampler2D guide2;
uniform sampler2D p;
	
vec3 currentRGB(sampler2D img)
{
	return texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
}

void main()
{
	vec3 v1 = currentRGB(guide1);
	vec3 v2 = currentRGB(guide2);
	vec3 pVec = currentRGB(p);

	float features[6] = float[6](v1.x,v1.y,v1.z,v2.x,v2.y,v2.z);
	
	vec3 mult_1 = features[0] * pVec;
	vec3 mult_2 = features[1] * pVec;
	vec3 mult_3 = features[2] * pVec;
	vec3 mult_4 = features[3] * pVec;
	vec3 mult_5 = features[4] * pVec;
	vec3 mult_6 = features[5] * pVec;

	gl_FragData[0].xyz = mult_1;
	gl_FragData[1].xyz = mult_2;
	gl_FragData[2].xyz = mult_3;
	gl_FragData[3].xyz = mult_4;
	gl_FragData[4].xyz = mult_5;
	gl_FragData[5].xyz = mult_6;
}