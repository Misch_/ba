#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform sampler2D smap;

int filtersize[4]	= int[](3, 5, 9, 17);

float gauss05[3]	= float[](	0.106506979, 0.786986042, 0.106506979);
float gauss1[5]		= float[](	0.054488685, 0.244201342, 0.402619947, 0.244201342, 0.054488685);
float gauss2[9]		= float[](	0.027630551, 0.066282245, 0.123831537, 0.180173823, 0.204163689,
								0.180173823, 0.123831537, 0.066282245, 0.027630551);
float gauss4[17]	= float[](	0.013960189, 0.022308318, 0.033488752, 0.047226710, 0.062565226,
								0.077863682, 0.091031867, 0.099978946, 0.103152619, 0.099978946,
								0.091031867, 0.077863682, 0.062565226, 0.047226710, 0.033488752,
								0.022308318, 0.013960189);

void main()
{
	
	gl_FragColor = vec4(0,0,0,0);
	
	float sigma = texture2D(smap, texCoord).y;
	
	if(sigma==0)
		for(int i=0; i<filtersize[0]; i++)
			gl_FragColor += gauss05[i]*texture2D(img, vec2(texCoord.x, texCoord.y-(floor(filtersize[0]/2.f)-i)/512.f));
	
	if(sigma==1)
		for(int i=0; i<filtersize[1]; i++)
			gl_FragColor += gauss1[i]*texture2D(img, vec2(texCoord.x, texCoord.y-(floor(filtersize[1]/2.f)-i)/512.f));
	
	if(sigma==2)
		for(int i=0; i<filtersize[2]; i++)
			gl_FragColor += gauss2[i]*texture2D(img, vec2(texCoord.x, texCoord.y-(floor(filtersize[2]/2.f)-i)/512.f));
	
	if(sigma==3)
		for(int i=0; i<filtersize[3]; i++)
			gl_FragColor += gauss4[i]*texture2D(img, vec2(texCoord.x, texCoord.y-(floor(filtersize[3]/2.f)-i)/512.f));
}