#pragma once
#include "gui.h"
#include "Denoiser.h"
#include <AntTweakBar.h>

TwBar* GUI::menuBar = NULL;
float GUI::gamma_weight = 0.15f;
float GUI::gamma_weight_d = 0.15f;
int GUI::ic_mode = 0;
float GUI::samples_per_frame = 4.f;
int GUI::max_bounces = 3;
int GUI::max_ssp_frame = 16;
int GUI::debug_textue_to_show = TexName::GF_FILTERED;
int GUI::debug_mult = 0;
int GUI::debug_channel = 0;
int GUI::init_samples = 4;
float GUI::outlier_treshold = 1.f;
bool GUI::debug_enabled = false;
bool GUI::debug_negate = false;
bool GUI::use_ic = false;
bool GUI::show_direct = false;
int GUI::show_component = 0;
int GUI::stop_frame = 1;
float GUI::gamma_correction = 3.0f;
float GUI::avgNS = 0.f;
float GUI::epsilon = 0.01;
int GUI::fltRadius = 6;

float GUI::mseRatioIC = 1.f;

bool GUI::filter_changed = false;

float GUI::minFilter_ind = 1/sqrtf(2.f);
float GUI::fScale_ind = 2.f;
float GUI::minFilter_dir = 1.f/sqrtf(2.f);
float GUI::fScale_dir = 2.f;

float GUI::dof_focal_plane = 100.f;
float GUI::dof_lens_radius = 0.f;

int GUI::denoiser_type = 0;

void GUI::load(){

	TwInit(TW_OPENGL, NULL);

	menuBar = TwNewBar("MenuBar");

	//changing variables
	TwAddVarRW(menuBar, "init samples", TW_TYPE_INT8, &init_samples, 
		"group='Variables' min=2 max=64 step=1 help='Number of samples shot in the initial pass.'");
	TwAddVarRW(menuBar, "gamma", TW_TYPE_FLOAT, &gamma_weight, 
		"group='Variables' min=0 max=0.4 step=0.01 help='Control of weight of bias in MSE calculation.'");
	
	TwAddVarRW(menuBar, "samples per iteration", TW_TYPE_FLOAT, &samples_per_frame, 
		"group='Variables' min=0.1 max=8.0 step=0.1 help='Samples distributed per iteration (times number of Pixels)'");
	
	TwAddVarRW(menuBar, "max spp per frame", TW_TYPE_INT8, &max_ssp_frame, 
		"group='Variables' min=1 max=64 step=1 help='Limits maximum number of samples shot in one pixel in a single iteration.'");
//	TwAddVarRW(menuBar, "outlier threshold", TW_TYPE_FLOAT, &outlier_treshold, 
//		"group='Variables' min=0.001 max=1000 step=0.1 help='Samples with difference to mean of pixel bigger than X times standarddeviation get some special threatment...'");
	
	TwAddVarRW(menuBar, "stop at frame", TW_TYPE_INT32, &stop_frame, 
		"group='Variables' min=0 max=10000 step=1 help='controlls how many iteration should be performed, 0 means never stop'");
		TwAddVarRO(menuBar, "est MSE", TW_TYPE_FLOAT, &mseRatioIC, 
		"group='Variables'");
	TwAddVarRO(menuBar, "avg ssp", TW_TYPE_FLOAT, &avgNS, 
		"group='Variables'");
	TwAddVarRW(menuBar, "gamma correction", TW_TYPE_FLOAT, &gamma_correction, 
		"group='Scene' min=0.1 max=10.0 step=0.1 help='changes the gamma-correction. This is applied directly to the optixdata, to have an effect on the sampling'");
	TwAddVarRW(menuBar, "max bounces", TW_TYPE_INT8, &max_bounces, 
		"group='Scene' min=0 max=10 step=1 help='Maximum number of bounces for paths.'");
	TwAddVarRW(menuBar, "focal dist", TW_TYPE_FLOAT, &dof_focal_plane, 
		"group='Scene' min=0.1 max=1000 step=0.1 help='Focal distance.'");
	TwAddVarRW(menuBar, "lens radius", TW_TYPE_FLOAT, &dof_lens_radius, 
		"group='Scene' min=0 max=10 step=0.01 help='Set the lens radius'");
	if(denoiser_type != DENOISER_GF){
	TwAddVarCB(menuBar, "Enable Debug", TW_TYPE_BOOLCPP, GUI::setDebug_options, GUI::getDebug_options, NULL, 
		" group='Variables' help='Show textures for debugging.' ");
	}
	else{
		TwAddVarCB(menuBar, "Enable Filter", TW_TYPE_BOOLCPP, GUI::setDebug_options, GUI::getDebug_options, NULL, 
		" group='Variables' help='Show textures for debugging.' ");
	}
	if(denoiser_type==DENOISER_GF){
	TwAddVarRW(menuBar, "Epsilon", TW_TYPE_FLOAT, &epsilon, "group='Variables' min=0 max=20 step=0.01 help='epsilon of guided filter'");
	
	TwAddVarRW(menuBar, "Filter radius", TW_TYPE_INT8, &fltRadius, 
		"group='Variables' min=0 max=100 step=1 help='Window size of the guided filter'");
	}

	//TwAddVarCB(menuBar, "MinFlt Ind", TW_TYPE_FLOAT, GUI::setFilterParam, GUI::getFilterParam, &minFilter_ind, 
	//	" group='Variables' min=0.1 max=16.0 step=0.1");

	TwAddVarCB(menuBar, "MinFlt Dir", TW_TYPE_FLOAT, GUI::setFilterParam, GUI::getFilterParam, &minFilter_dir, 
		" group='Variables' min=0.1 max=16.0 step=0.1");

	//TwAddVarCB(menuBar, "FltScales Ind", TW_TYPE_FLOAT, GUI::setFilterParam, GUI::getFilterParam, &fScale_ind, 
	//	" group='Variables' min=1 max=16.0 step=0.1");

	TwAddVarCB(menuBar, "FltScales Dir", TW_TYPE_FLOAT, GUI::setFilterParam, GUI::getFilterParam, &fScale_dir, 
		" group='Variables' min=1 max=16.0 step=0.1");

}

void TW_CALL GUI::setFilterParam( const void* val, void *clientData )
{
	filter_changed = true;
	*(float*)clientData = *(float*)val;
}

void TW_CALL GUI::getFilterParam( void* val,void *clientData )
{
	*(float*)val = *(float*)clientData;
}

void TW_CALL GUI::setDebug_options( const void* val, void *clientData )
{
	debug_enabled = * (const bool *) val;
	if(debug_enabled){
		//debug stuff

		if(denoiser_type==DENOISER_GEM){
			TwEnumVal texEV[] = {		
				{ TexName::MEAN_FILTERED1, "Mean Filtered 1" }, 
				{ TexName::MEAN_FILTERED2, "Mean Filtered 2" }, 
				{ TexName::MEAN_FILTERED3, "Mean Filtered 3" },
				{ TexName::MEAN_FILTERED4, "Mean Filtered 4" },
				{ TexName::VAR_FILTERED, "Var Filtered" },
				{ TexName::NSAMPLES_FILTERED, "NS Filtered" },
				{ TexName::STOPMAP_FILTERED, "Stopmap Filtered" },
				{ TexName::STOPMAP_UNIFLTERED, "Stopmap Unfiltered" },
				{ TexName::STOPMAP_DIFF_MSE, "Stopmap DiffMSE" },
				{ TexName::STOPMAP_DIFF_MSE_WEIGHTED, "Stopmap DiffMSE Weighted" },
				{ TexName::COSTMAP_MSE, "Costmap MSE Unfiltered" },
				{ TexName::COSTMAP_FITLERED, "Costmap MSE Filtered" },
				{ TexName::COSTMAP_STEPS, "Costmap Steps" },
				{ TexName::SCALEMIN_UNFILTERED, "Scalemin unfiltered" },
				{ TexName::SCALEMIN_FILTERED, "Scalemin filtered" },
				{ TexName::MEAN_INPUT, "Input Mean" },
				{ TexName::VAR_INPUT, "Input Var" },
				{ TexName::NSAMPLES_INPUT, "Input Ns" }		
			};

			TwType texType = TwDefineEnum( "Debug Mode", texEV, sizeof(texEV)/sizeof(TwEnumVal) );
			TwAddVarRW(menuBar, "Texture", texType, &debug_textue_to_show, 
				" group='Debug' help='Select which texture to show.' ");
		} else if(denoiser_type==DENOISER_GF){
			TwEnumVal texEV[] = {		
				{ TexName::MEAN_INPUT, "Input Mean" },
				{ TexName::NORMALS_INPUT, "Input Normals" },
				{ TexName::GF_FILTERED, "GF filtered"},
				{ TexName::BRDF_INPUT, "brdf"},
				{ TexName::POSITION_INPUT, "position"},
				{ TexName::NORMALS_NONNEGATIVE, "visible normals"}
				//{ TexName::TEST, "test"},
				//{ TexName::A_K, "a"},
				//{ TexName::B_K, "b"},
				//{ TexName::TMP1, "tmp1"},
				//{ TexName::TMP2, "tmp2"},
				//{ TexName::TMP3, "tmp3"},
				//{ TexName::TMP4, "tmp4"},
				//{ TexName::TMP5, "tmp5"},
				//{ TexName::TMP6, "tmp6"},
				//{ TexName::TMP7, "tmp7"},
				//{ TexName::TMP8, "tmp8"},
				//{ TexName::TMP9, "tmp9"}
				//{ TexName::BOX_SIZE, "boxsize"}
				//{ TexName::A_K_R, "a_k_r"},
				//{ TexName::A_K_G, "a_k_g"},
				//{ TexName::A_K_B, "a_k_b"}
			};
			TwType texType = TwDefineEnum( "Debug Mode", texEV, sizeof(texEV)/sizeof(TwEnumVal) );
			TwAddVarRW(menuBar, "Texture", texType, &debug_textue_to_show, 
				" group='Debug' help='Select which texture to show.' ");
		}

		TwEnumVal channelEV[] = {  
			{0, "RGBA" },{1, "R"},{2, "G"},{3, "B"}, {4, "A"} 
		};
		TwType channelType = TwDefineEnum( "Channel Mode", channelEV, 5 );
		TwAddVarRW(menuBar, "Channel", channelType, &debug_channel, 
			" group='Debug' help='Which channel should be shown.' ");

		TwAddVarRW(menuBar, "Multiplicator", TW_TYPE_INT32, &debug_mult, 
			" group='Debug' min=-10 max=10 step=1, help='Multiplies the texture-values with 10^x'");

		TwAddVarRW(menuBar, "Negate values", TW_TYPE_BOOLCPP, &debug_negate, 
			" group='Debug' help='Negates the values shown.' ");
	}else{
		TwRemoveVar(menuBar, "Debug Mode");
		TwRemoveVar(menuBar, "Texture");
		TwRemoveVar(menuBar, "Channel");
		TwRemoveVar(menuBar, "Multiplicator");
		TwRemoveVar(menuBar, "Negate values");
	}
}

void TW_CALL GUI::getDebug_options( void* val,void *clientData )
{
	*(bool *)val = debug_enabled;
}


