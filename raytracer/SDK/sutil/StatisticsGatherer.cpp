#pragma once
#include "StatisticsGatherer.h"
#include <iostream>
#include <fstream>


StatisticsGatherer::StatisticsGatherer(int height, int width, int channels)
{
	_mean = new float[channels*width*height];
	_height = height;
	_width = width;
	_channels = channels;
	_n=0;
}

StatisticsGatherer::~StatisticsGatherer()
{
	delete[] _mean;
}

void StatisticsGatherer::addImageToMean( RTbuffer buffer )
{
	_n++;
	float *dst = _mean;
	void *imageData;
	rtBufferMap(buffer, &imageData);

	float *src = (float*)imageData;

	for(int i=0; i<_height; i++)
		for(int j=0; j<_width; j++)
			for(int k=0; k<_channels; k++)
				*dst++ = *dst + ( *src++ - *dst)/_n;
	
	rtBufferUnmap(buffer);
}

void StatisticsGatherer::writeToFile( std::string name )
{
	std::ofstream file;
	file.open(name.c_str(), std::ios::binary | std::ios::trunc);
	file.write((char*)&_height, sizeof(int));
	file.write((char*)&_width, sizeof(int));
	file.write((char*)&_channels, sizeof(int));
	file.write((char*)_mean, _height*_width*_channels*sizeof(float));
	file.close();
}

float *StatisticsGatherer::readFile(std::string name)
{
	int h, w, c;
	std::ifstream file;
	file.open(name.c_str(), std::ios::binary);

	file.read((char*)&h, sizeof(int));
	file.read((char*)&w, sizeof(int));
	file.read((char*)&c, sizeof(int));

	float *data = new float[h*w*c];

	file.read((char*)data, h*w*c*sizeof(float));

	file.close();

	//hey fuck, this sould not be always zero! 
	//for(int i=0; i<h*w*c; i++)
	//	if(data[i]!=0) printf("%f\n", data[i]);

	return data;
}

const float * StatisticsGatherer::getMeanData()
{
	return _mean;
}

int StatisticsGatherer::getHeight()
{
	return _height;
}

int StatisticsGatherer::getWidth()
{
	return _width;
}

int StatisticsGatherer::getChannels()
{
	return _channels;
}

/*
void WriteSpectialSinusTextures(){


	std::ofstream file;
	std::string nameMean = "sin_mean";
	int height = 512;
	int width = 512;
	int channels = 4;
	file.open(nameMean.c_str(), std::ios::binary | std::ios::trunc);
	file.write((char*)&height, sizeof(int));
	file.write((char*)&width, sizeof(int));
	file.write((char*)&channels, sizeof(int));
	
	float * mean = new float[channels, height, width];
	for(int h = 0; h < height; h++){
		for(int w = 0; w< width; w++){
			for(int c = 0; c< channels; c++){
				mean[channels*(height*w + height)+c] = 0.5 + 0.2*sin(h);
			}
		}
	}

	file.write((char*)mean, height*width*channels*sizeof(float));
	file.close();

}*/