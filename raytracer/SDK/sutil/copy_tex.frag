#version 150 core

in vec2 texCoord;

uniform sampler2D tex;

void main(){	
	gl_FragData[0].xyzw = texture2D(tex, vec2(texCoord.x,texCoord.y)).xyzw;;
}