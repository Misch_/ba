#version 150 core

in vec2 texCoord;

uniform sampler2D I_sat;
uniform sampler2D p_sat;
uniform sampler2D Ip_r_sat;
uniform sampler2D Ip_g_sat;
uniform sampler2D Ip_b_sat;
uniform sampler2D II_rx_sat;
uniform sampler2D II_gx_sat;
uniform sampler2D II_bx_sat;
uniform sampler2D boxsize;

uniform int fltRadius;
uniform float epsilon;
uniform int width;
uniform int height;

vec3 getRGBat(sampler2D img, int i, int j)
{
	vec3 texVec = texture2D(img, vec2(texCoord.x + float(i)/float(width),texCoord.y + float(j)/float(height))).xyz;
	return texVec;
}

vec3 boxfilter(sampler2D sat, int fltRadius)
{
	float boxSize = getRGBat(boxsize,0,0).x;

	vec3 upRight	= getRGBat(sat,	fltRadius,		fltRadius);
	vec3 upLeft		= getRGBat(sat, -(fltRadius+1),	fltRadius);
	vec3 lowRight	= getRGBat(sat, fltRadius,		-(fltRadius+1));
	vec3 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec3 mean = upRight - upLeft - lowRight + lowLeft;
	
	mean /= boxSize;

	return mean;
}

void main()
{	
	vec3 mean_I = boxfilter(I_sat, fltRadius);
	vec3 mean_p = boxfilter(p_sat, fltRadius);

	vec3 mean_Ip_r = boxfilter(Ip_r_sat,fltRadius);
	vec3 mean_Ip_g = boxfilter(Ip_g_sat,fltRadius);
	vec3 mean_Ip_b = boxfilter(Ip_b_sat,fltRadius);
	

	vec3 cov_Ip_r = mean_Ip_r - mean_I.x * mean_p;
	vec3 cov_Ip_g = mean_Ip_g - mean_I.y * mean_p;
	vec3 cov_Ip_b = mean_Ip_b - mean_I.z * mean_p;
	
	mat3 sigma = mat3(	0, 0, 0,
						0, 0, 0,
						0, 0, 0);

	vec3 var_I_rx = boxfilter(II_rx_sat,fltRadius) - mean_I.x * mean_I;
	vec3 var_I_gx = boxfilter(II_gx_sat,fltRadius) - mean_I.y * mean_I;
	vec3 var_I_bx = boxfilter(II_bx_sat,fltRadius) - mean_I.z * mean_I;

	sigma[0][0] = var_I_rx.x;
	sigma[1][1] = var_I_gx.y;
	sigma[2][2] = var_I_bx.z;
	sigma[0][1] = var_I_rx.y;
	sigma[1][0] = sigma[0][1];
	sigma[0][2] = var_I_rx.z;
	sigma[2][0] = sigma[0][2];
	sigma[1][2] = var_I_gx.z;
	sigma[2][1] = sigma[1][2];


	mat3 id = mat3(1.0);
	sigma = sigma + epsilon * id;

	mat3 inverse = inverse(sigma);
	
	vec3 cov_Ip_input_r = vec3(cov_Ip_r.x, cov_Ip_g.x, cov_Ip_b.x);
	vec3 cov_Ip_input_g = vec3(cov_Ip_r.y, cov_Ip_g.y, cov_Ip_b.y);
	vec3 cov_Ip_input_b = vec3(cov_Ip_r.z, cov_Ip_g.z, cov_Ip_b.z);

	vec3 a_r = inverse * cov_Ip_input_r;
	vec3 a_g = inverse * cov_Ip_input_g;
	vec3 a_b = inverse * cov_Ip_input_b;
	
	float b_r = mean_p.x - a_r.x * mean_I.x - a_r.y * mean_I.y - a_r.z * mean_I.z;
	float b_g = mean_p.y - a_g.x * mean_I.x - a_g.y * mean_I.y - a_g.z * mean_I.z;
	float b_b = mean_p.z - a_b.x * mean_I.x - a_b.y * mean_I.y - a_b.z * mean_I.z;

	vec3 b = vec3(b_r,b_g,b_b);

	gl_FragData[0].xyz = a_r;
	gl_FragData[1].xyz = a_g;
	gl_FragData[2].xyz = a_b;
	gl_FragData[3].xyz = b;
}