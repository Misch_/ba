#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform sampler2D nsamples;
uniform samplerBuffer flt;

uniform int size;
const int MAX_FLT = 8;
int fltSize;
float f[MAX_FLT];
float tex, ns;
	
void filterTex(in sampler2D img, int s, inout int idx)
{
	f[s] = 0.f;
	fltSize = int(texelFetch(flt, s+1).x);
	float offset = floor(fltSize/2.f);
	for(int i=0; i<fltSize; i++, idx++){
		tex = texture2D(img, vec2(texCoord.x-(offset-i)/size, texCoord.y)).x;
		ns = texture2D(nsamples, vec2(texCoord.x-(offset-i)/size, texCoord.y)).x;
		f[s] += tex*texelFetch(flt, idx).x/ns; 
	}
}


void main()
{	
	int nScales = int(texelFetch(flt, 0).x);


	int idx = nScales+1;

	filterTex(img, 0, idx);
	filterTex(img, 1, idx);
	filterTex(img, 2, idx);
	filterTex(img, 3, idx);
	
	//here starts the first filter
	
/*	int fltSize;
	for(int s=0; s<nScales; s++){
		f[s] = 0.f;
		fltSize = int(texelFetch(flt, s+1).x);
		float offset = floor(fltSize/2.f);
		for(int i=0; i<fltSize; i++, idx++){
			tex = texture2D(img, vec2(texCoord.x-(offset-i)/size, texCoord.y)).x;
			ns = texture2D(nsamples, vec2(texCoord.x-(offset-i)/size, texCoord.y)).x;
			f[s] += tex*texelFetch(flt, idx).x/ns; 
		}
		
	}*/
	
	gl_FragData[0] = vec4(f[0], f[1], f[2], f[3]);
}