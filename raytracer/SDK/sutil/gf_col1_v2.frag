#version 150 core

in vec2 texCoord;

uniform sampler2D I1_sat;
uniform sampler2D I2_sat;
uniform sampler2D p_sat;
uniform sampler2D Ip_1_sat;
uniform sampler2D Ip_2_sat;
uniform sampler2D Ip_3_sat;
uniform sampler2D Ip_4_sat;
uniform sampler2D Ip_5_sat;
uniform sampler2D Ip_6_sat;
uniform sampler2D mat1_sat;
uniform sampler2D mat2_sat;
uniform sampler2D mat3_sat;
uniform sampler2D mat4_sat; 
uniform sampler2D mat5_sat;
uniform sampler2D mat6_sat;
uniform sampler2D boxsize;

uniform int fltRadius;
uniform float epsilon;
uniform int width;
uniform int height;

vec4 getRGBat(sampler2D img, int i, int j)
{
	vec4 texVec = texture2D(img, vec2(texCoord.x + float(i)/float(width),texCoord.y + float(j)/float(height))).xyzw;
	return texVec;
}

vec4 boxfilter(sampler2D sat, int fltRadius)
{

	float boxSize = getRGBat(boxsize,0,0).x;

	vec4 upRight	= getRGBat(sat,	fltRadius,		fltRadius);
	vec4 upLeft		= getRGBat(sat, -(fltRadius+1),	fltRadius);
	vec4 lowRight	= getRGBat(sat, fltRadius,		-(fltRadius+1));
	vec4 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec4 mean = upRight - upLeft - lowRight + lowLeft;
	
	mean /= boxSize;

	return mean;
}


float[36] invert_add_eps(float[36] data, float epsilon)  {
   
   int dim = 6;

   // add epsilon
	for (int i = 0; i<dim; i++){
		data[i*dim+i] += epsilon;
	}

    for (int i=1; i < dim; i++) data[i] /= data[0]; // normalize row 0
	for (int i=1; i < dim; i++)  { 
		for (int j=i; j < dim; j++)  { // do a column of L
			float sum = 0.0;
			for (int k = 0; k < i; k++)  
				sum += data[j*dim+k] * data[k*dim+i];
			data[j*dim+i] -= sum;
		}
		if (i == dim-1) continue;
		for (int j=i+1; j < dim; j++)  {  // do a row of U
			float sum = 0.0;
			for (int k = 0; k < i; k++)
				sum += data[i*dim+k]*data[k*dim+j];
			data[i*dim+j] = (data[i*dim+j]-sum) / data[i*dim+i];
		}
    }

    for ( int i = 0; i < dim; i++ )  // invert L
      for ( int j = i; j < dim; j++ )  {
        float x = 1.0;
        if ( i != j ) {
          x = 0.0;
          for ( int k = i; k < j; k++ ) 
              x -= data[j*dim+k]*data[k*dim+i];
          }
        data[j*dim+i] = x / data[j*dim+j];
        }
    for ( int i = 0; i < dim; i++ )   // invert U
      for ( int j = i; j < dim; j++ )  {
        if ( i == j ) continue;
        float sum = 0.0;
        for ( int k = i; k < j; k++ )
            sum += data[k*dim+j]*( (i==k) ? 1.0 : data[i*dim+k] );
        data[i*dim+j] = -sum;
        }
    for ( int i = 0; i < dim; i++ )   // final inversion
      for ( int j = 0; j < dim; j++ )  {
        float sum = 0.0;
        for ( int k = ((i>j)?i:j); k < dim; k++ )  
            sum += ((j==k)?1.0:data[j*dim+k])*data[k*dim+i];
        data[j*dim+i] = sum;
        }

	return data;
}

float[6] matMult(float[36] mat, float[6] mult_vec){
	float[6] result_vec = float[6](0.f,0.f,0.f,0.f,0.f,0.f);

	for (int j = 0; j < 6; j++){	// j says in which row we are
		for (int i = 0; i < 6; i++){ // i says in which column we are
			result_vec[j] += mat[i + j*6] * mult_vec[i];
		}
	}

	return result_vec;
}

void main()
{	
	vec3 mean_I1 = boxfilter(I1_sat, fltRadius).xyz;
	vec3 mean_I2 = boxfilter(I2_sat, fltRadius).xyz;
	float[6] mean_I = float[6](mean_I1.x,mean_I1.y,mean_I1.z, mean_I2.x, mean_I2.y, mean_I2.z);

	vec3 mean_p = boxfilter(p_sat, fltRadius).xyz;

	vec3 mean_Ip_1 = boxfilter(Ip_1_sat,fltRadius).xyz;
	vec3 mean_Ip_2 = boxfilter(Ip_2_sat,fltRadius).xyz;
	vec3 mean_Ip_3 = boxfilter(Ip_3_sat,fltRadius).xyz;
	vec3 mean_Ip_4 = boxfilter(Ip_4_sat,fltRadius).xyz;
	vec3 mean_Ip_5 = boxfilter(Ip_5_sat,fltRadius).xyz;
	vec3 mean_Ip_6 = boxfilter(Ip_6_sat,fltRadius).xyz;
	

	vec3 cov_Ip_1 = mean_Ip_1 - mean_I[0] * mean_p.xyz;
	vec3 cov_Ip_2 = mean_Ip_2 - mean_I[1] * mean_p.xyz;
	vec3 cov_Ip_3 = mean_Ip_3 - mean_I[2] * mean_p.xyz;
	vec3 cov_Ip_4 = mean_Ip_4 - mean_I[3] * mean_p.xyz;
	vec3 cov_Ip_5 = mean_Ip_5 - mean_I[4] * mean_p.xyz;
	vec3 cov_Ip_6 = mean_Ip_6 - mean_I[5] * mean_p.xyz;
	
	float sigma[36];

	//  sigma: variations of channels 1 to 6.
	//	[var11]	[var12]	[var13]	[var14]	[var15]	[var16]	(0-5)
	//	symm	[var22]	[var23]	[var24]	[var25]	[var26]	(6-11)
	//	symm	symm	[var33]	[var34]	[var35]	[var36]	(12-17)
	//	symm	symm	symm	[var44]	[var45]	[var46]	(18-23)
	//	symm	symm	symm	symm	[var55]	[var56]	(24-29)
	//	symm	symm	symm	symm	symm	[var66] (30-35)

	vec4 tmp = boxfilter(mat1_sat, fltRadius);
	sigma[0] = tmp.x - pow(mean_I[0],2.f);
	sigma[1] = tmp.y - mean_I[0]*mean_I[1];
	sigma[2] = tmp.z - mean_I[0]*mean_I[2];
	sigma[3] = tmp.w - mean_I[0]*mean_I[3];
	
	tmp = boxfilter(mat2_sat, fltRadius);
	sigma[4] = tmp.x - mean_I[0]*mean_I[4];
	sigma[5] = tmp.y - mean_I[0]*mean_I[5];
	sigma[6] = sigma[1];
	sigma[7] = tmp.z - pow(mean_I[1],2.f);
	sigma[8] = tmp.w - mean_I[1]*mean_I[2];
	
	tmp = boxfilter(mat3_sat, fltRadius);
	sigma[9]  = tmp.x - mean_I[1]*mean_I[3];
	sigma[10] = tmp.y - mean_I[1]*mean_I[4];
	sigma[11] = tmp.z - mean_I[1]*mean_I[5];
	sigma[12] = sigma[2];
	sigma[13] = sigma[8];
	sigma[14] = tmp.w - pow(mean_I[2],2.f);

	tmp = boxfilter(mat4_sat, fltRadius);
	sigma[15] = tmp.x - mean_I[2]*mean_I[3];
	sigma[16] = tmp.y - mean_I[2]*mean_I[4];
	sigma[17] = tmp.z - mean_I[2]*mean_I[5];
	sigma[18] = sigma[3];
	sigma[19] = sigma[9];
	sigma[20] = sigma[15];
	sigma[21] = tmp.w - pow(mean_I[3],2.f);

	tmp = boxfilter(mat5_sat, fltRadius);
	sigma[22] = tmp.x - mean_I[3]*mean_I[4];
	sigma[23] = tmp.y - mean_I[3]*mean_I[5];
	sigma[24] = sigma[4];
	sigma[25] = sigma[10];
	sigma[26] = sigma[16];
	sigma[27] = sigma[22];
	sigma[28] = tmp.z - pow(mean_I[4],2.f);
	sigma[29] = tmp.w - mean_I[4]*mean_I[5];

	tmp = boxfilter(mat6_sat, fltRadius);
	sigma[30] = sigma[5];
	sigma[31] = sigma[11];
	sigma[32] = sigma[17];
	sigma[33] = sigma[23];
	sigma[34] = sigma[29];
	sigma[35] = tmp.x - pow(mean_I[5],2.f);

	float inverse[36] = invert_add_eps(sigma, epsilon);

	float[6] cov_Ip_input_r = float[6](cov_Ip_1.x, cov_Ip_2.x, cov_Ip_3.x, cov_Ip_4.x, cov_Ip_5.x, cov_Ip_6.x);
	float[6] cov_Ip_input_g = float[6](cov_Ip_1.y, cov_Ip_2.x, cov_Ip_3.y, cov_Ip_4.y, cov_Ip_5.y, cov_Ip_6.y);
	float[6] cov_Ip_input_b = float[6](cov_Ip_1.z, cov_Ip_2.z, cov_Ip_3.z, cov_Ip_4.z, cov_Ip_5.z, cov_Ip_6.z);

	float[6] a_r = matMult(inverse, cov_Ip_input_r);
	float[6] a_g = matMult(inverse, cov_Ip_input_g);
	float[6] a_b = matMult(inverse, cov_Ip_input_b);

	float b_r = mean_p.x - a_r[0]*mean_I[0] - a_r[1]*mean_I[1] - a_r[2]*mean_I[2]- a_r[3]*mean_I[3] - a_r[4]*mean_I[4] - a_r[5]*mean_I[5];
	float b_g = mean_p.y - a_g[0]*mean_I[0] - a_g[1]*mean_I[1] - a_g[2]*mean_I[2]- a_g[3]*mean_I[3] - a_g[4]*mean_I[4] - a_g[5]*mean_I[5];
	float b_b = mean_p.z - a_b[0]*mean_I[0] - a_b[1]*mean_I[1] - a_b[2]*mean_I[2]- a_b[3]*mean_I[3] - a_b[4]*mean_I[4] - a_b[5]*mean_I[5];

	vec3 b = vec3(b_r,b_g,b_b);

	gl_FragData[0].xyz = vec3(a_r[0],a_r[1],a_r[2]);
	gl_FragData[1].xyz = vec3(a_r[3],a_r[4],a_r[5]);
	gl_FragData[2].xyz = vec3(a_g[0],a_g[1],a_g[2]);
	gl_FragData[3].xyz = vec3(a_g[3],a_g[4],a_g[5]);
	gl_FragData[4].xyz = vec3(a_b[0],a_b[1],a_b[2]);
	gl_FragData[5].xyz = vec3(a_b[3],a_b[4],a_b[5]);
	gl_FragData[6].xyz = b;
}