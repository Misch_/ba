#version 150 core

in vec2 texCoord;

uniform sampler2D guide;
uniform sampler2D p;
	
vec3 currentRGB(sampler2D img)
{
	return texture2D(img, vec2(texCoord.x,texCoord.y)).xyz;
}

void main()
{
	vec3 guideVec = currentRGB(guide);
	vec3 pVec = currentRGB(p);
	
	gl_FragData[0].xyz = guideVec.r * pVec;
	gl_FragData[1].xyz = guideVec.g * pVec;
	gl_FragData[2].xyz = guideVec.b * pVec;
}