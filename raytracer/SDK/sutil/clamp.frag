#version 150 core

in vec2 texCoord;

uniform sampler2D tex;
	
void main()
{
	vec4 clamped = clamp(texture2D(tex, vec2(texCoord.x,texCoord.y)).xyzw,0,1);
	gl_FragData[0].xyzw = clamped;
}