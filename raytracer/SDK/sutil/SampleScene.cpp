#pragma once
/*
 * Copyright (c) 2008 - 2009 NVIDIA Corporation.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property and proprietary
 * rights in and to this software, related documentation and any modifications thereto.
 * Any use, reproduction, disclosure or distribution of this software and related
 * documentation without an express license agreement from NVIDIA Corporation is strictly
 * prohibited.
 *
 * TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THIS SOFTWARE IS PROVIDED *AS IS*
 * AND NVIDIA AND ITS SUPPLIERS DISCLAIM ALL WARRANTIES, EITHER EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL NVIDIA OR ITS SUPPLIERS BE LIABLE FOR ANY
 * SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT
 * LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF
 * BUSINESS INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR
 * INABILITY TO USE THIS SOFTWARE, EVEN IF NVIDIA HAS BEEN ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGES
 */

#if defined(__APPLE__)
#  include <OpenGL/gl.h>
#else
#  include <GL/glew.h>
#  if defined(_WIN32)
#    include <GL/wglew.h>
#  endif
#  include <GL/gl.h>
#endif

#include <SampleScene.h>

#include <optixu/optixu_math_stream_namespace.h>
#include <optixu/optixu.h>

#include <iostream>
#include <sstream>
#include <cstdlib>
#include "Host_Constants.h"


using namespace optix;

//-----------------------------------------------------------------------------
// 
// SampleScene class implementation 
//
//-----------------------------------------------------------------------------

std::string SampleScene::_full_path; //i hate c++

SampleScene::SampleScene()
 : _camera_changed( true ), _is_moving( false), _use_vbo_buffer( true ), _num_devices( 0 ), _fboTex( 0 )
{
	_fboTex = new unsigned int[3];					//3 because we use the mean, the variance and the number of b samples
	_shared_texture = new optix::TextureSampler[3];	//3 because IC uses separate direct-, indirect light and  brdf- buffers (DEPRECATED one is enough now..)
    _context = Context::create();
}

SampleScene::InitialCameraData::InitialCameraData( const std::string &camstr)
{
  std::istringstream istr(camstr);
  istr >> eye >> lookat >> up >> vfov;
}

SUTILAPI void SampleScene::setPath(std::string path )
{
	_full_path = path;
}

const char* const SampleScene::ptxpath(const std::string& target, const std::string& base )
{
  static std::string path;
  path = _full_path + "build/lib/ptx/" + target + "_generated_" + base + ".ptx";
  return path.c_str();
}

Buffer SampleScene::createOutputBuffer( RTformat format,
                                        unsigned int width,
                                        unsigned int height )
{
  Buffer buffer;

  if ( _use_vbo_buffer )
  {
    /*
      Allocate first the memory for the gl buffer, then attach it to OptiX.
    */
    GLuint vbo = 0;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    size_t element_size;
    _context->checkError(rtuGetSizeForRTformat(format, &element_size));
    glBufferData(GL_ARRAY_BUFFER, element_size * width * height, 0, GL_STREAM_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    buffer = _context->createBufferFromGLBO(RT_BUFFER_OUTPUT, vbo);
    buffer->setFormat(format);
    buffer->setSize( width, height );
  }
  else {
    buffer = _context->createBuffer( RT_BUFFER_OUTPUT, format, width, height);
  }

  // Set number of devices to be used
  // Default, 0, means not to specify them here, but let OptiX use its default behavior.
  if(_num_devices)
  {
    int max_num_devices    = Context::getDeviceCount();
    int actual_num_devices = std::min( max_num_devices, std::max( 1, _num_devices ) );
    std::vector<int> devs(actual_num_devices);
    for( int i = 0; i < actual_num_devices; ++i ) devs[i] = i;
    _context->setDevices( devs.begin(), devs.end() );
  }

  return buffer;
}

void SampleScene::cleanUp()
{
  _context->destroy();
  _context = 0;
}

void SampleScene::resizeBuffer(Buffer buffer, int width, int height)
{
	buffer->setSize( width, height);
	if(_use_vbo_buffer)
	{
		buffer->unregisterGLBuffer();
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer->getGLBOId());
		glBufferData(GL_PIXEL_UNPACK_BUFFER, buffer->getElementSize() * width * height, 0, GL_STREAM_DRAW); 
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
		buffer->registerGLBuffer();
	}
}

void SampleScene::resize(unsigned int width, unsigned int height)
{
	try {		
		resizeBuffer(getBuffer("output_buffer"), width, height);
		resizeBuffer(getBuffer("output_var_buffer"), width, height);
		resizeBuffer(getBuffer("num_samples_buffer"), width, height);	
		resizeBuffer(getBuffer("out_normal"), width, height);
		resizeBuffer(getBuffer("out_brdf"), width, height);
		resizeBuffer(getBuffer("out_position"),width,height);
	} 
	catch( Exception& e ){
		sutilReportError( e.getErrorString().c_str() );
		exit(2);
  }

  // Let the user resize any other buffers
  doResize( width, height );
}
