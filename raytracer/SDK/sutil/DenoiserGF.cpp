#include "Denoiser.h"
#include "Host_Constants.h"
#include "Filter.h"
#include "gui.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <glext.h>

#ifndef GL_ARB_timer_query
#define GL_TIME_ELAPSED 0x88BF
#define GL_TIMESTAMP 0x8E28
#endif 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Initializer
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGF::init( unsigned int *img, int width, int height)
{
	_width = width;
	_height = height;
	output_map = img;

	mse.resize(1);

	//default filters
	//allocMem(4);

	//alloc mem for reduction step
	_log2n = int(ceilf(max(logf(float(_width))/logf(2.f), logf(float(_height))/logf(2.f))));
	_tex_reduct = new unsigned int[_log2n+1];

	//setup texture that is shared with optix
	GLHelper::setupTexture(output_map, 1, _width, _height, GL_R32F, GL_RED);

	//setup textures
	setAllTextures();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//What should happen if the filters change?
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGF::updateFilter(){
	setFilter(GUI::getFilterMinDirect(), GUI::getFilterScaleDirect(), 4);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Setup the filters (only Gaussians at the moment)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGF::setFilter( float minSigma, float stepSize, int nSteps )
{

	importance_modifier_scales = stepSize*stepSize;

	float step2sqrt = minSigma*stepSize;
	step2sqrt *= step2sqrt;

	float step1sqrt = minSigma*minSigma;

	bias_mluiplicator = (step2sqrt + step1sqrt)/(step2sqrt - step1sqrt);

	this->minSigma = minSigma;
	this->stepSize = stepSize;
	this->nSteps = nSteps;

	Filter flt(minSigma, stepSize, nSteps);

	std::vector<float> meanFilterBank, varFilterBank, imgFilterBank, coneFilterBank, coneNorm;

	flt.getGaussianFilterbank(meanFilterBank);
	GLHelper::setupTexBuffer(&meanFilterBank[0], meanFilterBank.size(), &_tbo_filtermean, &_tex_mean_filter_coef);
	flt.getVarFilterbank(varFilterBank);
	GLHelper::setupTexBuffer(&varFilterBank[0], varFilterBank.size(), &_tbo_filtervar, &_tex_var_filter_coef);
	flt.getGaussianBoxFilterbank(imgFilterBank);
	GLHelper::setupTexBuffer(&imgFilterBank[0], imgFilterBank.size(), &_tbo_filterimg, &_tex_img_filter_coef);
	flt.getGaussianBoxCenterOneFilterBank(coneFilterBank);
	GLHelper::setupTexBuffer(&coneFilterBank[0], coneFilterBank.size(), &_tbo_filtercone, &_tex_cone_filter_coef);
	flt.getGaussianBoxCenterOneNorm(coneNorm);
	GLHelper::setupTexBuffer(&coneNorm[0], coneNorm.size(), &_tbo_cone_norm, &_tex_cone_norm);

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The name of this method is not well chosen. 
// The reason is that DenoiserGF is of the type Denoiser and 
// DenoiserGEM needs this function.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGF::computeSamplingDistribution( unsigned int *mean, unsigned int *var,  unsigned int *ns, unsigned int *normal, unsigned int *brdf, unsigned int *position, float gamma, float epsilon, int fltRadius)
{
	this->input_mean = mean;
	this->input_var = var;
	this->input_ns = ns;
	this->gamma = gamma;
	this->epsilon = epsilon;
	this->fltRadius = fltRadius;

	unsigned int *guidance_mean = input_mean;

	tex[MEAN_INPUT] = *mean;
	tex[VAR_INPUT] = *var;
	tex[NSAMPLES_INPUT] = *ns;
	tex[NORMALS_INPUT] = *normal;
	
	// to watch the new texture:
	tex[BRDF_INPUT] = *brdf;
	tex[POSITION_INPUT] = *position;

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo_3targets);
	totalMSEReduct = 1; // MSE = Mean squared error



	GLHelper::shaderPass(Shader::getShaderID(GAMMA_CORR),
		"sampler img", 0, *input_mean,
		"float gamma", gamma,
		"out", 0, tex[P],
		'\0');

	GLHelper::shaderPass(Shader::getShaderID(WHITE),
		"out", 0, tex[TMP1],
		'\0');

	generateSAT(&tex[TMP1],&tex[ONES]);

	GLHelper::shaderPass(Shader::getShaderID(COMP_BOX_SIZE),
		"sampler ones", 0, tex[ONES],
		"int fltRadius", fltRadius,
		"int width", _width,
		"int height", _height,
		"out", 0, tex[BOX_SIZE],
		'\0');

	/*GLHelper::shaderPass(Shader::getShaderID(GAMMA_CORR),
		"sampler img", 0, *guidance_mean,
		"float gamma", gamma,
		"out", 0, tex[I],
		'\0');*/
	
	/*GLfloat noBuffers = 0.0f;
	glGetFloatv(GL_MAX_DRAW_BUFFERS, &noBuffers);

	cout << noBuffers;*/

	GLHelper::shaderPass(Shader::getShaderID(CLAMP),
		"sampler tex", 0, *position,
		"out", 0, tex[POSITION_INPUT],
		'\0');

	
	/* Comment out the filtering method you want to use:
		grayScaleGf(input, guidance): 
			Performs the guided image filter on any color channel independently.
			Applying this using a feature as a guidance image buffer may not lead to reasonable results
			because the guidance colors may have nothing to do with the input colors.

		colorGF(input, guidance):
			Performs the guided image filter, treating the guidance argument as 3-dimensional.
			This is the appropriate function to use if one feature buffer is available.

		colorGF2(input, guidance1, guidance2):
			If 2 feature buffers are available, then this function should be chosen.

		colorGF3(input, guidance1, guidance2, guidance3):
			If 3 feature buffers are available, then this function should be chosen.
	*/
	
	grayScaleGF(&tex[P],&tex[P]);
	//colorGF(&tex[P],&tex[BRDF_INPUT]);
	//colorGF2(&tex[P],&tex[BRDF_INPUT],&tex[NORMALS_INPUT]);
	//colorGF3(&tex[P],&tex[BRDF_INPUT],&tex[NORMALS_INPUT],&tex[POSITION_INPUT]);

	/* 
		For visualization of the normals, all the values should be in [0,1]
	*/
	GLHelper::shaderPass(Shader::getShaderID(NONNEGATIVE),
		"sampler tex", 0, *normal,
		"out", 0, tex[NORMALS_NONNEGATIVE],
		'\0');
}

void DenoiserGF::grayScaleGF(unsigned int *input, unsigned int *guide){
	GLuint time_query;
	GLuint time_gf;
	glGenQueries(1, &time_query);
	ofstream timeFile;

	glBeginQuery(GL_TIME_ELAPSED, time_query);
	GLHelper::shaderPass(Shader::getShaderID(MULT),
		"sampler tex1", 0, *input,
		"sampler tex2", 1, *guide,
		"out", 0, tex[TMP1],
		'\0');
	glEndQuery(GL_TIME_ELAPSED);

	GLHelper::shaderPass(Shader::getShaderID(MULT),
		"sampler tex1", 0, *guide,
		"sampler tex2", 1, *guide,
		"out", 0, tex[TMP2],
		'\0');

	generateSAT(input,&tex[P_SAT]);
	generateSAT(guide,&tex[I_SAT]);
	generateSAT(&tex[TMP1], &tex[IP_SAT]);
	generateSAT(&tex[TMP2], &tex[II_SAT]);

	GLHelper::shaderPass(Shader::getShaderID(GUIDED_FILTER1),
		"sampler p_sat", 0, tex[P_SAT],
		"sampler I_sat", 1, tex[I_SAT],
		"sampler Ip_sat", 2, tex[IP_SAT],
		"sampler II_sat", 3, tex[II_SAT],
		"sampler boxsize", 4, tex[BOX_SIZE],
		"int width", _width,
		"int height", _height,
		"int fltRadius", fltRadius,
		"float epsilon", epsilon,
		"out", 0, tex[TMP1],
		"out", 1, tex[TMP2],
		'\0');
	

	generateSAT(&tex[TMP1],&tex[A_K_SAT]);
	generateSAT(&tex[TMP2],&tex[B_K_SAT]);

	GLHelper::shaderPass(Shader::getShaderID(GUIDED_FILTER2),
		"sampler a_sat", 0, tex[A_K_SAT],
		"sampler b_sat", 1, tex[B_K_SAT],
		"sampler I", 2, *guide,
		"sampler boxsize", 3, tex[BOX_SIZE],
		"int width", _width,
		"int height", _height,
		"int fltRadius", fltRadius,
		"out", 0, tex[GF_FILTERED],
		'\0');
	

	glGetQueryObjectuiv(time_query,GL_QUERY_RESULT,&time_gf);
	timeFile.open("timeFile_grayScale.txt",ios::out | ios::app);
	timeFile << time_gf;
	timeFile << "\n";
	timeFile.close();
}

void DenoiserGF::colorGF(unsigned int *input, unsigned int *guide){
	GLuint time_query;
	GLuint time_gf;
	glGenQueries(1, &time_query);
	ofstream timeFile;

	glBeginQuery(GL_TIME_ELAPSED, time_query);
	GLHelper::shaderPass(Shader::getShaderID(MULT_CHANNEL_WISE),
		"sampler tex1", 0, *guide,
		"sampler tex2", 1, *input,
		"out", 0, tex[TMP1],
		"out", 1, tex[TMP2],
		"out", 2, tex[TMP3],
		'\0');

	generateSAT(&tex[TMP1], &tex[IP_R_SAT]);
	generateSAT(&tex[TMP2], &tex[IP_G_SAT]);
	generateSAT(&tex[TMP3], &tex[IP_B_SAT]);
	
	GLHelper::shaderPass(Shader::getShaderID(MULT_ALL_COMB),
		"sampler tex1", 0, *guide,
		"sampler tex2", 1, *guide,
		"out", 0, tex[TMP1],
		"out", 1, tex[TMP2],
		"out", 2, tex[TMP3],
		'\0');

	generateSAT(&tex[TMP1], &tex[II_RX_SAT]);
	generateSAT(&tex[TMP2], &tex[II_GX_SAT]);
	generateSAT(&tex[TMP3], &tex[II_BX_SAT]);
	generateSAT(input,&tex[P_SAT]);
	generateSAT(guide,&tex[I_SAT]);
	
	GLHelper::shaderPass(Shader::getShaderID(GF_COL1),
		"sampler I_sat", 0, tex[I_SAT],
		"sampler p_sat", 1, tex[P_SAT],
		"sampler Ip_r_sat", 2, tex[IP_R_SAT],
		"sampler Ip_g_sat", 3, tex[IP_G_SAT],
		"sampler Ip_b_sat", 4, tex[IP_B_SAT],
		"sampler II_rx_sat",5, tex[II_RX_SAT],
		"sampler II_gx_sat", 6, tex[II_GX_SAT],
		"sampler II_bx_sat", 7, tex[II_BX_SAT],
		"sampler boxsize", 8, tex[BOX_SIZE],
		"int width", _width,
		"int height", _height,
		"int fltRadius", fltRadius,
		"float epsilon", epsilon,
		"out", 0, tex[TMP1],
		"out", 1, tex[TMP2],
		"out", 2, tex[TMP3],
		"out", 3, tex[TMP4],
		'\0');
	
	generateSAT(&tex[TMP1],&tex[A_K_R_SAT]);
	generateSAT(&tex[TMP2],&tex[A_K_G_SAT]);
	generateSAT(&tex[TMP3],&tex[A_K_B_SAT]);
	generateSAT(&tex[TMP4],&tex[B_K_SAT]);

	GLHelper::shaderPass(Shader::getShaderID(GF_COL2),
		"sampler I", 0, *guide,
		"sampler b_sat", 1, tex[B_K_SAT],
		"sampler a_r_sat", 2, tex[A_K_R_SAT],
		"sampler a_g_sat", 3, tex[A_K_G_SAT],
		"sampler a_b_sat", 4, tex[A_K_B_SAT],
		"sampler boxsize", 5, tex[BOX_SIZE],
		"int width", _width,
		"int height", _height,
		"int fltRadius", fltRadius,
		"out", 0, tex[GF_FILTERED],
		'\0');
	glEndQuery(GL_TIME_ELAPSED);
	

	glGetQueryObjectuiv(time_query,GL_QUERY_RESULT,&time_gf);
	timeFile.open("timeFile_color3.txt",ios::out | ios::app);
	timeFile << time_gf;
	timeFile << "\n";
	timeFile.close();
}


void DenoiserGF::colorGF2(unsigned int *input, unsigned int *guide1, unsigned int *guide2){
	GLuint time_query;
	GLuint time_gf;
	glGenQueries(1, &time_query);
	ofstream timeFile;

	//glBeginQuery(GL_TIME_ELAPSED, time_query);
	GLHelper::shaderPass(Shader::getShaderID(MULT_CHANNEL_WISE2),
		"sampler guide1", 0, *guide1,	// guidance channels 1-3
		"sampler guide2", 1, *guide2,	// guidance channels 4-6
		"sampler p", 2, *input,
		"out", 0, tex[TMP1],	// channel1 * p
		"out", 1, tex[TMP2],	// channel2 * p
		"out", 2, tex[TMP3],	// channel3 * p
		"out", 3, tex[TMP4],	// channel4 * p
		"out", 4, tex[TMP5],	// channel5 * p
		"out", 5, tex[TMP6],	// channel6 * p
		'\0');

	generateSAT(&tex[TMP1], &tex[IP_1_SAT]);
	generateSAT(&tex[TMP2], &tex[IP_2_SAT]);
	generateSAT(&tex[TMP3], &tex[IP_3_SAT]);
	generateSAT(&tex[TMP4], &tex[IP_4_SAT]);
	generateSAT(&tex[TMP5], &tex[IP_5_SAT]);
	generateSAT(&tex[TMP6], &tex[IP_6_SAT]);
	
	GLHelper::shaderPass(Shader::getShaderID(MULT_ALL_COMB2),
		"sampler guide1", 0, *guide1,
		"sampler guide2", 1, *guide2,
		"out", 0, tex[TMP1],
		"out", 1, tex[TMP2],
		"out", 2, tex[TMP3],
		"out", 3, tex[TMP4],
		"out", 4, tex[TMP5],
		"out", 5, tex[TMP6],
		'\0');
	
	generateSAT(&tex[TMP1], &tex[TMP1_SAT]);
	generateSAT(&tex[TMP2], &tex[TMP2_SAT]);
	generateSAT(&tex[TMP3], &tex[TMP3_SAT]);
	generateSAT(&tex[TMP4], &tex[TMP4_SAT]);
	generateSAT(&tex[TMP5], &tex[TMP5_SAT]);
	generateSAT(&tex[TMP6], &tex[TMP6_SAT]);
	
	generateSAT(input,&tex[P_SAT]);
	generateSAT(guide1,&tex[I1_SAT]);
	generateSAT(guide2,&tex[I2_SAT]);

	//glBeginQuery(GL_TIME_ELAPSED, time_query); // to measure matrix inversion
	GLHelper::shaderPass(Shader::getShaderID(GF_COL1_V2),
		"sampler I1_sat", 0, tex[I1_SAT],
		"sampler I2_sat", 1, tex[I2_SAT],
		"sampler p_sat", 2, tex[P_SAT],
		"sampler Ip_1_sat", 3, tex[IP_1_SAT],
		"sampler Ip_2_sat", 4, tex[IP_2_SAT],
		"sampler Ip_3_sat", 5, tex[IP_3_SAT],
		"sampler Ip_4_sat", 6, tex[IP_4_SAT],
		"sampler Ip_5_sat", 7, tex[IP_5_SAT],
		"sampler Ip_6_sat", 8, tex[IP_6_SAT],
		"sampler mat1_sat",9, tex[TMP1_SAT],
		"sampler mat2_sat",10, tex[TMP2_SAT],
		"sampler mat3_sat",11, tex[TMP3_SAT],
		"sampler mat4_sat",12, tex[TMP4_SAT],
		"sampler mat5_sat",13, tex[TMP5_SAT],
		"sampler mat6_sat",14, tex[TMP6_SAT],
		"sampler boxsize", 15, tex[BOX_SIZE],
		"int width", _width,
		"int height", _height,
		"int fltRadius", fltRadius,
		"float epsilon", epsilon,
		"out", 0, tex[TMP1],
		"out", 1, tex[TMP2],
		"out", 2, tex[TMP3],
		"out", 3, tex[TMP4],
		"out", 4, tex[TMP5],
		"out", 5, tex[TMP6],
		"out", 6, tex[TMP7],
		'\0');
	//glEndQuery(GL_TIME_ELAPSED); // to measure matrix inversion
	
	generateSAT(&tex[TMP1],&tex[TMP1_SAT]);
	generateSAT(&tex[TMP2],&tex[TMP2_SAT]);
	generateSAT(&tex[TMP3],&tex[TMP3_SAT]);
	generateSAT(&tex[TMP4],&tex[TMP4_SAT]);
	generateSAT(&tex[TMP5],&tex[TMP5_SAT]);
	generateSAT(&tex[TMP6],&tex[TMP6_SAT]);
	generateSAT(&tex[TMP7],&tex[TMP7_SAT]);

	GLHelper::shaderPass(Shader::getShaderID(GF_COL2_V2),
		"sampler guide1", 0, *guide1,
		"sampler guide2", 1, *guide2,
		"sampler a_r1_sat", 2, tex[TMP1_SAT],
		"sampler a_r2_sat", 3, tex[TMP2_SAT],
		"sampler a_g1_sat", 4, tex[TMP3_SAT],
		"sampler a_g2_sat", 5, tex[TMP4_SAT],
		"sampler a_b1_sat", 6, tex[TMP5_SAT],
		"sampler a_b2_sat", 7, tex[TMP6_SAT],
		"sampler b_sat", 8, tex[TMP7_SAT],
		"sampler boxsize", 9, tex[BOX_SIZE],
		"int width", _width,
		"int height", _height,
		"int fltRadius", fltRadius,
		"out", 0, tex[GF_FILTERED],
		'\0');
	//glEndQuery(GL_TIME_ELAPSED);

	glGetQueryObjectuiv(time_query,GL_QUERY_RESULT,&time_gf);
	timeFile.open("timeFile_color6.txt",ios::out | ios::app);
	timeFile << time_gf;
	timeFile << "\n";
	timeFile.close();
}

void DenoiserGF::colorGF3(unsigned int *input, unsigned int *guide1, unsigned int *guide2, unsigned int *guide3){
	
	GLuint time_query;
	GLuint time_gf;
	glGenQueries(1, &time_query);
	ofstream timeFile;

	//glBeginQuery(GL_TIME_ELAPSED, time_query);
	GLHelper::shaderPass(Shader::getShaderID(MULT_CHANNEL_WISE3),
		"sampler guide", 0, *guide1,	// guidance channels 1-3
		"sampler p", 1, *input,
		"out", 0, tex[TMP1],	// channel1 * p
		"out", 1, tex[TMP2],	// channel2 * p
		"out", 2, tex[TMP3],	// channel3 * p
		'\0');
	

	GLHelper::shaderPass(Shader::getShaderID(MULT_CHANNEL_WISE3),
		"sampler guide", 0, *guide2,	// guidance channels 4-6
		"sampler p", 1, *input,
		"out", 0, tex[TMP4],	// channel4 * p
		"out", 1, tex[TMP5],	// channel5 * p
		"out", 2, tex[TMP6],	// channel6 * p
		'\0');

	GLHelper::shaderPass(Shader::getShaderID(MULT_CHANNEL_WISE3),
		"sampler guide", 0, *guide3,	// guidance channels 7-9
		"sampler p", 1, *input,
		"out", 0, tex[TMP7],	// channel7 * p
		"out", 1, tex[TMP8],	// channel8 * p
		"out", 2, tex[TMP9],	// channel9 * p
		'\0');

	generateSAT(&tex[TMP1], &tex[IP_1_SAT]);
	generateSAT(&tex[TMP2], &tex[IP_2_SAT]);
	generateSAT(&tex[TMP3], &tex[IP_3_SAT]);
	generateSAT(&tex[TMP4], &tex[IP_4_SAT]);
	generateSAT(&tex[TMP5], &tex[IP_5_SAT]);
	generateSAT(&tex[TMP6], &tex[IP_6_SAT]);
	generateSAT(&tex[TMP7], &tex[IP_7_SAT]);
	generateSAT(&tex[TMP8], &tex[IP_8_SAT]);
	generateSAT(&tex[TMP9], &tex[IP_9_SAT]);
	
	GLHelper::shaderPass(Shader::getShaderID(BOXFILTER_TEST),
		"sampler sat", 0, tex[IP_9_SAT],
		"sampler boxSize", 1, tex[BOX_SIZE],
		"int size", _width,
		"int fltRadius", fltRadius,
		"out", 0, tex[TEST],
		'\0');

	// TMP1-TMP8: first 32 entries of upper triangular matrix
	GLHelper::shaderPass(Shader::getShaderID(MULT_ALL_COMB_3_1),
		"sampler guide1", 0, *guide1,
		"sampler guide2", 1, *guide2,
		"sampler guide3", 2, *guide3,
		"out", 0, tex[TMP1],
		"out", 1, tex[TMP2],
		"out", 2, tex[TMP3],
		"out", 3, tex[TMP4],
		"out", 4, tex[TMP5],
		"out", 5, tex[TMP6],
		"out", 6, tex[TMP7],
		"out", 7, tex[TMP8],
		'\0');

	// TMP9-TMP12: last few entries of upper triangular matrix
	GLHelper::shaderPass(Shader::getShaderID(MULT_ALL_COMB_3_2),
		"sampler guide1", 0, *guide1,
		"sampler guide2", 1, *guide2,
		"sampler guide3", 2, *guide3,
		"out", 0, tex[TMP9],
		"out", 1, tex[TMP10],
		"out", 2, tex[TMP11],
		"out", 3, tex[TMP12],
		'\0');
	
	generateSAT(&tex[TMP1], &tex[TMP1_SAT]);
	generateSAT(&tex[TMP2], &tex[TMP2_SAT]);
	generateSAT(&tex[TMP3], &tex[TMP3_SAT]);
	generateSAT(&tex[TMP4], &tex[TMP4_SAT]);
	generateSAT(&tex[TMP5], &tex[TMP5_SAT]);
	generateSAT(&tex[TMP6], &tex[TMP6_SAT]);
	generateSAT(&tex[TMP7], &tex[TMP7_SAT]);
	generateSAT(&tex[TMP8], &tex[TMP8_SAT]);
	generateSAT(&tex[TMP9], &tex[TMP9_SAT]);
	generateSAT(&tex[TMP10], &tex[TMP10_SAT]);
	generateSAT(&tex[TMP11], &tex[TMP11_SAT]);
	generateSAT(&tex[TMP12], &tex[TMP12_SAT]);
	
	generateSAT(input,&tex[P_SAT]);
	generateSAT(guide1,&tex[I1_SAT]);
	generateSAT(guide2,&tex[I2_SAT]);
	generateSAT(guide3,&tex[I3_SAT]);

	glBeginQuery(GL_TIME_ELAPSED, time_query); // to measure matrix inversion
	GLHelper::shaderPass(Shader::getShaderID(GF_COL1_V3),
		"sampler I1_sat", 0, tex[I1_SAT],
		"sampler I2_sat", 1, tex[I2_SAT],
		"sampler I3_sat", 2, tex[I3_SAT],
		"sampler p_sat", 3, tex[P_SAT],
		"sampler Ip_1_sat", 4, tex[IP_1_SAT],
		"sampler Ip_2_sat", 5, tex[IP_2_SAT],
		"sampler Ip_3_sat", 6, tex[IP_3_SAT],
		"sampler Ip_4_sat", 7, tex[IP_4_SAT],
		"sampler Ip_5_sat", 8, tex[IP_5_SAT],
		"sampler Ip_6_sat", 9, tex[IP_6_SAT],
		"sampler Ip_7_sat", 10, tex[IP_7_SAT],
		"sampler Ip_8_sat", 11, tex[IP_8_SAT],
		"sampler Ip_9_sat", 12, tex[IP_9_SAT],
		"sampler mat1_sat",13, tex[TMP1_SAT],
		"sampler mat2_sat",14, tex[TMP2_SAT],
		"sampler mat3_sat",15, tex[TMP3_SAT],
		"sampler mat4_sat",16, tex[TMP4_SAT],
		"sampler mat5_sat",17, tex[TMP5_SAT],
		"sampler mat6_sat",18, tex[TMP6_SAT],
		"sampler mat7_sat",19, tex[TMP7_SAT],
		"sampler mat8_sat",20, tex[TMP8_SAT],
		"sampler mat9_sat",21, tex[TMP9_SAT],
		"sampler mat10_sat",22,tex[TMP10_SAT],
		"sampler mat11_sat",23,tex[TMP11_SAT],
		"sampler mat12_sat",24,tex[TMP12_SAT],
		"sampler boxsize", 25, tex[BOX_SIZE],
		"int width", _width,
		"int height", _height,
		"int fltRadius", fltRadius,
		"float epsilon", epsilon,
		"out", 0, tex[TMP1],	// channels 1-4 of a_r
		"out", 1, tex[TMP2],	// channels 5-8 of a_r
		"out", 2, tex[TMP3],	// channels 1-4 of a_g
		"out", 3, tex[TMP4],	// channels 5-8 of a_g
		"out", 4, tex[TMP5],	// channels 1-4 of a_b
		"out", 5, tex[TMP6],	// channels 5-8 of a_b
		"out", 6, tex[TMP7],	// channels 9 of a_r, a_g, a_b
		"out", 7, tex[TMP8],	// b
		'\0');
	glEndQuery(GL_TIME_ELAPSED); // to measure matrix inversion
	
	generateSAT(&tex[TMP1],&tex[TMP1_SAT]);
	generateSAT(&tex[TMP2],&tex[TMP2_SAT]);
	generateSAT(&tex[TMP3],&tex[TMP3_SAT]);
	generateSAT(&tex[TMP4],&tex[TMP4_SAT]);
	generateSAT(&tex[TMP5],&tex[TMP5_SAT]);
	generateSAT(&tex[TMP6],&tex[TMP6_SAT]);
	generateSAT(&tex[TMP7],&tex[TMP7_SAT]);
	generateSAT(&tex[TMP8],&tex[TMP8_SAT]);

	GLHelper::shaderPass(Shader::getShaderID(GF_COL2_V3),
		"sampler guide1", 0, *guide1,
		"sampler guide2", 1, *guide2,
		"sampler guide3", 2, *guide3,
		"sampler a_r1_sat", 3, tex[TMP1_SAT],
		"sampler a_r2_sat", 4, tex[TMP2_SAT],
		"sampler a_g1_sat", 5, tex[TMP3_SAT],
		"sampler a_g2_sat", 6, tex[TMP4_SAT],
		"sampler a_b1_sat", 7, tex[TMP5_SAT],
		"sampler a_b2_sat", 8, tex[TMP6_SAT],
		"sampler a_rgb8_sat",9,tex[TMP7_SAT],
		"sampler b_sat", 10, tex[TMP8_SAT],
		"sampler boxsize", 11, tex[BOX_SIZE],
		"int width", _width,
		"int height", _height,
		"int fltRadius", fltRadius,
		"out", 0, tex[GF_FILTERED],
		'\0');
	//glEndQuery(GL_TIME_ELAPSED);

	glGetQueryObjectuiv(time_query,GL_QUERY_RESULT,&time_gf);
	timeFile.open("timeFile_color9.txt",ios::out | ios::app);
	timeFile << time_gf;
	timeFile << "\n";
	timeFile.close();
}

// Generation of SAT (Summed-Area Table)
// according to the paper of Hensley et al
// "Fast Summed-Area Table Generation and its Applications"
// Result is stored to *out.
// Watch out: The in-texture will be overwritten!
void DenoiserGF::generateSAT(unsigned int *in, unsigned int *out){

	unsigned int *t_a = in;
	unsigned int *t_b = out;
	
	GLHelper::shaderPass(Shader::getShaderID(COPY_TEX),
		"sampler tex", 0, *t_a,
		"out", 0, tex[TMP_SAT],
		'\0');

	t_a = &tex[TMP_SAT];


	int n = computePasses(_width);
	int m = computePasses(_height);

	//// Horizontal phase
	for (int i = 0; i < n; i++){
		GLHelper::shaderPass(Shader::getShaderID(SAT_HORIZONTAL),
			"sampler img", 0, *t_a,
			"int size", _width,
			"int pass", i,
			"out", 0, *t_b,
			'\0');

		unsigned int *t_a_old = t_a;
		t_a = t_b;
		t_b = t_a_old;
	}

	//// Vertical phase
	for (int i = 0; i < m; i++){
		GLHelper::shaderPass(Shader::getShaderID(SAT_VERTICAL),
			"sampler img", 0, *t_a,
			"int size", _height,
			"int pass", i,
			"out", 0, *t_b,
			'\0');

		unsigned int *t_a_old = t_a;
		t_a = t_b;
		t_b = t_a_old;
	}
	out = t_a;
	// result in *out
}

// ========================================================	//
// Determine the number of passes for fast sat-generation	//
// ========================================================	//
//
// n = log2(size)
// -- if n is not an integer: n = ceil(n)
// -- else: n = n+1
int DenoiserGF::computePasses(int size){
	float passes = logf(float(size))/logf(2);
	
	int n = 0;
    if (float(int(passes)) == passes){
        n = int(passes)+1;
    }
    else{
        n = int(ceil(passes));
    }

	return n;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Setup All Texture
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGF::setAllTextures()
{
	GLHelper::setupTexture(&tex[0], NUM_OF_TEXTURES, _width, _height, GL_RGBA32F, GL_RGBA);	
	//	GLHelper::setupTexture(&_tex_debug[0], 4, _width, _height, GL_RGBA32F, GL_RGBA); 

	int c=1;
	int w = 1<<_log2n;
	int h = 1<<_log2n;
	for(int i=0; i<=_log2n; i++){
		GLHelper::setupTexture(&_tex_reduct[i],1,w/c, h/c, GL_R32F, GL_RED);
		c = c<<1;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Resize Texture
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGF::resizeTextures()
{
	TwWindowSize(_width, _height);

	glDeleteTextures(_log2n+1, &_tex_reduct[0]);
	delete[] _tex_reduct;

	_log2n = int(ceilf(max(logf(float(_width))/logf(2), logf(float(_height))/logf(2))));
	_tex_reduct = new unsigned int[_log2n+1];

	glDeleteTextures(NUM_OF_TEXTURES, &tex[0]);

	glDeleteTextures(1, output_map);	
	//glDeleteTextures(4, &_tex_debug[0]);

	GLHelper::setupTexture(output_map, 1, _width, _height, GL_R32F, GL_RED);

	setAllTextures();
	
	//setup FBOs
	GLHelper::setupFBO(&_fbo_3targets, &tex[0], 3);  // maximum of binded textures = 3
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// helperMethod to compute the avg number of samples
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGF::computeAvgNS(unsigned int* ns_map)
{
	//GET THE SUM OF DISTRIBUTED SAMPLES
	int w = 1<<_log2n;
	int h = 1<<_log2n;
	GLHelper::shaderPass(Shader::getShaderID(TEX_TO_POW_2), 
		"sampler img", 0, *ns_map,
		"out", 0, _tex_reduct[0],
		"viewport",  w, h, '\0');

	for( int i=1; i<_log2n+1; i++){
		w /= 2, h /=2;
		GLHelper::shaderPass(Shader::getShaderID(REDUCTION),
			"sampler img", 0, _tex_reduct[i-1],
			"int width", 2*w,
			"int height", 2*h,
			"out", 0, _tex_reduct[i],
			"viewport", w, h, '\0');
	}

	glViewport(0, 0, _width, _height);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _tex_reduct[_log2n]); 
	float *nsSum = new float[4];
	glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, nsSum);
	GUI::setAvgNS(nsSum[0]/float(_width*_height));
}