#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform sampler2D nonfiltered_stop;
uniform samplerBuffer flt;

//uniform sampler2D stop2;
uniform int size;

const int MAX_FLT = 8;
const float SIGMA = 0.001f;
float f[MAX_FLT];
int fltSize;
float tex;
	
void filterTex(in sampler2D img, int s, inout int idx)
{
	f[s] = 0.f;
	fltSize = int(texelFetch(flt, s+1).x);
	float offset = floor(fltSize/2.f);
	for(int i=0; i<fltSize; i++, idx++){
		tex = texture2D(img, vec2(texCoord.x, texCoord.y-(offset-i)/size))[s];
		f[s] += tex*texelFetch(flt, idx).x; 
	}
	
	//idx is now at the first elem of the next filter, eg offset+1 elems right of the centere of the last filter
	float center =  texelFetch(flt, idx-int(offset)-1).x;
	float diff = max(0.f, center * center - SIGMA);
	f[s] -= diff * texture2D(nonfiltered_stop, texCoord)[s];
	f[s] /= 1 - diff;
}


void main()
{

	
	int nScales = int(texelFetch(flt, 0).x);
	int idx = nScales + 1;
	
	filterTex(img, 0, idx);
	filterTex(img, 1, idx);
	filterTex(img, 2, idx);
	filterTex(img, 3, idx);
	
	/*for(int s=0; s<nScales; s++){
		f[s] = 0.f;
		fltSize = int(texelFetch(flt, s+1).x);
		float offset = floor(fltSize/2.f);
		for(int i=0; i<fltSize; i++, idx++){
			tex = texture2D(stop, vec2(texCoord.x, texCoord.y-(offset-i)/size))[s];
			f[s] += tex*texelFetch(flt, idx).x; 
		}
		
		//idx is now at the first elem of the next filter, eg offset+1 elems right of the centere of the last filter
		float center =  texelFetch(flt, idx-int(offset)-1).x;
		float diff = max(0.f, center * center - SIGMA);
		f[s] -= diff * texture2D(nonfiltered_stop, texCoord)[s];
		f[s] /= 1 - diff;
	}*/
	
	gl_FragData[0] = round(vec4(f[0], f[1], f[2], f[3]));

}
