#pragma once

#include "optix_host.h"
#include "StatisticsGatherer.h"

class ImageIO{
private:
	ImageIO();
	float *data;
public:
	static void writeStatisticsToFile( std::string name, StatisticsGatherer gatherer );
	static ImageIO readFromFile();
	float *getData();

};