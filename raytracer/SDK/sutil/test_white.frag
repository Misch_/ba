#version 150 core

in vec2 texCoord;

uniform int size; // window Size, e.g. 512x512px

vec3 getRGBat(sampler2D img, int i, int j)
{
	vec3 texVec = texture2D(img, vec2(texCoord.x + float(i)/float(size),texCoord.y + float(j)/float(size))).xyz;
	texVec = clamp(texVec,0,1);
	return texVec;
}



void main(){	
	vec3 col = vec3(1.0);
	float leftLowBd = float(204)/float(size);
	float rightUpBd = float(307)/float(size);
	if(texCoord.x < leftLowBd || texCoord.y < leftLowBd || texCoord.x > rightUpBd || texCoord.y > rightUpBd){
		col = vec3(0.0);
	}
	
	col = vec3(0.002);
	gl_FragData[0].xyz = col;
}