#version 150 core

in vec2 texCoord;


uniform sampler2D img;
uniform sampler2D img2;

uniform sampler2D img3;
uniform sampler2D img4;

//a trivial shader
//the magic happens when the viewport is smaller than the output texture

void main()
{	
	gl_FragData[0] = texture2D(img, texCoord);
}