#pragma once

#include <qstring.h>
#include "OpticalFlow\Coarse2FineTwoFrames.h"

class FlowFile
{
public:
	FlowFile(void);
	~FlowFile(void);

	bool init(const QString & fileName);

	int getWidth() const { return w; }
	int getHeight() const { return h; }
	int getNFrames() const { return nFrames; }

	void getFrame(FlowField & ff, int frame) const;
	void clear();

private:
	int h;
	int w;
	int nFrames;
	float * data;
};

