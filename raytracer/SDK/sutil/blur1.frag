#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform samplerBuffer flt;
uniform int size;

const int MAX_FLT = 8;
vec3 fltData[MAX_FLT];
vec3 tex;
int fltSize;
	
void filterTex(in sampler2D img, int s, inout int idx)
{
	fltData[s] = vec3(0);
	fltSize = int(texelFetch(flt, s+1).x);
	float offset = floor(fltSize/2.f);
	for(int i=0; i<fltSize; i++, idx++){
		tex = texture2D(img, vec2(texCoord.x-(offset-i)/size, texCoord.y)).xyz;
		fltData[s] += tex*texelFetch(flt, idx).x; 
	}
	gl_FragData[s].xyz = fltData[s];
}

void main()
{
	
	int nScales = int(texelFetch(flt, 0).x);

	//here starts the first filter
	int idx = nScales+1;
	int fltSize;
	
	filterTex(img, 0, idx);
	filterTex(img, 1, idx);
	filterTex(img, 2, idx);
	filterTex(img, 3, idx);

}