#version 150 core

in vec2 texCoord;

uniform sampler2D base;
uniform sampler2D chosenScale;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;

uniform samplerBuffer flt;
uniform int size;

const int MAX_FLT = 8;
vec3 f[MAX_FLT];
vec3 tex_val;
int fltSize;

void filterTex(in sampler2D tex, int s, inout int idx)
{
	f[s] = vec3(0.f);
	fltSize = int(texelFetch(flt, s+1).x);
	float offset = floor(fltSize/2.f);
	for(int i=0; i<fltSize; i++, idx++){
		tex_val = texture2D(tex, vec2(texCoord.x, texCoord.y-(offset-i)/size)).xyz;
		f[s] += tex_val * texelFetch(flt, idx).x; 
	}	
}

void main()
{

	int nScales = int(texelFetch(flt, 0).x);


	//here starts the first filter
	int idx = nScales+1;
	
	filterTex(tex0, 0, idx);
	filterTex(tex1, 1, idx);
	filterTex(tex2, 2, idx);
	filterTex(tex3, 3, idx);
	
	gl_FragData[0].xyz = f[0] + f[1] + f[2] + f[3];
	if(texture2D(chosenScale, texCoord).x==0.f)
		gl_FragData[0].xyz += texture2D(base, texCoord).xyz;
}