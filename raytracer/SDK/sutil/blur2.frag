#version 150 core

in vec2 texCoord;

uniform int size;
uniform samplerBuffer flt;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;


void filterTex(in sampler2D tex, int s, inout int idx)
{
	vec3 fltData = vec3(0);
	vec3 texData;
	int fltSize = int(texelFetch(flt, s+1).x);
	float offset = floor(fltSize/2.f);
	
	for(int i=0; i<fltSize; i++, idx++){
		texData = texture2D(tex, vec2(texCoord.x, texCoord.y-(offset-i)/size)).xyz;
		fltData += texData*texelFetch(flt, idx).x; 
	}
	
	gl_FragData[s].xyz = fltData;
}

void main()
{

	int nScales = int(texelFetch(flt, 0).x);
	
	int idx = nScales+1;
	
	sampler2D tex[4] = sampler2D[](tex0, tex1, tex2 ,tex3);
	
	filterTex(tex[0], 0, idx);
	filterTex(tex[1], 1, idx);
	filterTex(tex[2], 2, idx);
	filterTex(tex[3], 3, idx);
	
}