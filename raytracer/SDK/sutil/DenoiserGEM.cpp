#include "Denoiser.h"
#include "Host_Constants.h"
#include "Filter.h"
#include "gui.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Initializer
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGEM::init( unsigned int *img, int width, int height)
{
	_width = width;
	_height = height;
	output_map = img;

	mse.resize(1);

	//default filters
	//allocMem(4);

	//alloc mem for reduction step
	_log2n = int(ceilf(max(logf(float(_width))/logf(2.f), logf(float(_height))/logf(2.f))));
	_tex_reduct = new unsigned int[_log2n+1];

	//setup texture that is shared with optix
	GLHelper::setupTexture(output_map, 1, _width, _height, GL_R32F, GL_RED);

	//setup textures
	setAllTextures();

	//setup FBOs
	GLHelper::setupFBO(&_fbo_3targets, &tex[0], 3);
}

void DenoiserGEM::updateFilter(){
	setFilter(GUI::getFilterMinDirect(), GUI::getFilterScaleDirect(), 4);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Setup the filters (only Gaussians at the moment)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGEM::setFilter( float minSigma, float stepSize, int nSteps )
{

	importance_modifier_scales = stepSize*stepSize;

	float step2sqrt = minSigma*stepSize;
	step2sqrt *= step2sqrt;

	float step1sqrt = minSigma*minSigma;

	bias_mluiplicator = (step2sqrt + step1sqrt)/(step2sqrt - step1sqrt);

	this->minSigma = minSigma;
	this->stepSize = stepSize;
	this->nSteps = nSteps;

	Filter flt(minSigma, stepSize, nSteps);

	std::vector<float> meanFilterBank, varFilterBank, imgFilterBank, coneFilterBank, coneNorm;

	flt.getGaussianFilterbank(meanFilterBank);
	GLHelper::setupTexBuffer(&meanFilterBank[0], meanFilterBank.size(), &_tbo_filtermean, &_tex_mean_filter_coef);
	flt.getVarFilterbank(varFilterBank);
	GLHelper::setupTexBuffer(&varFilterBank[0], varFilterBank.size(), &_tbo_filtervar, &_tex_var_filter_coef);
	flt.getGaussianBoxFilterbank(imgFilterBank);
	GLHelper::setupTexBuffer(&imgFilterBank[0], imgFilterBank.size(), &_tbo_filterimg, &_tex_img_filter_coef);
	flt.getGaussianBoxCenterOneFilterBank(coneFilterBank);
	GLHelper::setupTexBuffer(&coneFilterBank[0], coneFilterBank.size(), &_tbo_filtercone, &_tex_cone_filter_coef);
	flt.getGaussianBoxCenterOneNorm(coneNorm);
	GLHelper::setupTexBuffer(&coneNorm[0], coneNorm.size(), &_tbo_cone_norm, &_tex_cone_norm);

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Returns the sampling map
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGEM::computeSamplingDistribution( unsigned int *mean, unsigned int *var,  unsigned int *ns, unsigned int *normal, unsigned int *brdf, unsigned int *position, float gamma, float epsilon, int fltRadius)
{
	this->input_mean = mean;
	this->input_var = var;
	this->input_ns = ns;
	this->gamma = gamma;

	tex[MEAN_INPUT] = *mean;
	tex[VAR_INPUT] = *var;
	tex[NSAMPLES_INPUT] = *ns;
	tex[NORMALS_INPUT] = *normal;

	//you can manually switch to GEM if you like...
	greedyErrorMinimization();
	//trivialFiltering();

}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Setup All Texture
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGEM::setAllTextures()
{

	GLHelper::setupTexture(&tex[0], NUM_OF_TEXTURES, _width, _height, GL_RGBA32F, GL_RGBA);	
	//	GLHelper::setupTexture(&_tex_debug[0], 4, _width, _height, GL_RGBA32F, GL_RGBA); 

	int c=1;
	int w = 1<<_log2n;
	int h = 1<<_log2n;
	for(int i=0; i<=_log2n; i++){
		GLHelper::setupTexture(&_tex_reduct[i],1,w/c, h/c, GL_R32F, GL_RED);
		c = c<<1;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Resize Texture
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGEM::resizeTextures()
{
	TwWindowSize(_width, _height);

	glDeleteTextures(_log2n+1, &_tex_reduct[0]);
	delete[] _tex_reduct;

	_log2n = int(ceilf(max(logf(float(_width))/logf(2), logf(float(_height))/logf(2))));
	_tex_reduct = new unsigned int[_log2n+1];

	glDeleteTextures(NUM_OF_TEXTURES, &tex[0]);

	glDeleteTextures(1, output_map);	
	//glDeleteTextures(4, &_tex_debug[0]);

	GLHelper::setupTexture(output_map, 1, _width, _height, GL_R32F, GL_RED);

	setAllTextures();
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// performs the adaptive filtering method described in [Rousselle 2011]
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGEM::greedyErrorMinimization()
{

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo_3targets);
	
	//FILTER MEAN
	GLHelper::shaderPass(Shader::getShaderID(MEAN_FILTER1), 
		"sampler img", 0, *input_mean, 
		"array flt", 1, _tex_img_filter_coef,
		"int size", _width, 
		"out", 0, tex[TMP1], 
		"out", 1, tex[TMP2], 
		"out", 2, tex[TMP3],
		"out", 3, tex[TMP4], '\0');

	GLHelper::shaderPass(Shader::getShaderID(MEAN_FILTER2), 
		"sampler tex0", 0, tex[TMP1], 
		"sampler tex1", 1, tex[TMP2], 
		"sampler tex2", 2, tex[TMP3],
		"sampler tex3", 3, tex[TMP4],
		"array flt", 4, _tex_img_filter_coef,
		"int size", _height, 
		"out", 0, tex[MEAN_FILTERED1],
		"out", 1, tex[MEAN_FILTERED2], 
		"out", 2, tex[MEAN_FILTERED3], 
		"out", 3, tex[MEAN_FILTERED4], '\0');


	//FILTER VARIANCE
	GLHelper::shaderPass(Shader::getShaderID(VAR_FILTER1),
		"sampler img", 0, *input_var,
		"sampler nsamples", 1, *input_ns,
		"array flt", 2, _tex_var_filter_coef,
		"int size", _width,
		"out", 0, tex[TMP1], '\0');

	GLHelper::shaderPass(Shader::getShaderID(VAR_FILTER2),
		"sampler img", 0, tex[TMP1],
		"array flt", 1, _tex_var_filter_coef,
		"int size", _height,
		"out", 0, tex[VAR_FILTERED], '\0');



	//FILTER NSAMPLES-MAP
	GLHelper::shaderPass(Shader::getShaderID(NSAMPLES_FILTER1),
		"sampler img", 0, *input_ns,
		"array flt", 1, _tex_cone_filter_coef,
		"int size", _width,
		"out", 0, tex[TMP1], '\0');

	GLHelper::shaderPass(Shader::getShaderID(NSAMPLES_FILTER2),
		"sampler img", 0, tex[TMP1],
		"array flt", 1, _tex_cone_filter_coef,
		"int size", _height,
		"out", 0, tex[NSAMPLES_FILTERED], '\0');



	//CREATE STOPMAP
	float p = pow(1.9f*gamma, 1.f/sqrt(2.f));
	float gamma_z = -log(1.f - p);

	GLHelper::shaderPass(Shader::getShaderID(STOPMAP_CREATE), 
		"sampler mean0", 0, tex[MEAN_FILTERED1], 
		"sampler mean1", 1, tex[MEAN_FILTERED2], 
		"sampler mean2", 2, tex[MEAN_FILTERED3], 
		"sampler mean3", 3, tex[MEAN_FILTERED4], 
		"sampler var0", 4,  tex[VAR_FILTERED], 
		"sampler basens", 5, *input_ns, 
		"sampler basemean", 6, *input_mean,
		"sampler basevar", 7, *input_var,
		"sampler ns", 8, tex[NSAMPLES_FILTERED],
		"array nsNorm", 9, _tex_cone_norm,
		//	"sampler scene_gradient_base", 10, tex[SCENE_GRADIENT],
		//	"sampler scene_gradient", 11, tex[SCENE_GRADIENT_FILTERED],
		"float gamma_z", gamma_z,
		"float scaleFactor", stepSize,
		"out", 0, tex[STOPMAP_DIFF_MSE_WEIGHTED], 
		"out", 1, tex[STOPMAP_DIFF_MSE], 
		"out", 2, tex[BRIGHTNESS_BASE], 
		"out", 3, tex[BRIGHTNESS2], 
		"out", 4, tex[SCALEMIN_UNFILTERED],'\0');



	//BINARIZE STOPMAP
	GLHelper::shaderPass(Shader::getShaderID(STOPMAP_BINARIZE), 
		"sampler stop", 0, tex[STOPMAP_DIFF_MSE_WEIGHTED],
		"sampler ns", 1, *input_ns,
		"out", 0, tex[STOPMAP_UNIFLTERED], '\0');



	//FILTER STOPMAP
	GLHelper::shaderPass(Shader::getShaderID(STOPMAP_FILTER1), 
		"sampler img", 0, tex[STOPMAP_UNIFLTERED],
		"array flt", 1, _tex_img_filter_coef,
		"int size", _width,
		"out", 0, tex[TMP1], '\0');

	GLHelper::shaderPass(Shader::getShaderID(STOPMAP_FILTER2),
		"sampler img",0,  tex[TMP1],
		"sampler nonfiltered_stop", 1, tex[STOPMAP_UNIFLTERED],
		"array flt", 2, _tex_img_filter_coef,
		"int size", _height,
		"out", 0, tex[STOPMAP_FILTERED], '\0');


	//COMPUTE COSTMAP
	GLHelper::shaderPass(Shader::getShaderID(COSTMAP_CREATE),
		"sampler basevar", 0, *input_var,
		"sampler cost0", 1, tex[STOPMAP_DIFF_MSE],
		"sampler stop0", 2, tex[STOPMAP_FILTERED],
		//"sampler stop0", 2, tex[STOPMAP_UNIFLTERED],
		"sampler ns", 3, tex[NSAMPLES_FILTERED],
		"sampler base_bright", 4, tex[BRIGHTNESS_BASE],
		"sampler bright", 5, tex[BRIGHTNESS2],
		"sampler basens", 6, *input_ns,
		"array nsNorm", 7, _tex_cone_norm,
		"sampler var", 8, tex[VAR_FILTERED],
		"float s_imp", importance_modifier_scales,
		"out", 0, tex[COSTMAP_MSE],
		"out", 1, tex[COSTMAP_STEPS],
		"out", 2, tex[SCALEMIN_UNFILTERED], '\0');


	//FILTER TRUE SCALEMIN (FOR INTERPOLATION)
	GLHelper::shaderPass(Shader::getShaderID(SCALEMIN_FILTER1),
		"sampler img", 0, tex[SCALEMIN_UNFILTERED],
		"array flt", 1, _tex_img_filter_coef,
		"int size", _width,
		"out", 0, tex[TMP1], '\0' );

	GLHelper::shaderPass(Shader::getShaderID(SCALEMIN_FILTER2),
		"sampler img", 0, tex[TMP1],
		"array flt", 1, _tex_img_filter_coef,
		"int size", _height,
		"out", 0, tex[SCALEMIN_FILTERED], '\0' );


	//REDUCTION PASSES TO GET SUM OF ALL COSTS
	int w = 1<<_log2n;
	int h = 1<<_log2n;

	GLHelper::shaderPass(Shader::getShaderID(TEX_TO_POW_2), 
		"sampler img", 0, tex[COSTMAP_MSE],
		"out", 0, _tex_reduct[0],
		"viewport",  w, h, '\0');

	for( int i=1; i<_log2n+1; i++){
		w /= 2, h /=2;
		GLHelper::shaderPass(Shader::getShaderID(REDUCTION),
			"sampler img", 0, _tex_reduct[i-1],
			"int width", 2*w,
			"int height", 2*h,
			"out", 0, _tex_reduct[i],
			"viewport", w, h, '\0');
	}


	//FILTER COSTMAP
	GLHelper::shaderPass(Shader::getShaderID(COSTMAP_FILTER1),
		"sampler img", 0, tex[COSTMAP_MSE],
		"sampler img2", 1, tex[COSTMAP_STEPS],
		"array flt", 2, _tex_img_filter_coef,
		"int size", _width,
		"out", 0, tex[TMP1],
		"viewport", _width, _height,'\0');

	GLHelper::shaderPass(Shader::getShaderID(COSTMAP_FILTER2),
		"sampler tex0", 0, tex[TMP1],
		"sampler base", 2, tex[COSTMAP_MSE],
		"sampler chosenScale", 3, tex[COSTMAP_STEPS],
		"array flt", 4, _tex_img_filter_coef,
		"int size", _height,
		"out", 0, tex[COSTMAP_FITLERED], '\0');


	//SUM UP AND NORMALIZE COSTMAP
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _tex_reduct[_log2n]); 
	float *tpx = new float[4];
	glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tpx);

	totalMSEReduct = tpx[0];

	GLHelper::shaderPass(Shader::getShaderID(COSTMAP_NORMALIZE),
		"sampler img", 0, tex[COSTMAP_FITLERED],
		"float total", tpx[0],
		"out", 0, *output_map, '\0');

	if(_groundTruth){
		//FILTER MEAN
		GLHelper::shaderPass(Shader::getShaderID(MEAN_FILTER1), 
			"sampler img", 0, *input_mean, 
			"array flt", 1, _tex_mean_filter_coef,
			"int size", _width, 
			"out", 0, tex[TMP1], 
			"out", 1, tex[TMP2], 
			"out", 2, tex[TMP3],
			"out", 3, tex[TMP4], '\0');

		GLHelper::shaderPass(Shader::getShaderID(MEAN_FILTER2), 
			"sampler tex0", 0, tex[TMP1], 
			"sampler tex1", 1, tex[TMP2], 
			"sampler tex2", 2, tex[TMP3],
			"sampler tex3", 3, tex[TMP4],
			"array flt", 4, _tex_mean_filter_coef,
			"int size", _height, 
			"out", 0, tex[MEAN_FILTERED1],
			"out", 1, tex[MEAN_FILTERED2], 
			"out", 2, tex[MEAN_FILTERED3], 
			"out", 3, tex[MEAN_FILTERED4], '\0');
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// helperMewthod to compute the avg number of samples
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DenoiserGEM::computeAvgNS(unsigned int* ns_map)
{
	//GET THE SUM OF DISTRIBUTED SAMPLES
	int w = 1<<_log2n;
	int h = 1<<_log2n;
	GLHelper::shaderPass(Shader::getShaderID(TEX_TO_POW_2), 
		"sampler img", 0, *ns_map,
		"out", 0, _tex_reduct[0],
		"viewport",  w, h, '\0');

	for( int i=1; i<_log2n+1; i++){
		w /= 2, h /=2;
		GLHelper::shaderPass(Shader::getShaderID(REDUCTION),
			"sampler img", 0, _tex_reduct[i-1],
			"int width", 2*w,
			"int height", 2*h,
			"out", 0, _tex_reduct[i],
			"viewport", w, h, '\0');
	}

	glViewport(0, 0, _width, _height);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _tex_reduct[_log2n]); 
	float *nsSum = new float[4];
	glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, nsSum);
	GUI::setAvgNS(nsSum[0]/float(_width*_height));
}