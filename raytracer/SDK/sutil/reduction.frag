#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform int height; //size of img
uniform int width;

// |-----'-----|-----'-----| res of screen
// |--'--|--'--|--'--|--'--| res of img

void main(void)
{
	gl_FragData[0] = vec4(0,0,0,0);
	
	float w = 1.f/(2*width);
	float h = 1.f/(2*height);
	/*
	//sum up values only the x component matters here
	gl_FragData[0].x += texture2D(img, vec2(texCoord.x-w, texCoord.y-h)).x;
	gl_FragData[0].x += texture2D(img, vec2(texCoord.x+w, texCoord.y-h)).x;
	gl_FragData[0].x += texture2D(img, vec2(texCoord.x-w, texCoord.y+h)).x;
	gl_FragData[0].x += texture2D(img, vec2(texCoord.x+w, texCoord.y+h)).x;
	*/
		
	float v0 = texture2D(img, vec2(texCoord.x-w, texCoord.y-h)).x;
	float v1 = texture2D(img, vec2(texCoord.x+w, texCoord.y-h)).x;
	float v2 = texture2D(img, vec2(texCoord.x-w, texCoord.y+h)).x;
	float v3 = texture2D(img, vec2(texCoord.x+w, texCoord.y+h)).x;
	
	//sum up values only the x component matters here
	gl_FragData[0].x = v0 + v1 + v2 + v3;
	
	//gl_FragData[0].x = max(v0, max(v1, max(v2, v3)));
}