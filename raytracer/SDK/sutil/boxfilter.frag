#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform int size; // window Size, e.g. 512x512px
uniform int fltRadius; // filter radius: a "box" will have the size of 2*fltRadius+1
uniform samplerBuffer flt;

const int MAX_FLT = 8;
vec3 fltData;
vec3 tex;
	
void filterTex(in sampler2D img, inout int idx)
{
	fltData = vec3(0);
	int boxSize = 2*fltRadius + 1;
	
	for (int i=-fltRadius; i<=fltRadius; i++)
	{
		for(int j = -fltRadius; j<=fltRadius;j++)
		{
			// only red channel for now (==> .xxx)
			tex = texture2D(img, vec2(texCoord.x + (float(i))/size,texCoord.y + (float(j)/size))).xyz;
			fltData += tex * (1.f/pow(float(boxSize),2.f));
		}
			gl_FragData[0].xyz = fltData;
	}
}

void main()
{
	int nScales = int(texelFetch(flt, 0).x);

	//here starts the first filter
	int idx = nScales+1;
	
	filterTex(img,idx);


}