#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform int width;
uniform int height;

// |-----'-----|-----'-----| res of screen
// |--'--|--'--|--'--|--'--| res of img
// pixels in img are shifted by 0.5/sizeimg. 
// We need to take this into account to sample centers of texels!

void main()
{
	gl_FragData[0] = vec4(0,0,0,0);
	
	float w = 1.f/(2*width);
	float h = 1.f/(2*height);
	int c=0;
	
	//compure average of significant components in a 2x2 neighborhood
	vec4 tmp = texture2D(img, vec2(texCoord.x-w, texCoord.y-h));
	if(tmp.w!=0) c++;
	gl_FragData[0] += tmp;
	
	tmp = texture2D(img, vec2(texCoord.x+w, texCoord.y-h));
	if(tmp.w!=0) c++;
	gl_FragData[0] += tmp;
	
	tmp = texture2D(img, vec2(texCoord.x-w, texCoord.y+h));
	if(tmp.w!=0) c++;
	gl_FragData[0] += tmp;
	
	tmp = texture2D(img, vec2(texCoord.x+w, texCoord.y+h));
	if(tmp.w!=0) c++;
	gl_FragData[0] += tmp;
	
	//divide by number of significant elements to get the average
	//a element without smaples would bias the average in a wrong way otherwise
	//do not divide the w component!
	if(c>0)
		gl_FragData[0].xyz /= c;
}