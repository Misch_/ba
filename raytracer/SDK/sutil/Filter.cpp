#pragma once

#include "Filter.h"





Filter::Filter( float ms, float sf, int ns ):
	minScale(ms), scaleFactor(sf), nScales(ns)
{
	gauss.resize(nScales);
	var.resize(nScales);
	img.resize(nScales);
	centerOne.resize(nScales);
	createFilters();
}

//static funciton that returns vector of norms of a filtervector
void Filter::getGaussianBoxCenterOneNorm(vector<float> &norm)
{
	norm.resize(centerOne.size());

	for(unsigned int i=0; i<centerOne.size(); i++){
		norm.at(i) = 0.f;
		for(unsigned int j=0; j<centerOne.at(i).size(); j++){
			norm.at(i) += centerOne.at(i).at(j);
		}
	}
}

void Filter::getGaussianBoxCenterOneFilterBank(vector<float> &result)
{
	convertTo1DArray(centerOne, result);
}

void Filter::getGaussianFilterbank(vector<float> &result)
{
	convertTo1DArray(gauss, result);
}

void Filter::getVarFilterbank(vector<float> &result)
{
	convertTo1DArray(var, result);
}

void Filter::getGaussianBoxFilterbank(vector<float> &result)
{
	convertTo1DArray(img, result);
}

void Filter::createFilters()
{
	//create the gaussian
	

	vector< vector<float> > flt_4res(nScales, 0);
	//boxfilter of size SUB_RES
	vector<float> boxFlt(SUB_RES, 1.f/SUB_RES);
	vector<float> tmpImgRes, tmpVarRes;
	float ms = minScale;

	for(int i=0; i<nScales; i++){

		//compute gaussian with subpixel-sampling
		gaussian(ms, flt_4res.at(i));

		//compute pixel-resolution gaussian based on subpixel sampling of gaussian
		sub2pix(ms, flt_4res.at(i), gauss.at(i));

		//convolve subpixel filter with appropriate boxfilter
		conv(flt_4res.at(i), boxFlt, tmpImgRes);

		//compute pixel-resoultion filter of convolved subpixel filter (beware of pixel-shifting!)
		sub2pixHalf(ms, tmpImgRes, img.at(i), 1);

		//square coefs of img filter = var filter
		sqrtVec(tmpImgRes, tmpVarRes);

		//compute pixel-resoultion filter of convolved subpixel filter (beware of pixel-shifting!)
		sub2pixHalf(ms, tmpVarRes, var.at(i), float(SUB_RES));

		//gaussfilter with center normalized to one = centerOne filter
		centerToOne(img.at(i), centerOne.at(i));

		ms *= scaleFactor;
	}
}
void Filter::centerToOne(vector<float> &flt, vector<float> &result)
{
	result.resize(flt.size());
	float center = flt.at(flt.size()/2); //int corresponds to floor
	for(unsigned int i=0; i<flt.size(); i++){
		result.at(i) = flt.at(i)/center;
	}
}

void Filter::gaussian(float sigma, vector<float> &flt)
{
	float p1, p2, coeff, flt_sum = 0;
	int s = ceil( TIMES_SIGMA * sigma );
	float r = 1.f/SUB_RES;
	//create gaussian on sub4 grid
	for(float x=-s+r/2; x<=s; x+=r){
		p1 = -.5f * powf((x/sigma), 2.f);
		p2 = sigma * sqrtf(2 * M_PI );
		coeff = expf(p1) / p2;
		flt_sum += coeff;
		flt.push_back(coeff); 
	}
	//normalize to one so that no energy is lost
	for(int i=0; i<SUB_RES*(2*s); i++)
		flt.at(i) /= flt_sum;
}

void Filter::sub2pix(float sigma, vector<float> &subFlt, vector<float> &pixFlt )
{
	float r = 1.f/SUB_RES;
	int s = ceil( TIMES_SIGMA * sigma );
	pixFlt.resize(2*s+1, 0);
	for(int i=0; i<SUB_RES*(2*s); i++){
		int pixIdx = int((i+SUB_RES/2)/SUB_RES);
		pixFlt.at(pixIdx) += subFlt.at(i);
	}
}

/*
*	Sampling-posiitons after convolution are shifted by half a pixel, 
*	we need to take this in accuont when reducing the filter-resolution
*/
void Filter::sub2pixHalf( float sigma, vector<float> &subFlt, vector<float> &pixFlt, float f )
{
	float r = 1.f/SUB_RES;
	int s = ceil( TIMES_SIGMA * sigma );
	pixFlt.resize(2*s+1, 0);
	for(int i=0; i<subFlt.size(); i++){
		int pixIdx = int((i+1)/SUB_RES);
		if((i+1)%SUB_RES==0){
			pixFlt.at(pixIdx-1) += f*subFlt.at(i)/2.f;
			pixFlt.at(pixIdx) += f*subFlt.at(i)/2.f;
		}
		else
			pixFlt.at(pixIdx) += f*subFlt.at(i);
	}
}

void Filter::sqrtVec( vector<float> &flt, vector<float> &result)
{
	result.resize(flt.size());
	for(int i=0; i<flt.size(); i++)
		result.at(i) = flt.at(i)*flt.at(i);
}

//computes (f*g)[n]
void Filter::conv( vector<float> &flt1, vector<float> &flt2, vector<float> &result )
{	
	int s1 = flt1.size();
	int s2 = flt2.size();
	result.clear();
	result.resize(s1+s2-1, 0);
	float flt1Val;
	int rsltIdx=0;

	for(int m=-s2+1; m<s1; m++, rsltIdx++)
		for(int n=s2-1; n>=0; n--){
			flt1Val = (m+n>=0 && m+n<s1) ? flt1.at(m+n) : 0;
			result.at(rsltIdx) += flt1Val * flt2.at(n);
		}
}

/*
*	Format:
*	[nScales][sizes of the filters][filterdata]
**/
void Filter::convertTo1DArray( vector< vector<float> > &flt, vector<float> &result)
{
	//1st elem stores numofScales
	result.push_back(nScales);
	//int pos = nScales+1;
	
	//subsequent nScales elem: store size of the different filters
	for(int i=0; i<nScales; i++){
		result.push_back(flt.at(i).size());
	}

	//store filters in rest
	for(int i=0; i<flt.size(); i++)
		for(int j=0; j<flt.at(i).size(); j++){
				result.push_back(flt.at(i).at(j));
		}
}
