#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform sampler2D img2;
uniform samplerBuffer flt;

uniform int size;

const int MAX_FLT = 8;
vec3 f[MAX_FLT];
float weight;
vec3 tex;
int fltSize;

void filterTex(int s, inout int idx)
{
	f[s] = vec3(0.f);
	fltSize = int(texelFetch(flt, s+1).x);
	float offset = floor(fltSize/2.f);
	for(int i=0; i<fltSize; i++, idx++){
		weight = texture2D(img2,  vec2(texCoord.x-(offset-i)/size, texCoord.y)).x;
		if(round(weight*4)-1==s){
			tex = texture2D(img, vec2(texCoord.x-(offset-i)/size, texCoord.y)).xyz; 
			f[s] += tex * texelFetch(flt, idx).x; 
		}
	}	
	gl_FragData[s].xyz = f[s];	
}

void main()
{
	int nScales = int(texelFetch(flt, 0).x);


	//here starts the first filter
	int idx = nScales+1;
	
	filterTex(0, idx);
	filterTex(1, idx);
	filterTex(2, idx);
	filterTex(3, idx);

	gl_FragData[4] = texture2D(img, texCoord);
	
}
