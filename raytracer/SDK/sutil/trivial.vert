#version 150 core

in vec3 vert;
in vec2 tex;

out vec2 texCoord;

void main(){
	gl_Position =  vec4(vert, 1);
	
	
	
	texCoord = tex;
}