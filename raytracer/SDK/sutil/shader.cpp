#pragma once

#include <fstream>
#if defined(__APPLE__)
#  include <GLUT/glut.h>
#  define GL_FRAMEBUFFER_SRGB_EXT           0x8DB9
#  define GL_FRAMEBUFFER_SRGB_CAPABLE_EXT   0x8DBA
#else
#  include <GL/glew.h>
#  if defined(_WIN32)
#    include <GL/wglew.h>
#  endif
#  include <GL/glut.h>
#endif
#include "shader.h"
#include <string>
#include "Host_Constants.h"

using namespace std;

static GLubyte shaderText[MAX_SHADER_LENGTH]; //shadersize limited to 16k byte

static unsigned int shaderID[NUM_OF_SHADERS];

std::string Shader::_path;


/////////////////////// ////////////////////////////////////////////////////////////////////////////////////////////////////////
// Setup A Shader
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Shader::setupShader(unsigned int& id, std::string vert, std::string frag){
	Shader s;
//	std::string path("C:/Users/Marco/Documents/My Dropbox/Master Thesis/OPTIX/OptiX/SDK/sutil/");
//	std::string path("C:/Users/mmanzi/Dropbox/Master Thesis/OPTIX/OptiX/SDK/sutil/");
	std::string path(_path+"SDK/sutil/");
	id = s.create((path+vert+".vert").c_str(), (path+frag+".frag").c_str());
}


/////////////////////// ////////////////////////////////////////////////////////////////////////////////////////////////////////
// Loads all shaders that might be used in the application.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Shader::loadAllShaders(std::string& path)
{
	//setup shaders
	_path = path;

	setupShader( shaderID[TRIVIAL],			"trivial", "trivial");
	setupShader( shaderID[BASE_FILTER1],		"trivial", "baseblur1");
	setupShader( shaderID[BASE_FILTER2],		"trivial", "baseblur2");
	setupShader( shaderID[MEAN_FILTER1],		"trivial", "blur1");
	setupShader( shaderID[MEAN_FILTER2],		"trivial", "blur2");
	setupShader( shaderID[VAR_FILTER1],		"trivial", "blur1var");
	setupShader( shaderID[VAR_FILTER2],		"trivial", "blur2var");

	setupShader( shaderID[STOPMAP_CREATE],	"trivial", "stopcreate");

	setupShader( shaderID[STOPMAP_BINARIZE],	"trivial", "stopbinary");
	setupShader( shaderID[STOPMAP_FILTER1],	"trivial", "stopblur1");
	setupShader( shaderID[STOPMAP_FILTER2],	"trivial", "stopblur2");
	setupShader( shaderID[TEX_TO_POW_2],		"trivial", "texToPow2");
	setupShader( shaderID[REDUCTION],			"trivial", "reduction");
	setupShader( shaderID[COSTMAP_CREATE],	"trivial", "costmap");
	setupShader( shaderID[COSTMAP_FILTER1],	"trivial", "costmapblur1");
	setupShader( shaderID[COSTMAP_FILTER2],	"trivial", "costmapblur2");
	setupShader( shaderID[COSTMAP_NORMALIZE], "trivial", "costmapsum");
	setupShader( shaderID[FINAL],				"trivial", "final1");
	setupShader( shaderID[FINAL_TRIVIAL],				"trivial", "trivial_final");
	setupShader( shaderID[IRRADIANCE_CACHING],  "trivial", "finalIrradianceCaching");
	setupShader( shaderID[NSAMPLES_FILTER1],	"trivial", "nSamplesFilter1");
	setupShader( shaderID[NSAMPLES_FILTER2],	"trivial", "nSamplesFilter2");
	setupShader( shaderID[OUTLIERS_FILTER1],	"trivial", "outlierblur1");
	setupShader( shaderID[OUTLIERS_FILTER2],	"trivial", "outlierblur2");
	setupShader( shaderID[SCALEMIN_FILTER1],	"trivial", "scalemin_filter1");
	setupShader( shaderID[SCALEMIN_FILTER2],	"trivial", "scalemin_filter2");
	setupShader( shaderID[SCENE_DATA_GRADIENT],	"trivial", "scene_data_gradient");
	setupShader( shaderID[SCENE_DATA_GRADIENT_FILTER1],	"trivial", "scene_data_gradient_filter1");
	setupShader( shaderID[SCENE_DATA_GRADIENT_FILTER2],	"trivial", "scene_data_gradient_filter2");
	setupShader( shaderID[SHOW_COSTMAP],		"trivial", "show_Cost");
	setupShader( shaderID[SHOW_NSAMPLES],		"trivial", "show_SamplingDensity");
	setupShader( shaderID[SHOW_STOPMAP],		"trivial", "show_SelectedFilter");
	setupShader( shaderID[SHOW_NO_RECONSTR],	"trivial", "show_WithoutReconstruction");
	setupShader( shaderID[SHOW_VARIANCE],		"trivial", "show_VarianceMap");

	/* new by Misch */
	setupShader( shaderID[GUIDED_FILTER1],		"trivial", "guided_filter1");
	setupShader( shaderID[GUIDED_FILTER2],		"trivial", "guided_filter2");
	setupShader( shaderID[GAMMA_CORR],			"trivial", "gamma_corr");
	setupShader( shaderID[GF_COL1],				"trivial", "gf_col1");
	setupShader( shaderID[GF_COL2],				"trivial", "gf_col2");
	setupShader( shaderID[TEST_WHITE],			"trivial", "test_white");
	setupShader( shaderID[SAT_HORIZONTAL],		"trivial", "sat_horizontal");
	setupShader( shaderID[SAT_VERTICAL],		"trivial", "sat_vertical");
	setupShader( shaderID[COPY_TEX],			"trivial", "copy_tex");
	setupShader( shaderID[BOXFILTER_TEST],		"trivial", "boxfilter_test");
	setupShader( shaderID[MULT],				"trivial", "mult");
	setupShader( shaderID[MULT_CHANNEL_WISE],	"trivial", "mult_channel_wise");
	setupShader( shaderID[MULT_ALL_COMB],		"trivial", "mult_all_combinations");
	setupShader( shaderID[NORMALIZE],			"trivial", "normalize");
	setupShader( shaderID[WHITE],				"trivial", "white");
	setupShader( shaderID[COMP_BOX_SIZE],		"trivial", "comp_box_size");
	setupShader( shaderID[CLAMP],				"trivial", "clamp");
	setupShader( shaderID[NONNEGATIVE],			"trivial", "nonnegative");

	/* for multiple features */
	setupShader( shaderID[MULT_CHANNEL_WISE2],	"trivial", "mult_channel_wise2");
	setupShader( shaderID[MULT_ALL_COMB2],		"trivial", "mult_all_combinations2");
	setupShader( shaderID[GF_COL1_V2],			"trivial", "gf_col1_v2");
	setupShader( shaderID[GF_COL2_V2],			"trivial", "gf_col2_v2");

	setupShader( shaderID[MULT_CHANNEL_WISE3],	"trivial", "mult_channel_wise3");
	setupShader( shaderID[MULT_ALL_COMB_3_1],	"trivial", "mult_all_combinations3_1");
	setupShader( shaderID[MULT_ALL_COMB_3_2],	"trivial", "mult_all_combinations3_2");
	setupShader( shaderID[GF_COL1_V3],			"trivial", "gf_col1_v3");
	setupShader( shaderID[GF_COL2_V3],			"trivial", "gf_col2_v3");
}
GLuint Shader::create(const char *vert, const char *frag){
	
	GLuint vertShader;
	GLuint fragShader;
	GLuint ret = 0;
	GLint testVal;

	vertShader = glCreateShader(GL_VERTEX_SHADER);
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	//Lade Vertex Programm
	if(!loadShaderFile(vert, vertShader)){
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		printf("Shader at %s not found!", vert);
		return (GLuint)NULL;
	}

	//Lade Fragment Programm
	if(!loadShaderFile(frag,  fragShader)){ //this here fucks shaderID up sometimes
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		printf("Shader at %s not found!", frag);
		return (GLuint)NULL;
	}

	//Kompiliere die Shader
	glCompileShader(vertShader);
	glCompileShader(fragShader);

	//Vertex-Shader korrekt kompiliert?
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &testVal);
	if(testVal==GL_FALSE){
		char errMsg[1024];
		glGetShaderInfoLog(vertShader, 1024, NULL, errMsg);
		printf("Vertex Shader %s failed to compile:\n%s\n", vert, errMsg);
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		return (GLuint)NULL;
	}

	//FragmentShader korrekt Kompiliert?
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &testVal);
	if(testVal==GL_FALSE){
		char errMsg[1024];
		glGetShaderInfoLog(fragShader, 1024, NULL, errMsg);
		printf("Fragment Shader %s failed to compile:\n%s\n", frag, errMsg);
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		return (GLuint)NULL;
	}

	//Binde die Shader an das zu benutzende Programm
	ret = glCreateProgram();
	glAttachShader(ret, vertShader);
	glAttachShader(ret, fragShader);

/*	//(optionale) Argumentenliste abarbeiten...
	va_list attList;
	va_start(attList, frag);

	char *nextarg;
	int argcount = va_arg(attList, int);
	for(int i = 0; i<argcount; i++){
		int index = va_arg(attList, int);
		nextarg = va_arg(attList, char*);
		glBindAttribLocation(ret, index, nextarg);
	}
	va_end(attList);*/
	
	//link das ganze
	glLinkProgram(ret);

	//l�sche was nicht mehr gebraucht wird
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	//alles gut verlinkt?
	glGetProgramiv(ret, GL_LINK_STATUS, &testVal);
	if(testVal == GL_FALSE){
		char errMsg[1024];
		glGetProgramInfoLog(ret, 1024, NULL, errMsg);
		printf("Programm linking failed:\n%s\n", errMsg);
		glDeleteProgram(ret);
		return (GLuint)NULL;
	}
/*	Shader::id = ret;

	glGenVertexArrays(1,&(Shader::vao));
	glBindVertexArray(Shader::vao);*/

	return ret;
}

bool Shader::loadShaderFile(const char * file, GLuint id){
	GLint shaderLength = 0;
	FILE *fp;
	fp = fopen(file, "r");

	if(fp!=NULL){
		//l�nge des Files
		while(fgetc(fp) != EOF)
			shaderLength++;

		rewind(fp);
		if(shaderText != NULL)
			fread(shaderText, 1, shaderLength, fp); //this here fucks shaderID up sometimes when loadShaderFile(frag,  fragShader) is called when stopcreate is generated

		shaderText[shaderLength] = '\0';
		fclose(fp);
	} else
		return false;
	
	GLchar *strPrt[1];
	strPrt[0] = (GLchar *)((const char *) shaderText);
	glShaderSource(id, 1, (const char **)strPrt, NULL);

	return true;
}


void Shader::bindAttribute(float *data, int elem, const char *name){
	size_t numOfElements = sizeof(data)/sizeof(GLfloat)/elem;

	GLuint bon_vert; // buffer object name
	glGenBuffers(1,&bon_vert);
	glBindBuffer(GL_ARRAY_BUFFER,bon_vert);
	glBufferData(GL_ARRAY_BUFFER,sizeof(GLfloat)*elem*numOfElements,data,GL_STATIC_DRAW);
	const GLint loc_vert(glGetAttribLocation(Shader::id, name));

	glVertexAttribPointer(loc_vert,elem,GL_FLOAT,GL_TRUE,0,NULL);
	glEnableVertexAttribArray(loc_vert);
}
void Shader::disable(){
	glUseProgram(0);
}

void Shader::enable(){
	glUseProgram(id);
}

unsigned int Shader::getShaderID( int n )
{
	return shaderID[n];
}
