#pragma once
#include <string>

/************************************************************************/
/* This class consists of Unit-Test for the openGL shaders
 * Because openGL lacks unittest support, we use dummy inputs and write them to files, we can then inspect the outputs manually 
 * (therefore the outputsizes should be as small as possible, while still beeing usefull)
 * Remark: Damn the chronos-group for lack of any kind of debugging help!
/************************************************************************/
class Shader_Tester{
public:
	void testFilterMean(bool writeToFile = true);
	void testFilterVariance(bool writeToFile = true);
	void testFilterNSamples(bool writeToFile = true);
	void testStopmapCreate(bool writeToFile = true);
	void testFilterStopmap(bool writeToFile = true);
	void testStopmapBinarize(bool writeToFile = true);
	void testCostmapCreate( bool writeToFile = true );
	void testFilterCostmap(bool writeToFile = true);
private:
	unsigned int meanFlt[4];
	unsigned int varFlt;
	unsigned int nsFlt;
	unsigned int meanBase;
	unsigned int varBase;
	unsigned int nsBase;
	unsigned int brightness;
	unsigned int brightnessBase;
	unsigned int stop_not_weighted;
	unsigned int stop_weighted;
	unsigned int stopmap_non_filtered;
	unsigned int stopmap_filtered;
	unsigned int costmap_steps;
	unsigned int costmap_mse;
	unsigned int costmap_filtered;
	void printTest( float * texd, int size, int channel, std::string name );
	void testTexture_ZeroEverywhere_OneCenter(float *data, int size);
	void testTexture_RndEverywhere(float *data, float val, int size );
	void testTexture_ValEverywhere(float *data, float val, int size );
	void testTexture_Box( float *data, int bs, int size );
	
};