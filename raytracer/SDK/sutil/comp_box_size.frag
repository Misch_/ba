#version 150 core

in vec2 texCoord;

uniform sampler2D ones_sat;
uniform int fltRadius;
uniform int width;
uniform int height;

vec3 getRGBat(sampler2D img, int i, int j)
{
	return texture2D(img, vec2(texCoord.x + float(i)/float(width),texCoord.y + float(j)/float(height))).xyz;
}
vec3 boxfilter(sampler2D sat, int fltRadius)
{
	int boxSize = 2*fltRadius + 1;

	vec3 upRight	= getRGBat(sat,	fltRadius,		fltRadius);
	vec3 upLeft		= getRGBat(sat, -(fltRadius+1),	fltRadius);
	vec3 lowRight	= getRGBat(sat, fltRadius,		-(fltRadius+1));
	vec3 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec3 mean = upRight - upLeft - lowRight + lowLeft;
	return mean;
}

void main()
{
	vec3 size = boxfilter(ones_sat,fltRadius);

	gl_FragData[0].xyz = size;
	
}