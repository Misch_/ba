#pragma once
#include <string>
#include <stdarg.h>
#include "GL\glew.h"
#include <fstream>
#include "Filter.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Enums

//Identifiers for the shader, if GEM is not used you can ignore most of them...
enum ShaderName{TRIVIAL, BASE_FILTER1, BASE_FILTER2, MEAN_FILTER1, MEAN_FILTER2, VAR_FILTER1, VAR_FILTER2, STOPMAP_CREATE, STOPMAP_BINARIZE,
STOPMAP_FILTER1, STOPMAP_FILTER2, NSAMPLES_FILTER1, NSAMPLES_FILTER2, COSTMAP_CREATE, COSTMAP_FILTER1, COSTMAP_FILTER2,
TEX_TO_POW_2, REDUCTION, COSTMAP_NORMALIZE, OUTLIERS_FILTER1, OUTLIERS_FILTER2, FINAL, FINAL_TRIVIAL, IRRADIANCE_CACHING, SHOW_COSTMAP, SHOW_NSAMPLES, SHOW_STOPMAP, 
SHOW_NO_RECONSTR, SHOW_VARIANCE, SCALEMIN_FILTER1, SCALEMIN_FILTER2, SCENE_DATA_GRADIENT, SCENE_DATA_GRADIENT_FILTER1, SCENE_DATA_GRADIENT_FILTER2, COMP_BOX_SIZE, WHITE,
GUIDED_FILTER1, GUIDED_FILTER2, GAMMA_CORR, NORMALIZE, MULT_CHANNEL_WISE2, MULT_ALL_COMB2, GF_COL1_V2, GF_COL2_V2, GF_COL1_V3, GF_COL2_V3,NONNEGATIVE,
GF_COL1,GF_COL2,TEST_WHITE,SAT_HORIZONTAL, SAT_VERTICAL, COPY_TEX, BOXFILTER_TEST, MULT, MULT_CHANNEL_WISE, MULT_ALL_COMB, MULT_ALL_COMB_3_1, MULT_ALL_COMB_3_2, MULT_CHANNEL_WISE3, CLAMP,
NUM_OF_SHADERS};
//A lot of these textures are used only for GEM or deprecated. Still, to avoid the risk of messing up everything, keep them right now.
//If new textures are used for other purposes add new enum somewhere in front of NUM_OF_TEXTURES
enum TexName{	MEAN_FILTERED1, MEAN_FILTERED2, MEAN_FILTERED3, MEAN_FILTERED4, 
	VAR_FILTERED, NSAMPLES_FILTERED, 
	STOPMAP_FILTERED, STOPMAP_UNIFLTERED,
	STOPMAP_DIFF_MSE, STOPMAP_DIFF_MSE_WEIGHTED, 
	COSTMAP_FITLERED, COSTMAP_STEPS, COSTMAP_MSE,  
	BRIGHTNESS_BASE, BRIGHTNESS2,
	ONES, BOX_SIZE, TEST,
	TMP1, TMP2,TMP3, TMP4, TMP5, TMP6, TMP7, TMP8, TMP9, TMP10, TMP11, TMP12,
	TMP1_SAT, TMP2_SAT,TMP3_SAT, TMP4_SAT, TMP5_SAT, TMP6_SAT, TMP7_SAT, TMP8_SAT, TMP9_SAT, TMP10_SAT, TMP11_SAT, TMP12_SAT,
	II_R1R1_G1G1_B1B1_SAT,II_R2R2_G2G2_B2B2_SAT,II_R1G1_R1B1_R1R2_R1G2_SAT,II_R1B2_G1B1_G1R2_G1G2_SAT,II_G1B2_B1R2_B1G2_B1B2_SAT,II_R2G2_R2B2_G2B2_SAT,
	IP_1_SAT, IP_2_SAT, IP_3_SAT, IP_4_SAT, IP_5_SAT, IP_6_SAT, IP_7_SAT, IP_8_SAT, IP_9_SAT,
	SCALEMIN_UNFILTERED, SCALEMIN_FILTERED, GF_FILTERED, A_K_R_SAT, A_K_G_SAT, A_K_B_SAT, 
	P_SAT, I_SAT, TMP_SAT, IP_SAT, II_SAT, A_K_SAT, B_K_SAT, P, I, IP_R_SAT, IP_G_SAT, IP_B_SAT, II_RX_SAT, II_GX_SAT, II_BX_SAT,
	I1_SAT, I2_SAT, I3_SAT, NORMALS_NONNEGATIVE,ZERO_TEX, 
	NUM_OF_TEXTURES,						//NOT A TEXTURE IDENTIFIER: stores the number of textures we need to create for the denoiser (kind of a hack) 
	MEAN_INPUT, VAR_INPUT, NSAMPLES_INPUT, NORMALS_INPUT, BRDF_INPUT, POSITION_INPUT	//redundant identifiers for the denoiser independent input data, simplifies access to them by the GUI
};

enum DenoiserType{
	DENOISER_GF, DENOISER_GEM
};

//identifier for the optiX entry points
const int PINHOLE_CAMERA_INIT_ENTRY				= 0;
const int PINHOLE_CAMERA_ADAPTIVE_ENTRY			= 1;

namespace GLHelper{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Creates a Texture Buffer Object, fills it with data and binds it to a TextureID
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static void setupTexBuffer(float *data, int size, unsigned int *tboID, unsigned int *texID)
{
	glGenBuffers(1, tboID);
	glBindBuffer(GL_TEXTURE_BUFFER_EXT, *tboID);
	glBufferData(GL_TEXTURE_BUFFER_EXT, sizeof(float)*size, data, GL_STATIC_DRAW);
	glGenTextures(1, texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_BUFFER_EXT, *texID);
	glTexBufferEXT(GL_TEXTURE_BUFFER_EXT, GL_R32F, *tboID);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Setup frame buffer object...
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static void setupFBO(unsigned int* id, unsigned int* tex, int size){

	glGenFramebuffers(1, id);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, *id);

	for(int i=0; i<size; i++){
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, tex[i], 0);
	}

	if(glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE) { //36053
		printf("Framebuffer has not been set up properly. exit program for your own safety");
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		exit(0);
	}
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Error checker, use this for debugging whenever a openGL bug needs to be tracked
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static void errorCheck(){
	int err = glGetError();
	if(err!=GL_NO_ERROR){
		const GLubyte* errString = gluErrorString(err);
		printf("\nOpenGL error: %s", errString);
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Setup Texture
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static void setupTexture( unsigned int* id, unsigned int num, int width, int height, int internalFormat, int format, float *data = NULL)
{
	glGenTextures(num, id); //potential memory leak!
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	for(unsigned int i=0; i<num; i++){
		glBindTexture( GL_TEXTURE_2D, *(id+i));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_FLOAT, data);
		glBindTexture( GL_TEXTURE_2D, 0);
	}
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0);
}

/*
 * This function allows to activate a shader with (nearly) arbitrary settings and bindings.
 * The syntax for attributes works in general as follow:
 * 
 * "<type T>  <uname(if needed)>", values, ..., values
 * 
 * (The exact syntax for each Type is denoted as comment in the switch-statement)
 * 
 * Example: 
 * pass( shader_program, "sampler tex0", 0, _tex0, "float size", sizeVal, "out", 1, _tex_output, "viewport", 512, 256, '\0')
 * 
 * At the end of the argument list a delimiter is needed to indicate the end of the arguments. 
 * The delimiter must be '\0'
 **/
static void shaderPass(int prog, ...){

	glUseProgram(prog);

	va_list arg;
	va_start(arg, prog);

	GLenum c_att = GL_COLOR_ATTACHMENT0; //all outputs are stored in color_attachmet buffers
	GLenum fboBuffer[] = { c_att, c_att, c_att, c_att, c_att, c_att, c_att, c_att}; //assume for now no more than 8 targets
	int nRenderTargets = 0;

	char *data = va_arg(arg, char *);
	char *name;

	do{
		//strtok needs a char-array as argument. char* will yield unpredictable results.
		char buffer[512];
		sprintf( buffer, data );
		name = strtok(buffer, " ");

		switch(name[0]){
			//"int NAME", value
			case 'i':{
				name = strtok(NULL, " ");
				int val = va_arg(arg, int);
				glUniform1i(glGetUniformLocation(prog, name), val);
				break;
					 }
			//"float NAME", value
			case 'f':{
				name = strtok(NULL, " ");
				//va_args puts floats as double on the argument stack.
				float val = float(va_arg(arg, double));
				glUniform1f(glGetUniformLocation(prog, name), val);
				break;
					 }
			// "sampler NAME", textureUnit, textureIdentifier
			case 's':{
				name = strtok(NULL, " "); 
				int unit = va_arg(arg, int);
				int val = va_arg(arg, int);
				glActiveTexture(GL_TEXTURE0+unit);
				glBindTexture(GL_TEXTURE_2D, val);
				glUniform1i(glGetUniformLocation(prog, name), unit);
				break;
					 }
			// "out", renderTarget, textureIdentifier
			case 'o':{
				int unit = va_arg(arg, int);
				int val = va_arg(arg, int);
				glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+unit, GL_TEXTURE_2D, val, 0);			
				fboBuffer[nRenderTargets] += unit;
				nRenderTargets++;
				break;
					 }
			// "viewport", width, height
			case 'v':{
				int w = va_arg(arg, int);
				int h = va_arg(arg, int);
				glViewport(0, 0, w, h);
				break;
					 }
			//"array NAME", textureUnit, identifier
			case 'a':{
				name = strtok(NULL, " "); 
				int unit = va_arg(arg, int);
				int val = va_arg(arg, int);
				glActiveTexture(GL_TEXTURE0+unit);
				glBindTexture(GL_TEXTURE_BUFFER_EXT, val);
				glUniform1i(glGetUniformLocation(prog, name), unit);
				break;
					 }
			//or as uniform array... (i never used this...)
			case 'x':{
				name = strtok(NULL, " ");
				int length = va_arg(arg, int);
				float *val = va_arg(arg, float*);
				glUniform1fv(glGetUniformLocation(prog, name), length, val);
				}
		}
		data = va_arg(arg, char *);
	}while(data!=0);

	if(nRenderTargets!=0)
		glDrawBuffers(nRenderTargets, fboBuffer);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
	va_end(arg);
}

static void screenshot (char filename[160],int x, int y, unsigned int texId){
	long imageSize = x * y * 3;
	unsigned char *data = new unsigned char[imageSize];
	glBindTexture(GL_TEXTURE_2D, texId);
	glGetTexImage(GL_TEXTURE_2D,0,GL_BGR,GL_UNSIGNED_BYTE,data);
	//glReadPixels(0,0,x,y, GL_BGR,GL_UNSIGNED_BYTE,data);// split x and y sizes into bytes
	int xa= x % 256;
	int xb= (x-xa)/256;int ya= y % 256;
	int yb= (y-ya)/256;//assemble the header
	unsigned char header[18]={0,0,2,0,0,0,0,0,0,0,0,0,(char)xa,(char)xb,(char)ya,(char)yb,24,0};

	// write header and data to file
	fstream File(filename, ios::out | ios::binary);
	File.write (reinterpret_cast<char *>(header), sizeof (char)*18);
	File.write (reinterpret_cast<char *>(data), sizeof (char)*imageSize);
	File.close();

	delete[] data;
	data=NULL;
}

} //end of namescope