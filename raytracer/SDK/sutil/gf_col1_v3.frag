#version 150 core

in vec2 texCoord;

uniform sampler2D I1_sat;
uniform sampler2D I2_sat;
uniform sampler2D I3_sat;
uniform sampler2D p_sat;
uniform sampler2D Ip_1_sat;
uniform sampler2D Ip_2_sat;
uniform sampler2D Ip_3_sat;
uniform sampler2D Ip_4_sat;
uniform sampler2D Ip_5_sat;
uniform sampler2D Ip_6_sat;
uniform sampler2D Ip_7_sat;
uniform sampler2D Ip_8_sat;
uniform sampler2D Ip_9_sat;
uniform sampler2D mat1_sat;
uniform sampler2D mat2_sat;
uniform sampler2D mat3_sat;
uniform sampler2D mat4_sat; 
uniform sampler2D mat5_sat;
uniform sampler2D mat6_sat;
uniform sampler2D mat7_sat;
uniform sampler2D mat8_sat;
uniform sampler2D mat9_sat;
uniform sampler2D mat10_sat; 
uniform sampler2D mat11_sat;
uniform sampler2D mat12_sat;
uniform sampler2D boxsize;

uniform int fltRadius;
uniform float epsilon;
uniform int width;
uniform int height;

vec4 getRGBat(sampler2D img, int i, int j)
{
	vec4 texVec = texture2D(img, vec2(texCoord.x + float(i)/float(width),texCoord.y + float(j)/float(height))).xyzw;
	return texVec;
}

vec4 boxfilter(sampler2D sat, int fltRadius)
{

	float boxSize = getRGBat(boxsize,0,0).x;

	vec4 upRight	= getRGBat(sat,	fltRadius,		fltRadius);
	vec4 upLeft		= getRGBat(sat, -(fltRadius+1),	fltRadius);
	vec4 lowRight	= getRGBat(sat, fltRadius,		-(fltRadius+1));
	vec4 lowLeft	= getRGBat(sat, -(fltRadius+1), -(fltRadius+1));

	vec4 mean = upRight - upLeft - lowRight + lowLeft;
	
	mean /= boxSize;

	return mean;
}


float[81] invert_add_eps(float[81] data, float epsilon)  {
   
   int dim = 9;

   // add epsilon
	for (int i = 0; i<dim; i++){
		data[i*dim+i] += epsilon;
	}

    for (int i=1; i < dim; i++) data[i] /= data[0]; // normalize row 0
	for (int i=1; i < dim; i++)  { 
		for (int j=i; j < dim; j++)  { // do a column of L
			float sum = 0.0;
			for (int k = 0; k < i; k++)  
				sum += data[j*dim+k] * data[k*dim+i];
			data[j*dim+i] -= sum;
		}
		if (i == dim-1) continue;
		for (int j=i+1; j < dim; j++)  {  // do a row of U
			float sum = 0.0;
			for (int k = 0; k < i; k++)
				sum += data[i*dim+k]*data[k*dim+j];
			data[i*dim+j] = (data[i*dim+j]-sum) / data[i*dim+i];
		}
    }

    for ( int i = 0; i < dim; i++ )  // invert L
      for ( int j = i; j < dim; j++ )  {
        float x = 1.0;
        if ( i != j ) {
          x = 0.0;
          for ( int k = i; k < j; k++ ) 
              x -= data[j*dim+k]*data[k*dim+i];
          }
        data[j*dim+i] = x / data[j*dim+j];
        }
    for ( int i = 0; i < dim; i++ )   // invert U
      for ( int j = i; j < dim; j++ )  {
        if ( i == j ) continue;
        float sum = 0.0;
        for ( int k = i; k < j; k++ )
            sum += data[k*dim+j]*( (i==k) ? 1.0 : data[i*dim+k] );
        data[i*dim+j] = -sum;
        }
    for ( int i = 0; i < dim; i++ )   // final inversion
      for ( int j = 0; j < dim; j++ )  {
        float sum = 0.0;
        for ( int k = ((i>j)?i:j); k < dim; k++ )  
            sum += ((j==k)?1.0:data[j*dim+k])*data[k*dim+i];
        data[j*dim+i] = sum;
        }

	return data;
}

float[9] matMult(float[81] mat, float[9] mult_vec){
	float[9] result_vec = float[9](0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f);

	for (int j = 0; j < 9; j++){	// j says in which row we are
		for (int i = 0; i < 9; i++){ // i says in which column we are
			result_vec[j] += mat[i + j*9] * mult_vec[i];
		}
	}

	return result_vec;
}

void main()
{	
	vec3 mean_I1 = boxfilter(I1_sat, fltRadius).xyz;
	vec3 mean_I2 = boxfilter(I2_sat, fltRadius).xyz;
	vec3 mean_I3 = boxfilter(I3_sat, fltRadius).xyz;
	float[9] mean_I = float[9](mean_I1.x,mean_I1.y,mean_I1.z, mean_I2.x, mean_I2.y, mean_I2.z, mean_I3.x, mean_I3.y, mean_I3.z);

	vec3 mean_p = boxfilter(p_sat, fltRadius).xyz;

	vec3 mean_Ip_1 = boxfilter(Ip_1_sat,fltRadius).xyz;
	vec3 mean_Ip_2 = boxfilter(Ip_2_sat,fltRadius).xyz;
	vec3 mean_Ip_3 = boxfilter(Ip_3_sat,fltRadius).xyz;
	vec3 mean_Ip_4 = boxfilter(Ip_4_sat,fltRadius).xyz;
	vec3 mean_Ip_5 = boxfilter(Ip_5_sat,fltRadius).xyz;
	vec3 mean_Ip_6 = boxfilter(Ip_6_sat,fltRadius).xyz;
	vec3 mean_Ip_7 = boxfilter(Ip_7_sat,fltRadius).xyz;
	vec3 mean_Ip_8 = boxfilter(Ip_8_sat,fltRadius).xyz;
	vec3 mean_Ip_9 = boxfilter(Ip_9_sat,fltRadius).xyz;
	

	vec3 cov_Ip_1 = mean_Ip_1 - mean_I[0] * mean_p.xyz;
	vec3 cov_Ip_2 = mean_Ip_2 - mean_I[1] * mean_p.xyz;
	vec3 cov_Ip_3 = mean_Ip_3 - mean_I[2] * mean_p.xyz;
	vec3 cov_Ip_4 = mean_Ip_4 - mean_I[3] * mean_p.xyz;
	vec3 cov_Ip_5 = mean_Ip_5 - mean_I[4] * mean_p.xyz;
	vec3 cov_Ip_6 = mean_Ip_6 - mean_I[5] * mean_p.xyz;
	vec3 cov_Ip_7 = mean_Ip_7 - mean_I[6] * mean_p.xyz;
	vec3 cov_Ip_8 = mean_Ip_8 - mean_I[7] * mean_p.xyz;
	vec3 cov_Ip_9 = mean_Ip_9 - mean_I[8] * mean_p.xyz;
	
	float sigma[81];

	//  sigma: variations of channels 1 to 6.
	//	[var11]	[var12]	[var13]	[var14]	[var15]	[var16]	var[17] var[18] var[19] (0-8)
	//	symm	[var22]	[var23]	[var24]	[var25]	[var26]	var[27] var[28] var[29] (9-17)
	//	symm	symm	[var33]	[var34]	[var35]	[var36]	var[37] var[38] var[39] (18-26)
	//	symm	symm	symm	[var44]	[var45]	[var46]	var[47] var[48] var[49] (27-35)
	//	symm	symm	symm	symm	[var55]	[var56]	var[57] var[58] var[59] (36-44)
	//	symm	symm	symm	symm	symm	[var66] var[67] var[68] var[69] (45-53)
	//	symm	symm	symm	symm	symm	symm	var[77] var[78] var[79] (54-62)
	//  symm	symm	symm	symm	symm	symm	symm	var[88] var[89] (63-71)
	//	symm	symm	symm	symm	symm	symm	symm	symm	var[99] (72-80)

	vec4 tmp = boxfilter(mat1_sat, fltRadius);
	sigma[0] = tmp.x - pow(mean_I[0],2.f);
	sigma[1] = tmp.y - mean_I[0]*mean_I[1];
	sigma[2] = tmp.z - mean_I[0]*mean_I[2];
	sigma[3] = tmp.w - mean_I[0]*mean_I[3];
	
	tmp = boxfilter(mat2_sat, fltRadius);
	sigma[4] = tmp.x - mean_I[0]*mean_I[4];
	sigma[5] = tmp.y - mean_I[0]*mean_I[5];
	sigma[6] = tmp.z - mean_I[0]*mean_I[6];
	sigma[7] = tmp.w - mean_I[0]*mean_I[7];
	
	
	tmp = boxfilter(mat3_sat, fltRadius);
	sigma[8] = tmp.x - mean_I[0]*mean_I[8];
	sigma[9] = sigma[1];
	sigma[10] = tmp.y - mean_I[1]*mean_I[1];
	sigma[11] = tmp.z - mean_I[1]*mean_I[2];
	sigma[12] = tmp.w - mean_I[1]*mean_I[3];

	tmp = boxfilter(mat4_sat, fltRadius);
	sigma[13] = tmp.x - mean_I[1]*mean_I[4];
	sigma[14] = tmp.y - mean_I[1]*mean_I[5];
	sigma[15] = tmp.z - mean_I[1]*mean_I[6];
	sigma[16] = tmp.w - mean_I[1]*mean_I[7];

	tmp = boxfilter(mat5_sat, fltRadius);
	sigma[17] = tmp.x - mean_I[1]*mean_I[8];
	sigma[18] = sigma[2];
	sigma[19] = sigma[11];
	sigma[20] = tmp.y - mean_I[2]*mean_I[2];
	sigma[21] = tmp.z - mean_I[2]*mean_I[3];
	sigma[22] = tmp.w - mean_I[2]*mean_I[4];

	tmp = boxfilter(mat6_sat, fltRadius);
	sigma[23] = tmp.x - mean_I[2]*mean_I[5];
	sigma[24] = tmp.y - mean_I[2]*mean_I[6];
	sigma[25] = tmp.z - mean_I[2]*mean_I[7];
	sigma[26] = tmp.w - mean_I[2]*mean_I[8];

	tmp = boxfilter(mat7_sat, fltRadius);
	sigma[27] = sigma[3];
	sigma[28] = sigma[12];
	sigma[29] = sigma[21];
	sigma[30] = tmp.x - mean_I[3]*mean_I[3];
	sigma[31] = tmp.y - mean_I[3]*mean_I[4];
	sigma[32] = tmp.z - mean_I[3]*mean_I[5];
	sigma[33] = tmp.w - mean_I[3]*mean_I[6];

	tmp = boxfilter(mat8_sat, fltRadius);
	sigma[34] = tmp.x - mean_I[3]*mean_I[7];
	sigma[35] = tmp.y - mean_I[3]*mean_I[8];
	sigma[36] = sigma[4];
	sigma[37] = sigma[13];
	sigma[38] = sigma[22];
	sigma[39] = sigma[31];
	sigma[40] = tmp.z - mean_I[4]*mean_I[4];
	sigma[41] = tmp.w - mean_I[4]*mean_I[5];


	tmp = boxfilter(mat9_sat, fltRadius);
	sigma[42] = tmp.x - mean_I[4]*mean_I[6];
	sigma[43] = tmp.y - mean_I[4]*mean_I[7];
	sigma[44] = tmp.z - mean_I[4]*mean_I[8];
	sigma[45] = sigma[5];
	sigma[46] = sigma[14];
	sigma[47] = sigma[23];
	sigma[48] = sigma[32];
	sigma[49] = sigma[41];
	sigma[50] = tmp.w -mean_I[5]*mean_I[5];

	tmp = boxfilter(mat10_sat, fltRadius);
	sigma[51] = tmp.x - mean_I[5]*mean_I[6];
	sigma[52] = tmp.y - mean_I[5]*mean_I[7];
	sigma[53] = tmp.z - mean_I[5]*mean_I[8];
	sigma[54] = sigma[6];
	sigma[55] = sigma[15];
	sigma[56] = sigma[24];
	sigma[57] = sigma[33];
	sigma[58] = sigma[42];
	sigma[59] = sigma[51];
	sigma[60] = tmp.w - mean_I[6]*mean_I[6];

	tmp = boxfilter(mat11_sat, fltRadius);
	sigma[61] = tmp.x - mean_I[6]*mean_I[7];
	sigma[62] = tmp.y - mean_I[6]*mean_I[8];
	sigma[63] = sigma[7];
	sigma[64] = sigma[16];
	sigma[65] = sigma[25];
	sigma[66] = sigma[34];
	sigma[67] = sigma[43];
	sigma[68] = sigma[52];
	sigma[69] = sigma[61];
	sigma[70] = tmp.z - mean_I[7]*mean_I[7];
	sigma[71] = tmp.w - mean_I[7]*mean_I[8];

	tmp = boxfilter(mat12_sat, fltRadius);
	sigma[72] = sigma[8];
	sigma[73] = sigma[17];
	sigma[74] = sigma[26];
	sigma[75] = sigma[35];
	sigma[76] = sigma[44];
	sigma[77] = sigma[53];
	sigma[78] = sigma[62];
	sigma[79] = sigma[71];
	sigma[80] = tmp.x - mean_I[8]*mean_I[8];


	float inverse[81] = invert_add_eps(sigma, epsilon);

	float[9] cov_Ip_input_r = float[9](cov_Ip_1.x, cov_Ip_2.x, cov_Ip_3.x, cov_Ip_4.x, cov_Ip_5.x, cov_Ip_6.x, cov_Ip_7.x, cov_Ip_8.x, cov_Ip_9.x);
	float[9] cov_Ip_input_g = float[9](cov_Ip_1.y, cov_Ip_2.y, cov_Ip_3.y, cov_Ip_4.y, cov_Ip_5.y, cov_Ip_6.y, cov_Ip_7.y, cov_Ip_8.y, cov_Ip_9.y);
	float[9] cov_Ip_input_b = float[9](cov_Ip_1.z, cov_Ip_2.z, cov_Ip_3.z, cov_Ip_4.z, cov_Ip_5.z, cov_Ip_6.z, cov_Ip_7.z, cov_Ip_8.z, cov_Ip_9.z);

	float[9] a_r = matMult(inverse, cov_Ip_input_r);
	float[9] a_g = matMult(inverse, cov_Ip_input_g);
	float[9] a_b = matMult(inverse, cov_Ip_input_b);

	float b_r = mean_p.x - a_r[0]*mean_I[0] - a_r[1]*mean_I[1] - a_r[2]*mean_I[2]- a_r[3]*mean_I[3] - a_r[4]*mean_I[4] - a_r[5]*mean_I[5] - a_r[6]*mean_I[6] - a_r[7]*mean_I[7] - a_r[8]*mean_I[8];
	float b_g = mean_p.y - a_g[0]*mean_I[0] - a_g[1]*mean_I[1] - a_g[2]*mean_I[2]- a_g[3]*mean_I[3] - a_g[4]*mean_I[4] - a_g[5]*mean_I[5] - a_g[6]*mean_I[6] - a_g[7]*mean_I[7] - a_g[8]*mean_I[8];
	float b_b = mean_p.z - a_b[0]*mean_I[0] - a_b[1]*mean_I[1] - a_b[2]*mean_I[2]- a_b[3]*mean_I[3] - a_b[4]*mean_I[4] - a_b[5]*mean_I[5] - a_b[6]*mean_I[6] - a_b[7]*mean_I[7] - a_b[8]*mean_I[8];

	vec3 b = vec3(b_r,b_g,b_b);

	gl_FragData[0].xyzw = vec4(a_r[0],a_r[1],a_r[2],a_r[3]);
	gl_FragData[1].xyzw = vec4(a_r[4],a_r[5],a_r[6],a_r[7]);
	gl_FragData[2].xyzw = vec4(a_g[0],a_g[1],a_g[2],a_g[3]);
	gl_FragData[3].xyzw = vec4(a_g[4],a_g[5],a_g[6],a_g[7]);
	gl_FragData[4].xyzw = vec4(a_b[0],a_b[1],a_b[2],a_b[3]);
	gl_FragData[5].xyzw = vec4(a_b[4],a_b[5],a_b[6],a_b[7]);
	gl_FragData[6].xyz = vec3(a_r[8],a_g[8],a_b[8]);
	gl_FragData[7].xyz = b;
}