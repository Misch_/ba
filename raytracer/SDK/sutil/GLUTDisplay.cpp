#pragma once
/*
 * Copyright (c) 2008 - 2009 NVIDIA Corporation.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property and proprietary
 * rights in and to this software, related documentation and any modifications thereto.
 * Any use, reproduction, disclosure or distribution of this software and related
 * documentation without an express license agreement from NVIDIA Corporation is strictly
 * prohibited.
 *
 * TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THIS SOFTWARE IS PROVIDED *AS IS*
 * AND NVIDIA AND ITS SUPPLIERS DISCLAIM ALL WARRANTIES, EITHER EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL NVIDIA OR ITS SUPPLIERS BE LIABLE FOR ANY
 * SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT
 * LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF
 * BUSINESS INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR
 * INABILITY TO USE THIS SOFTWARE, EVEN IF NVIDIA HAS BEEN ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGES
 */

#include <fstream>

#if defined(__APPLE__)
#  include <GLUT/glut.h>
#  define GL_FRAMEBUFFER_SRGB_EXT           0x8DB9
#  define GL_FRAMEBUFFER_SRGB_CAPABLE_EXT   0x8DBA
#else
#  include <GL/glew.h>
#  if defined(_WIN32)
#    include <GL/wglew.h>
#  endif
#  include <GL/glut.h>
#endif

#include "GLUTDisplay.h"
#include <Mouse.h>
#include <DeviceMemoryLogger.h>


#include <optixu/optixu_math_stream_namespace.h>

#include <iostream>
#include <cstdlib>
#include <cstdio> //sprintf
#include <sstream>
#include <string>
#include "gui.h"
 //until I know how to add libraries to cmake, adding AntTweakBar.lib to the linker input is necessary 
 //after each rebuild by cmake (totally gay shit)

#include "shader.h"
#include "vertexData.h"
#include <stdarg.h>
#include "Host_Constants.h"
#include "ScreenImage.h"
#include "ShaderTests.h"
#include <winnt.h>
#include <winbase.h>
#include <wtypes.h>

using namespace optix;

//-----------------------------------------------------------------------------
// 
// GLUTDisplay class implementation 
//-----------------------------------------------------------------------------
bool old_ic = false; 
const int	MAX_HEIGHT	= 1080;
const int	MAX_WIDTH	= 1920;

std::string _path;
TwBar*		   GLUTDisplay::menuBar				  = 0;
Mouse*         GLUTDisplay::_mouse                = 0;
PinholeCamera* GLUTDisplay::_camera               = 0;
SampleScene*   GLUTDisplay::_scene                = 0;

unsigned int	GLUTDisplay::_mode					= MODE_SHOW_IMAGE;
unsigned int	GLUTDisplay::_vao					= 0;
unsigned int	GLUTDisplay::_vertSize				= 0;


unsigned int	GLUTDisplay::_tex_input_mean		= 0;
unsigned int	GLUTDisplay::_tex_input_var			= 0;
unsigned int	GLUTDisplay::_tex_input_ns			= 0;
unsigned int	GLUTDisplay::_tex_input_normal		= 0;
unsigned int	GLUTDisplay::_tex_input_brdf		= 0;
unsigned int	GLUTDisplay::_tex_input_position	= 0;

int				GLUTDisplay::_width					= 512;
int				GLUTDisplay::_height				= 512;

int				GLUTDisplay::_log2n					= 9;

double         GLUTDisplay::_last_frame_time      = 0.0;
unsigned int   GLUTDisplay::_last_frame_count     = 0;
unsigned int   GLUTDisplay::_frame_count          = 0;

bool           GLUTDisplay::_display_fps          = true;
double         GLUTDisplay::_fps_update_threshold = 0.5;
char           GLUTDisplay::_fps_text[32];
float3         GLUTDisplay::_text_color           = make_float3( 0.95f );
float3         GLUTDisplay::_text_shadow_color    = make_float3( 0.10f );

bool           GLUTDisplay::_print_mem_usage      = false;

GLUTDisplay::contDraw_E GLUTDisplay::_app_continuous_mode = CDNone;
GLUTDisplay::contDraw_E GLUTDisplay::_cur_continuous_mode = CDNone;

bool           GLUTDisplay::_display_frames       = true;
bool           GLUTDisplay::_save_frames_to_file  = false;
std::string    GLUTDisplay::_save_frames_basename = "";

std::string    GLUTDisplay::_camera_pose          = "";

int            GLUTDisplay::_initial_window_width  = -1;
int            GLUTDisplay::_initial_window_height = -1;

int            GLUTDisplay::_old_window_height    = -1;
int            GLUTDisplay::_old_window_width     = -1;
int            GLUTDisplay::_old_window_x         = -1;
int            GLUTDisplay::_old_window_y         = -1;


bool           GLUTDisplay::_sRGB_supported       = false;
bool           GLUTDisplay::_use_sRGB             = false;

bool           GLUTDisplay::_initialized          = false;
bool           GLUTDisplay::_benchmark_no_display = false;
unsigned int   GLUTDisplay::_warmup_frames        = 50u;
unsigned int   GLUTDisplay::_timed_frames         = 100u;
double         GLUTDisplay::_warmup_start         = 0;
double         GLUTDisplay::_warmup_time          = 10.0;
double         GLUTDisplay::_benchmark_time       = 10.0;
unsigned int   GLUTDisplay::_benchmark_frame_start= 0;
double         GLUTDisplay::_benchmark_frame_time = 0;
std::string    GLUTDisplay::_title                = "";

double         GLUTDisplay::_progressive_timeout   = -1.;
double         GLUTDisplay::_start_time           = 0.0;

int            GLUTDisplay::_num_devices          = 0;

Denoiser	*GLUTDisplay::_denoiser;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ???
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void removeArg( int& i, int& argc, char** argv ) 
{
  char* disappearing_arg = argv[i];
  for(int j = i; j < argc-1; ++j) {
    argv[j] = argv[j+1];
  }
  argv[argc-1] = disappearing_arg;
  --argc;
  --i;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Print usage!
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::printUsage()
{
  std::cerr
    << "Standard options:\n"
    << "  -d  | --dim=<width>x<height>               Set image dimensions\n"
    << "  -D  | --num-devices=<num_devices>          Set desired number of GPUs\n"
    << "  -p  | --pose=\"[<eye>][<lookat>][<up>]vfov\" Camera pose, e.g. --pose=\"[0,0,-1][0,0,0][0,1,0]45.0\"\n"
    << "  -s  | --save-frames[=<file_basename>]      Save each frame to frame_XXXX.ppm or file_basename_XXXX.ppm\n"
    << "  -N  | --no-display                         Don't display the image to the GLUT window\n"
    << "  -M  | --mem-usage                          Print memory usage after every frame\n"
    << "  -b  | --benchmark[=<w>x<t>]                Render and display 'w' warmup and 't' timing frames, then exit\n"
    << "  -bb | --timed-benchmark=<w>x<t>            Render and display 'w' warmup and 't' timing seconds, then exit\n"
    << "  -B  | --benchmark-no-display=<w>x<t>       Render 'w' warmup and 't' timing frames, then exit\n"
    << "  -BB | --timed-benchmark-no-display=<w>x<t> Render 'w' warmup and 't' timing seconds, then exit\n"
    << std::endl;

  std::cerr
    << "Standard mouse interaction:\n"
    << "  left mouse           Camera Rotate/Orbit (when interactive)\n"
    << "  middle mouse         Camera Pan/Truck (when interactive)\n"
    << "  right mouse          Camera Dolly (when interactive)\n"
    << "  right mouse + shift  Camera FOV (when interactive)\n"
    << std::endl;

  std::cerr
    << "Standard keystrokes:\n"
    << "  q Quit\n"
    << "  f Toggle full screen\n"
    << "  r Toggle continuous mode (progressive refinement, animation, or benchmark)\n"
    << "  R Set progressive refinement to never timeout and toggle continuous mode\n"
    << "  b Start/stop a benchmark\n"
    << "  d Toggle frame rate display\n"
    << "  s Save a frame to 'out.ppm'\n"
    << "  m Toggle memory usage printing\n"
    << "  c Print camera pose\n"
    << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Init!
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::init( int& argc, char** argv )
{
	//get the actual complete path
	std::string path(argv[0]);
	_path = path.substr(0,(path.substr(0,(path.substr(0, (path.substr(0, path.find_last_of( '\\' ))).find_last_of( '\\'))).find_last_of( '\\'))).find_last_of('\\')+1); //lovely :)
  
	_initialized = true;
  for (int i = 1; i < argc; ++i ) {
    std::string arg( argv[i] );
    if ( arg == "-s" || arg == "--save-frames" ) {
      _save_frames_to_file = true;
      removeArg( i, argc, argv );
    } else if ( arg.substr( 0, 3 ) == "-s=" || arg.substr( 0, 14 )  == "--save-frames=" ) {
      _save_frames_to_file = true;
      _save_frames_basename = arg.substr( arg.find_first_of( '=' ) + 1 );
      removeArg( i, argc, argv );
    } else if ( arg == "-N" || arg == "--no-display" ) {
      _display_frames = false;
      removeArg( i, argc, argv );
    } else if ( arg == "-M" || arg == "--mem-usage" ) {
      _print_mem_usage = true;
      removeArg( i, argc, argv );
    } else if ( arg.substr( 0, 3 ) == "-p=" || arg.substr( 0, 7 ) == "--pose=" ) {
      _camera_pose = arg.substr( arg.find_first_of( '=' ) + 1 );
      std::cerr << " got <<" << _camera_pose << ">>" << std::endl;
      removeArg( i, argc, argv );
    } else if( arg.substr( 0, 3) == "-D=" || arg.substr( 0, 14 ) == "--num-devices=" ) {
      std::string dims_arg = arg.substr( arg.find_first_of( '=' ) + 1 );
      _num_devices = atoi(dims_arg.c_str());
      if ( _num_devices < 1 ) {
        std::cerr << "Invalid num devices: '" << dims_arg << "'" << std::endl;
        printUsage();
        quit(1);
      }
      removeArg( i, argc, argv );
    } else if( arg.substr( 0, 3) == "-d=" || arg.substr( 0, 6 ) == "--dim=" ) {
      std::string dims_arg = arg.substr( arg.find_first_of( '=' ) + 1 );
      unsigned int width, height;
      if ( sutilParseImageDimensions( dims_arg.c_str(), &width, &height ) != RT_SUCCESS ) {
        std::cerr << "Invalid window dimensions: '" << dims_arg << "'" << std::endl;
        printUsage();
        quit(1);
      }
      _initial_window_width = width;
      _initial_window_height = height;
      removeArg( i, argc, argv );
    } else if ( arg == "-b" || arg == "--benchmark" ) {
      _app_continuous_mode = CDBenchmark;
      removeArg( i, argc, argv );
    } else if ( arg == "-B" || arg == "--benchmark-no-display" ) {
      _app_continuous_mode = CDBenchmark;
      _benchmark_no_display = true;
      removeArg( i, argc, argv );
    } else if ( arg == "-bb" || arg == "--timed-benchmark" ) {
      _app_continuous_mode = CDBenchmarkTimed;
      removeArg( i, argc, argv );
    } else if ( arg == "-BB" || arg == "--timed-benchmark-no-display" ) {
      _app_continuous_mode = CDBenchmarkTimed;
      _benchmark_no_display = true;
      removeArg( i, argc, argv );
    } else if ( arg.substr( 0, 3 ) == "-b=" || arg.substr( 0, 12 ) == "--benchmark=" ) {
      _app_continuous_mode = CDBenchmark;
      std::string bnd_args = arg.substr( arg.find_first_of( '=' ) + 1 );
      if ( sutilParseImageDimensions( bnd_args.c_str(), &_warmup_frames, &_timed_frames ) != RT_SUCCESS ) {
        std::cerr << "Invalid --benchmark arguments: '" << bnd_args << "'" << std::endl;
        printUsage();
        quit(1);
      }
      removeArg( i, argc, argv );
    } else if ( arg.substr( 0, 3) == "-B=" || arg.substr( 0, 23 ) == "--benchmark-no-display=" ) {
      _app_continuous_mode = CDBenchmark;
      _benchmark_no_display = true;
      std::string bnd_args = arg.substr( arg.find_first_of( '=' ) + 1 );
      if ( sutilParseImageDimensions( bnd_args.c_str(), &_warmup_frames, &_timed_frames ) != RT_SUCCESS ) {
        std::cerr << "Invalid --benchmark-no-display arguments: '" << bnd_args << "'" << std::endl;
        printUsage();
        quit(1);
      }
      removeArg( i, argc, argv );
    } else if ( arg.substr( 0, 4) == "-bb=" || arg.substr( 0, 18 ) == "--timed-benchmark=" ) {
      _app_continuous_mode = CDBenchmarkTimed;
      std::string bnd_args = arg.substr( arg.find_first_of( '=' ) + 1 );
      if ( sutilParseFloatDimensions( bnd_args.c_str(), &_warmup_time, &_benchmark_time ) != RT_SUCCESS) {
        std::cerr << "Invalid --timed-benchmark (-bb) arguments: '" << bnd_args << "'" << std::endl;
        printUsage();
        quit(1);
      }
      removeArg( i, argc, argv );
    } else if ( arg.substr( 0, 4) == "-BB=" || arg.substr( 0, 29 ) == "--timed-benchmark-no-display=" ) {
      _app_continuous_mode = CDBenchmarkTimed;
      _benchmark_no_display = true;
      std::string bnd_args = arg.substr( arg.find_first_of( '=' ) + 1 );
      if ( sutilParseFloatDimensions( bnd_args.c_str(), &_warmup_time, &_benchmark_time ) != RT_SUCCESS ) {
        std::cerr << "Invalid --timed-benchmark-no-display (-BB) arguments: '" << bnd_args << "'" << std::endl;
        printUsage();
        quit(1);
      }
      removeArg( i, argc, argv );
    }
  }

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Run!
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::run( const std::string& title, SampleScene* scene, contDraw_E continuous_mode )
{
  if ( !_initialized ) {
    std::cerr << "ERROR - GLUTDisplay::run() called before GLUTDisplay::init()" << std::endl;
    exit(2);
  }
  _scene = scene;
  _title = "Test-Application";//title;
  _scene->setNumDevices( _num_devices );


  if( _print_mem_usage ) {
    DeviceMemoryLogger::logDeviceDescription(_scene->getContext(), std::cerr);
    DeviceMemoryLogger::logCurrentMemoryUsage(_scene->getContext(), std::cerr, "Initial memory available: " );
    std::cerr << std::endl;
  }

  // Initialize GLUT and GLEW first. Now initScene can use OpenGL and GLEW.
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowSize( 1, 1 );
  glutInitWindowPosition(100,100);
  glutCreateWindow( _title.c_str() );
  glutHideWindow();
#if !defined(__APPLE__)
  glewInit();
  if (glewIsSupported( "GL_EXT_texture_sRGB GL_EXT_framebuffer_sRGB")) {
    _sRGB_supported = true;
  }
#else
  _sRGB_supported = true;
#endif
#if defined(_WIN32)
  // Turn off vertical sync
  wglSwapIntervalEXT(0);
#endif

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  // If _app_continuous_mode was already set to CDBenchmark* on the command line then preserve it.
  setContinuousMode( _app_continuous_mode == CDNone ? continuous_mode : _app_continuous_mode );

  //@marco: own function to infuse own shader and textures...
  setUp();

  int buffer_width;
  int buffer_height;
  try {
    // Set up scene
    SampleScene::InitialCameraData camera_data;
	_scene->setPath(_path);
    _scene->initScene( camera_data, _width, _height );

    if( _initial_window_width > 0 && _initial_window_height > 0)
      _scene->resize( _initial_window_width, _initial_window_height );

    if ( !_camera_pose.empty() )
      camera_data = SampleScene::InitialCameraData( _camera_pose );

    // Initialize camera according to scene params
    _camera = new PinholeCamera( camera_data.eye,
                                 camera_data.lookat,
                                 camera_data.up,
                                 -1.0f, // hfov is ignored when using keep vertical
                                 camera_data.vfov,
                                 PinholeCamera::KeepVertical );

	Buffer buffer = _scene->getBuffer("output_buffer");
    RTsize buffer_width_rts, buffer_height_rts;
    buffer->getSize( buffer_width_rts, buffer_height_rts );
    buffer_width  = static_cast<int>(buffer_width_rts); 
    buffer_height = static_cast<int>(buffer_height_rts);
	_width = buffer_width;
	_height = buffer_height;
    _mouse = new Mouse( _camera, buffer_width, buffer_height );
  } catch( Exception& e ){
    sutilReportError( e.getErrorString().c_str() );
    exit(2);
  }

  // reshape window to the correct window resize
  glutReshapeWindow( buffer_width, buffer_height);

  // Initialize state
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, 1, 0, 1, -1, 1 );
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, buffer_width, buffer_height);

  glutShowWindow();

  // Set callbacks
  glutReshapeFunc(resize);
  glutDisplayFunc(display);

  // Initialize timer
  sutilCurrentTime( &_last_frame_time );
  _frame_count = 0;
  _last_frame_count = 0;
  _start_time = _last_frame_time;
  if( _cur_continuous_mode == CDBenchmarkTimed ) {
    _warmup_start = _last_frame_time;
    _warmup_frames = 0;
    _timed_frames = 0;
  }
  _benchmark_frame_start = 0;

  // Enter main loop
  glutMainLoop();
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Setup some stuff...
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



void GLUTDisplay::setUp(){

	//uncomment the denoiser you want to use... 
	
	//DenoiserGEM *den = new DenoiserGEM;
	DenoiserGF *den = new DenoiserGF;

	_denoiser = den;
	_denoiser->init( _scene->getFBOTexture(0), _width, _height);
	_denoiser->updateFilter();

	GUI::load();
	GUI::setDenoiserType(_denoiser->getDenoiserType());

	glutKeyboardFunc(keyPressed);
	glutMouseFunc(mouseButton);
	glutMotionFunc(mouseMotion);
	glutKeyboardUpFunc(keyUp);
	glutPassiveMotionFunc((GLUTmousemotionfun)TwEventMouseMotionGLUT);
	glutSpecialFunc((GLUTspecialfun)TwEventSpecialGLUT);
	TwGLUTModifiersFunc(glutGetModifiers);

	Shader::loadAllShaders(_path);

	glUseProgram(Shader::getShaderID(TRIVIAL));

	//make vertexdata (for simple quad)
	GLfloat vertices[] = {	// in vec3 vert;
		-1.f,  1.f, 0.f,	// xyz 
		-1.f, -1.f, 0.f, 
		1.f,  1.f, 0.f,
		1.f, -1.f, 0.f };
	_vertSize=sizeof(vertices)/sizeof(GLfloat)/3;

	float p = 1.f;
	float q = 1.f;
	GLfloat texData[] = {	// in vec2 tex
		0.f, 1.f*q,		// st
		0.f, 0.f,
		1.f*p, 1.f*q,
		1.f*p, 0.f };
	size_t texSize = sizeof(texData)/sizeof(GLfloat)/2;

	VertexData d;
	_vao = d.create(2);
	d.storeAttribute(0, vertices, 3, _vertSize, "vert");
	d.storeAttribute(1, texData, 2, texSize, "tex");
	d.bindToShader(Shader::getShaderID(TRIVIAL));
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SetCamera. (NEVER CALLED WTF? DEPRECATED!!!)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::setCamera(SampleScene::InitialCameraData& camera_data)
{
 _camera->setParameters(camera_data.eye,
                         camera_data.lookat,
                         camera_data.up,
                         camera_data.vfov, 
                         camera_data.vfov,
                         PinholeCamera::KeepVertical );
  glutPostRedisplay();  
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is an internal function that does the actual work.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::setCurContinuousMode(contDraw_E continuous_mode)
{
  _cur_continuous_mode = continuous_mode;

  sutilCurrentTime( &_start_time );
  glutIdleFunc( _cur_continuous_mode!=CDNone ? idle : 0 );
  glutPostRedisplay();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is an API function for the app to specify its desired mode.
// Changing progressive_timeout determines how long additional samples are shot...
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::setContinuousMode(contDraw_E continuous_mode)
{
  _app_continuous_mode = continuous_mode;

  // Unless the user has overridden it, progressive implies a finite continuous drawing timeout.
  if(_app_continuous_mode == CDProgressive && _progressive_timeout < 0.0) {
    _progressive_timeout = 100.0;
  }
  setCurContinuousMode(_app_continuous_mode);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Post Redisplay
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::postRedisplay()
{
  glutPostRedisplay();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Draw Text
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::drawText( const std::string& text, float x, float y, void* font )
{
  // Save state
  glPushAttrib( GL_CURRENT_BIT | GL_ENABLE_BIT );

  glDisable( GL_TEXTURE_2D );
  glDisable( GL_LIGHTING );
  glDisable( GL_DEPTH_TEST);

  glColor3fv( &( _text_shadow_color.x) ); // drop shadow
  // Shift shadow one pixel to the lower right.
  glWindowPos2f(x + 1.0f, y - 1.0f);
  for( std::string::const_iterator it = text.begin(); it != text.end(); ++it )
    glutBitmapCharacter( font, *it );

  glColor3fv( &( _text_color.x) );        // main text
  glWindowPos2f(x, y);
  for( std::string::const_iterator it = text.begin(); it != text.end(); ++it )
    glutBitmapCharacter( font, *it );

  // Restore state
  glPopAttrib();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Key handler
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::keyPressed(unsigned char key, int x, int y)
{
	float speed;
	//speed = 0.5f;
	speed = 0.05f;
	if(TwEventKeyboardGLUT(key, x, y))
		return;

  try {
    if( _scene->keyPressed(key, x, y) ) {
      glutPostRedisplay();
      return;
    }
  } catch( Exception& e ){
    sutilReportError( e.getErrorString().c_str() );
    exit(2);
  }

  ostringstream fileName;
	//fileName << _denoiser->epsilon;
	fileName << _denoiser->fltRadius;
	string fileNameString = fileName.str();
	fileNameString.append(".tga");

  switch (key) {
  case 27: // esc
  case 'q':
    quit();

  case 'f':
    if ( _old_window_width == -1) { // We are in non-fullscreen mode
      _old_window_width  = glutGet(GLUT_WINDOW_WIDTH);
      _old_window_height = glutGet(GLUT_WINDOW_HEIGHT);
      _old_window_x      = glutGet(GLUT_WINDOW_X);
      _old_window_y      = glutGet(GLUT_WINDOW_Y);
      glutFullScreen();
    } else { // We are in fullscreen mode
      glutPositionWindow( _old_window_x, _old_window_y );
      glutReshapeWindow( _old_window_width, _old_window_height );
      _old_window_width = _old_window_height = -1;
    }
    glutPostRedisplay();
    break;

  case '-':
	  sutilDisplayFilePPM( "out.ppm", _scene->getBuffer("output_buffer")->get() );
	  sutilDisplayFilePPM( "out_n.ppm", _scene->getBuffer("out_normal")->get());
	  GLHelper::screenshot("visualizedNormals.tga", 512,512, *_denoiser->getTex(NORMALS_NONNEGATIVE));
	  sutilDisplayFilePPM( "out_brdf.ppm", _scene->getBuffer("out_brdf")->get());
	  //sutilDisplayFilePPM( "out_p.ppm", _scene->getBuffer("out_position")->get());	// This yields the buffer with some defauls values at places where no object is. Sometimes it's white, sometimes black. Take therefore the POSITION_INTPUT-texture where these values are always 1.
	  GLHelper::screenshot("out_p.tga", 512,512, *_denoiser->getTex(POSITION_INPUT));
	  
	  
	  //GLHelper::screenshot((char*)fileNameString.c_str(),512,512, *_denoiser->getTex(GF_FILTERED));
	  GLHelper::screenshot("screenshot.tga",512,512, *_denoiser->getTex(GF_FILTERED));
	  GLHelper::screenshot("screenshot_gem.tga",512,512, *_denoiser->getTex(GL_TEXTURE0));
	  
    break;

  case 'm':
    _print_mem_usage =  !_print_mem_usage;
    glutPostRedisplay();
    break;

  case 'b':{
	  optix::float3 eye, look, up;
	  float fov1, fov2;
	  _camera->getEyeLookUpFOV(eye, look, up, fov1, fov2);
	  int i=0;
	  break;
		   }
	  //quake-like controls
  case 'w':
	  sutilCurrentTime( &_start_time );
	  _camera->translate(_camera->lookdir*speed);
	   _scene->setMovingMode(true);
	  _scene->signalCameraChanged();
	  glutPostRedisplay();
	  break;

  case 'a':
	  sutilCurrentTime( &_start_time );
	  _camera->translate(cross(_camera->up, _camera->lookdir)*speed);
	   _scene->setMovingMode(true);
	  _scene->signalCameraChanged();
	  glutPostRedisplay();
	  break;

  case 'd':
	  sutilCurrentTime( &_start_time );
	  _camera->translate(cross(_camera->lookdir, _camera->up)*speed);
	   _scene->setMovingMode(true);
	  _scene->signalCameraChanged();
	  glutPostRedisplay();
	  break;

  case 's':
	  sutilCurrentTime( &_start_time );
	  _scene->setMovingMode(true);
	  _camera->translate(-_camera->lookdir*speed);
	  _scene->signalCameraChanged();
	  glutPostRedisplay();
	  break;

  case 'c':
	  float3 eye, lookat, up;
	  float hfov, vfov;

	  _camera->getEyeLookUpFOV(eye, lookat, up, hfov, vfov);
	  std::cerr << '"' << eye << lookat << up << vfov << '"' << std::endl;
	  break;

   case 'v':
	  /* Print OpenGL version */
	  std::cerr << glGetString(GL_VERSION);
	  break;

  default:
    return;
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// just a little function triggered whenever a key is released...
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void GLUTDisplay::keyUp(unsigned char key, int x, int y){

	switch(key){
		case 'w':
			_scene->setMovingMode(false);
			break;
		case 'a':
			_scene->setMovingMode(false);
			break;
		case 's':
			_scene->setMovingMode(false);
			break;
		case 'd':
			_scene->setMovingMode(false);
			break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mouse button handler
// button is the mouse-button who changed state. 
// state is either GLUT_DOWN or GLUT_UP depending if the mouse-button was pressed or released.
// x, y are the relative window coordinates
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::mouseButton(int button, int state, int x, int y)
{
	if(TwEventMouseButtonGLUT(button, state, x, y)){
		_scene->setMovingMode(false);
		return;
	}

	if ( button==GLUT_LEFT_BUTTON && state==GLUT_DOWN)
		_scene->setMovingMode(true);
	if(button==GLUT_LEFT_BUTTON && state==GLUT_UP)
		_scene->setMovingMode(false);

  sutilCurrentTime( &_start_time );
  _mouse->handleMouseFunc( button, state, x, y, glutGetModifiers() );
  if ( state != GLUT_UP )
    _scene->signalCameraChanged();
  glutPostRedisplay();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mouse motion handler
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::mouseMotion(int x, int y)
{
	if(TwEventMouseMotionGLUT(x, y))
		return;

  sutilCurrentTime( &_start_time );
  _mouse->handleMoveFunc( x, y );
  _scene->signalCameraChanged();
  if (_app_continuous_mode == CDProgressive) {
    setCurContinuousMode(CDProgressive);
  }
  glutPostRedisplay();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Resize Function.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::resize(int width, int height)
{
  // disallow size 0
  width  = max(1, width);
  height = max(1, height);

  _width = width;
  _height = height;

  GUI::resize(_width, _height); //resize the antTweakBar

  //to resize the shared resources we need to unregister them from optix...
	_scene->unregisterSamplingMap(0);
	_denoiser->resize( _width,_height);
	_scene->registerSamplingMap(0);


  sutilCurrentTime( &_start_time );
  _scene->signalCameraChanged();
  _mouse->handleResize( width, height );

  try {
    _scene->resize(width, height);
  } catch( Exception& e ){
    sutilReportError( e.getErrorString().c_str() );
    exit(2);
  }

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, 1, 0, 1, -1, 1);
  glViewport(0, 0, width, height);
  if (_app_continuous_mode == CDProgressive) {
    setCurContinuousMode(CDProgressive);
  }
  glutPostRedisplay();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Idle Function
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::idle()
{
  glutPostRedisplay();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sets up a vertex buffer object containing the image of a optix output buffer.
// Then the stuff is written into the texture bound to 'texUnit'
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::setupVBO(optix::Buffer buffer, unsigned int& texId)
{ 
	RTsize buffer_width_rts, buffer_height_rts;
	buffer->getSize( buffer_width_rts, buffer_height_rts );
	int buffer_width  = static_cast<int>(buffer_width_rts);
	int buffer_height = static_cast<int>(buffer_height_rts);
	RTformat buffer_format = buffer->getFormat();

	unsigned int vboId = buffer->getGLBOId();

	//setting up tex
	if (!texId)
	{
		glGenTextures( 1, &texId );
		glBindTexture( GL_TEXTURE_2D, texId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture( GL_TEXTURE_2D, 0);
	}
	glBindTexture( GL_TEXTURE_2D, texId );

	//update unpack_buffer
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, vboId); 
	RTsize elementSize = buffer->getElementSize();
	if      ((elementSize % 8) == 0) glPixelStorei(GL_UNPACK_ALIGNMENT, 8);
	else if ((elementSize % 4) == 0) glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	else if ((elementSize % 2) == 0) glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
	else                             glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	if(buffer_format == RT_FORMAT_UNSIGNED_BYTE4) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, buffer_width, buffer_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	} else if(buffer_format == RT_FORMAT_FLOAT4) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, buffer_width, buffer_height, 0, GL_RGBA, GL_FLOAT, 0);
	} else if(buffer_format == RT_FORMAT_FLOAT3) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F_ARB, buffer_width, buffer_height, 0, GL_RGB, GL_FLOAT, 0);
	} else if(buffer_format == RT_FORMAT_FLOAT) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE32F_ARB, buffer_width, buffer_height, 0, GL_LUMINANCE, GL_FLOAT, 0);
	} else {
		assert(0 && "Unknown buffer format");
	}
	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, 0 );
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Display Function Helper, handles processing-logic of the openGL part of the program
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::displayFrame()
{
	//get actual size of buffers
	Buffer b = _scene->getBuffer("output_buffer");
	RTsize buffer_width_rts, buffer_height_rts;
	b->getSize( buffer_width_rts, buffer_height_rts );
	int buffer_width  = static_cast<int>(buffer_width_rts);
	int buffer_height = static_cast<int>(buffer_height_rts);

	setupVBO(_scene->getBuffer("output_buffer"), _tex_input_mean);
	setupVBO(_scene->getBuffer("output_var_buffer"), _tex_input_var);
	setupVBO(_scene->getBuffer("num_samples_buffer"), _tex_input_ns);
	setupVBO(_scene->getBuffer("out_normal"), _tex_input_normal);
	setupVBO(_scene->getBuffer("out_brdf"), _tex_input_brdf);
	setupVBO(_scene->getBuffer("out_position"), _tex_input_position);

	//call the vertex array object
	glBindVertexArray(_vao);

	//preview mode or not?
	if(_scene->CameraIsMoving())
	{
			ScreenImage::finalImageSimple(&_tex_input_mean,_width,_height);
	}
	else
	{			
		//the denoiser performs whatever denoising operation has to be done on the image data
		if(_denoiser->getDenoiserType()==DENOISER_GF)
			_denoiser->computeSamplingDistribution(&_tex_input_mean, &_tex_input_var, &_tex_input_ns, &_tex_input_normal, &_tex_input_brdf, &_tex_input_position, GUI::getGammaCorr(), GUI::getEpsilon(), GUI::getFltRadius());			
		else
			_denoiser->computeSamplingDistribution(&_tex_input_mean, &_tex_input_var, &_tex_input_ns, &_tex_input_normal, &_tex_input_brdf, &_tex_input_position, GUI::getGamma(), GUI::getEpsilon(), GUI::getFltRadius());			
		_denoiser->computeAvgNS(&_tex_input_ns);
		
		//put stuff on screen
		if(_denoiser->getDenoiserType()==DENOISER_GEM)
		ScreenImage::finalImageGEM(&_tex_input_mean, _denoiser->getTex(MEAN_FILTERED1), _denoiser->getTex(COSTMAP_STEPS),
			_denoiser->getTex(SCALEMIN_FILTERED), _denoiser->getTex(SCALEMIN_UNFILTERED),_width,_height);
		else if(_denoiser->getDenoiserType()==DENOISER_GF)
			ScreenImage::finalImageSimple(&_tex_input_mean,_width,_height);

		GUI::setTotalMSE(_denoiser->getTotalMSEReduct());

		//if debugging is enabled visualize using the debugger visualization
		if(GUI::isDebugEnabled()){
				ScreenImage::finalImageDebug(_denoiser->getTex(GUI::getDebugTexture()),_width,_height);
		}
	}

	//cleanup
	glBindVertexArray(0); 
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Display Function.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::display()
{

	glDisable( GL_DEPTH_TEST);

	//check if params of filters of the denoiser have changed, if so recreate them...
	if(GUI::filterChanged()){
		_denoiser->updateFilter();
		GUI::toggleFilterChanged();
	}

  try {
    // render the scene
    float3 eye, U, V, W;
    _camera->getEyeUVW( eye, U, V, W );
    // Don't be tempted to just start filling in the values outside of a constructor, 
    // because if you add a parameter it's easy to forget to add it here.
    SampleScene::RayGenCameraData camera_data( eye, U, V, W );
    _scene->trace( camera_data );

    // Always count rendered frames
    ++_frame_count;

    // Not strictly necessary
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    if( _display_frames ) {
      displayFrame();
    }
  } catch( Exception& e ){
    sutilReportError( e.getErrorString().c_str() );
    exit(2);
  }

  // Output fps 
  if ( _display_fps && _frame_count > 1 ) {
    double current_time;
    sutilCurrentTime( &current_time );
    double dt = current_time - _last_frame_time;
    if( dt > _fps_update_threshold ) {
      sprintf( _fps_text, "fps: %7.2f", (_frame_count - _last_frame_count) / dt );
      _last_frame_time = current_time;
      _last_frame_count = _frame_count;
    } else if( _frame_count == 1 ) {
      sprintf( _fps_text, "fps: %7.2f", 0.f );
    }
    drawText( _fps_text, 10.0f, 10.0f, GLUT_BITMAP_8_BY_13 );
  }

  // Output memory
  if( _print_mem_usage ) {
    std::ostringstream str;
    DeviceMemoryLogger::logCurrentMemoryUsage(_scene->getContext(), str);
    drawText( str.str(), 10.0f, 26.0f, GLUT_BITMAP_8_BY_13 );
  }


  GLHelper::errorCheck();

  //draw GUI using AntTweakBar
  GUI::draw();
  GLHelper::errorCheck();

  // Swap buffers
  glutSwapBuffers();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Quit Function, performs some clean-up 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GLUTDisplay::quit(int return_code)
{
		
  delete _denoiser;
  try {
    if(_scene)
    {
      _scene->cleanUp();
      if (_scene->getContext().get() != 0)
      {
        sutilReportError( "Derived scene class failed to call SampleScene::cleanUp()" );
        exit(2);
      }
    }
    exit(return_code);
  } catch( Exception& e ) {
    sutilReportError( e.getErrorString().c_str() );
    exit(2);
  }
}
