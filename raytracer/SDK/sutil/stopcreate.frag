#version 150 core

in vec2 texCoord;
 
//uniform variables
uniform float gamma_z;
uniform float scaleFactor;

//uniform sampler2D scene_gradient_base;
//uniform sampler2D scene_gradient;

uniform sampler2D basemean;
uniform sampler2D basevar;

uniform sampler2D mean0;
uniform sampler2D mean1;
uniform sampler2D mean2;
uniform sampler2D mean3;
uniform sampler2D var0;
uniform sampler2D basens;

uniform sampler2D ns;
uniform samplerBuffer nsNorm;

//global vaiables
//float beta = 1.666667f;
float b_offset = 0.01f;
vec3 illum = vec3(0.299f, 0.587f, 0.114f); 
float NS_LIMIT = 16;
float NS_MIN = 32;


//vec3 illum = vec3(0.3333333f, 0.3333333f, 0.3333333f);

//function declarations
void computeMSEdiff(out float selectorMSE, out float MSE, in float fine, in float coarse, 
					in float fineVar, in float coarseVar, in float sgF, in float sgC, in float weight, in float scale);

void estimateVarAndMean(inout float[5] var, inout float[5] mean, in float validity);

void findQuadraticFunc(out vec3 coef, in float y0, in float y1, in float y2, in float x0, in float x1, in float x2);
/*
*	Be carefull with the bias term beta*(c-f)^2
*	beta = ( rfsqrt + rcsqrt ) / ( rfsqrt - rcsqrt )
*/
void main()
{	
	float diffMSE[4], totalMSE[4], weight;
	
	float sgrad_base = 0;//texture2D(scene_gradient_base, texCoord).x;
	vec4 sgrad = vec4(0,0,0,0);//texture2D(scene_gradient, texCoord);
	
	float sg[5] = float[](sgrad_base, sgrad.x, sgrad.y, sgrad.z, sgrad.w);
	
	//compute weighting for the bias-term
	float base_nSamples = texture2D(basens, texCoord).x;
	vec4 nSamples = texture2D(ns, texCoord);

	//texture fetches, we only need the illuminance of each pixel
	float meanImg = dot(illum,texture2D(basemean, texCoord).xyz);
	float m0 = dot(illum,texture2D(mean0, texCoord).xyz);
	float m1 = dot(illum,texture2D(mean1, texCoord).xyz);
	float m2 = dot(illum,texture2D(mean2, texCoord).xyz);
	float m3 = dot(illum,texture2D(mean3, texCoord).xyz);
	float mean[5] = float[](meanImg, m0, m1, m2, m3);
	
	//get variance-map, each channel contains a different scalevariance the basevar needs to be divided by the num of smaples
	//for the other scales this has already be done in the filtering
	float varImg = texture2D(basevar, texCoord).x/base_nSamples;
	vec4 v = texture2D(var0, texCoord);
	float var[5] = float[](varImg, v.x, v.y, v.z, v.w);
	
	//this arra specifies which variance is considered as valid.
	//if a variance is zero we assume it is not valid and compute the variance according to coarser scales 
	float validity = 0;
	validity += var[0]>0 || base_nSamples > NS_LIMIT ? 1: 0;
	validity += var[1]>0 || nSamples.x > NS_LIMIT ? 1: 0;
	validity += var[2]>0 || nSamples.y > NS_LIMIT ? 1: 0;	
	validity += var[3]>0 || nSamples.z > NS_LIMIT ? 1: 0;	
	
	estimateVarAndMean(var, mean, validity);
	
	
	//correctionfactors if the Variance is estimated with data from a higher scale
	
	
	weight = gamma_z * (1.f - 1.f/(nSamples.x/texelFetch(nsNorm,0).x));
	//for the first scale we dont trust the variance data, so estimate it based on the second scale!
	//float var_zeroscale = scaleFactor * scaleFactor * var[1];
	computeMSEdiff(diffMSE[0], totalMSE[0], mean[0], mean[1], var[0]/*_zeroscale*/, var[1], sg[0], sg[1], weight, 1.0f);
	
	weight = gamma_z * (1.f - 1.f/(nSamples.y/texelFetch(nsNorm,1).x));
	computeMSEdiff(diffMSE[1], totalMSE[1], mean[1], mean[2], var[1], var[2], sg[1], sg[2], weight, scaleFactor);
	
	weight = gamma_z * (1.f - 1.f/(nSamples.z/texelFetch(nsNorm,2).x));	
	computeMSEdiff(diffMSE[2], totalMSE[2], mean[2], mean[3], var[2], var[3], sg[2], sg[3], weight, scaleFactor);	
	
	weight = gamma_z * (1.f - 1.f/(nSamples.w/texelFetch(nsNorm,3).x));	
	computeMSEdiff(diffMSE[3], totalMSE[3], mean[3], mean[4], var[3], var[4], sg[3], sg[4], weight, scaleFactor); 
	
		
	gl_FragData[0] = vec4(diffMSE[0], diffMSE[1], diffMSE[2], diffMSE[3]);
	
	gl_FragData[1] = vec4(totalMSE[0], totalMSE[1], totalMSE[2], totalMSE[3]);
	
	//compute and store the brightness for later...
	gl_FragData[2].x = mean[0];
	gl_FragData[2].x *= gl_FragData[2].x; 
	gl_FragData[2].x += b_offset;
	
	gl_FragData[3] = vec4(m0,m1,m2,m3);
	gl_FragData[3] *= gl_FragData[3];
	gl_FragData[3] += vec4(b_offset);
	
	//compute quadratic function to interpolate later...
	//question, do we really need a quadratic function?? 
	//reason: we store the differecnes, this is in fact equivalent 
	//to storing finite differences of the quadratic MSE-function (is the MSE-funciton quadratic???)
	//therefore the differecne funciton should be approximatly linear, right?
	//so it should be enough to threat the MSEdiff - function as linear function!
	//but later we need the total MSE so we still need to do something quadratic, therefore it is maybe easier
	//to perform the computation directly on the totalMSE function (which is quadratic) and then compute the position of the minima!
	
	/*float mse0 = var[0];
	float mse1 = mse0+diffMSE[0];
	float mse2 = mse1+diffMSE[1];
	float mse3 = mse2+diffMSE[2];
	float mse4 = mse3+diffMSE[3];
	
	int scale = 4;
	if(diffMSE[0] >= 0.0)
		scale = 0;
	else if(diffMSE[1] >= 0.0)
		scale = 1;	
	else if(diffMSE[2] >= 0.0)
		scale = 2;	
	else if(diffMSE[3] >= 0.0)
		scale = 3;
	
	vec3 funcCoef;
	if(scale>=1){
		findQuadraticFunc(funcCoef, mse0, mse1, mse2, 0, 1, 2);		
	} else if(scale==2){
		findQuadraticFunc(funcCoef, mse1, mse2, mse3, 1, 2, 3);

	} else{
		findQuadraticFunc(funcCoef, mse2, mse3, mse4, 2, 3, 4);
	}
	
	float minVal = -funcCoef.y/(2.f*funcCoef.x);
	
	//stores location of the minimum of the quadratic MSE function... 
	//this vlaue is used later for interpolation purposes
	//be carefull due to noisy data, vlaues can be totally wrong therefore ignore obviously wrong results...
	if(minVal >= 0 && minVal <= 4)
		gl_FragData[4].x = minVal;
	else
		gl_FragData[4].x = scale;*/
}


/*
* Computed the a,b and c coefficients of a quadratic function f(x) = a*x*x + b*x + c given three points.
*/
void findQuadraticFunc(out vec3 coef, in float y0, in float y1, in float y2, in float x0, in float x1, in float x2){
	coef.x = (x0*(y1-y2)+x1*(y2-y0)+x2*(y0-y1))/((x0-x1)*(x0-x2)*(x2-x1));
	coef.y = (x0*x0*(y1-y2)+x1*x1*(y2-y0)+x2*x2*(y0-y1))/((x0-x1)*(x0-x2)*(x1-x2));
	coef.z = (x0*x0*(x1*y2-x2*y1)+x0*(x2*x2*y1-x1*x1*y2)+x1*x2*y0*(x1-x2))/((x0-x1)*(x0-x2)*(x1-x2));
}
/*
* Computes the selectorMSE when changing from one scale to the other and the total MSE of a scale
*/
void computeMSEdiff(out float selectorMSE, out float MSE, in float fine, in float coarse, 
					in float fineVar, in float coarseVar, in float sgF, in float sgC, in float weight, in float scale)
{
	float beta = scale*scale;
	if(scale>1)
		beta = (beta+1)/(beta-1);
	
	float bias = coarse - fine;
	bias *= bias;
	
	float variance = coarseVar - fineVar;
	
	float sg = max(0, sgC - sgF);
	
	selectorMSE =(weight * beta * bias + variance /*+ 0.002 * sg*/); //forget the scenegradient for the moment
	
	if(scale>1)
		beta = 1.f/ (1.f - 1.f/(scale*scale));
	beta *= beta;
	
	MSE = weight*beta*bias + coarseVar; 
}

/*
* Estimates var and mean by using valid scales. This is usefull in very dark reigons, where we often dont have any usefull data at all on lower scales.
* Use this kind of estimation only where everything else fails, because it severly biases the variance- and mean-data
*/
void estimateVarAndMean(inout float[5] var, inout float[5] mean, in float validity)
{
	float sPow2 = scaleFactor*scaleFactor;
	float sPow4 = sPow2*sPow2;
	
	if(validity==1){
		var[0] = sPow2 * var[1];	
		
		//mean[0] = (1.f/sPow4) * mean[1];
	}
	if(validity==2){
		var[1] = sPow2 * var[2];
		var[0] = sPow2 * sPow2 * var[2];
		
		//mean[1] = (1.f/sPow4) * mean[2];
		//mean[0] = (1.f/sPow4) * mean[1];//(1.f/(sPow4*sPow4)) * mean[2];
	}
	if(validity==2){
		var[2] = sPow2 * var[3];
		var[1] = sPow2 * sPow2 * var[3];
		var[0] = sPow2 * sPow2 * sPow2 *var[3];
		
		//mean[2] = (1.f/sPow4) * mean[3];
		//mean[1] = (1.f/sPow4) * mean[2];//(1.f/(sPow4*sPow4)) * mean[3];
		//mean[0] = (1.f/sPow4) * mean[1];//(1.f/(sPow4*sPow4*sPow4)) * mean[3];
	}
	
	if(validity==3){
		var[3] = sPow2 * var[4];
		var[2] = sPow2 * sPow2 * var[4];
		var[1] = sPow2 * sPow2 * sPow2 *var[4];
		var[0] = sPow2 * sPow2 * sPow2 * sPow2 *var[4];
		
		//mean[3] = (1.f/sPow4) * mean[4];
		//mean[2] = (1.f/sPow4) * mean[3];//(1.f/(sPow4*sPow4)) * mean[4];
		//mean[1] = (1.f/sPow4) * mean[2];//(1.f/(sPow4*sPow4*sPow4)) * mean[4];
		//mean[0] = (1.f/sPow4) * mean[1];//(1.f/(sPow4*sPow4*sPow4*sPow4)) * mean[4];
	}
}