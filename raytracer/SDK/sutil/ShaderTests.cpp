#pragma once
#include "ShaderTests.h"
#include <vector>
#include "Filter.h"
#include "Host_Constants.h"
#include "GL\glew.h"
#include "shader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <valarray>
#include <stdlib.h>

/************************************************************************/
/* Testclass for Mean filtering.
 * PASSED
/************************************************************************/
void Shader_Tester::testFilterMean(bool writeToFile)
{

	unsigned int flt, tbo, fbo;
	unsigned int tmpTex[4];
	Filter flts(1.f, 2.f, 4);
	vector<float> meanFilterBank;
	flts.getGaussianFilterbank(meanFilterBank);
	GLHelper::setupTexBuffer(&meanFilterBank[0], meanFilterBank.size(), &tbo, &flt);

	int size = 15;
	float *texd = new float[4*size*size];

	testTexture_Box(texd, 5, size);

	GLHelper::setupTexture(&meanBase, 1, size, size, GL_RGBA32F, GL_RGBA,  texd);
	GLHelper::setupTexture(meanFlt, 4, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(tmpTex, 4, size, size, GL_RGBA32F, GL_RGBA);

	GLHelper::errorCheck();
	
	GLHelper::setupFBO(&fbo, &meanFlt[0], 3);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

	//FILTER MEAN
	GLHelper::shaderPass(Shader::getShaderID(MEAN_FILTER1), 
		"sampler img", 0, meanBase, 
		"array flt", 1, flt,
		"int size", size, 
		"out", 0, tmpTex[0], 
		"out", 1, tmpTex[1], 
		"out", 2, tmpTex[2],
		"out", 3, tmpTex[3], 
		"viewport", size, size, '\0');


	GLHelper::shaderPass(Shader::getShaderID(MEAN_FILTER2), 
		"sampler tex0", 0, tmpTex[0], 
		"sampler tex1", 1, tmpTex[1], 
		"sampler tex2", 2, tmpTex[2],
		"sampler tex3", 3, tmpTex[3],
		"array flt", 4, flt,
		"int size", size, 
		"out", 0, meanFlt[0],
		"out", 1, meanFlt[1], 
		"out", 2, meanFlt[2], 
		"out", 3, meanFlt[3], '\0');

	//glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	if(writeToFile){
		float *t0 = new float[4*size*size];
		float *t1 = new float[4*size*size];
		float *t2 = new float[4*size*size];
		float *t3 = new float[4*size*size];

		float *tmp0 = new float[4*size*size];
		float *tmp1 = new float[4*size*size];
		float *tmp2 = new float[4*size*size];
		float *tmp3 = new float[4*size*size];

		float *tx = new float[4*size*size];

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, meanBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tx);

		glBindTexture(GL_TEXTURE_2D, meanFlt[0]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, t0);

		glBindTexture(GL_TEXTURE_2D, meanFlt[1]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, t1);

		glBindTexture(GL_TEXTURE_2D, meanFlt[2]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, t2);

		glBindTexture(GL_TEXTURE_2D, meanFlt[3]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, t3);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tmpTex[0]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tmp0);

		glBindTexture(GL_TEXTURE_2D, tmpTex[1]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tmp1);

		glBindTexture(GL_TEXTURE_2D, tmpTex[2]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tmp2);

		glBindTexture(GL_TEXTURE_2D, tmpTex[3]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tmp3);


		printTest(tx, size, 0,"../meanInput.txt");
		printTest(t0, size, 0,"../meanOutput0.txt");
		printTest(t1, size, 0,"../meanOutput1.txt");
		printTest(t2, size, 0,"../meanOutput2.txt");
		printTest(t3, size, 0,"../meanOutput3.txt");
		printTest(tmp0, size, 0,"../meanTmp0.txt");
		printTest(tmp1, size, 0,"../meanTmp1.txt");
		printTest(tmp2, size, 0,"../meanTmp2.txt");
		printTest(tmp3, size, 0,"../meanTmp3.txt");

		delete[] tx, t0, t1, t2, t3, tmp0, tmp1, tmp2, tmp3;
	}
}
/************************************************************************/
/* Testclass for the variance filtering shaders
 * PASSED
/************************************************************************/
void Shader_Tester::testFilterVariance(bool writeToFile)
{
	unsigned int flt, tbo, tex2, fbo;
	unsigned int tmpTex;
	Filter flts(1.f, 2.f, 4);
	vector<float> meanFilterBank;
	flts.getVarFilterbank(meanFilterBank);
	GLHelper::setupTexBuffer(&meanFilterBank[0], meanFilterBank.size(), &tbo, &flt);

	int size = 15;
	float *vardata = new float[4*size*size];
	float *nsd = new float[4*size*size];
	testTexture_ValEverywhere(vardata, 0.1f, size);
	testTexture_ValEverywhere(nsd, 4.f, size);

	GLHelper::setupTexture(&varBase, 1, size, size, GL_RGBA32F, GL_RGBA,  vardata);
	GLHelper::setupTexture(&tex2, 1, size, size, GL_RGBA32F, GL_RGBA,  nsd);
	GLHelper::setupTexture(&varFlt, 1, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(&tmpTex, 1, size, size, GL_RGBA32F, GL_RGBA);

	GLHelper::setupFBO(&fbo, &varFlt, 1);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

	//FILTER VARIANCE
	GLHelper::shaderPass(Shader::getShaderID(VAR_FILTER1),
		"sampler img", 0, varBase,
		"sampler nsamples", 1, tex2,
		"array flt", 2, flt,
		"int size", size,
		"out", 0, tmpTex, 
		"viewport", size, size, '\0');

	GLHelper::shaderPass(Shader::getShaderID(VAR_FILTER2),
		"sampler img", 0, tmpTex,
		"array flt", 1, flt,
		"int size", size,
		"out", 0, varFlt, '\0');

	if(writeToFile){
		float *inVar = new float[4*size*size];
		float *inNS = new float[4*size*size];
		float *tmp = new float[4*size*size];
		float *out = new float[4*size*size];

		glBindTexture(GL_TEXTURE_2D, varBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, inVar);
		glBindTexture(GL_TEXTURE_2D, tex2); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, inNS);
		glBindTexture(GL_TEXTURE_2D, tmpTex); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tmp);
		glBindTexture(GL_TEXTURE_2D, varFlt); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, out);

		printTest(inVar, size, 0,"../varInput.txt");
		printTest(inNS, size, 0,"../varInputNS.txt");
		printTest(tmp, size, 0,"../varTmpX.txt");
		printTest(tmp, size, 1,"../varTmpY.txt");
		printTest(tmp, size, 2,"../varTmpZ.txt");
		printTest(tmp, size, 3,"../varTmpW.txt");
		printTest(out, size, 0,"../varOutX.txt");
		printTest(out, size, 1,"../varOutY.txt");
		printTest(out, size, 2,"../varOutZ.txt");
		printTest(out, size, 3,"../varOutW.txt");

		delete[] inVar, inNS, tmp, out;
	}
}
/************************************************************************/
/* Testclass for the nsmaples filter shaders
 * PASSED
/************************************************************************/
void Shader_Tester::testFilterNSamples(bool writeToFile)
{
	unsigned int flt, tbo, tex, fbo;
	unsigned int tmpTex;
	Filter flts(1.f, 2.f, 4);
	vector<float> meanFilterBank;
	flts.getGaussianBoxCenterOneFilterBank(meanFilterBank);
	GLHelper::setupTexBuffer(&meanFilterBank[0], meanFilterBank.size(), &tbo, &flt);

	int size = 15;
	float *texd = new float[4*size*size];
	float *nsd = new float[4*size*size];
	testTexture_ZeroEverywhere_OneCenter(texd, size);
	testTexture_ValEverywhere(nsd, 4.f, size);

	GLHelper::setupTexture(&tex, 1, size, size, GL_RGBA32F, GL_RGBA,  texd);
	GLHelper::setupTexture(&nsBase, 1, size, size, GL_RGBA32F, GL_RGBA,  nsd);
	GLHelper::setupTexture(&nsFlt, 1, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(&tmpTex, 1, size, size, GL_RGBA32F, GL_RGBA);

	GLHelper::setupFBO(&fbo, &nsFlt, 1);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

	GLHelper::shaderPass(Shader::getShaderID(NSAMPLES_FILTER1),
		"sampler img", 0, nsBase,
		"array flt", 1, flt,
		"int size", size,
		"out", 0, tmpTex, 
		"viewport", size, size, '\0');

	GLHelper::shaderPass(Shader::getShaderID(NSAMPLES_FILTER2),
		"sampler tex0", 0, tmpTex,
		"array flt", 1, flt,
		"int size", size,
		"out", 0, nsFlt, '\0');

	if(writeToFile){
		float *inNS = new float[4*size*size];
		float *tmp = new float[4*size*size];
		float *out = new float[4*size*size];

		glBindTexture(GL_TEXTURE_2D, nsBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, inNS);
		glBindTexture(GL_TEXTURE_2D, tmpTex); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tmp);
		glBindTexture(GL_TEXTURE_2D, nsFlt); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, out);

		printTest(inNS, size, 0,"../nsInput.txt");
		printTest(tmp, size, 0,"../nsTmpX.txt");
		printTest(tmp, size, 1,"../nsTmpY.txt");
		printTest(tmp, size, 2,"../nsTmpZ.txt");
		printTest(tmp, size, 3,"../nsTmpW.txt");
		printTest(out, size, 0,"../nsOutX.txt");
		printTest(out, size, 1,"../nsOutY.txt");
		printTest(out, size, 2,"../nsOutZ.txt");
		printTest(out, size, 3,"../nsOutW.txt");

		delete[] inNS, tmp, out;
	}

}

/************************************************************************/
/* Testcase 1: Compare manually computed value with value in shader
 * filterNSamples, filter Mean and filterVariancem ust be called before this to generate input
 * PASSED (small precision issues,  but they should be meaningless) 
/************************************************************************/
void Shader_Tester::testStopmapCreate(bool writeToFile)
{
	float gamma = 0.2f;
	float p = pow(1.9f*gamma, 1.f/sqrt(2.f));
	float gamma_z = -log(1.f - p);

	int size = 15;

	unsigned int fbo;

	GLHelper::setupTexture(&brightness, 1, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(&brightnessBase, 1, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(&stop_not_weighted, 1, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(&stop_weighted, 1, size, size, GL_RGBA32F, GL_RGBA);

	GLHelper::setupFBO(&fbo, &brightness, 1);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

	GLHelper::shaderPass(Shader::getShaderID(STOPMAP_CREATE), 
		"sampler mean0", 0, meanFlt[0], 
		"sampler mean1", 1, meanFlt[1], 
		"sampler mean2", 2, meanFlt[2], 
		"sampler mean3", 3, meanFlt[3], 
		"sampler var0", 4,  varFlt, 
		"sampler ns", 5, nsBase, 
		"sampler basemean", 6, meanBase,
		"sampler basevar", 7, varBase,
		"float gamma_z", gamma_z,
		"out", 0, stop_weighted, 
		"out", 1, stop_not_weighted, 
		"out", 2, brightnessBase, 
		"out", 3, brightness,
		"viewport", size, size, '\0');

	if(writeToFile){
		float *weighted = new float[4*size*size];
		float *not_weighted = new float[4*size*size];
		float *bbase = new float[4*size*size];
		float *bright = new float[4*size*size];
		float *nsb = new float[4*size*size];
		float *meanb = new float[4*size*size];
		float *varb = new float[4*size*size];
		float *m0 = new float[4*size*size];
		float *m1 = new float[4*size*size];
		float *m2 = new float[4*size*size];
		float *m3 = new float[4*size*size];
		float *varf = new float[4*size*size];

		
		glBindTexture(GL_TEXTURE_2D, varFlt); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, varf);

		glBindTexture(GL_TEXTURE_2D, meanFlt[0]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, m0);
		glBindTexture(GL_TEXTURE_2D, meanFlt[1]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, m1);
		glBindTexture(GL_TEXTURE_2D, meanFlt[2]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, m2);
		glBindTexture(GL_TEXTURE_2D, meanFlt[3]); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, m3);

		glBindTexture(GL_TEXTURE_2D, nsBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, nsb);
		glBindTexture(GL_TEXTURE_2D, meanBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, meanb);
		glBindTexture(GL_TEXTURE_2D, varBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, varb);

		glBindTexture(GL_TEXTURE_2D, stop_weighted); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, weighted);
		glBindTexture(GL_TEXTURE_2D, stop_not_weighted); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, not_weighted);
		glBindTexture(GL_TEXTURE_2D, brightnessBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, bbase);
		glBindTexture(GL_TEXTURE_2D, brightness); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, bright);

		//wrong its the stopmap stupid!
		printTest(varf, size, 0,"../stopmapCreate_varFlt0.txt");
		printTest(varf, size, 1,"../stopmapCreate_varFlt1.txt");
		printTest(varf, size, 2,"../stopmapCreate_varFlt2.txt");
		printTest(varf, size, 3,"../stopmapCreate_varFlt3.txt");

		printTest(m0, size, 0,"../stopmapCreate_meanFlt0.txt");
		printTest(m1, size, 0,"../stopmapCreate_meanFlt1.txt");
		printTest(m2, size, 0,"../stopmapCreate_meanFlt2.txt");
		printTest(m3, size, 0,"../stopmapCreate_meanFlt3.txt");

		printTest(m0, size, 0,"../stopmapCreate_meanFlt0.txt");
		printTest(m1, size, 0,"../stopmapCreate_meanFlt1.txt");
		printTest(m2, size, 0,"../stopmapCreate_meanFlt2.txt");
		printTest(m3, size, 0,"../stopmapCreate_meanFlt3.txt");

		printTest(nsb, size, 0,"../stopmapCreate_ns_Base.txt");
		printTest(meanb, size, 0,"../stopmapCreate_mean_Base.txt");
		printTest(varb, size, 0,"../stopmapCreate_var_Base.txt");

		printTest(weighted, size, 0,"../stopmapCreate_DiffMSE_Weighted_X.txt");
		printTest(weighted, size, 1,"../stopmapCreate_DiffMSE_Weighted_Y.txt");
		printTest(weighted, size, 2,"../stopmapCreate_DiffMSE_Weighted_Z.txt");
		printTest(weighted, size, 3,"../stopmapCreate_DiffMSE_Weighted_W.txt");
		printTest(not_weighted, size, 0,"../stopmapCreate_DiffMSE_Not_Weighted_X.txt");
		printTest(not_weighted, size, 1,"../stopmapCreate_DiffMSE_Not_Weighted_Y.txt");
		printTest(not_weighted, size, 2,"../stopmapCreate_DiffMSE_Not_Weighted_Z.txt");
		printTest(not_weighted, size, 3,"../stopmapCreate_DiffMSE_Not_Weighted_W.txt");
		printTest(bbase, size, 0,"../stopmapCreate_Brightness_Base.txt");
		printTest(bright, size, 0,"../stopmapCreate_BrightnessFlt_X.txt");
		printTest(bright, size, 1,"../stopmapCreate_BrightnessFlt_Y.txt");
		printTest(bright, size, 2,"../stopmapCreate_BrightnessFlt_Z.txt");
		printTest(bright, size, 3,"../stopmapCreate_BrightnessFlt_W.txt");

	
		//compare to manually computation
		// compare scale 0->1, 1->2, 2->3, 3->4...
		// seems to do what it should!
		float *mse = new float[4*size*size];
		float *meanIllum = new float[4*size*size];

		//mean to illuminance
		for(int i=0; i<size*size; i++){
			meanIllum[4*i] = 0.299f*m0[4*i] + 0.587f*m0[4*i+1] + 0.114f*m0[4*i+2];
			meanIllum[4*i+1] = 0.299f*m1[4*i] + 0.587f*m1[4*i+1] + 0.114f*m1[4*i+2];
			meanIllum[4*i+2] = 0.299f*m2[4*i] + 0.587f*m2[4*i+1] + 0.114f*m2[4*i+2];
			meanIllum[4*i+3] = 0.299f*m3[4*i] + 0.587f*m3[4*i+1] + 0.114f*m3[4*i+2];
		}
		//manual computation of weighted diff MSE (non_weighted must not be checked, correctness follows from this)
		for(int s=0; s<4; s++){
			for(int i=0; i<size*size; i++){

				float variance = (s==0)? 
					varf[4*i] - varb[4*i]/nsb[4*i] :
					varf[4*i+s] - varf[4*i+s-1];

					float weight = (1 - 1/nsb[4*i])*gamma_z;

				float bias = (s==0)? 
					meanIllum[4*i] - (0.299f*meanb[4*i] + 0.587f*meanb[4*i+1] + 0.114f*meanb[4*i+2]) :
					meanIllum[4*i+s] - meanIllum[4*i+s-1];

				bias *= (s==0) ? bias : bias*1.666667f;

				mse[4*i+s] = 1000.f*(weight*bias + variance);	
			}
			//difference of both buffers (ascii-floats do not show all digits so here a more accurate test...)
			//biggest error is 1e-6 so I dont think this makes any difference... 
			float maxDiff=0;
			float diff=0;
			for(int i=0; i<size*size; i++){
				diff = abs(mse[4*i+s]-weighted[4*i+s]);
				if(diff>maxDiff) maxDiff=diff;
			}
			printf("max diff %i -> %i is %f \n", s, s+1, maxDiff);
		}
		printTest(mse, size, 0,"../stopmapCreate_Manually_DiffMSE_Weigthed_X.txt");
		printTest(mse, size, 1,"../stopmapCreate_Manually_DiffMSE_Weigthed_Y.txt");
		printTest(mse, size, 2,"../stopapCreate_Manually_DiffMSE_Weigthed_Z.txt");
		printTest(mse, size, 3,"../stopmapCreate_Manually_DiffMSE_Weigthed_W.txt");


		delete[] weighted, not_weighted, bbase, bright, nsb, meanb, varb, m0, m1, m2, m3, varf, mse, meanIllum;
	}
}

/************************************************************************/
/* Stopmap binarize shader
 * PENDING
/************************************************************************/
void Shader_Tester::testStopmapBinarize(bool writeToFile)
{

	int size = 15;
	GLHelper::setupTexture(&stopmap_non_filtered, 1, size, size, GL_RGBA32F, GL_RGBA);

	GLHelper::shaderPass(Shader::getShaderID(STOPMAP_BINARIZE), 
		"sampler stop0", 0, stop_weighted,
		"out", 0, stopmap_non_filtered, '\0');

	if(writeToFile){
		float *weighted = new float[4*size*size];
		float *stopmap = new float[4*size*size];

		glBindTexture(GL_TEXTURE_2D, stopmap_non_filtered); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, stopmap);
		glBindTexture(GL_TEXTURE_2D, stop_weighted); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, weighted);

		printTest(weighted, size, 0,"../stopmapBinarize_Input_X.txt");
		printTest(weighted, size, 1,"../stopmapBinarize_Input_Y.txt");
		printTest(weighted, size, 2,"../stopmapBinarize_Input_Z.txt");
		printTest(weighted, size, 3,"../stopmapBinarize_Input_W.txt");

		printTest(stopmap, size, 0,"../stopmapBinarize_Output_X.txt");
		printTest(stopmap, size, 1,"../stopmapBinarize_Output_Y.txt");
		printTest(stopmap, size, 2,"../stopmapBinarize_Output_Z.txt");
		printTest(stopmap, size, 3,"../stopmapBinarize_Output_W.txt");

		delete[] weighted, stopmap;
	}
}
/************************************************************************/
/* Tests the costmap filter shaders
 * PENDING
/************************************************************************/
void Shader_Tester::testFilterStopmap( bool writeToFile /*= true*/ )
{

	unsigned int tmp, fbo, tbo, flt;
	int size = 15;

	Filter flts(1.f, 2.f, 4);
	vector<float> meanFilterBank;
	flts.getGaussianBoxFilterbank(meanFilterBank);
	GLHelper::setupTexBuffer(&meanFilterBank[0], meanFilterBank.size(), &tbo, &flt);

	GLHelper::setupTexture(&tmp, 1, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(&stopmap_filtered, 1, size, size, GL_RGBA32F, GL_RGBA);

	GLHelper::setupFBO(&fbo, &tmp, 1);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

	GLHelper::shaderPass(Shader::getShaderID(STOPMAP_FILTER1), 
		"sampler stop0", 0, stopmap_non_filtered,
		"array flt", 1, flt,
		"int size", size,
		"out", 0, tmp,
		"viewport", size, size, '\0');

	GLHelper::shaderPass(Shader::getShaderID(STOPMAP_FILTER2),
		"sampler stop",0,  tmp,
		"sampler nonfiltered_stop", 1, stopmap_non_filtered,
		"array flt", 2, flt,
		"int size", size,
		"out", 0, stopmap_filtered, '\0');

	if(writeToFile){
		float *intex	= new float[4*size*size];
		float *tmpt		= new float[4*size*size];
		float *outtex	= new float[4*size*size];

		glBindTexture(GL_TEXTURE_2D, stopmap_non_filtered); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, intex);
		glBindTexture(GL_TEXTURE_2D, tmp); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, tmpt);
		glBindTexture(GL_TEXTURE_2D, stopmap_filtered); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, outtex);

		printTest(tmpt, size, 0,"../stopFlt_TmpX.txt");
		printTest(tmpt, size, 1,"../stopFlt_TmpY.txt");
		printTest(tmpt, size, 2,"../stopFlt_TmpZ.txt");
		printTest(tmpt, size, 3,"../stopFlt_TmpW.txt");

		printTest(intex, size, 0,"../stopFltInputX.txt");
		printTest(intex, size, 1,"../stopFltInputY.txt");
		printTest(intex, size, 2,"../stopFltInputZ.txt");
		printTest(intex, size, 3,"../stopFltInputW.txt");

		printTest(outtex, size, 0,"../stopFltOutputX.txt");
		printTest(outtex, size, 1,"../stopFltOutputY.txt");
		printTest(outtex, size, 2,"../stopFltOutputZ.txt");
		printTest(outtex, size, 3,"../stopFltOutputW.txt");

		delete[] intex, tmpt, outtex;

	}
}

/************************************************************************/
/* Tests the costmap filter shaders
 * PENDING
/************************************************************************/
void Shader_Tester::testCostmapCreate( bool writeToFile /*= true*/ )
{
	int size = 15;

	unsigned int fbo;

	GLHelper::setupTexture(&costmap_mse, 1, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(&costmap_steps, 1, size, size, GL_RGBA32F, GL_RGBA);

	GLHelper::setupFBO(&fbo, &costmap_steps, 1);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

	GLHelper::shaderPass(Shader::getShaderID(COSTMAP_CREATE),
		"sampler basevar", 0, varBase,
		"sampler cost0", 1, stop_not_weighted,
		"sampler stop0", 2, stopmap_filtered,
		"sampler ns", 3, nsFlt,
		"sampler base_bright", 4, brightnessBase,
		"sampler bright", 5, brightness,
		"sampler basens", 6, nsBase,
		"out", 0, costmap_mse,
		"out", 1, costmap_steps,
		"viewport", size, size, '\0');

	if(writeToFile){
		float *mse = new float[4*size*size];
		float *steps = new float[4*size*size];
		float *varB = new float[4*size*size];
		float *nsB = new float[4*size*size];

		glBindTexture(GL_TEXTURE_2D, nsBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, nsB);
		glBindTexture(GL_TEXTURE_2D, varBase); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, varB);
		glBindTexture(GL_TEXTURE_2D, costmap_steps); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, steps);
		glBindTexture(GL_TEXTURE_2D, costmap_mse); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, mse);

		printTest(steps, size, 0,"../Costmap_Steps_X.txt");

		printTest(mse, size, 0,"../Costmao_mse_X.txt");

	
		//compute it manually and compare results...
		float *man_mse = new float[4*size*size];
		for(int i=0; i<4*size*size; i++){
			man_mse[i] = 1000.f*varB[i]/nsB[i];
		}

		delete[] mse, steps;
	}
}
 
/************************************************************************/
/* costmap filter shader testing
 * PASSED (99%) 
/************************************************************************/
void Shader_Tester::testFilterCostmap(bool writeToFile)
{

	int size = 15;

	unsigned int flt, tbo, tmp;

	GLHelper::setupTexture(&costmap_filtered, 1, size, size, GL_RGBA32F, GL_RGBA);
	GLHelper::setupTexture(&tmp, 1, size, size, GL_RGBA32F, GL_RGBA);

	Filter flts(1.f, 2.f, 4);
	vector<float> meanFilterBank;
	flts.getGaussianBoxFilterbank(meanFilterBank);
	GLHelper::setupTexBuffer(&meanFilterBank[0], meanFilterBank.size(), &tbo, &flt);

	GLHelper::shaderPass(Shader::getShaderID(COSTMAP_FILTER1),
		"sampler img", 0, costmap_mse,
		"sampler img2", 1, costmap_steps,
		"array flt", 2, flt,
		"int size", size,
		"out", 0, tmp,
		"viewport", size, size,'\0');

	GLHelper::shaderPass(Shader::getShaderID(COSTMAP_FILTER2),
		"sampler tex0", 0, tmp,
		"sampler base", 2, costmap_mse,
		"sampler chosenScale", 3, costmap_steps,
		"array flt", 4, flt,
		"int size", size,
		"out", 0, costmap_filtered, '\0');

	if(writeToFile){
		float *unfiltered = new float[4*size*size];
		float *oneDfiltered = new float[4*size*size];
		float *filtered = new float[4*size*size];
		float *steps = new float[4*size*size];

		glBindTexture(GL_TEXTURE_2D, costmap_filtered); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, filtered);
		glBindTexture(GL_TEXTURE_2D, costmap_steps); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, steps);
		glBindTexture(GL_TEXTURE_2D, tmp); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, oneDfiltered);
		glBindTexture(GL_TEXTURE_2D, costmap_mse); 
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA, GL_FLOAT, unfiltered);

		printTest(filtered, size, 0,  "../costmap_filteredX.txt");

		//an additional test: see if the filtered version sums up to the same as the unfiltered one.
		// There was an error that has been fixed!
		// 
		//sum_filtered < sum_1dfiltered < sum_unfiltered. 
		// This can be explained by cropping at the border: (
		//		we distribute the contribution of a pixel over a region around, 
		//		at the border this region is cropped to one side. 
		//		Unless we dont take special care of this cases, 
		//		some of the contribution is lost there.
		//		=>This is normal and should not pose any problem.
		float sum_filtered = 0;
		float sum_unfiltered = 0;
		float sum_1Dfiltered = 0;
		for(int i=0; i<4*size*size; i++){
			sum_filtered += filtered[i];
			sum_unfiltered += unfiltered[i];
			sum_1Dfiltered += oneDfiltered[i];
			if(steps[i]==0.f)
				sum_1Dfiltered += unfiltered[i]; 
		}
		printf("costmap sum filtered: %f, 1Dfiltered: %f, unfiltered: %f\n", sum_filtered, sum_1Dfiltered, sum_unfiltered);

		

		delete [] filtered, unfiltered;

	}

}
/************************************************************************/
/* Writes a color-channel of a texture as a matrix into a file                                                                     */
/************************************************************************/
void Shader_Tester::printTest( float * texd, int size, int channel, std::string name )
{

	std::ofstream file;
	file.open(name.c_str(), std::ios::trunc);

	for(int i=0; i<size*size; i++){
	char buffer [256];
	sprintf (buffer, "%f\t", texd[4*i+channel]/*, texd[4*i+1], texd[4*i+2], texd[4*i+3]*/);

	file.write(buffer, strlen(buffer)*sizeof(char));
	if(i%size==size-1){
		char c = '\n';
		file.write(&c, sizeof(char));
	}
	}
	file.close();
}


//************************************
//Returns a texData array. The Tex is one in the central pixel and zero elswhere
//************************************
void Shader_Tester::testTexture_ZeroEverywhere_OneCenter( float *data, int size )
{

	int idx = 4*(size/2 * size + size/2);
	for(int i=0; i<4*size*size; i++)
		data[i] = 0.f;

	data[idx] = 1.f;
	data[idx+1] = 1.f;
	data[idx+2] = 1.f;
	data[idx+3] = 1.f;
}

//************************************
//Returns a texData array. The Tex is one in a central quadratic area of size bs, and zero everywhere else
//************************************
void Shader_Tester::testTexture_Box( float *data, int bs, int size )
{

	int b = (size-bs)/2;
	for(int i=0; i<size; i++){
		for(int j=0; j<size; j++){
			if(i>=b && i<size-b && j>=b && j<size-b){
				data[4*(i*size+j)] = 1.f;
				data[4*(i*size+j)+1] = 1.f;
				data[4*(i*size+j)+2] = 1.f;
				data[4*(i*size+j)+3] = 1.f;
			} else{
				data[4*(i*size+j)] = 0.f;
				data[4*(i*size+j)+1] = 0.f;
				data[4*(i*size+j)+2] = 0.f;
				data[4*(i*size+j)+3] = 0.f;
			}

		}
	}

}

//************************************
//Returns a texData array. The Tex is VAL everywhere
//************************************
void Shader_Tester::testTexture_ValEverywhere(float *data, float val, int size )
{

	for(int i=0; i<4*size*size; i++)
		data[i] = val;
}


//************************************
//Returns a texData array. The Tex has a rnd vlaue  in [0,1] everywhere except for the central element where it is val
//************************************
void Shader_Tester::testTexture_RndEverywhere(float *data, float val, int size )
{

	int idx = 4*(size/2 * size + size/2);
	for(int i=0; i<4*size*size; i++)
		data[i] = rand() / 32767.f ;

}




