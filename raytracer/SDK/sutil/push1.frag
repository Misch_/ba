#version 150 core

in vec2 texCoord;

uniform sampler2D oimg; //original img
uniform sampler2D rimg; //reduced img

// |--'--|--'--|--'--|--'--| res of oimg & screen
// |-----'-----|-----'-----| res of rimg
// no corrections for rimg needed, if GL_NEAREST is used as interpolation method
//because no matter if the pixel right or left from a rimg texel is rendered, 
//the nearest texel by using texCoord should always be the correct one. 


void main(void)
{
	gl_FragData[0] = vec4(0,0,0,0);
	
	vec4 odata = texture2D(oimg, texCoord); //w stores the samples shot 
	
	//if no sample was shot in this region, pull data up from reduced image
	//important note: w component of a pulled element must still be zero otherwise in the basefiltering 
	//the number of samples shot in a pixel will be incorrect
	gl_FragData[0] = 
		(odata.w==0)? 
			vec4(texture(rimg, texCoord).xyz, 0) : 
			odata; 
}