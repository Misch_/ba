#version 150 core

in vec2 texCoord;

uniform sampler2D stop;
uniform sampler2D base;
uniform sampler2D ns;

void main(){
	gl_FragData[0] = vec4(0);
	
	vec4 cost0 = texture2D(stop, texCoord);

	if(cost0.x >= 0.01/texture2D(ns, texCoord).x){
		gl_FragData[0].x = 1;
	}
	
	if(cost0.y >=0.001/texture2D(ns, texCoord).x){
		gl_FragData[0].y = 1;
	}
	
	if(cost0.z >= 0.0){
		gl_FragData[0].z = 1;
	}
	
	if(cost0.w >= 0.0){
		gl_FragData[0].w = 1;
	}
	

/*
	float mse_base = texture2D(base, texCoord).x/texture2D(ns, texCoord).x;
	vec4 mse = texture2D(stop, texCoord);
	
	if(mse_base < min(mse.x, min(mse.y, min(mse.z,mse.w))) )
		gl_FragData[0].x = 1;
	
	if(mse.x < min(mse_base, min(mse.y, min(mse.z,mse.w))) )
		gl_FragData[0].y = 1;
	
	if(mse.y < min(mse.x, min(mse_base, min(mse.z,mse.w))) )
		gl_FragData[0].z = 1;
	
	if(mse.z < min(mse.x, min(mse.y, min(mse_base,mse.w))) )
		gl_FragData[0].w = 1;
	*/
}