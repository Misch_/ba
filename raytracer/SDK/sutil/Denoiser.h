#pragma once

#include <string>
#include <optixu/optixpp_namespace.h>
#include <sutil.h>
#include<cmath>
#include "../nvcommon/include/framebufferObject.h"
#include <AntTweakBar.h>
#include <optixu/optixu_math_stream_namespace.h>
#include <minmax.h>

#include <iostream>
#include <cstdlib>
#include <cstdio> //sprintf
#include <sstream>
#include <string>

//#include <GL/glut.h>
#include "shader.h"
#include "SampleScene.h"
#include <vector>
#include "Host_Constants.h"
/***************************************************************************************************************************************
 * The Denoiser filters an image adaptively according to the input data and generates a sampling map for the next frame.   
 * The enumerated Textures are created with the denoiser and, can be accessed from outside, they can be accessed by using getTex(ENUM_NAME)
 * Because the sampling map (output_tex) needs to be known priorly to optiX we pass it as initialization parameter, 
 * if and how it is used is outside of the scope of this class.
 *Right now the only denoiser supported is the GEM denoiser described in Rousselle et al 2011. 
 **************************************************************************************************************************************/
/*
//A lot of these textures are used only for GEM or deprecated. Still, to avoid the risk of messing up everything, keep them right now.
//If new textures are used for other purposes add new enum somewhere in front of NUM_OF_TEXTURES
enum TexName{	MEAN_FILTERED1, MEAN_FILTERED2, MEAN_FILTERED3, MEAN_FILTERED4, 
				VAR_FILTERED, NSAMPLES_FILTERED, 
				STOPMAP_FILTERED, STOPMAP_UNIFLTERED,
				STOPMAP_DIFF_MSE, STOPMAP_DIFF_MSE_WEIGHTED, 
				COSTMAP_FITLERED, COSTMAP_STEPS, COSTMAP_MSE,  
				BRIGHTNESS_BASE, BRIGHTNESS2,
				TMP1, TMP2,TMP3, TMP4, 
				SCALEMIN_UNFILTERED, SCALEMIN_FILTERED, 
				NUM_OF_TEXTURES,						//NOT A TEXTURE IDENTIFIER: stores the number of textures we need to create for the denoiser (kind of a hack) 
				MEAN_INPUT, VAR_INPUT, NSAMPLES_INPUT	//redundant identifiers for the denoiser independent input data, simplifies access to them by the GUI
};

enum DenoiserType{
	DENOISER_GF, DENOISER_GEM
};*/

class Denoiser{
public:
	Denoiser(){}

	//returns the result of the MSE reduction
	float getTotalMSEReduct(){ return totalMSEReduct; }
	//accessor for the associated textures (warning this creates the possibility to change their content outside the denoiser!)
	unsigned int * getTex(int i);
	//resizes the textures associated with this denoiser (except output_map!)
	void resize(int w, int h);

	//virtual functions, to be implemented in derived classes...
	//initialize denoiser by assigning a shared resource (output_Map) to it in which the sampling map is stored 
	virtual void init( unsigned int *output_map, int width, int height) = 0;
	//sets the filter bank for the denoiser (at the moment it supports only gaussian filters)
	virtual void updateFilter() = 0;
	//computes the sampling map and creates the filtered images
	virtual void computeSamplingDistribution( unsigned int *input_mean, unsigned int *input_var, unsigned int *input_ns, unsigned int *normal, unsigned int *brdf, unsigned int *position, float gamma, float epsilon, int fltRadius) = 0;
	//computes the average number of samples per pixel, used for the GUI
	virtual void computeAvgNS(unsigned int * ns) = 0;
	//returns the denoiser type
	virtual DenoiserType getDenoiserType() = 0;
	float epsilon;
	int fltRadius;

protected:
	void errorCheck();
	virtual void setAllTextures() = 0;
	virtual void resizeTextures() = 0;


	DenoiserType type;

	unsigned int tex[NUM_OF_TEXTURES+7];
	float totalMSEReduct;
	float minSigma;
	float stepSize;
	int nSteps;
	float gamma;



	unsigned int _vertSize, _mode;
	unsigned int _fbo_3targets, _fbo_1target;
	unsigned int *_tex_reduct;
	int	_width, _height, _log2n;	
	bool _groundTruth;
	float bias_mluiplicator;
	float importance_modifier_scales;

	std::vector<float> mse; //no clue why this is a vector but it surely has its reasons *hust*

	unsigned int *input_mean;
	unsigned int *input_var;
	unsigned int *input_ns; 
	unsigned int *output_flt;
	unsigned int *output_map;
	unsigned int *inout_outlier;

	//identifiers for the texture objects containing the filters
	//we also need to store the texture buffer object identifiers so that old filters are overwritten by the new ones (otherwise we have a memory leak)
	unsigned int _tbo_filtervar;
	unsigned int _tex_var_filter_coef;
	unsigned int _tbo_filtermean;
	unsigned int _tex_mean_filter_coef;
	unsigned int _tbo_filterimg;
	unsigned int _tex_img_filter_coef;
	unsigned int _tbo_filtercone;
	unsigned int _tex_cone_filter_coef;
	unsigned int _tbo_cone_norm;
	unsigned int _tex_cone_norm;


	unsigned int _ref_mean;
	unsigned int _ref_var;
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DERIVED CLASSES
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
class DenoiserGEM: public Denoiser{
public:
	DenoiserGEM():type(DENOISER_GEM){}
	void init( unsigned int *img, int width, int height);
	void computeSamplingDistribution( unsigned int *mean, unsigned int *var,  unsigned int *ns, unsigned int *normal, unsigned int *brdf, unsigned int *position, float gamma, float epsilon, int fltRadius);
	void computeAvgNS(unsigned int* ns_map);
	void updateFilter();
	DenoiserType getDenoiserType(){ return type; }
protected:
	DenoiserType type;
	void setAllTextures();
	void resizeTextures();
private:
	void setFilter( float minSigma, float stepSize, int nSteps );
	void greedyErrorMinimization();
};

class DenoiserGF: public Denoiser{
public:
	DenoiserGF():type(DENOISER_GF){}
	void init( unsigned int *img, int width, int height);
	void computeSamplingDistribution( unsigned int *mean, unsigned int *var,  unsigned int *ns, unsigned int *normal, unsigned int *brdf, unsigned int *position, float gamma, float epsilon, int fltRadius);
	void generateSAT( unsigned int *in, unsigned int *out);
	int computePasses(int size);
	void computeAvgNS(unsigned int* ns_map);
	void updateFilter();
DenoiserType getDenoiserType(){ return type; }

protected:
	DenoiserType type;
	void setAllTextures();
	void resizeTextures();

private:
	void setFilter( float minSigma, float stepSize, int nSteps );
	void grayScaleGF(unsigned int *input, unsigned int *guide);
	void colorGF(unsigned int *input, unsigned int *guide);
	void colorGF2(unsigned int *input, unsigned int *guide1, unsigned int *guide2);
	void colorGF3(unsigned int *input, unsigned int *guide1, unsigned int *guide2, unsigned int *guide3);
	//void screenshot (char filename[160],int x, int y);
};