#pragma once
#include "sutil.h"

class ScreenImage{
public:
	void static finalImageGEM(unsigned int *base, unsigned int *flt, unsigned int *map, unsigned int *minscale, unsigned int *minscale_base, int width, int height);
	void static finalImageSimple(unsigned int *base, int width, int height);
	void static finalImageDebug(unsigned int *base, int width, int height);
};