#include "ScreenImage.h"
#include "GL\glew.h"
#include "gui.h"
#include "Host_Constants.h"
#include <wtypes.h>
#include <winbase.h>
#include "shader.h"
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Generates the final image and displays it to the screen. 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 void ScreenImage::finalImageGEM( unsigned int *base, unsigned int *flt, unsigned int *map, unsigned int *minscale, unsigned int *minscale_base, int width, int height )
 {
	unsigned int final=0;
	GLHelper::setupTexture(&final,1,width,height,GL_RGB32F, GL_RGB);
	GLHelper::shaderPass(Shader::getShaderID(FINAL),
		"float gamma", GUI::getGammaCorrection(),
		"sampler smap", 0, *map,
		"sampler base", 1, *base,
		"sampler img0", 2, flt[0],
		"sampler img1", 3, flt[1],
		"sampler img2", 4, flt[2],
		"sampler img3", 5, flt[3], 
		"sampler smin", 6, *minscale,
		"sampler smin_base", 7, *minscale_base, 
		"out", 0, final,
		'\0'
		);

	GLenum windowBuffer[] = { GL_BACK };
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glDrawBuffers(1, windowBuffer);

	GLHelper::shaderPass(Shader::getShaderID(FINAL_TRIVIAL),
		"sampler tex", 0, final, 
		'\0'
		);
	
	/* This will export the final texture to a tga-file. */
	// GLHelper::screenshot("screenshot_gem.tga",512,512, final);
	
	glDeleteTextures(1,&final);
}

 void ScreenImage::finalImageSimple(unsigned int *base, int width, int height)
 {
	 GLenum windowBuffer[] = { GL_BACK };
	 glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	 glDrawBuffers(1, windowBuffer);

	 GLHelper::shaderPass(Shader::getShaderID(TRIVIAL),
		 "sampler img", 0, *base,
		 "int channel", 0,
		 "int negate", 0,
		 "float mult", 1.f,
		 "float gamma", GUI::getGammaCorrection(),
		 "viewport", width, height, '\0');
 }

 void ScreenImage::finalImageDebug( unsigned int *base, int width, int height )
 {
	 GLenum windowBuffer[] = { GL_BACK };
	 glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	 glDrawBuffers(1, windowBuffer);

	 GLHelper::shaderPass(Shader::getShaderID(TRIVIAL),
		 "sampler img", 0, *base,
		 "int channel", GUI::getDebugChannel(),
		 "int negate", GUI::isNegateActive(),
		 "float mult", powf(10.f, GUI::getDebugMult()),
		 //"float gamma", GUI::getGammaCorrection(),
		 "float gamma", 1.0,
		 "viewport", width, height, '\0');
 }
