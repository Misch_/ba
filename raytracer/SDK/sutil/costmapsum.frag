#version 150 core

in vec2 texCoord;

uniform sampler2D img;
uniform float total;

void main()
{
	gl_FragData[0].x = texture2D(img, texCoord).x;
	gl_FragData[0].x /= 1;//total;	//division now in optix
}