#pragma once

#include "optix_host.h"
#include "sutil.h"
#include <string>

class StatisticsGatherer{
private:
	float *_mean;
	int _height, _width, _channels, _n;
public:
	SUTILAPI StatisticsGatherer(){}
	SUTILAPI StatisticsGatherer(int height, int width, int channels);
	SUTILAPI ~StatisticsGatherer();
	SUTILAPI void addImageToMean(RTbuffer buffer);
	SUTILAPI void writeToFile(std::string name);
	SUTILAPI const float *getMeanData();
	SUTILAPI int getHeight();
	SUTILAPI int getWidth();
	SUTILAPI int getChannels();
	SUTILAPI static float *readFile(std::string name);

};