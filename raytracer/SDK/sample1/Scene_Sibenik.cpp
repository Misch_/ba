#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "structs.h"
#include "sutil.h"
#include "SampleScene.h"
#include "ObjLoader.h"
#include "ImageLoader.h"
#include "scenes.h"

extern void Scenes::precomputation(AreaLight& a);

namespace Scenes{
	using namespace optix;

	void Sibenik(optix::Context& _context){
		AreaLight light;
		light.color = make_float3(10.f, 10.f, 10.f);
		light.position = make_float3(100.f, -85.f, -0.f);
		light.direction = make_float3(0.f, -1.f, 1.f);

		light.width = 10.f;
		light.length = 10.f;
		light.samples = 1;
		precomputation(light);
		_context["light"]->setUserData(sizeof(light), &light);

		//compute rectangle that visualizes the lightsource
		float3 light_bottomleft		= light.position + 0.5f*(-light.width*light.side - light.length*light.up);
		float3 light_topleft		= light.position + 0.5f*( light.width*light.side - light.length*light.up);
		float3 light_topright		= light.position + 0.5f*( light.width*light.side + light.length*light.up);
		float3 light_bottomright	= light.position + 0.5f*(-light.width*light.side + light.length*light.up);

		//create programs for meshes
		std::string i_ptx( SampleScene::ptxpath("project", "triangle.cu"));
		Program bounds = _context->createProgramFromPTXFile(i_ptx, "bounds_triangle_mesh");
		Program intersect = _context->createProgramFromPTXFile(i_ptx, "intersect_triangle_mesh");

		Material diffuse_mat = _context->createMaterial();
		std::string mat_ptx( SampleScene::ptxpath( "project", "hit.cu" ) ); 
		Program diffuse_hit = _context->createProgramFromPTXFile( mat_ptx, "diffuse_closest_hit" );
		Program shadow_any_hit = _context->createProgramFromPTXFile( mat_ptx, "any_hit_shadow" );
		diffuse_mat->setClosestHitProgram(0, diffuse_hit);
		diffuse_mat->setAnyHitProgram(1, shadow_any_hit);

		Material emissive_mat = _context->createMaterial();
		mat_ptx = SampleScene::ptxpath( "project", "hit.cu" ) ; 
		Program emissive_hit = _context->createProgramFromPTXFile( mat_ptx, "emissive_closest_hit" );
		emissive_mat->setClosestHitProgram(0, emissive_hit);

		std::vector<GeometryInstance> gis;
		Primitives geomLoader(_context, intersect, bounds);

		PhongMaterial red = { make_float3(0.8f, 0.5f, 0.5f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
		PhongMaterial green = { make_float3(0.5f, 0.8f, 0.5f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
		PhongMaterial white = { make_float3(0.8f, 0.8f, 0.8f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };

		PhongMaterial lightmat = { make_float3(0.f, 0.f, 0.f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(1.f, 1.f, 1.f), 60.f, 0.f };


		//OBJECT-MESH

		PhongMaterial boxMat = { 
			make_float3(.5f, .5f, .5f), make_float3(0.0f, .03f, 0.f), make_float3(.1f, .1f, .1f), 25.f, 0.9f };
			diffuse_mat["mat"]->setUserData(sizeof(boxMat), &boxMat);

		std::string cow_path("C:/Users/mmanzi/Dropbox/Master Thesis/OPTIX/OptiX/SDK/Obj/sibenik/sibenik.obj");
		GeometryGroup cow_group = _context->createGeometryGroup();
		ObjLoader cow_loader(cow_path.c_str(), _context, cow_group, diffuse_mat, true);
		cow_loader.load();
		Acceleration cow_accel = cow_group->getAcceleration();
		//cow_accel->setBuilder( "SBvh" );
	
		Matrix4x4 cow_mat = Matrix4x4::translate(make_float3(0, 0, 0.));
		cow_mat *= Matrix4x4::scale(make_float3(10,10,10));
		Matrix4x4 cow_mat_inv = cow_mat.inverse();

		Transform cow_trans = _context->createTransform();
		cow_trans->setMatrix(false, cow_mat.getData(), cow_mat_inv.getData());
		cow_trans->setChild(cow_group);

		Group top = _context->createGroup();
		top->setChildCount(1);
		top->setChild(0, cow_trans);
	//	top->setChild(1, box_group);
		top->setAcceleration( _context->createAcceleration("NoAccel","NoAccel") );


		//set GeometryInstance as root of the scene-graph
		_context["top_shadower"]->set( top );


		//Light
		gis.push_back(geomLoader.createQuad(light_bottomleft, light_topleft, light_topright, light_bottomright));
		setMaterial(gis.back(), emissive_mat, lightmat);

		GeometryGroup light_group = _context->createGeometryGroup(gis.begin(), gis.end());
		light_group->setAcceleration( _context->createAcceleration("NoAccel","NoAccel") );

		Group top_shadow = _context->createGroup();
		top_shadow->setChildCount(2);
		top_shadow->setChild(0, cow_trans);
		top_shadow->setChild(1, light_group);
		top_shadow->setAcceleration( _context->createAcceleration("NoAccel","NoAccel") );


		_context["top_object"]->set( top_shadow );


	}
}