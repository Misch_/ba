
/*
 * Sphere Geometry primitive
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optix_world.h>

using namespace optix;

rtBuffer<float3> vertex_buffer;
//rtBuffer<float3> normal_buffer;
rtBuffer<uint3> index_buffer;
rtBuffer<float2> texcoord_buffer;

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );
rtDeclareVariable(float2, texcoord, attribute texcoord, ); 
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );


RT_PROGRAM void intersect_triangle_mesh(int primIdx)
{
	float3 n;
	float t, beta, gamma;
	uint3 vidx = index_buffer[primIdx];
	float3 e1 = vertex_buffer[vidx.x];
	float3 e2 = vertex_buffer[vidx.y];
	float3 e3 = vertex_buffer[vidx.z];
	if(intersect_triangle(ray, e1, e2, e3, n, t, beta, gamma))
		if(rtPotentialIntersection(t)){
			//make sure the normal looks into the viewer direction
			shading_normal = faceforward(-n, ray.direction, n);
			texcoord = (1-beta-gamma)*texcoord_buffer[vidx.x] + beta*texcoord_buffer[vidx.y] + gamma*texcoord_buffer[vidx.z];
			rtReportIntersection(0);
		}

}

RT_PROGRAM void bounds_triangle_mesh(int primIdx, float result[6])
{
	uint3 vidx = index_buffer[primIdx];
	float3 e1 = vertex_buffer[vidx.x];
	float3 e2 = vertex_buffer[vidx.y];
	float3 e3 = vertex_buffer[vidx.z];

	float max_x = max(e1.x, max(e2.x, e3.x));
	float max_y = max(e1.y, max(e2.y, e3.y));
	float max_z = max(e1.z, max(e2.z, e3.z));
	float min_x = min(e1.x, min(e2.x, e3.x));
	float min_y = min(e1.y, min(e2.y, e3.y));
	float min_z = min(e1.z, min(e2.z, e3.z));

	optix::Aabb* aabb = (optix::Aabb*)result;

	aabb->m_min = make_float3(min_x, min_y, min_z);
	aabb->m_max = make_float3(max_x, max_y, max_z);

}

