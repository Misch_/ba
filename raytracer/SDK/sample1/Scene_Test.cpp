#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "structs.h"
#include "sutil.h"
#include "SampleScene.h"
#include "ObjLoader.h"
#include "ImageLoader.h"
#include "scenes.h"

extern void Scenes::precomputation(AreaLight& a);


namespace Scenes{
	using namespace optix;

	void Test(optix::Context& _context){

		AreaLight light;
		light.color = make_float3(100.f, 100.f, 100.f);
		light.position = make_float3(278.0f, 530.0f, 279.5f);
		light.direction = make_float3(0.f, -1.0f, 0.0f);

		light.width = 120.f;
		light.length = 120.f;
		light.samples = 1;
		precomputation(light);
		_context["light"]->setUserData(sizeof(light), &light);

		//create programs for meshes
		std::string i_ptx( SampleScene::ptxpath("project", "sphere.cu"));
		Program bounds = _context->createProgramFromPTXFile(i_ptx, "bounds_sphere");
		Program intersect = _context->createProgramFromPTXFile(i_ptx, "intersect_sphere");

		Material mat = _context->createMaterial();
		std::string mat_ptx( SampleScene::ptxpath( "project", "hit.cu" ) ); 
		Program hit = _context->createProgramFromPTXFile( mat_ptx, "closest_hit_test" );
		mat->setClosestHitProgram(0, hit);

		std::vector<GeometryInstance> gis;
		Primitives geomLoader(_context, intersect, bounds);

		PhongMaterial white = { make_float3(1.f, 1.f, 1.f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f), 60.f, 0.f };

		///////////////////////////////////////////////////--BOX--////////////////////////////////////////////////////////////////////

		// Floor
		gis.push_back(geomLoader.createSphere(make_float3(0,0,0),150));
		setMaterial(gis.back(), mat, white);

		gis.back()->addMaterial(mat);
		gis.back()["mat"]->setUserData(sizeof(white), &white);


		// Create geometry group
		GeometryGroup geometry_group = _context->createGeometryGroup(gis.begin(), gis.end());
		geometry_group->setAcceleration( _context->createAcceleration("Bvh","Bvh") );
		_context["top_object"]->set( geometry_group );
	}
}