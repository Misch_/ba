#pragma once

#include <optix_world.h>
#include <optixu/optixpp_namespace.h>
#include <optix_math.h>
#include <string>
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Some constants... This is better style than using #defines
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const int CAMERA_RAY	= 0;
const int SHADOW_RAY	= 1;

const int POINT_LIGHT	= 0;
const int AREA_LIGHT	= 1;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Some structs used by both the device and the host to enhance readability of the code
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct PointLight{
	optix::float3 position;
	optix::float3 color;
	float emission;
//	int light_type = POINT_LIGHT;
//	optix::float3 normal = make_float3(0.f, 0.f, 0.f);
//	float size = 0;
};

struct AreaLight{

	optix::float3 position;
	optix::float3 direction;
	optix::float3 color;
	float width;
	float length;
	int samples;

	optix::float3 up;
	optix::float3 side;
};


struct PhongMaterial{
	optix::float3 diffuse;
	optix::float3 ambient;
	optix::float3 specular;
	optix::float3 emissive;
	float shininess;
	float glossiness; //value between 0 and 1, the higher the sharper are reflections (lambertian material has glossiness of zero) 
};

struct GlossyMaterial{
	optix::float3 tone;
	float angle;
};

struct RayPayload
{
	optix::float3 radiance;
	uint seed;
	//this params are needed for multi-bounce algorithms
	// these params should all be set in the hit programs and the recursion itself should be performed in the ray_gen program
	// this way, the recursion code is independent of the material that was hit
	bool recurse;			//is further recursion desired?
	bool shade;				//should this ray be shaded?
	float depth;			//depth of the recursion
	optix::float3 origin;	//origin of ray-to-be-traced
	optix::float3 direction;//direction of the ray-to-be-traced
	optix::float3 surfaceNormal;
	float weight;		//weight of the ray-to-be-recursed
	optix::float3 alpha;
	optix::float3 brdf;
};

struct ShadowRayPayload
{
	float attenuation;
	uint seed;
	float importance;
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Generic handler for arrays. By encapsulating the array we don't need to call delete [] explicitly.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
template <class T>
class Array{
public:
	T* array;
	Array(int size){
		array = new T[size];
	}
	~Array(){
		delete [] array;
	}

};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Helper-method declarations. Putting them here reduces dependencies through the code
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 



