
/*
 * Sphere Geometry primitive
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include "device_helpers.h"
#include "structs.h"
#include "random.h"
#include <optix_world.h>

using namespace optix;
rtDeclareVariable(float3, position, , );

rtDeclareVariable(float3, start_position, , );
rtDeclareVariable(float3, end_position, , );

rtDeclareVariable(float, radius, , );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );
rtDeclareVariable(float2, texcoord, attribute texcoord, ); 
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );


RT_PROGRAM void intersect_sphere(int primIdx)
{
	//we need to solve the quadratic equation t^2 + 2*<o,d>*t + <o,o> = r^2
	
	// this is solved by t{1,2} = (-b -+ sqrt(b^2 - 4ac))/2a 
	// with a = 1, b = 2*<o,d>, c = <o,o>-r^2
	// which simplifies in our case to t{1,2} = -b +-sqrt(b^2-4c)

	float3 O = ray.origin - position;
	float3 D = ray.direction;

	float b = dot(O, D);
	float c = dot(O, O) - radius*radius;

	float discr = b*b - c;

	if(discr>0.f){
		discr = sqrt(discr);
		float root = -b-discr;
		if(rtPotentialIntersection(root)){	//within valid range? eg here only invalid if <0
			shading_normal = normalize((O +root*D)/radius);
			texcoord = make_float2(0,0); //no texture support
			if(rtReportIntersection(0))
				return;
		}

		root = -b+discr;
		if(rtPotentialIntersection(root)){
			shading_normal = normalize((O +root*D)/radius);
			texcoord = make_float2(0,0);//no texture support
			rtReportIntersection(0);
		}
	}
}

RT_PROGRAM void bounds_sphere(int, float result[6])
{
	float3 rad = make_float3(radius);
	optix::Aabb* aabb = (optix::Aabb*)result;
	aabb->m_min = position - rad;
	aabb->m_max = position + rad;
}

rtDeclareVariable(RayPayload, payload, rtPayload, );
rtDeclareVariable(ShadowRayPayload, shadow_payload, rtPayload, );
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Moving sphere
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RT_PROGRAM void intersect_motionblurred_sphere(int primIdx)
{
	//pos is shifted by rnd value 
	
	float pos_rnd = ray.ray_type ==0 ? rnd(payload.seed): rnd(shadow_payload.seed);
	pos_rnd = pow(pos_rnd, 1.5f);
	float3 pos = (1.f-pos_rnd)*start_position+pos_rnd*end_position;
	float3 O = ray.origin - pos;
	float3 D = ray.direction;

	float b = dot(O, D);
	float c = dot(O, O) - radius*radius;

	float discr = b*b - c;

	if(discr>0.f){
		discr = sqrt(discr);
		float root = -b-discr;
		if(rtPotentialIntersection(root)){
			shading_normal = normalize((O +root*D)/radius);
			texcoord = make_float2(0,0); //no texture support
			if(rtReportIntersection(0))
				return;
		}

		root = -b+discr;
		if(rtPotentialIntersection(root)){
			shading_normal = normalize((O +root*D)/radius);
			texcoord = make_float2(0,0); //no texture support
			rtReportIntersection(0);
		}
	}
}

RT_PROGRAM void bounds_motionblurred_sphere(int, float result[6])
{
	float3 min_pos = make_float3(min(start_position.x, end_position.x),
								 min(start_position.y, end_position.y),
								 min(start_position.z, end_position.z)); 
	float3 max_pos = make_float3(max(start_position.x, end_position.x),
								 max(start_position.y, end_position.y),
								max(start_position.z, end_position.z)); 
	float3 rad = make_float3(radius);
	optix::Aabb* aabb = (optix::Aabb*)result;
	aabb->m_min = min_pos - rad;
	aabb->m_max = max_pos + rad;
}