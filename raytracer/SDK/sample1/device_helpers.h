/*
 *This header contains some helpfull methods for the device... 
*/
#pragma once
/*
#include <optix_math.h>
#include <optix.h>

rtDeclareVariable(uint, rnd_samples_size, , );
rtBuffer <float2, 2> rnd_samples_buffer;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This helper-method provides 2 random values of the interval [-0.5f, 0.5f]
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__device__ __inline__ float2 getRandomOffset(uint2 idx)
{
	uint2 norm_idx = make_uint2(idx.x%rnd_samples_size, idx.y%rnd_samples_size);
	return rnd_samples_buffer[norm_idx]; 
}

__device__ __inline__ float getRandomOffset1(uint2 idx)
{
	uint2 norm_idx = make_uint2((idx.x)&(rnd_samples_size-1), (idx.y)&(rnd_samples_size-1));
	float2 out = rnd_samples_buffer[norm_idx];
	return (out.x + out.y)*0.5f; 
}*/

//floats of range 0 - 255 to uchar
__device__ __inline__ uchar4 make_uchar4(float x, float y, float z, float w)
{
	uchar4 out;
	out.x = (unsigned char)(min(max(x, 0.f),1.f)*255);
	out.y = (unsigned char)(min(max(y, 0.f),1.f)*255);
	out.z = (unsigned char)(min(max(z, 0.f),1.f)*255);
	out.w = (unsigned char)(min(max(w, 0.f),1.f)*255);
	return out;
}

__device__ __inline__ uchar4 make_uchar4(float4 f)
{
	return make_uchar4(f.x, f.y, f.z, f.w);
}

__device__ __inline__ uchar4 make_uchar4(float3 f, float w)
{
	return make_uchar4(f.x, f.y, f.z, w);
}

__device__ __inline__ float4 make_float4(uchar4 c)
{
	float4 out;
	out.x = (float)c.x/255.f;
	out.y = (float)c.y/255.f;
	out.z = (float)c.z/255.f;
	out.w = (float)c.w/255.f;
	return out;
}

// Create ONB from normalalized vector
__device__ __inline__ void createONB( const optix::float3& n,
									 optix::float3& U,
									 optix::float3& V)
{
	using namespace optix;

	U = cross( n, make_float3( 0.0f, 1.0f, 0.0f ) );
	if ( dot(U, U) < 1.e-3f )
		U = cross( n, make_float3( 1.0f, 0.0f, 0.0f ) );
	U = normalize( U );
	V = cross( n, U );
}