#include "head.h"

VertexData::~VertexData(){
		delete [] VertexData::attrib;
}

GLuint VertexData::create(int numOfAttributes){
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);

	attrib = new Attribute[numOfAttributes];
	attNum = numOfAttributes;

	return vao;
}

void VertexData::storeAttribute(float *data, int elemSize, int elem, const char *name){

	attrib[actualIndex].name = name;
	attrib[actualIndex].elemSize = elemSize;
	attrib[actualIndex].numOfElem = elem;

	GLuint bon; // buffer object name
	glGenBuffers(1, &bon);
	glBindBuffer(GL_ARRAY_BUFFER, bon);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*elemSize*elem, data, GL_STATIC_DRAW);

	attrib[actualIndex].vbo = bon;

	actualIndex++;
}

void VertexData::storeIndices(GLuint *data, int size){
	glBindVertexArray(vao);
	GLuint bon;
	glGenBuffers(1, &bon);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bon);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}

void VertexData::bindToShader(GLuint id){
	
	glBindVertexArray(vao);

	for(int i=0; i<attNum; i++){
		glBindBuffer(GL_ARRAY_BUFFER,attrib[i].vbo);
		const GLint loc(glGetAttribLocation(id, attrib[i].name));
		if(loc<0)
			printf("The name %s is not an active Attribute in the Shader with the id %i\n", attrib[i].name, id);
		else{
			glVertexAttribPointer(loc,attrib[i].elemSize, GL_FLOAT, GL_TRUE, 0, NULL);
			glEnableVertexAttribArray(loc);
		}
	}
}



