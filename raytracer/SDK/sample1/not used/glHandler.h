#include "head.h"

/*
 *The glHandler has to be a singleton in order to work with member-functions...
 */
class glHandler{

private:
	static glHandler* instance; //reference to singleton
	int shaderID, vao, VertexArrayCount, width, height;
	GLuint texUnit[1];

	//its a singleton so it shall not the constructible from outside, nor shall it be copyable
	glHandler() {}    
	glHandler(const glHandler&) {}             
	glHandler& operator=(const glHandler&) {}  
	~glHandler() {}

	//setup a texture for openGL
	void setupTex(int unit, int id, GLvoid *img, char *name, int w, int h);
	//initialization of openGL
	void initGL(void* img);
	//displayfunction of openGL, this is repeated until exit
	void display();

	//glut cannot use callbacks with member-functions, so fix around this... 
	static void sDisplay(){
		instance->display();
	}

public:
	//singleton  creation (application of GoF-Singleton)
	static glHandler& getInstance(){
		if(instance==NULL)
			instance = new glHandler();
		return *instance;
	}
	//singleton release (never used atm)
	void release(){
		if(instance!=NULL)
			delete instance;
		instance=NULL;
	}

	//starts the GLUT-mainloop
	void initGLUT(int w, int h, void* img);
};

