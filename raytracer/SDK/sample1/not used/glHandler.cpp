#include "head.h"

//This assignment is absolutely mandatory to make the singleton pattern work properly, without it, we are screwed :-)
glHandler* glHandler::instance=NULL;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Sets up a texture for openGL
// @Params: 
// (unit) is the textureUnit, 
// (id) the texture_ID by which it is bound, 
// (img) the texture-data, 
// (name) the name of the uniform in the shader,
// (w), (h) width and height of image-data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void glHandler::setupTex(int unit, int id, GLvoid *img, char *name, int w, int h){
	glActiveTexture(GL_TEXTURE0+unit);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_FLOAT, img);
	glUniform1i(glGetUniformLocation(shaderID, name), unit);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// initialization of the openGL stuff.
// Maybe it would be better to separate some stuff into own methods for the sake of readability
// At the moment it does the following stuff:
// *initializing and compiling the shader
// *bind uniform and attributes to the shader
// *making the whole state accessible by a vertex array buffer
// @Params:
// (img) the input-data we want to visualize
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void glHandler::initGL(void* img){
	//create a shader, enable it and store its ID for later use. Path is not very generic ;-) fix this...
	Shader s;
	shaderID = s.create("C:/Users/Marco/Documents/Studium/Master Thesis/OptiX/SDK/project/trivial.vert", 
						"C:/Users/Marco/Documents/Studium/Master Thesis/OptiX/SDK/project/trivial.frag");
	s.enable();

	//setup vertexArray and TextureArray. Here both are trivial
	float S = 1.f;
	GLfloat vertices[] = { // in vec3 vert;
		-S,  S, 0.0, // xyz 
		-S, -S, 0.0, 
		 S,  S, 0.0,
		 S, -S, 0.0
	};
	VertexArrayCount=sizeof(vertices)/sizeof(GLfloat)/3; // 3 for {x y z}

	float R = 1.0f;
	GLfloat tex[] = {
		0.0, R,
		0.0, 0.0,
		R,   R,
		R,   0.0
	};
	size_t TexArray = sizeof(tex)/sizeof(GLfloat)/2;

	//setup vertex array buffer
	VertexData d;
	vao = d.create(2);
	d.storeAttribute(vertices, 3, VertexArrayCount, "vert");
	d.storeAttribute(tex, 2, TexArray, "tex");
	d.bindToShader(shaderID);

	//bind textures to opengl
	glGenTextures(1, texUnit);
	setupTex(0, texUnit[0], img, "img", width, height);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The display-function called by GLUTs main-loop
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void glHandler::display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLE_STRIP,0,VertexArrayCount);

	glutSwapBuffers();
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Function to set up GLUT with given parameters. 
// We rely on 'gl3w' to provide us with a openGL 3.3 core-profile context
// currently no key and reshape functions are bound to GLUT
// @Params:
// (w), (h) size of the window
// (img) input-data we want to visualize
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void glHandler::initGLUT(int w, int h, void* img){
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	width= w;
	height= h;
	glutInitWindowSize(w, h);
	glutCreateWindow("Optix Demo");

//	glutReshapeFunc(&reshape);
	glutDisplayFunc(&sDisplay);
//	glutSpecialFunc(&SpecialKeys);
//	glutKeyboardFunc(&KeyPressed);

	if (gl3wInit()) {
		fprintf(stderr, "failed to initialize OpenGL\n");
		return;
	}
	if (!gl3wIsSupported(3, 3)) {
		fprintf(stderr, "OpenGL 3.3 not supported\n");
		return;
	}
	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
		glGetString(GL_SHADING_LANGUAGE_VERSION));

	initGL(img);

	glutMainLoop();

//	cleanUp();
}
