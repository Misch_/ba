
/*
 * Sphere Geometry primitive
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optix_world.h>

using namespace optix;
rtDeclareVariable(float3, normal, , );
rtDeclareVariable(float, position, , );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );
rtDeclareVariable(float3, normal, attribute normal, ); 


RT_PROGRAM void intersect_sphere(int primIdx)
{
	
}

RT_PROGRAM void bounds_sphere(int, float result[6])
{
	float3 rad = make_float3(radius);
	optix::Aabb* aabb = (optix::Aabb*)result;
	aabb->m_min = position - rad;
	aabb->m_max = position + rad;
}

