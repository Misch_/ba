#pragma once

#include <optix_world.h>
#include <optixu/optixpp_namespace.h>
#include <optix_math.h>

using namespace optix;

class Primitives {
	
public:
	Context& _context;
	Program& _intersect;
	Program& _bounds;

	Primitives(Context& context, Program& intersect, Program& bounds): 
		_context(context), _intersect(intersect), _bounds(bounds){}

	GeometryInstance createSphere(float3 position, float radius);
	GeometryInstance createTriangleMesh(int num_triangles, int num_vertices, int3* index_data, float3* vertex_data, float2* texcoord_data = NULL  );
	GeometryInstance createMotionBlurredSphere(float3 start_position, float3 end_position, float radius);
	GeometryInstance createQuad(float3 bottomleft, float3 topleft, float3 topright, float3 bottomright );
	GeometryInstance createCylinder( float3 position, float height, float radius ); 

};
