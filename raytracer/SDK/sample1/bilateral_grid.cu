
/*
 * Implementation of the Bilateral Grid approach of Chen et al. 07
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optix_world.h>

using namespace optix;
rtBuffer <float3, 2>	output_indirect_buffer;	 //mean buffer from raytracer, here its he input...
rtBuffer <float3, 2>	output_filtered_buffer;	 //self-explaining...
rtBuffer <float4, 3>	bilateral_grid;  //bilateral grid with resolution resx/ss x resy/ss x 1/sr
rtBuffer <float4, 3>	bilateral_grid2; //tmp storage

rtBuffer <float, 1>		flt_ss; //rather than compute the gaussian coeffs on the fly we precompute them...
rtBuffer <float, 1>		flt_sr;

rtDeclareVariable(uint3, launch_index, rtLaunchIndex, );
rtDeclareVariable(uint3, launch_dim,   rtLaunchDim, );

rtDeclareVariable(float, ss, , "grid samplingdistance");
rtDeclareVariable(float, sr, , "1/intensitylevels");

rtDeclareVariable(uint, flt_ss_size, , "size of used ss filter");
rtDeclareVariable(uint, flt_sr_size, , "size of used sr filter");

/*
* Performed on 2D grid-resolution
* Initializes the grid with zeros
*/
RT_PROGRAM void init_grid()
{
	bilateral_grid[launch_index] = make_float4(0.f);
}

/*
* Performed on 2D input resultion.
* Fills up the bilateral_grid that has resolution resx/ss x resy/ss x 1/sr
*/
RT_PROGRAM void create_grid()
{
	float3 meanVal = output_indirect_buffer[make_uint2(launch_index.x, launch_index.y)];
	float illum =  0.299f * meanVal.x + 0.587f * meanVal.y + 0.114f * meanVal.z;

	uint x = uint(float(launch_index.x)/ss+0.5f);
	uint y = uint(float(launch_index.y)/ss+0.5f);
	uint z = min(1u, uint(illum/sr+0.5f)); //intensity can be bigger than one...

	bilateral_grid[make_uint3(x,y,z)] += make_float4(meanVal, 1);

}

/*
* Perfgormed on the 3D bilateral-grid resolution
* Performs separated 3D gaussian filtering (3 x 1D gaussians in one direction)
* The three functions must be called sequentially in order to prevent synchronization problems.
* TODO: TAKE CARE OF BORDERS!
*/
RT_PROGRAM void filter_grid_x()
{
	bilateral_grid2[launch_index]= make_float4(0.f);

	uint offset = flt_ss_size/2 - 1;
	uint3 idx = launch_index;
	idx.x -= offset;

	for(int i=0; i<flt_ss_size; i++){
		bilateral_grid2[launch_index] += flt_ss[i]* make_float4(bilateral_grid[idx].x, bilateral_grid[idx].y, bilateral_grid[idx].z, 1);
		idx.x++;
	}
}

RT_PROGRAM void filter_grid_y()
{
	bilateral_grid[launch_index]= make_float4(0.f);

	uint offset = flt_ss_size/2 - 1;
	uint3 idx = launch_index;
	idx.y -= offset;

	for(int i=0; i<flt_ss_size; i++){
		bilateral_grid[launch_index] += flt_ss[i]*make_float4(bilateral_grid2[idx].x, bilateral_grid2[idx].y, bilateral_grid2[idx].z, 1);
		idx.y++;
	}
}

RT_PROGRAM void filter_grid_z()
{
	bilateral_grid2[launch_index]= make_float4(0.f);

	uint offset = flt_sr_size/2 - 1;
	uint3 idx = launch_index;
	idx.z -= offset;

	for(int i=0; i<flt_sr_size; i++){
		bilateral_grid2[launch_index] += flt_sr[i]*make_float4(bilateral_grid[idx].x, bilateral_grid[idx].y, bilateral_grid[idx].z, 1);
		idx.z++;
	}
}

/*
* Performed on 2D image resolution
* Creates Trilinear filtered 2D slice
*TODO:TAKE CARE OF FILTERING ON BORDERS
*/
RT_PROGRAM void slice_grid()
{
	float3 meanVal = output_indirect_buffer[make_uint2(launch_index.x, launch_index.y)];
	float illum =  0.299f * meanVal.x + 0.587f * meanVal.y + 0.114f * meanVal.z;

	float x = float(launch_index.x)/ss;
	uint xi = uint(x);
	float diffx = x-float(xi);

	float y = float(launch_index.y)/ss;
	uint yi = uint(y);
	float diffy = y-float(yi);

	float z = min(0.999f, illum/sr);	//just so we dont fall out of the grid...
	uint zi = uint(z);
	float diffz = z-float(zi);

	//get the 8 neighbouring points
	float4 bbb = bilateral_grid2[make_uint3(xi,yi,zi)];
	float4 tbb = bilateral_grid2[make_uint3(xi+1,yi,zi)];
	float4 btb = bilateral_grid2[make_uint3(xi,yi+1,zi)];
	float4 bbt = bilateral_grid2[make_uint3(xi,yi,zi+1)];
	float4 ttb = bilateral_grid2[make_uint3(xi+1,yi+1,zi)];
	float4 tbt = bilateral_grid2[make_uint3(xi+1,yi,zi+1)];
	float4 btt = bilateral_grid2[make_uint3(xi,yi+1,zi+1)];
	float4 ttt = bilateral_grid2[make_uint3(xi+1,yi+1,zi+1)];

	//interpolate x
	float4 bb = (1.f-diffx)*bbb + diffx*tbb;
	float4 tb = (1.f-diffx)*btb + diffx*ttb;
	float4 bt = (1.f-diffx)*bbt + diffx*tbt;
	float4 tt = (1.f-diffx)*tbb + diffx*ttt;

	//interpolate y
	float4 b = (1.f-diffy)*bb + diffy*tb;
	float4 t = (1.f-diffy)*bt + diffy*tt;

	//interpolate z
	float4 data = (1.f-diffz)*b + diffz*t;

	//homogenious division
	data /= data.w;

	output_filtered_buffer[make_uint2(launch_index.x, launch_index.y)] = make_float3(data.x, data.y, data.z);
	
}
