#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include <ctime> 
#include <cstdlib>
#include "structs.h"
#include "windows.h"
#include "sutil.h"
#include "SampleScene.h"
#include "GLUTDisplay.h"
#include "Host_Constants.h"
#include "ObjLoader.h"
#include "ImageLoader.h"
#include "scenes.h"
#include "StatisticsGatherer.h"
#include "Filter.h"
#include "gui.h"
#include "ScreenImage.h"
#include <iostream>
#include <fstream>
#include <wtypes.h>
#include <winbase.h>

//the path you put the project into...
//#define PATH "Z:\Mastet Thesis\RealTimeRT\" 

using namespace optix;


class AdaptiveRenderer : public SampleScene{

private:

	StatisticsGatherer _statMeanGatherer, _statVarGatherer, _statOutGatherer;
	int _counter;
	int _init_seed;
	int _shadow_samples;
	int _budget;
	int _use_irradiance_caching;
	int _debug_counter;
	int  _framecounter;
	void createGeometry( InitialCameraData& camera);

public:

	AdaptiveRenderer(): _framecounter(3), _counter(0), _shadow_samples(1), _debug_counter(0), _budget(0), _statMeanGatherer(512, 512, 3), _statVarGatherer(512, 512, 1), _statOutGatherer(512,512,3){}
	
	void initScene( InitialCameraData& camera_data, int width, int height );
	Buffer getBuffer(std::string name);
	void doResize(unsigned int width, unsigned int height);
	void trace(const RayGenCameraData& camera_data);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialize the scene
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AdaptiveRenderer::initScene( InitialCameraData& camera_data, int width, int height )
{
	//initialize a new seed in each execution to avoid deterministic results
	srand((unsigned)time(NULL)); 	
	_init_seed = rand();

	//create the optix context and set up global states
	_context->setRayTypeCount(2);							//number of specified ray types (here light and shadow rays)
	_context->setEntryPointCount(2);						//its important that this matches to the number of entry points we specify later!
	_context->setStackSize(1000);							//really no clue about this... 
	_context->setExceptionEnabled(RT_EXCEPTION_ALL, true);	//enables exceptions in optix kernels, turn this off in final version!

	//get path of entry-point programs
	std::string path_to_ptx( ptxpath("sample1", "ray_gen.cu" ) ); //TODO: change the projects name, sample1 is ridiculous...

	//get path of the entry points
	Program ray_gen = _context->createProgramFromPTXFile(path_to_ptx, "pinhole_camera_init");				//entry point PINHOLE_CAMERA_INIT_ENTRY
	Program ray_gen_adaptive = _context->createProgramFromPTXFile(path_to_ptx, "pinhole_camera_adaptive");	//entry point PINHOLE_CAMERA_ADAPTIVE_ENTRY
	Program exception_progr = _context->createProgramFromPTXFile(path_to_ptx, "exception");					//exception program
	
	//add a ray generation programs (entry points) to the context
	_context->setRayGenerationProgram(PINHOLE_CAMERA_INIT_ENTRY, ray_gen);
	_context->setRayGenerationProgram(PINHOLE_CAMERA_ADAPTIVE_ENTRY, ray_gen_adaptive);

	//add exception programs to each entry point
	_context->setExceptionProgram(PINHOLE_CAMERA_INIT_ENTRY, exception_progr);
	_context->setExceptionProgram(PINHOLE_CAMERA_ADAPTIVE_ENTRY, exception_progr);

	//set miss program (called in case a ray does not hit anything)
	_context->setMissProgram( CAMERA_RAY, _context->createProgramFromPTXFile( ptxpath( "sample1", "hit.cu" ), "miss" ) );

	//create buffers for indirect light
	_context["output_buffer"]->set(createOutputBuffer( RT_FORMAT_FLOAT3, width, height)); 
	_context["output_var_buffer"]->set(createOutputBuffer(RT_FORMAT_FLOAT, width, height));
	_context["num_samples_buffer"]->set(createOutputBuffer(RT_FORMAT_FLOAT, width, height));
	_context["var_S"]->set(_context->createBuffer( RT_BUFFER_OUTPUT, RT_FORMAT_FLOAT3, width, height));

	_context["out_normal"]->set(createOutputBuffer(RT_FORMAT_FLOAT3, width, height));
	_context["out_brdf"]->set(createOutputBuffer(RT_FORMAT_FLOAT3, width, height));
	_context["out_position"]->set(createOutputBuffer(RT_FORMAT_FLOAT3, width, height));

	//set variables
	_budget = int(GUI::getSamplesPerFrame()*float(width)*float(height));			//in each frame we need to know how many new samples should be distributed in total 
	_context["sample_budget"]->setInt(_budget);
	_context["init_seed"]->setUint(_init_seed);										//pass a seed for the GPU rnd generator
	_context["draw_color"]->setFloat(make_float3(0.5f, 0.f, 0.4f));					//no clue...
	_context["local_offset"]->setUint(0, 0);										//no clue...
	_context["bad_color"]->setFloat(make_float3(1.0f, 0.5f, 0.f));					//color code in case an exception arises in the kernel
	_context["num_of_init_samples"]->setInt(GUI::getInitSamples());					//initial number of samples, must be >=2 or no sample variance can be computed
	_context["num_of_bounces"]->setInt(GUI::getMaxBounces());						//max number of indirect bounces for a light path
	_context["max_spp_frame"]->setInt(GUI::getMaxSSPFrame());						//max number of samples distributed per pixel per frame
	_context["denoiser_type"]->setInt(GUI::getDenoiserType());						//the denoiser type that is used

	//SAMPLINGMAP (shared resource with openGL) 
	unsigned int fbotex_id2 = *getFBOTexture(0);
	_shared_texture[0] = _context->createTextureSamplerFromGLImage(fbotex_id2, RT_TARGET_GL_TEXTURE_2D);
	_shared_texture[0]->setWrapMode(0, RT_WRAP_CLAMP_TO_EDGE);
	_shared_texture[0]->setWrapMode(1, RT_WRAP_CLAMP_TO_EDGE);
	_shared_texture[0]->setIndexingMode(RT_TEXTURE_INDEX_ARRAY_INDEX);
	_shared_texture[0]->setReadMode(RT_TEXTURE_READ_ELEMENT_TYPE);
	_shared_texture[0]->setMaxAnisotropy(1.0f);
	_shared_texture[0]->setFilteringModes(RT_FILTER_NEAREST, RT_FILTER_NEAREST, RT_FILTER_NONE);
	_context["samplingmap"]->setTextureSampler(_shared_texture[0]);

	//ENVOROMENT MAP
	std::string env_map_filename(_full_path+"SDK");
	//env_map_filename += "/images/CedarCity.hdr";
	env_map_filename += "/images/san_fran_sphere.ppm";
	_context["env_map"]->setTextureSampler( loadTexture( _context, env_map_filename, make_float3(0.5f, 0.5f, 0.5f)) );

	std::string b(_full_path+"SDK");
	_context["tex"]->setTextureSampler(loadTexture( _context, b+"/images/duck.ppm", make_float3(0.5f, 0.5f, 0.5f)));

	// Populate scene hierarchy
	createGeometry(camera_data);

	//for debugging activate printf in optix
	_context->setPrintEnabled(true);

	//validate and compile the context
	_context->validate();
	_context->compile();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Getter method for the output_buffer
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Buffer AdaptiveRenderer::getBuffer(std::string name)
{
	return _context[name]->getBuffer();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Changing size of all buffers that must match the viewport size. Called when the viewport size is changed...
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AdaptiveRenderer::doResize(unsigned int width, unsigned int height)
{
	_budget = int(GUI::getSamplesPerFrame()*float(width)*float(height));
	_context["sample_budget"]->setInt(_budget);
	_context["var_S"]->getBuffer()->setSize(width, height);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (Ray)Trace the scene
// remember: all optix calls are synchronous. This means they block the program until completion!
// This is usually bad in terms of performance, but in our case its a great relief: 
// We do not need to bother about openGL/optix synchronization! :-)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AdaptiveRenderer::trace( const RayGenCameraData& camera_data )
{
	//pass camera-data to optix
	_context["eye"]->setFloat( camera_data.eye );
	_context["U"]->setFloat( camera_data.U );
	_context["V"]->setFloat( camera_data.V );
	_context["W"]->setFloat( camera_data.W );

	//get size of render-target
	Buffer buffer = _context["output_buffer"]->getBuffer();
	RTsize buffer_width, buffer_height;
	buffer->getSize( buffer_width, buffer_height );
	
	//in each frame we need to know how many new samples should be distributed in total 
	_budget = int(GUI::getSamplesPerFrame()*float(buffer_width)*float(buffer_height));	

	//update variable params
	_context["sample_budget"]->setInt(_budget);		
	_context["init_seed"]->setUint(++_init_seed);										//increment seed to generate different rnd values in each frame	
	_context["num_of_init_samples"]->setInt(GUI::getInitSamples());						//number of samples per pixel in a initial pass (after size change or camera movement)
	_context["num_of_bounces"]->setInt(GUI::getMaxBounces());							//how many indirect bounces can a light path have in maximum?
	_context["max_ssp_frame"]->setInt(GUI::getMaxSSPFrame());							//what is the max number of samples we want to distribute per pixel per frame?
	_context["gamma_correction"]->setFloat(GUI::getGammaCorrection());					//sets the gamma correction of the scene (is it really needed??)
	_context["focal_plane"]->setFloat(GUI::getDOFfocalplane());							//sets the focal plane of the thin lens
	_context["lens_radius"]->setFloat(GUI::getDOFlensradius());							//sets the radius of the thin lens
	_context["tot_mse"]->setFloat(GUI::getTotalMSE());									//total MSE, needed to distribute samples in a meaningful way
	_context["do_nothing"]->setInt(0);													//flag, if any new samples should be distributed 

	//if a max number of progressive steps has been specified, stop refinement by changing the sample distribution flag as soon as the number of frames has been reached
	if(GUI::getStopFrame()>0 && GUI::getStopFrame()<=_framecounter)
		_context["do_nothing"]->setInt(1);

	//"moving" pass: should be as cheap as possible to be able to move through the scene seamlessly.
	// At the moment we simply use a low sample count, which leads to hyper noisy images
	// It would be cool to use simplified geometry and shading for this step (so that no noise arises)
	if(_is_moving){
		_context["num_of_init_samples"]->setInt(1);
		_context["num_of_bounces"]->setInt(0);
		_context->launch(PINHOLE_CAMERA_INIT_ENTRY, buffer_width, buffer_height);	
	}
	//initial pass: first frame after the camera stopped moving. Basically this is only shooting uniformly a fixed number of samples into the scene
	else if (_camera_changed){
		
			_context->launch(PINHOLE_CAMERA_INIT_ENTRY, buffer_width, buffer_height);
			_framecounter = 1;
			_camera_changed = false;	
			// Try to get some statistic data to make a reference image. 
			//_statOutGatherer.addImageToMean(_context["output_buffer"]->getBuffer()->get());
			//_statOutGatherer.writeToFile("testfile.txt");
		}
	//progressive refinement pass: Additional samples are distributed in a adaptive way into the scene
	else {
		if(GUI::getStopFrame()>_framecounter)
			_framecounter ++;
		_context->launch(PINHOLE_CAMERA_ADAPTIVE_ENTRY, buffer_width, buffer_height); 		
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Specify the scenes geometry
// All scene parameters, like initial camera position, geometry and light sources, must be defined directly in the Scene_XY.cpp files. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AdaptiveRenderer::createGeometry( InitialCameraData& camera)
{
	float3 eye, lookat, up;
	float vfov;

	//change this to whatever scene description you want to use

	CornellBox scene(_context, _full_path);
	//Conference scene(_context, _full_path);
	//Sponza scene(_context, _full_path); 

	scene.getCameraParameter(eye,lookat,up,vfov);

	camera.eye		= eye;
	camera.lookat	= lookat;
	camera.up		= up;
	camera.vfov		= vfov;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The main function
// IMPORTANT: Exit the application by closing the rendering window first. 
// Otherwise no cleanup operations are performed (and this usually crashes the video driver!). 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	GLUTDisplay::init( argc, argv );
	try {
		AdaptiveRenderer scene;
		GLUTDisplay::run( "TestScene", &scene, GLUTDisplay::CDProgressive );
	}catch(Exception &e){
		sutilReportError( e.getErrorString().c_str() );    
		exit(2);
	}
}
