#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "structs.h"
#include "sutil.h"
#include "SampleScene.h"
#include "ObjLoader.h"
#include "ImageLoader.h"
#include "scenes.h"


//HAS NEVER BEEN UPDATED TO NEW CODE!!!

/*
extern void Scenes::precomputation(AreaLight& a);

namespace Scenes{
	using namespace optix;

	void VenusTemple(optix::Context& _context){

		AreaLight light;
		light.color = make_float3(15.f, 15.f, 5.f);
		light.position = make_float3(278.0f, 548.7f, 279.5f);
		light.direction = make_float3(0.0f, -1.0f, 0.0f);

		light.length = 105.f; //side length
		light.width = 130.f; //side length
		light.samples = 1;
		precomputation(light);
		_context["light"]->setUserData(sizeof(light), &light);

		//compute rectangle that visualizes the lightsource
		float3 light_bottomleft		= light.position + 0.5f*(light.width*-light.side-light.length*light.up);
		float3 light_topleft		= light.position + 0.5f*(light.width*light.side-light.length*light.up);
		float3 light_topright		= light.position + 0.5f*(light.width*light.side+light.length*light.up);
		float3 light_bottomright	= light.position + 0.5f*(light.width*-light.side+light.length*light.up);


		//create programs for meshes
		std::string i_ptx( SampleScene::ptxpath("sample1", "triangle.cu"));
		Program bounds = _context->createProgramFromPTXFile(i_ptx, "bounds_triangle_mesh");
		Program intersect = _context->createProgramFromPTXFile(i_ptx, "intersect_triangle_mesh");


		std::string i_ptxSphere( SampleScene::ptxpath("sample1", "sphere.cu"));
		Program boundsSphere = _context->createProgramFromPTXFile(i_ptxSphere, "bounds_motionblurred_sphere");
		Program intersectSphere = _context->createProgramFromPTXFile(i_ptxSphere, "intersect_motionblurred_sphere");


		Material diffuse_mat = _context->createMaterial();
		std::string mat_ptx( SampleScene::ptxpath( "sample1", "hit.cu" ) ); 
		Program diffuse_hit = _context->createProgramFromPTXFile( mat_ptx, "diffuse_closest_hit" );
		Program shadow_any_hit = _context->createProgramFromPTXFile( mat_ptx, "any_hit_shadow" );
		diffuse_mat->setClosestHitProgram(0, diffuse_hit);
		diffuse_mat->setAnyHitProgram(1, shadow_any_hit);

		Material emissive_mat = _context->createMaterial();
		mat_ptx = SampleScene::ptxpath( "sample1", "hit.cu" ) ; 
		Program emissive_hit = _context->createProgramFromPTXFile( mat_ptx, "emissive_closest_hit" );
		emissive_mat->setClosestHitProgram(0, emissive_hit);

		std::vector<GeometryInstance> gis;
		Primitives geomLoader(_context, intersect, bounds);

		Primitives geomLoaderSphere(_context, intersectSphere, boundsSphere);

		PhongMaterial red = { make_float3(0.8f, 0.05f, 0.05f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
		PhongMaterial blue = { make_float3(0.05f, 0.8f, 0.8f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
		PhongMaterial green = { make_float3(0.05f, 0.8f, 0.05f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
		PhongMaterial white = { make_float3(0.8f, 0.8f, 0.8f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };

		PhongMaterial lightmat = { make_float3(0.f, 0.f, 0.f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(1.f, 1.f, 1.f), 60.f, 0.f };
		///////////////////////////////////////////////////--BOX--////////////////////////////////////////////////////////////////////

		gis.push_back(geomLoaderSphere.createMotionBlurredSphere(make_float3(170, 400, 290), make_float3(170, 200, 390), 100.f));
		setMaterial(gis.back(), diffuse_mat, red);

		gis.push_back(geomLoaderSphere.createMotionBlurredSphere(make_float3(430, 200, 330), make_float3(300, 200, 330), 80.f));
		setMaterial(gis.back(), diffuse_mat, blue);

		// Floor
		gis.push_back(geomLoader.createQuad(
			make_float3( 552.8f, 0.0f, 0.0f ),	make_float3( 0.0f, 0.0f, 0.0f ),
			make_float3( 0.0f, 0.0f, 559.2f ),	make_float3( 549.6f, 0.0f, 559.2f )));
		setMaterial(gis.back(), diffuse_mat, white);

		// Ceiling
		gis.push_back(geomLoader.createQuad(
			make_float3( 556.0f, 548.8f, 0.0f ),	make_float3( 556.8f, 548.8f, 559.2f ),
			make_float3( 0.0f, 548.8f, 559.2f ),	make_float3( 0.0f, 548.8f, 0.0f )));
		setMaterial(gis.back(), diffuse_mat, white);

		//Back Wall
		gis.push_back( geomLoader.createQuad( 
			make_float3(549.6f, 0.0f, 559.2f), make_float3(0.0f, 0.0f, 559.2f),
			make_float3(0.0f, 548.8f, 559.2f), make_float3(556.0f, 548.8f, 559.2f)));
		std::string texpath = std::string(sutilSamplesDir()) + "/images/duck.ppm";
		setMaterial(gis.back(), diffuse_mat, white);

		//Right Wall
		gis.push_back( geomLoader.createQuad( 
			make_float3(0.0f, 0.0f, 559.2f), make_float3(0.0f, 0.0f, 0.0f),
			make_float3(0.0f, 548.8f, 0.0f), make_float3(0.0f, 548.8f, 559.2f)));
		setMaterial(gis.back(), diffuse_mat, green);


		//Left Wall
		gis.push_back( geomLoader.createQuad( 
			make_float3(552.8f, 0.0f, 0.0f), make_float3(549.6f, 0.0f, 559.2f),
			make_float3(556.0f, 548.8f, 559.2f), make_float3(556.0f, 548.8f, 0.0)));
		setMaterial(gis.back(), diffuse_mat, red);


		// Create geometry group
		GeometryGroup shadow_group = _context->createGeometryGroup(gis.begin(), gis.end());
		shadow_group->setAcceleration( _context->createAcceleration("Bvh","Bvh") );
		_context["top_shadower"]->set( shadow_group );


		//Light
		gis.push_back(geomLoader.createQuad(light_bottomleft, light_topleft, light_topright, light_bottomright));
		setMaterial(gis.back(), emissive_mat, lightmat);

		// Create geometry group
		GeometryGroup geometry_group = _context->createGeometryGroup(gis.begin(), gis.end());
		geometry_group->setAcceleration( _context->createAcceleration("Bvh","Bvh") );
		_context["top_object"]->set( geometry_group );
	}
};*/