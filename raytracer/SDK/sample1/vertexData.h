/*	This class handles an Vertex Array Object. We can add Attribut-Arrays into the GPUs memory and bind them to a shader
*	
*
*/

#include "head.h"


struct Attribute{
	GLuint vbo;
	int elemSize;
	int numOfElem;
	const char *name;
};

class VertexData{
private:
	GLuint vao;
	int attNum;
	int actualIndex;
	Attribute *attrib;
public:
	VertexData(): vao(0), actualIndex(0), attrib(NULL), attNum(0){};
	~VertexData();
	GLuint create(int);
	void storeAttribute(float *, int, int, const char *);
	void storeIndices(GLuint *, int );
	void bindToShader(GLuint);
};