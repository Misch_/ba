#include <fstream>
#include "head.h"
using namespace std;

static GLubyte shaderText[MAX_SHADER_LENGTH];

GLuint Shader::create(const char *vert, const char *frag){
	
	GLuint vertShader;
	GLuint fragShader;
	GLuint ret = 0;
	GLint testVal;

	vertShader = glCreateShader(GL_VERTEX_SHADER);
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	//Lade Vertex Programm
	if(!loadShaderFile(vert, vertShader)){
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		printf("Shader at %s not found!", vert);
		return (GLuint)NULL;
	}

	//Lade Fragment Programm
	if(!loadShaderFile(frag,  fragShader)){
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		printf("Shader at %s not found!", frag);
		return (GLuint)NULL;
	}

	//Kompiliere die Shader
	glCompileShader(vertShader);
	glCompileShader(fragShader);

	//Vertex-Shader korrekt kompiliert?
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &testVal);
	if(testVal==GL_FALSE){
		char errMsg[1024];
		glGetShaderInfoLog(vertShader, 1024, NULL, errMsg);
		printf("Vertex Shader failed to compile:\n%s\n", errMsg);
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		return (GLuint)NULL;
	}

	//FragmentShader korrekt Kompiliert?
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &testVal);
	if(testVal==GL_FALSE){
		char errMsg[1024];
		glGetShaderInfoLog(fragShader, 1024, NULL, errMsg);
		printf("Fragment Shader failed to compile:\n%s\n", errMsg);
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		return (GLuint)NULL;
	}

	//Binde die Shader an das zu benutzende Programm
	ret = glCreateProgram();
	glAttachShader(ret, vertShader);
	glAttachShader(ret, fragShader);

/*	//(optionale) Argumentenliste abarbeiten...
	va_list attList;
	va_start(attList, frag);

	char *nextarg;
	int argcount = va_arg(attList, int);
	for(int i = 0; i<argcount; i++){
		int index = va_arg(attList, int);
		nextarg = va_arg(attList, char*);
		glBindAttribLocation(ret, index, nextarg);
	}
	va_end(attList);*/
	
	//link das ganze
	glLinkProgram(ret);

	//l�sche was nicht mehr gebraucht wird
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	//alles gut verlinkt?
	glGetProgramiv(ret, GL_LINK_STATUS, &testVal);
	if(testVal == GL_FALSE){
		char errMsg[1024];
		glGetProgramInfoLog(ret, 1024, NULL, errMsg);
		printf("Programm linking failed:\n%s\n", errMsg);
		glDeleteProgram(ret);
		return (GLuint)NULL;
	}
	Shader::id = ret;

	glGenVertexArrays(1,&(Shader::vao));
	glBindVertexArray(Shader::vao);

	return ret;
}

bool Shader::loadShaderFile(const char * file, GLuint id){
	GLint shaderLength = 0;
	FILE *fp;
	fp = fopen(file, "r");

	if(fp!=NULL){
		//l�nge des Files
		while(fgetc(fp) != EOF)
			shaderLength++;

		rewind(fp);
		if(shaderText != NULL)
			fread(shaderText, 1, shaderLength, fp);

		shaderText[shaderLength] = '\0';
		fclose(fp);
	} else
		return false;
	
	GLchar *strPrt[1];
	strPrt[0] = (GLchar *)((const char *) shaderText);
	glShaderSource(id, 1, (const char **)strPrt, NULL);

	return true;
}


void Shader::bindAttribute(float *data, int elem, const char *name){
	size_t numOfElements = sizeof(data)/sizeof(GLfloat)/elem;

	GLuint bon_vert; // buffer object name
	glGenBuffers(1,&bon_vert);
	glBindBuffer(GL_ARRAY_BUFFER,bon_vert);
	glBufferData(GL_ARRAY_BUFFER,sizeof(GLfloat)*elem*numOfElements,data,GL_STATIC_DRAW);
	const GLint loc_vert(glGetAttribLocation(Shader::id, name));

	glVertexAttribPointer(loc_vert,elem,GL_FLOAT,GL_TRUE,0,NULL);
	glEnableVertexAttribArray(loc_vert);
}
void Shader::disable(){
	glUseProgram(id);
}

void Shader::enable(){
	glUseProgram(id);
}