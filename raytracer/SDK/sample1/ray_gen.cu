
/*
 * Copyright (c) 2008 - 2009 NVIDIA Corporation.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property and proprietary
 * rights in and to this software, related documentation and any modifications thereto.
 * Any use, reproduction, disclosure or distribution of this software and related
 * documentation without an express license agreement from NVIDIA Corporation is strictly
 * prohibited.
 *
 * TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THIS SOFTWARE IS PROVIDED *AS IS*
 * AND NVIDIA AND ITS SUPPLIERS DISCLAIM ALL WARRANTIES, EITHER EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL NVIDIA OR ITS SUPPLIERS BE LIABLE FOR ANY
 * SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT
 * LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF
 * BUSINESS INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR
 * INABILITY TO USE THIS SOFTWARE, EVEN IF NVIDIA HAS BEEN ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGES
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include "structs.h"
#include "random.h"
#include "device_helpers.h"

#define DIRECT_AND_INDIRECT 0
#define DIRECT_ONLY 1
#define INDIRECT_ONLY 2


using namespace optix;

rtBuffer <float3,2>		output_buffer;
rtBuffer <float, 2>		output_var_buffer;
rtBuffer <float, 2>		num_samples_buffer;
rtBuffer <float3, 2>	var_S;
rtBuffer <float3, 2>    out_normal;
rtBuffer <float3, 2>	out_brdf;
rtBuffer <float3, 2>	out_position;

rtTextureSampler<float, 2>  samplingmap;

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );
rtDeclareVariable(uint2, launch_dim,   rtLaunchDim, );

rtDeclareVariable(unsigned int, init_seed, , );
rtDeclareVariable(float3,		draw_color, , );
rtDeclareVariable(uint2,		local_offset, , );
rtDeclareVariable(float3,       eye, , );
rtDeclareVariable(float3,       U, , );
rtDeclareVariable(float3,       V, , );
rtDeclareVariable(float3,       W, , );
rtDeclareVariable(float,		scene_epsilon, , )			= 0.001f;
rtDeclareVariable(rtObject,     top_object, , );
rtDeclareVariable( int,			samples_until_now, , )		= 0;
rtDeclareVariable( int,			sample_budget, , );
rtDeclareVariable( int,			max_ssp_frame, , )			= 16;
rtDeclareVariable( int,			num_of_init_samples, , )	= 4;
rtDeclareVariable( float,		tot_mse, , )				= 1.f;
rtDeclareVariable( int,			do_nothing, ,)				= 0;
rtDeclareVariable( float,		gamma_correction, , )		= 2.2f;
rtDeclareVariable(float,		focal_plane, , )			= 50.f;
rtDeclareVariable(float,		lens_radius, , )			= .2f;
rtDeclareVariable(int,		denoiser_type, , )			= 0;

__device__ __inline__ float illum(float3 &c)
{
	return 0.299f * c.x + 0.587f * c.y + 0.114f * c.z;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Generates a path of light
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__device__ __inline__ void  path_trace(unsigned int &seed, float3 &direct, float3 &indirect, float3 &brdf,  float3 &normal, float3 &position){

	//values too close to one can lead to rounding errors, so clamp them at 0.99
	float2 rnd_offset = make_float2(min(0.99f, rnd(seed)), min(0.99f, rnd(seed)));

	//construct primary ray
	float2 d = (make_float2(launch_index)+rnd_offset) / make_float2(launch_dim) * 2.f - 1.f;
	float3 ray_direction = normalize(d.x*U + d.y*V + W);
	float3 ray_origin = eye;

	//depth of filed effect...
	float2 rnd_lens_offset = make_float2(min(0.99f, rnd(seed)), min(0.99f, rnd(seed)));
	float3 p = eye + focal_plane * ray_direction; //point of focus
	//construct primary ray start point
	float2 d_l = rnd_lens_offset*2.f - 1.f;
	ray_origin = eye + lens_radius*d_l.x*U + lens_radius*d_l.y*V;
	ray_direction = normalize(p - ray_origin);

	float3 result_radiance = make_float3(0.f, 0.f, 0.f);

	//initialize rays payload
	RayPayload prd;
	prd.seed = seed;
	prd.recurse = TRUE;
	prd.alpha = make_float3(1,1,1);
	prd.depth = 0;
	indirect = make_float3(0.f, 0.f, 0.f);
	brdf = make_float3(0.f);

	//main loop for creating the path
	for(;;){
		optix::Ray ray(ray_origin, ray_direction, CAMERA_RAY, scene_epsilon );
		rtTrace(top_object, ray, prd);

		//russian roulette
		if(prd.depth>=2){
			float pcont = max(prd.alpha.x, max(prd.alpha.y, prd.alpha.z));
			if(rnd(prd.seed) >= pcont)
				break;
			prd.alpha /= pcont;
		}
		
		if(prd.depth==0){
			brdf = prd.brdf;
			normal = prd.surfaceNormal;
			position = prd.origin; //origin of path n+1 is hitpos of path n...
		}
			indirect += prd.radiance * prd.alpha;

		//stop if max recursion-depth reached
		if(prd.recurse){
			prd.shade = TRUE;
			prd.depth++;
			ray_origin = prd.origin;
			ray_direction = prd.direction;
		}else{
			break;
		}
	}
	//important detail: we want the next sample to use the actualized seed, not the old one!
	seed = prd.seed;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Computes the ouptut buffers. Variance is computed following the method of P.B.Welford 1962
// mean, var and ns store the buffers needed for the adaptive sampling. var_s is a helping buffer to simplify the variance computation
// x stores the radiance to be added to the mean & variance
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__device__ __inline__ void compute_output(float3 &mean, float &var, float &ns, float3 &var_s, float3 x)
{
		ns++;

		float3 old_M = mean;

		mean	= old_M + (x-old_M)/ns; 
		var_s	= ns==1 ? make_float3(0.f, 0.f, 0.f) : var_s + (x-old_M)*(x-mean);

		float3 variance = var_s/(ns-1); //do not use variance of mean
		var = illum(variance);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Computes how many samples to shoot in a pixel based on the samplingmap information and the total budget
// MSE must store MSE(Pixel)/MSE(Total).
// If ns is between two Integers N and N+1 use a rnd function to determine if N or N+1 samples should be shot!
// Take special care if MSE is zero (remember this should never happen in scene with global illumination, if it does, it means
// no good path was generated so far)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__device__ __inline__ int samples_to_shoot(float mse, unsigned int &seed)
{
	float cost	=	mse * sample_budget;
	int ns		=	cost;
	ns			+= (cost-ns > rnd(seed)) ? 1 : 0;
	return ns;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Initial sampling. n samples are shot in each pixel
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RT_PROGRAM void pinhole_camera_init()
{
	// ////////////////////////////////////
	// normalization of position buffer: //
	// ////////////////////////////////////
	// - CornellBox: factor = 500
	// - Sponza: factor = 85
	// - Conference: factor = 115
	// ////////////////////////////////////
	float normalizationFactor = 500;
	
	unsigned int seed = tea<32>(launch_dim.x*launch_index.y+launch_index.x, init_seed);
	
	//clear buffers
	output_buffer[launch_index]		=	make_float3(0.f, 0.f, 0.f); 
	var_S[launch_index]				=	make_float3(0.f, 0.f, 0.f);
	out_normal[launch_index]		=	make_float3(0.f,0.f,0.f);
	out_brdf[launch_index]			=	make_float3(0.f,0.f,0.f);
	out_position[launch_index]		=	make_float3(0.f,0.f,0.f);
	num_samples_buffer[launch_index]=	0;

	float3 direct, indirect, brdf, normal, position;

	//shoot 4 initial samples per pixel
	for(int i=0; i<num_of_init_samples; i++){

			path_trace(seed, direct, indirect, brdf, normal, position);	
		
			compute_output(	output_buffer[launch_index], 
							output_var_buffer[launch_index], 
							num_samples_buffer[launch_index],
							var_S[launch_index], 
							indirect);
			out_normal[launch_index] =	(normal+num_samples_buffer[launch_index]*out_normal[launch_index])/(num_samples_buffer[launch_index]+1);
			out_brdf[launch_index] =	(brdf + num_samples_buffer[launch_index]*out_brdf[launch_index])/(num_samples_buffer[launch_index]+1);
			


			out_position[launch_index] =	(position/normalizationFactor + num_samples_buffer[launch_index]*out_position[launch_index])/(num_samples_buffer[launch_index]+1);
	}
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Adaptive sampling. Shots samples accordingly to the samplingmap.
// To gauarantee a certain speed the number of spp is limited to 16
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

RT_PROGRAM void pinhole_camera_adaptive()
{
	// ////////////////////////////////////
	// normalization of position buffer: //
	// ////////////////////////////////////
	// - CornellBox: factor = 500
	// - Sponza: factor = 85
	// - Conference: factor = 115
	// ////////////////////////////////////
	float normalizationFactor = 500;

	//if stall-mode is selected return directly
	if(do_nothing)
		return;


	float3 direct, indirect, brdf, normal, position;
	unsigned int seed = tea<32>(launch_dim.x*launch_index.y+launch_index.x, init_seed);


	float dir_ss, irr_ss, brdf_ss;

	//determine num of samples to shoot (as fraction of total num of samples)
	float sts=0;

	//use different sampling strategies depending on the denoiser type that is used.
	if(denoiser_type==0)
		sts = 1.f/(launch_dim.x*launch_dim.y);								//non adaptive sampling (progressive uniform sampling)
	else if (denoiser_type==1)
		sts = (tex2D(samplingmap, launch_index.x, launch_index.y))/tot_mse;	//adaptive sampling
	

	//map float number of samples to integer number of samples to shoot
	
	int ns = samples_to_shoot(sts, seed);

	float c_brdf = 0;

	//loop over samples
	for(int i=0; i<ns; i++){
		
		//trace paths
		path_trace(seed, direct, indirect, brdf, normal, position);

		compute_output(	output_buffer[launch_index], 
						output_var_buffer[launch_index], 
						num_samples_buffer[launch_index],
						var_S[launch_index], 
						indirect);
		out_normal[launch_index] = (normal+num_samples_buffer[launch_index]*out_normal[launch_index])/(num_samples_buffer[launch_index]+1);
		out_brdf[launch_index] =	(brdf + num_samples_buffer[launch_index]*out_brdf[launch_index])/(num_samples_buffer[launch_index]+1);
		out_position[launch_index] =	(position/normalizationFactor + num_samples_buffer[launch_index]*out_position[launch_index])/(num_samples_buffer[launch_index]+1);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Exception Program.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rtDeclareVariable(float3,        bad_color, , );

RT_PROGRAM void exception()
{
  const unsigned int code = rtGetExceptionCode();
  rtPrintf( "Caught exception 0x%X at launch index (%d,%d)\n", code, launch_index.x, launch_index.y );
//  output_buffer[launch_index] = bad_color;
}

