
/*
 * Sphere Geometry primitive
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include "device_helpers.h"
#include "structs.h"
#include "random.h"
#include <optix_world.h>

using namespace optix;
rtDeclareVariable(float3, position, , );
rtDeclareVariable(float, radius, , );
rtDeclareVariable(float, height, , );

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

rtDeclareVariable(float2, texcoord, attribute texcoord, ); 
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );


RT_PROGRAM void intersect_cylinder(int primIdx)
{
	float3 O = ray.origin - position;
	float3 D = ray.direction;

	float zmax = height/2.f;
	float zmin = -zmax;

	float a = D.x*D.x + D.z*D.z;
	float b = 2.f*O.x*D.x + 2.f*O.z*D.z; 
	float c = O.x*O.x + O.z*O.z - radius*radius;

	float discr = b*b - 4.f*a*c;
	if(discr>0){
		float root = sqrtf(discr);
		float ITwoA = 1.f/(2.f*a);
		float t1 = ITwoA * (-b-root);;
		float3 hit1 = O+t1*D; 

		if( hit1.y<=zmax && hit1.y>=zmin)
			if(rtPotentialIntersection(t1)){		
				shading_normal = normalize((O +t1*D)/radius);
				shading_normal.y = 0.f;
				if(rtReportIntersection(0))
					return;
		}
		
		float t2 = ITwoA * (-b+root);
		float3 hit2 = O+t2*D;

		//special cases involving the top and bottom side
		if(hit1.y<=zmin && hit2.y>=zmin){ 
			float t3 = (zmin - O.y)/D.y;
			if(rtPotentialIntersection(t3)){
				shading_normal = make_float3(0.f, -1.f, 0.f);
				if(rtReportIntersection(0))
					return;
			}
		}

		if(hit1.y>=zmax && hit2.y<=zmax){ 
			float t3 = (zmax - O.y)/D.y;
			if(rtPotentialIntersection(t3)){
				shading_normal = make_float3(0.f, 1.f, 0.f);
				if(rtReportIntersection(0))
					return;
			}
		}

		//only case that hits second intersection (viewpoint inside cylinder)
		if(hit2.y<=zmax && hit2.y>=zmin)
			if(rtPotentialIntersection(t2)){
				shading_normal = normalize((O +t2*D)/radius);
				shading_normal.y = 0.f;
				rtReportIntersection(0);
		}
	}
}

RT_PROGRAM void bounds_cylinder(int, float result[6])
{
	float3 rad = make_float3(radius, height/2, radius);
	optix::Aabb* aabb = (optix::Aabb*)result;
	aabb->m_min = position - rad;
	aabb->m_max = position + rad;
}

