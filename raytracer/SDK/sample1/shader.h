#define MAX_SHADER_LENGTH 8192

class Shader {
private:
	int id;
	GLuint vao;
	bool loadShaderFile(const char *, GLuint);
public:
	Shader(): id(0), vao(0){};
	GLuint create(const char *, const char *);
	void bindAttribute(float *, int, const char *);

	GLuint getVAO();
	void enable();
	void disable();
};