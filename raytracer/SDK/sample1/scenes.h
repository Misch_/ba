#pragma once

#include <optix_world.h>
#include <optixu/optixpp_namespace.h>
#include <optix_math.h>
#include "structs.h"
#include "primitives.h"
#include <string>

/////////////////////////////////////////////////////////////////////////////////////////////////
// Base class for scene descriptions
/////////////////////////////////////////////////////////////////////////////////////////////////

class Scene{

public:
	void getCameraParameter(float3& eye, float3&lookat, float3& up, float& vfov){
		eye = _eye; lookat = _lookat; up = _up; vfov = _vfov;}
	//virtual functions: to be implemented in derived classes
	virtual void setGeometry(optix::Context& _context, std::string path) = 0;
	virtual void setCamera() = 0;

protected:

	float3 _eye, _lookat, _up;
	float _vfov;

	//simplifies setting new materials for geom. instances
	void setMaterial(GeometryInstance& gi, Material mat, PhongMaterial data,  TextureSampler tex = NULL){
		gi->addMaterial(mat);
		gi["mat"]->setUserData(sizeof(data), &data);
		if(tex!=NULL){
			gi["use_texture"]->setUint(1u);
			gi["tex"]->setTextureSampler(tex);
		}
	}

	//when a new area light source is defined or modified, call this method to update the state of the struct
	void preprocessAreaLight(AreaLight& a){
		a.direction = normalize(a.direction);
		a.up.z = a.direction.y;
		a.up.y = -a.direction.z;
		a.up.x = 0.f;
		a.up = normalize(a.up);
		a.side = cross(a.up, a.direction);
	}

	//helper for adding new lightsources
	void addLightSource(AreaLight& light, float3& bottomleft, float3& topleft, float3& topright, float3& bottomright){
			preprocessAreaLight(light);
			bottomleft	= light.position + 0.5f*(light.width*-light.side-light.length*light.up);
			topleft		= light.position + 0.5f*(light.width*light.side-light.length*light.up);
			topright	= light.position + 0.5f*(light.width*light.side+light.length*light.up);
			bottomright	= light.position + 0.5f*(light.width*-light.side+light.length*light.up);


	}

};

/////////////////////////////////////////////////////////////////////////////////////////////////
// Derived classes
/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////// 
class CornellBox: public Scene{
public:
	CornellBox(optix::Context& _context, std::string path) {setGeometry(_context, path); setCamera();}
	void setGeometry(optix::Context& _context, std::string path);
	void setCamera();
};

class Conference: public Scene{
public:
	Conference(optix::Context& _context, std::string path) {setGeometry(_context, path); setCamera();}
	void setGeometry(optix::Context& _context, std::string path);
	void setCamera();
};

class Sponza: public Scene{
public:
	Sponza(optix::Context& _context, std::string path) {setGeometry(_context, path); setCamera();}
	void setGeometry(optix::Context& _context, std::string path);
	void setCamera();
};

class VenusTemple: public Scene{ //not implemented 
public:
	VenusTemple(optix::Context& _context, std::string path) {setGeometry(_context, path); setCamera();}
	void setGeometry(optix::Context& _context, std::string path);
	void setCamera();
};

