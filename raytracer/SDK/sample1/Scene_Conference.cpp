#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "structs.h"
#include "sutil.h"
#include "SampleScene.h"
#include "ObjLoader.h"
#include "ImageLoader.h"
#include "scenes.h"



void Conference::setCamera(){
		//_eye	= make_float3( -38.6063f, 56.1289f, -41.7712f );			// eye
		//_lookat = 	make_float3( -36.1158f, 54.8914f, -40.6079f );			// lookat
		//_up		=	make_float3( 0.0f, 1.0f,  0.0f );				// up
		//_vfov	=	45.0f;

		_eye	= make_float3(-42.9749f, 58.9617f, -43.0737f );			// eye
		_lookat = 	make_float3( -40.2961f, 57.8241f, -42.2876f);			// lookat
		_up		=	make_float3( 0.0f, 1.0f,  0.0f );				// up
		_vfov	=	45.0f;
	}

	void Conference::setGeometry(optix::Context& _context, std::string path){

		//create programs for meshes
		std::string i_ptx( SampleScene::ptxpath("sample1", "triangle.cu"));
		Program bounds = _context->createProgramFromPTXFile(i_ptx, "bounds_triangle_mesh");
		Program intersect = _context->createProgramFromPTXFile(i_ptx, "intersect_triangle_mesh");

		Material diffuse_mat = _context->createMaterial();
		std::string mat_ptx( SampleScene::ptxpath( "sample1", "hit.cu" ) ); 
		Program diffuse_hit = _context->createProgramFromPTXFile( mat_ptx, "diffuse_closest_hit" );
		Program shadow_any_hit = _context->createProgramFromPTXFile( mat_ptx, "any_hit_shadow" );
		diffuse_mat->setClosestHitProgram(0, diffuse_hit);
		diffuse_mat->setAnyHitProgram(1, shadow_any_hit);


		Material emissive_mat = _context->createMaterial();
		mat_ptx = SampleScene::ptxpath( "sample1", "hit.cu" ) ; 
		Program emissive_hit = _context->createProgramFromPTXFile( mat_ptx, "emissive_closest_hit" );
		emissive_mat->setClosestHitProgram(0, emissive_hit);

		std::vector<GeometryInstance> gis;
		Primitives geomLoader(_context, intersect, bounds);

		PhongMaterial red = { make_float3(0.8f, 0.5f, 0.5f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
		PhongMaterial green = { make_float3(0.5f, 0.8f, 0.5f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
		PhongMaterial white = { make_float3(0.8f, 0.8f, 0.8f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
		PhongMaterial lightmat = { make_float3(0.f, 0.f, 0.f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(1.f, 1.f, 1.f), 60.f, 0.f };

		//OBJECT-MESH
		PhongMaterial boxMat = { 
			make_float3(.5f, .5f, .5f), make_float3(0.0f, .03f, 0.f), make_float3(.1f, .1f, .1f), 25.f, 0.9f };
			diffuse_mat["mat"]->setUserData(sizeof(boxMat), &boxMat);

			std::string cow_path(path+"SDK/Obj/conference/conference.obj"); //bad boy! no hard coded paths please....
			GeometryGroup cow_group = _context->createGeometryGroup();
			ObjLoader cow_loader(cow_path.c_str(), _context, cow_group, diffuse_mat, true);
			cow_loader.load();
			Acceleration cow_accel = cow_group->getAcceleration();
			//cow_accel->setBuilder( "SBvh" );

			Matrix4x4 cow_mat = Matrix4x4::translate(make_float3(0, 0, 0.));
			cow_mat *= Matrix4x4::scale(make_float3(.1,.1,.1));
			Matrix4x4 cow_mat_inv = cow_mat.inverse();

			Transform cow_trans = _context->createTransform();
			cow_trans->setMatrix(false, cow_mat.getData(), cow_mat_inv.getData());
			cow_trans->setChild(cow_group);

			Group top = _context->createGroup();
			top->setChildCount(1);
			top->setChild(0, cow_trans);
			//	top->setChild(1, box_group);
			top->setAcceleration( _context->createAcceleration("NoAccel","NoAccel") );


			//set GeometryInstance as root of the scene-graph
			_context["top_shadower"]->set( top );

		
			////////////////////////////////////////////////////--LightSources--////////////////////////////////////////////////////////////

			int nlights=1;

			AreaLight lights[] = { 
				{make_float3(40.f, 65.f, -13.f), make_float3(0.f, -1.f, 0.f), make_float3(10.f, 10.f, 10.f), 80.f, 10.f, 1 }		
			};

			//add light source visualization to optix geometry
			float3 light_bottomleft, light_topleft, light_topright, light_bottomright;
			for(int i=0; i<nlights;i++){
				addLightSource(lights[i], light_bottomleft, light_topleft, light_topright, light_bottomright);
				gis.push_back(geomLoader.createQuad(light_bottomleft, light_topleft, light_topright, light_bottomright));
				setMaterial(gis.back(), emissive_mat, lightmat);
			}

			//add lights to optix light source buffer
			Buffer light_buffer = _context->createBuffer(RT_BUFFER_INPUT);
			light_buffer->setFormat(RT_FORMAT_USER);
			light_buffer->setElementSize(sizeof(AreaLight));
			light_buffer->setSize(sizeof(lights)/sizeof(lights[0]));
			memcpy(light_buffer->map(), lights, sizeof(lights));
			light_buffer->unmap();
			_context["lights"]->set(light_buffer);
			_context["nlights"]->setInt(nlights);


			GeometryGroup light_group = _context->createGeometryGroup(gis.begin(), gis.end());
			light_group->setAcceleration( _context->createAcceleration("NoAccel","NoAccel") );

			Group top_shadow = _context->createGroup();
			top_shadow->setChildCount(2);
			top_shadow->setChild(0, cow_trans);
			top_shadow->setChild(1, light_group);
			top_shadow->setAcceleration( _context->createAcceleration("NoAccel","NoAccel") );
			_context["top_object"]->set( top_shadow );
	}