#pragma once

#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>
#include "primitives.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creates a sphere
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GeometryInstance Primitives::createSphere(float3 position, float radius)
{
	Geometry geom = _context->createGeometry();
	geom->setPrimitiveCount( 1u );
	geom->setBoundingBoxProgram( _bounds );
	geom->setIntersectionProgram( _intersect );
	//set variables of sphere
	geom["position"]->setFloat(position);
	geom["radius"]->setFloat(radius);

	GeometryInstance gi = _context->createGeometryInstance();
	gi->setGeometry(geom);
	return gi;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creates a moving sphere
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GeometryInstance Primitives::createMotionBlurredSphere(float3 start_position, float3 end_position, float radius)
{
	Geometry geom = _context->createGeometry();

	geom->setPrimitiveCount( 1u );
	geom->setBoundingBoxProgram( _bounds );
	geom->setIntersectionProgram( _intersect );
	//set variables of moving sphere
	geom["start_position"]->setFloat(start_position);
	geom["end_position"]->setFloat(end_position);
	geom["radius"]->setFloat(radius);

	GeometryInstance gi = _context->createGeometryInstance();
	gi->setGeometry(geom);
	return gi;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creates a cylinder
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GeometryInstance Primitives::createCylinder(float3 position, float height, float radius )
{
	Geometry geom = _context->createGeometry();
	geom->setPrimitiveCount(1u);
	geom->setBoundingBoxProgram( _bounds);
	geom->setIntersectionProgram(_intersect);
	
	geom["position"]->setFloat(position);
	geom["height"]->setFloat(height);
	geom["radius"]->setFloat(radius);

	GeometryInstance gi = _context->createGeometryInstance();
	gi->setGeometry(geom);
	return gi;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creates a triangle-mesh
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GeometryInstance Primitives::createTriangleMesh(int num_triangles, int num_vertices, int3* index_data, float3* vertex_data, float2* texcoord_data)
{
	Geometry geom = _context->createGeometry();
	geom->setPrimitiveCount( num_triangles );
	geom->setBoundingBoxProgram( _bounds );
	geom->setIntersectionProgram( _intersect );
	//create the buffers and map them to a array in memory
	Buffer vertex_buffer = _context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, num_vertices );
	float3 *vertex_buffer_data = static_cast<float3*>( vertex_buffer->map() );
	Buffer index_buffer = _context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_INT3, num_triangles );
	int3 *index_buffer_data = static_cast<int3*>( index_buffer->map() );
	//bind buffers to program
	geom["vertex_buffer"]->setBuffer(vertex_buffer);
	geom["index_buffer"]->setBuffer(index_buffer);
	//copy data into mapped array
	std::copy(vertex_data, vertex_data+num_vertices, vertex_buffer_data);
	std::copy(index_data, index_data+num_triangles, index_buffer_data);
	//unmap buffer
	vertex_buffer->unmap();
	index_buffer->unmap();

	if(texcoord_data!=NULL){
		Buffer texcoord_buffer = _context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT2, num_vertices );
		float2 *texcoord_buffer_data = static_cast<float2*>( texcoord_buffer->map() );
		geom["texcoord_buffer"]->setBuffer(texcoord_buffer);
		std::copy(texcoord_data, texcoord_data+num_vertices, texcoord_buffer_data);
		texcoord_buffer->unmap();
	}

	GeometryInstance gi = _context->createGeometryInstance();
	gi->setGeometry(geom);
	return gi;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creates a quad from 4 vertices. 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GeometryInstance Primitives::createQuad(float3 bottomleft, float3 topleft, float3 topright, float3 bottomright )
{
	Geometry geom = _context->createGeometry();
	int num_triangles = 2;
	int num_vertices = 4;
	float3 vertex_data[4] = {bottomleft, topleft, topright, bottomright} ;
	int3 index_data[2] = {make_int3(0, 1, 2), make_int3(0, 2, 3)};
	float2 tex_data[4] = {make_float2(0.f,0.f), make_float2(1.f, 0.f), make_float2(1.f, 1.f), make_float2(0.f, 1.f)};
	geom->setPrimitiveCount( num_triangles );
	geom->setBoundingBoxProgram( _bounds );
	geom->setIntersectionProgram( _intersect );

	//create the buffers and map them to a array in memory
	Buffer vertex_buffer = _context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, num_vertices );
	float3* vertex_buffer_data = static_cast<float3*>( vertex_buffer->map() );

	Buffer index_buffer = _context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_INT3, num_triangles );
	int3* index_buffer_data = static_cast<int3*>( index_buffer->map() );

	Buffer tex_buffer = _context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT2, num_vertices );
	float2* tex_buffer_data = static_cast<float2*>( tex_buffer->map() );

	//bind buffers to program
	geom["vertex_buffer"]->setBuffer(vertex_buffer);
	geom["index_buffer"]->setBuffer(index_buffer);
	geom["texcoord_buffer"]->setBuffer(tex_buffer);

	//copy data into mapped array
	std::copy(&vertex_data[0], &vertex_data[0]+num_vertices, vertex_buffer_data);
	std::copy(&index_data[0], &index_data[0]+num_triangles, index_buffer_data);
	std::copy(&tex_data[0], &tex_data[0]+num_vertices, tex_buffer_data);

	//unmap buffer
	vertex_buffer->unmap();
	index_buffer->unmap();
	tex_buffer->unmap();

	GeometryInstance gi = _context->createGeometryInstance();
	gi->setGeometry(geom);
	return gi;
}


