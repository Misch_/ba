#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "structs.h"
#include "sutil.h"
#include "SampleScene.h"
#include "ObjLoader.h"
#include "ImageLoader.h"
#include "scenes.h"
#include <string>

//extern void Scenes::precomputation(AreaLight& a);
void CornellBox::setCamera(){
	/*_eye	= make_float3( 278.0f, 273.0f, -800.0f );			// eye
	_lookat = 	make_float3( 278.0f, 273.0f,  0.0f );			// lookat
	_up		=	make_float3( 0.0f, 1.0f,  0.0f );				// up
	_vfov	=	45.0f;*/

	/*_eye	= make_float3( 413.0f, 336.0f, -472.0f );			// eye
	_lookat = 	make_float3( 278.0f, 273.0f,  314.0f );			// lookat
	_up		=	make_float3( 0.0f, 1.0f,  0.0f );				// up
	_vfov	=	45.0f;*/
	
	_eye	= make_float3(372.339f, 412.665f, -409.377f);			// eye
	_lookat = 	make_float3(369.817f, 273.579f, 378.449f);			// lookat
	_up		=	make_float3( 0.0f, 1.0f,  0.0f );				// up
	_vfov	=	45.0f;
}

void CornellBox::setGeometry(optix::Context& _context, std::string path){

	//create programs for meshes
	std::string i_ptx( SampleScene::ptxpath("sample1", "triangle.cu"));
	Program bounds = _context->createProgramFromPTXFile(i_ptx, "bounds_triangle_mesh");
	Program intersect = _context->createProgramFromPTXFile(i_ptx, "intersect_triangle_mesh");


	std::string i_ptxSphere( SampleScene::ptxpath("sample1", "sphere.cu"));
	Program boundsSphere = _context->createProgramFromPTXFile(i_ptxSphere, "bounds_motionblurred_sphere");
	Program intersectSphere = _context->createProgramFromPTXFile(i_ptxSphere, "intersect_motionblurred_sphere");


	Material diffuse_mat = _context->createMaterial();
	std::string mat_ptx( SampleScene::ptxpath( "sample1", "hit.cu" ) ); 
	Program diffuse_hit = _context->createProgramFromPTXFile( mat_ptx, "diffuse_closest_hit" );
	Program shadow_any_hit = _context->createProgramFromPTXFile( mat_ptx, "any_hit_shadow" );
	diffuse_mat->setClosestHitProgram(0, diffuse_hit);
	diffuse_mat->setAnyHitProgram(1, shadow_any_hit);

	Material emissive_mat = _context->createMaterial();
	mat_ptx = SampleScene::ptxpath( "sample1", "hit.cu" ) ; 
	Program emissive_hit = _context->createProgramFromPTXFile( mat_ptx, "emissive_closest_hit" );
	emissive_mat->setClosestHitProgram(0, emissive_hit);

	std::vector<GeometryInstance> gis;
	Primitives geomLoader(_context, intersect, bounds);

	Primitives geomLoaderSphere(_context, intersectSphere, boundsSphere);

	PhongMaterial red = { make_float3(0.8f, 0.05f, 0.05f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
	PhongMaterial green = { make_float3(0.05f, 0.8f, 0.05f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };
	PhongMaterial white = { make_float3(0.8f, 0.8f, 0.8f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(0.f, 0.f, 0.f), 60.f, 0.f };

	PhongMaterial lightmat = { make_float3(0.f, 0.f, 0.f),  make_float3(0.0f, 0.f, 0.f), make_float3(0.f, 0.f, 0.f),  make_float3(1.f, 1.f, 1.f), 60.f, 0.f };
	///////////////////////////////////////////////////--BOX--////////////////////////////////////////////////////////////////////

	gis.push_back(geomLoaderSphere.createMotionBlurredSphere(make_float3(140, 210, 200), make_float3(140, 210, 200), 50.f));
	setMaterial(gis.back(), diffuse_mat, red);
	
	// Test
	//gis.push_back( geomLoaderSphere.createSphere( make_float3(170, 300, 390), 50.f));
	//setMaterial(gis.back(), diffuse_mat, red);

	// Floor
	gis.push_back(geomLoader.createQuad(
		make_float3( 552.8f, 0.0f, 0.0f ),	make_float3( 0.0f, 0.0f, 0.0f ),
		make_float3( 0.0f, 0.0f, 559.2f ),	make_float3( 549.6f, 0.0f, 559.2f )));
	setMaterial(gis.back(), diffuse_mat, white);

	// Ceiling
	gis.push_back(geomLoader.createQuad(
		make_float3( 556.0f, 548.8f, 0.0f ),	make_float3( 556.8f, 548.8f, 559.2f ),
		make_float3( 0.0f, 548.8f, 559.2f ),	make_float3( 0.0f, 548.8f, 0.0f )));
	setMaterial(gis.back(), diffuse_mat, white);

	//Back Wall
	gis.push_back( geomLoader.createQuad( 
		make_float3(549.6f, 0.0f, 559.2f), make_float3(0.0f, 0.0f, 559.2f),
		make_float3(0.0f, 548.8f, 559.2f), make_float3(556.0f, 548.8f, 559.2f)));
	std::string texpath = std::string(sutilSamplesDir()) + "/images/duck.ppm";
	
	/* with duck*/
	setMaterial(gis.back(), diffuse_mat, white, loadTexture(_context, texpath, make_float3(0.f, 0.f, 0.f)));

	/* without duck */
	//setMaterial(gis.back(), diffuse_mat, white);

	//Right Wall
	gis.push_back( geomLoader.createQuad( 
		make_float3(0.0f, 0.0f, 559.2f), make_float3(0.0f, 0.0f, 0.0f),
		make_float3(0.0f, 548.8f, 0.0f), make_float3(0.0f, 548.8f, 559.2f)));
	setMaterial(gis.back(), diffuse_mat, green);


	//Left Wall
	gis.push_back( geomLoader.createQuad( 
		make_float3(552.8f, 0.0f, 0.0f), make_float3(549.6f, 0.0f, 559.2f),
		make_float3(556.0f, 548.8f, 559.2f), make_float3(556.0f, 548.8f, 0.0)));
	setMaterial(gis.back(), diffuse_mat, red);

	////////////////////////////////////////////////--SHORT BLOCK--////////////////////////////////////////////////////////////////
	gis.push_back( geomLoader.createQuad( 
		make_float3(130.0f, 165.0f, 65.0f), make_float3(82.0f, 165.0f, 225.0f),
		make_float3(240.0f, 165.0f, 272.0f), make_float3(290.0f, 165.0f, 114.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	gis.push_back( geomLoader.createQuad( 
		make_float3(290.0f, 0.0f, 114.0f), make_float3(290.0f, 165.0f, 114.0f),
		make_float3(240.0f, 165.0f, 272.0f), make_float3(240.0f, 0.0f, 272.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	gis.push_back( geomLoader.createQuad( 
		make_float3(130.0f, 0.0f, 65.0f), make_float3(130.0f, 165.0f, 65.0f),
		make_float3(290.0f, 165.0f, 114.0f), make_float3(290.0f, 0.0f, 114.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	gis.push_back( geomLoader.createQuad( 
		make_float3(82.0, 0.0f, 225.0f), make_float3(82.0f, 165.0f, 225.0f),
		make_float3(130.0f, 165.0f, 65.0f), make_float3(130.0f, 0.0f, 65.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	gis.push_back( geomLoader.createQuad( 
		make_float3(240.0f, 0.0f, 272.0f), make_float3(240.0f, 165.0f, 272.0f),
		make_float3(82.0f, 165.0f, 225.0f), make_float3(82.0f, 0.0f, 225.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	////////////////////////////////////////////////--TALL BLOCK--////////////////////////////////////////////////////////////////

	gis.push_back( geomLoader.createQuad( 
		make_float3(423.0f, 330.0f, 247.0f), make_float3(265.0f, 330.0f, 296.0f),
		make_float3(314.0f, 330.0f, 456.0f), make_float3(472.0f, 330.0f, 406.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	gis.push_back( geomLoader.createQuad( 
		make_float3(423.0f, 0.0f, 247.0f), make_float3(423.0f, 330.0f, 247.0f),
		make_float3(472.0f, 330.0f, 406.0f), make_float3(472.0f, 0.0f, 406.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	gis.push_back( geomLoader.createQuad( 
		make_float3(472.0f, 0.0f, 406.0f), make_float3(472.0f, 330.0f, 406.0f),
		make_float3(314.0f, 330.0f, 456.0f), make_float3(314.0f, 0.0f, 456.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	gis.push_back( geomLoader.createQuad( 
		make_float3(314.0, 0.0f, 456.0f), make_float3(314.0f, 330.0f, 456.0f),
		make_float3(265.0f, 330.0f, 296.0f), make_float3(265.0f, 0.0f, 296.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	gis.push_back( geomLoader.createQuad( 
		make_float3(265.0f, 0.0f, 296.0f), make_float3(265.0f, 330.0f, 296.0f),
		make_float3(423.0f, 330.0f, 247.0f), make_float3(423.0f, 0.0f, 247.0f)));
	setMaterial(gis.back(), diffuse_mat, white);

	// Create geometry group
	GeometryGroup shadow_group = _context->createGeometryGroup(gis.begin(), gis.end());
	shadow_group->setAcceleration( _context->createAcceleration("Bvh","Bvh") );
	_context["top_shadower"]->set( shadow_group );

	////////////////////////////////////////////////////--LightSources--////////////////////////////////////////////////////////////

	//define light parameters
	int nlights = 1;
	//position, direction, color, width,length,#samples
	AreaLight lights[] = {	
		{make_float3(278.0f, 548.7f, 279.5f),  make_float3(0.0f, -1.0f, 0.0f), make_float3(10.f, 10.f, 10.f),130.f, 105.f, 1},
		/* Mich�le:
			This was a second light source somewhat strangely positioned in the scene... o.O
			Away with it! AWAY WITH IT!
		*/
		//{make_float3(128.0f, 348.7f, 279.5f),  make_float3(1.0f, -1.0f, 0.0f), make_float3(0.f, 15.f, 0.f), 100.f, 105.f, 1}
	};

	//add light source visualization to optix geometry
	float3 light_bottomleft, light_topleft, light_topright, light_bottomright;
	for(int i=0; i<nlights;i++){
		addLightSource(lights[i], light_bottomleft, light_topleft, light_topright, light_bottomright);
		gis.push_back(geomLoader.createQuad(light_bottomleft, light_topleft, light_topright, light_bottomright));
		setMaterial(gis.back(), emissive_mat, lightmat);
	}



	//add lights to optix light source buffer
	Buffer light_buffer = _context->createBuffer(RT_BUFFER_INPUT);
	light_buffer->setFormat(RT_FORMAT_USER);
	light_buffer->setElementSize(sizeof(AreaLight));
	light_buffer->setSize(sizeof(lights)/sizeof(lights[0]));
	memcpy(light_buffer->map(), lights, sizeof(lights));
	light_buffer->unmap();
	_context["lights"]->set(light_buffer);
	_context["nlights"]->setInt(nlights);

	// Create geometry group
	GeometryGroup geometry_group = _context->createGeometryGroup(gis.begin(), gis.end());
	geometry_group->setAcceleration( _context->createAcceleration("Bvh","Bvh") );
	_context["top_object"]->set( geometry_group );

}
