
/*
 * Sphere Geometry primitive
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optix_world.h>

using namespace optix;
rtDeclareVariable(float3, position, , );
rtDeclareVariable(float, radius, , );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );
rtDeclareVariable(float3, normal, attribute normal, ); 


RT_PROGRAM void intersect_sphere(int primIdx)
{
	//we need to solve the quadratic equation t^2 + 2*<o,d>*t + <o,o> = r^2
	float3 O = position - ray.origin;
	float3 D = ray.direction;

	float b = 2*dot(O, D);
	float c = dot(O, O) - radius*radius;

	float discr = b*b - 4*c;

	if(discr>0.f){
		discr = sqrt(discr);
		float root = (-b-discr)*0.5f;

		if(rtPotentialIntersection(root)){
			normal = normalize((O +root*D)/radius);
			if(rtReportIntersection(0))
				return;
		}

		root = (-b+discr)*0.5f;
		if(rtPotentialIntersection(root)){
			normal = normalize((O +root*D)/radius);
			rtReportIntersection(0);
		}
	}
}

RT_PROGRAM void bounds_sphere(int, float result[6])
{
	float3 rad = make_float3(radius);
	optix::Aabb* aabb = (optix::Aabb*)result;
	aabb->m_min = position - rad;
	aabb->m_max = position + rad;
}

RT_PROGRAM void intersect_plane(int primIdx)
{

}

