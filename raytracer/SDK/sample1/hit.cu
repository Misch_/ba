
/*
 * Hit Programs
 */


#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include "structs.h"
#include "random.h"
#include "device_helpers.h"

using namespace optix;

rtBuffer<AreaLight> lights;

rtTextureSampler<float4, 2>     env_map;
rtTextureSampler<float4, 2>     tex;

rtDeclareVariable(uint2,			launch_index,	rtLaunchIndex, );
rtDeclareVariable(optix::Ray,		ray,			rtCurrentRay, );
rtDeclareVariable(RayPayload,		payload,		rtPayload, );
rtDeclareVariable(ShadowRayPayload, shadow_payload, rtPayload, );
rtDeclareVariable(float,			t_hit,			rtIntersectionDistance, );

rtDeclareVariable(float2,			texcoord,			attribute texcoord, ); 
rtDeclareVariable(float3,			geometric_normal,	attribute geometric_normal, );
rtDeclareVariable(float3,			shading_normal,		attribute shading_normal, );

rtDeclareVariable(int,				nlights, , );
rtDeclareVariable(int,				rot_size, ,);
rtDeclareVariable(uint,				use_texture, ,)				 = 0;
rtDeclareVariable(PhongMaterial,	mat, , );
rtDeclareVariable(GlossyMaterial,	glossymat, , );
rtDeclareVariable(float,			scene_epsilon, , )			= 0.001f;
rtDeclareVariable(rtObject,			top_object, , );
rtDeclareVariable(rtObject,			top_shadower, , );
rtDeclareVariable(int,				num_of_bounces, , )			= 6;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Miss program for primary rays. Sipmly fetch the colorvalue in the enviroment map
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RT_PROGRAM void miss()
{
/*
  float theta = atan2f( ray.direction.x, ray.direction.z );
  float phi   = M_PIf * 0.5f -  acosf( ray.direction.y );
  float u     = (theta + M_PIf) * (0.5f * M_1_PIf);
  float v     = 0.5f * ( 1.0f + sin(phi) );
  payload.radiance = clamp(make_float3( tex2D(env_map, u, v) ), 0.f, 1.f); //just in case its a hdr-map		
*/

	//env spheres
	float3 dir = normalize(ray.direction);
	float p = sqrtf(dir.x*dir.x + dir.y*dir.y + (dir.z-1.f)*(dir.z-1.f));
	float i2p = 1.f/(2.f*p);
	float u = (dir.x*i2p+0.5f);
	float v = (dir.y*i2p+0.5f);
	payload.radiance = clamp(make_float3( tex2D(env_map, u, v) ), 0.f, 1.f);
	payload.alpha *= clamp(make_float3( tex2D(env_map, u, v) ), 0.f, 1.f);
		
	//payload.radiance = make_float3(0.f,0.f,0.f);
	//payload.brdf = make_float3(0.f);
	payload.surfaceNormal = -ray.direction;
	payload.origin = make_float3(100000000000.f);//sweet
	payload.recurse = FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Called when a lightsource is hit. Lightsource-visualization itself should not have an impact on the pathtraycing, 
//so only do something usefull if it is hit by a primary ray.
//Visualizes lightsources by its emissive component and only in the correct direction (light has a front and a backside)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RT_PROGRAM void emissive_closest_hit()
{
	//AreaLight light = lights[0];

	float3 n_tmp = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, shading_normal));
	float3 n = faceforward( n_tmp, -ray.direction, n_tmp );
	payload.surfaceNormal = n;

	if(payload.depth==0)
		payload.radiance = dot(-ray.direction, n)<=0 ? make_float3(0.f,0.f,0.f) : mat.emissive;	
	else
		payload.radiance = make_float3(0.f);

	payload.alpha *= dot(-ray.direction, n)<=0 ? make_float3(0.f,0.f,0.f) : mat.emissive;
	payload.origin = make_float3(100000000000000.f);//sweet
	payload.recurse = FALSE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Closest hit program. Performs diffuse shading for path tracing
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RT_PROGRAM void diffuse_closest_hit()
{	
	//compute normal
	float3 n_tmp = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, shading_normal));
	float3 n = faceforward( n_tmp, -ray.direction, n_tmp );
	payload.surfaceNormal = n;

	//update the origin;
	float3 hit_pos = ray.origin + t_hit*ray.direction;
	payload.origin = hit_pos;

	//sample a new random direction with a cos-distribution over the hemisphere
	float z1= rnd(payload.seed);	
	float z2= rnd(payload.seed);
	float sintheta=sqrtf(z1);
	float costheta=sqrtf(1.f-z1);
	float phi=(2.f*M_PIf)*z2;
	float cosphi=cosf(phi)*sintheta; //cos(2*pi*rnd1)*sqrt(rnd2)
	float sinphi=sinf(phi)*sintheta; //sin(2*pi*rnd1)*sqrt(rnd2)
	float3 v1, v2;
	createONB(n, v1, v2); //orthonormal base (we already know n)
	payload.direction = n*costheta + v1*cosphi + v2*sinphi;

	//if a texture is used, use its color as diffuse color, otherwise use the materials default diffuse color
	float3 diffuse = use_texture ? make_float3(tex2D(tex, texcoord.x, texcoord.y)) : mat.diffuse;

	//in some cases we might decide only to compute a path-segment without any interest in actually shading this point.
	if(!payload.shade){
		payload.brdf = diffuse;
		return;
	}

	payload.radiance = make_float3(0.f, 0.f, 0.f);

	//select the light source we want to sample randomly...
	AreaLight light = lights[floor(rnd(payload.seed)*nlights)];

	float2 rndVal = (make_float2(rnd(payload.seed)-0.5f, rnd(payload.seed)-0.5f));
	float3 light_pos = light.position + light.length*rndVal.x*light.up + light.width*rndVal.y*light.side;	
	float light_dist = length(light_pos - hit_pos);				
	float3 light_vec = normalize(light_pos - hit_pos);
	float dot_hit = max(0.f, dot(light_vec, n));					//dot of light_vec and normal
	float dot_light = max(0.f, dot(-light_vec, light.direction));	//dot of light_vec and light_direction

	if(dot_hit>0.f && dot_light>0.f){
		ShadowRayPayload shadow_payload;
		shadow_payload.attenuation = 1.f;

		Ray shadow_ray(hit_pos, light_vec, SHADOW_RAY, scene_epsilon, light_dist);

		rtTrace(top_shadower, shadow_ray, shadow_payload); //recursion!

		if(shadow_payload.attenuation >0.f){
		//payload.radiance =  ;
			payload.radiance = light.color * ( dot_light * dot_hit * light.length * light.width / (M_PIf * light_dist*light_dist));
		}
	}

	payload.brdf = diffuse;
	payload.alpha *= diffuse ;

	//stop recursion if max number of bounces has been reached
	if(payload.depth>=num_of_bounces)
		payload.recurse = FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Trivial any hit program for shadow rays. Early ray termination should speed up things a bit
//If the hit object is a light source ignore. TEMPORARY SOLUTION, wont work if more than one lightsource is used...
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RT_PROGRAM void any_hit_shadow()
{
	shadow_payload.attenuation = 0;
	rtTerminateRay();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

//From here on: TESTPROGRAMS to create debug scenes like noisy circles ect. 
//(NOT USED ANYMORE, but leave it, it does not hurt and could become useful again...)
 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Miss program for primary rays. Sipmly fetch the colorvalue in the enviroment map
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RT_PROGRAM void miss_test()
{

	//s  = 0.5 + x, x e [-0.25, 0.25]
	float s = 0.25f + 0.5f*(rnd(payload.seed)-0.5f); 

	//	payload.radiance += make_float3(0.2f*sin(25*ray.direction.x));
	//	payload.radiance += make_float3(0.2f*(int(25*ray.direction.y)%3));
	payload.recurse = FALSE;
}

RT_PROGRAM void closest_hit_test()
{
	//diffuse = (0,0,0) + x, x e [0,1]
	payload.radiance = mat.diffuse * rnd(payload.seed) ;
	payload.recurse = FALSE;
}