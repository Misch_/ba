#version 150 core

out vec4 fragcolor;

in vec2 texCoord;

uniform sampler2D img;

void main(){

		fragcolor = texture2D(img, texCoord);

}