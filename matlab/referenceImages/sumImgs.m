clc;
close all;
run('../doImages')
numberOfImages = numberOfImages + 1
c = clock;  time = c(4)+c(5)*0.01+c(6)*1e-4

imgs_red{numberOfImages} = input(:,:,1);
imgs_green{numberOfImages} = input(:,:,2);
imgs_blue{numberOfImages} = input(:,:,3);

sum_r = sum(cat(3,imgs_red{:}),3); 
sum_g = sum(cat(3,imgs_green{:}),3); 
sum_b = sum(cat(3,imgs_blue{:}),3);

sum_all = zeros(512,512,3); 

sum_all(:,:,1) = sum_r; 
sum_all(:,:,2) = sum_g; 
sum_all(:,:,3) = sum_b;

avg = (sum_all ./ numberOfImages);
if (mod(numberOfImages,10) == 0)
    imtool(avg);
end