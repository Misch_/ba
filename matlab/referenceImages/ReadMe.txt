- Load the data of the so far computed images if there's any. If not, then only create a variable

	numberOfImages = 0;

- Let the ray tracer produce an image with a few samples, then hit '-' to dump the images
- run the script 'sumImgs.m' to add the dumped image to the already existing data and recompute the average image
- At the end save the following variables back to the file:-
- The output is always stored to avg

    imgs_blue
    imgs_green
    imgs_red
    nubmerOfImages
    avg (optionally)