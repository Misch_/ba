function [similar_white, others_white,orig_pixel_marked] = visualizeSimilarPixels(image,threshold,refPosX, refPosY)
%% 
% A function that compares the similarity of pixels in an image.
% Parameters:
% image:        the image that will be considered
%
% threshold:    the threshold which determines whether to pixels are similar
%               or not. A pixel is similar, if its euclidean distance to the reference
%               color is smaller than threshold.
% 
% positionOfReference:
%               the position of the pixel that will determine the reference
%               color with which all the other ones will be compared
%% Usage:
% load('castelVecchio')
% [similar,notsimilar,orig] = visualizeSimilarPixels(castelVecchio,0.27,250,130); imtool([orig,similar,notsimilar]);
% imtool([orig,similar,notSimilar]);


similar_white = image;
others_white = image;
orig_pixel_marked = image;

for i=-4:4
orig_pixel_marked(refPosY+i,refPosX,:) = [1 0 0];
orig_pixel_marked(refPosY+i,refPosX-1,:) = [1 0 0];
orig_pixel_marked(refPosY+i,refPosX+1,:) = [1 0 0];
orig_pixel_marked(refPosY,refPosX+i,:) = [1 0 0];
orig_pixel_marked(refPosY-1,refPosX+i,:) = [1 0 0];
orig_pixel_marked(refPosY+1,refPosX+i,:) = [1 0 0];
end
refCol = [image(refPosY,refPosX,1) image(refPosY,refPosX,2) image(refPosY,refPosX,3)];
for i = 1:size(image,1)
   for j = 1:size(image,2)
       testCol = image(i,j,:);
       testCol = [testCol(1,1,1) testCol(1,1,2) testCol(1,1,3)];
       difference = testCol - refCol;
       difference = sqrt(difference * difference');
       if(difference < threshold)
          similar_white(i,j,:) = [1 1 1]; 
       else
           others_white(i,j,:) = [1 1 1];
       end
   end 
end