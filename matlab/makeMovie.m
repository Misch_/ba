% This script can be used to produce the movies that were seen in the
% presentation of December 19, 2013.
clc
numberOfImages = 15;
movieImages = 2*(numberOfImages-1);
images = zeros(512,512,3,movieImages);
i = 0
for eps=0.01:0.01:(numberOfImages-1)/100
   i = i+1
   path = [strcat('..\raytracer\build\sample1\epsilon_shots_4spp_radius4\',num2str(eps,'%.2f'),'.tga')];
   images(:,:,:,i) = im2double(LoadTGA(path));
end

for eps=(numberOfImages/100):-0.01:0.02
   i = i+1
   path = [strcat('..\raytracer\build\sample1\epsilon_shots_4spp_radius4\',num2str(eps,'%.2f'),'.tga')];
   images(:,:,:,i) = im2double(LoadTGA(path));
end

mov = immovie(images);
movie2avi(mov,'test-movie3.avi','compression','None','FPS',20);