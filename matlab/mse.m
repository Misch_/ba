function [abs, rel]  = mse(image, reference)
% This function computes the absolute and the relative mean squared error
% between an image and a reference image.
%
% Input
%   image: An input image that will be compared to a reference
%   reference: The reference image
%
% Output
%   abs: the absolute MSE
%   rel: the relative MSE
sqrError = (image-reference).^2;
rel = mean(mean(mean(sqrError ./(reference.^2 + 1e-3))));
abs = mean(mean(mean(sqrError)));


% visualize rel mse
imtool(mean((((image-reference).^2)./(reference.^2 +1e-3)),3)* 100);
end