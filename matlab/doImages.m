% This script imports the images that were exported by the ray tracer

% import filtered images
filtered_gf = im2double(LoadTGA('..\raytracer\build\sample1\screenshot.tga'));
filtered_gem = im2double(LoadTGA('..\raytracer\build\sample1\screenshot_gem.tga'));

% import input
input = im2double(imread('..\raytracer\build\sample1\out.ppm')).^(1/3);

% feature buffers
normals = im2double(LoadTGA('..\raytracer\build\sample1\visualizedNormals.tga'));
position = im2double(LoadTGA('..\raytracer\build\sample1\out_p.tga'));
brdf = im2double(imread('..\raytracer\build\sample1\out_brdf.ppm'));