function out = boxfilterRGB( src, r )
out = zeros(size(src));

for i = 1:3
out(:,:,i) = boxfilter(src(:,:,i),r)./(2*r+1)^2;
end

imtool(out)
end

