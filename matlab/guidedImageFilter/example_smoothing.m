% Matlab code by He et al.
%
% example: edge-preserving smoothing
% figure 1 in our paper

close all;

%I = double(imread('.\img_smoothing\cat.bmp')) / 255;
% I = im2double(imread('..\raytracer\build\sample1\out.ppm')).^(1/3);
% I2 = im2double(imread('..\raytracer\build\sample1\out.ppm'));
I = photo2;

p = I;
% p = grayScale;

r = 8; % try r=2, 4, or 8
eps = 0.18^2; % try eps=0.1^2, 0.2^2, 0.4^2
q = zeros(size(I));

q(:, :, 1) = guidedfilter(I(:, :, 1), p(:, :, 1), r, eps);
q(:, :, 2) = guidedfilter(I(:, :, 2), p(:, :, 2), r, eps);
q(:, :, 3) = guidedfilter(I(:, :, 3), p(:, :, 3), r, eps);
% q = guidedfilter(I, p,r, eps);

%imtool([I,q])
imtool(q);