% example: multiple feature buffers

close all;

load('exampleInputMultipleFeatures');
I1 = brdf; I2 = normals; I3 = position; p = input;

width = size(I1,1);
height = size(I1,2);
dim = 6;

I = zeros(width,height,dim);
I(:,:,1:3) = I1;
I(:,:,4:6) = I2;
I(:,:,7:9) = I3;

r = 4;
eps = 0.04;

% Apply colored guided image filter on every input channel
q = zeros(size(I1));
q(:,:,1) = guidedfilter_color3(I,p(:,:,1),r,eps);
q(:,:,2) = guidedfilter_color3(I,p(:,:,2),r,eps);
q(:,:,3) = guidedfilter_color3(I,p(:,:,3),r,eps);

imtool([I1,I2,I3,p,q]);