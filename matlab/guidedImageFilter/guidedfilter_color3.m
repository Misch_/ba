function q = guidedfilter_color3(I, p, r, eps)
%   GUIDEDFILTER_COLOR   O(1) time implementation of guided filter using a color image as the guidance.
%
%   - guidance image: I (should be a color (RGB) image)
%   - filtering input image: p (should be a gray-scale/single channel image)
%   - local window radius: r
%   - regularization parameter: eps

[hei, wid] = size(p);
N = boxfilter(ones(hei, wid), r); % the size of each local patch; N=(2r+1)^2 except for boundary pixels.

mean_I_1 = boxfilter(I(:,:,1), r) ./ N;
mean_I_2 = boxfilter(I(:,:,2), r) ./ N;
mean_I_3 = boxfilter(I(:,:,3), r) ./ N;
mean_I_4 = boxfilter(I(:,:,4), r) ./ N;
mean_I_5 = boxfilter(I(:,:,5), r) ./ N;
mean_I_6 = boxfilter(I(:,:,6), r) ./ N;
mean_I_7 = boxfilter(I(:,:,7), r) ./ N;
mean_I_8 = boxfilter(I(:,:,8), r) ./ N;
mean_I_9 = boxfilter(I(:,:,9), r) ./ N;


mean_p = boxfilter(p, r) ./ N;

mean_Ip_1 = boxfilter(I(:, :, 1).*p, r) ./N;
mean_Ip_2 = boxfilter(I(:, :, 2).*p, r) ./N;
mean_Ip_3 = boxfilter(I(:, :, 3).*p, r) ./N;
mean_Ip_4 = boxfilter(I(:, :, 4).*p, r) ./N;
mean_Ip_5 = boxfilter(I(:, :, 5).*p, r) ./N;
mean_Ip_6 = boxfilter(I(:, :, 6).*p, r) ./N;
mean_Ip_7 = boxfilter(I(:, :, 7).*p, r) ./N;
mean_Ip_8 = boxfilter(I(:, :, 8).*p, r) ./N;
mean_Ip_9 = boxfilter(I(:, :, 9).*p, r) ./N;

% covariance of (I, p) in each local patch.
cov_Ip_1 = mean_Ip_1 - mean_I_1 .* mean_p;
cov_Ip_2 = mean_Ip_2 - mean_I_2 .* mean_p;
cov_Ip_3 = mean_Ip_3 - mean_I_3 .* mean_p;
cov_Ip_4 = mean_Ip_4 - mean_I_4 .* mean_p;
cov_Ip_5 = mean_Ip_5 - mean_I_5 .* mean_p;
cov_Ip_6 = mean_Ip_6 - mean_I_6 .* mean_p;
cov_Ip_7 = mean_Ip_7 - mean_I_7 .* mean_p;
cov_Ip_8 = mean_Ip_8 - mean_I_8 .* mean_p;
cov_Ip_9 = mean_Ip_9 - mean_I_9 .* mean_p;

% Note the variance in each local patch is a 6x6 symmetric matrix:
%            11, 12, 13, 14, 15, 16
%            21, 22, 23, 24, 25, 26
%Sigma =     31, 32, 33, 34, 35, 36
%            41, 42, 43, 44, 45, 46
%            51, 52, 53, 54, 55, 56
%            61, 62, 63, 64, 65, 66
var_I_11 = boxfilter(I(:,:,1).*I(:,:,1),r) ./ N - mean_I_1 .* mean_I_1;
var_I_12 = boxfilter(I(:,:,1).*I(:,:,2),r) ./ N - mean_I_1 .* mean_I_2;
var_I_13 = boxfilter(I(:,:,1).*I(:,:,3),r) ./ N - mean_I_1 .* mean_I_3;
var_I_14 = boxfilter(I(:,:,1).*I(:,:,4),r) ./ N - mean_I_1 .* mean_I_4;
var_I_15 = boxfilter(I(:,:,1).*I(:,:,5),r) ./ N - mean_I_1 .* mean_I_5;
var_I_16 = boxfilter(I(:,:,1).*I(:,:,6),r) ./ N - mean_I_1 .* mean_I_6;
var_I_17 = boxfilter(I(:,:,1).*I(:,:,7),r) ./ N - mean_I_1 .* mean_I_7;
var_I_18 = boxfilter(I(:,:,1).*I(:,:,8),r) ./ N - mean_I_1 .* mean_I_8;
var_I_19 = boxfilter(I(:,:,1).*I(:,:,9),r) ./ N - mean_I_1 .* mean_I_9;

var_I_22 = boxfilter(I(:,:,2).*I(:,:,2),r) ./ N - mean_I_2 .* mean_I_2;
var_I_23 = boxfilter(I(:,:,2).*I(:,:,3),r) ./ N - mean_I_2 .* mean_I_3;
var_I_24 = boxfilter(I(:,:,2).*I(:,:,4),r) ./ N - mean_I_2 .* mean_I_4;
var_I_25 = boxfilter(I(:,:,2).*I(:,:,5),r) ./ N - mean_I_2 .* mean_I_5;
var_I_26 = boxfilter(I(:,:,2).*I(:,:,6),r) ./ N - mean_I_2 .* mean_I_6;
var_I_27 = boxfilter(I(:,:,2).*I(:,:,7),r) ./ N - mean_I_2 .* mean_I_7;
var_I_28 = boxfilter(I(:,:,2).*I(:,:,8),r) ./ N - mean_I_2 .* mean_I_8;
var_I_29 = boxfilter(I(:,:,2).*I(:,:,9),r) ./ N - mean_I_2 .* mean_I_9;

var_I_33 = boxfilter(I(:,:,3).*I(:,:,3),r) ./ N - mean_I_3 .* mean_I_3;
var_I_34 = boxfilter(I(:,:,3).*I(:,:,4),r) ./ N - mean_I_3 .* mean_I_4;
var_I_35 = boxfilter(I(:,:,3).*I(:,:,5),r) ./ N - mean_I_3 .* mean_I_5;
var_I_36 = boxfilter(I(:,:,3).*I(:,:,6),r) ./ N - mean_I_3 .* mean_I_6;
var_I_37 = boxfilter(I(:,:,3).*I(:,:,7),r) ./ N - mean_I_3 .* mean_I_7;
var_I_38 = boxfilter(I(:,:,3).*I(:,:,8),r) ./ N - mean_I_3 .* mean_I_8;
var_I_39 = boxfilter(I(:,:,3).*I(:,:,9),r) ./ N - mean_I_3 .* mean_I_9;

var_I_44 = boxfilter(I(:,:,4).*I(:,:,4),r) ./ N - mean_I_4 .* mean_I_4;
var_I_45 = boxfilter(I(:,:,4).*I(:,:,5),r) ./ N - mean_I_4 .* mean_I_5;
var_I_46 = boxfilter(I(:,:,4).*I(:,:,6),r) ./ N - mean_I_4 .* mean_I_6;
var_I_47 = boxfilter(I(:,:,4).*I(:,:,7),r) ./ N - mean_I_4 .* mean_I_7;
var_I_48 = boxfilter(I(:,:,4).*I(:,:,8),r) ./ N - mean_I_4 .* mean_I_8;
var_I_49 = boxfilter(I(:,:,4).*I(:,:,9),r) ./ N - mean_I_4 .* mean_I_9;

var_I_55 = boxfilter(I(:,:,5).*I(:,:,5),r) ./ N - mean_I_5 .* mean_I_5;
var_I_56 = boxfilter(I(:,:,5).*I(:,:,6),r) ./ N - mean_I_5 .* mean_I_6;
var_I_57 = boxfilter(I(:,:,5).*I(:,:,7),r) ./ N - mean_I_5 .* mean_I_7;
var_I_58 = boxfilter(I(:,:,5).*I(:,:,8),r) ./ N - mean_I_5 .* mean_I_8;
var_I_59 = boxfilter(I(:,:,5).*I(:,:,9),r) ./ N - mean_I_5 .* mean_I_9;

var_I_66 = boxfilter(I(:,:,6).*I(:,:,6),r) ./ N - mean_I_6 .* mean_I_6;
var_I_67 = boxfilter(I(:,:,6).*I(:,:,7),r) ./ N - mean_I_6 .* mean_I_7;
var_I_68 = boxfilter(I(:,:,6).*I(:,:,8),r) ./ N - mean_I_6 .* mean_I_8;
var_I_69 = boxfilter(I(:,:,6).*I(:,:,9),r) ./ N - mean_I_6 .* mean_I_9;

var_I_77 = boxfilter(I(:,:,7).*I(:,:,7),r) ./ N - mean_I_7 .* mean_I_7;
var_I_78 = boxfilter(I(:,:,7).*I(:,:,8),r) ./ N - mean_I_7 .* mean_I_8;
var_I_79 = boxfilter(I(:,:,7).*I(:,:,9),r) ./ N - mean_I_7 .* mean_I_9;

var_I_88 = boxfilter(I(:,:,8).*I(:,:,8),r) ./ N - mean_I_8 .* mean_I_8;
var_I_89 = boxfilter(I(:,:,8).*I(:,:,9),r) ./ N - mean_I_8 .* mean_I_9;

var_I_99 = boxfilter(I(:,:,9).*I(:,:,9),r) ./ N - mean_I_9 .* mean_I_9;

a = zeros(hei, wid, 9);

for y=1:hei
   for x=1:wid               
      Sigma =  [var_I_11(y,x), var_I_12(y,x), var_I_13(y,x), var_I_14(y,x), var_I_15(y,x), var_I_16(y,x), var_I_17(y,x), var_I_18(y,x), var_I_19(y,x);
                var_I_12(y,x), var_I_22(y,x), var_I_23(y,x), var_I_24(y,x), var_I_25(y,x), var_I_26(y,x), var_I_27(y,x), var_I_28(y,x), var_I_29(y,x);
                var_I_13(y,x), var_I_23(y,x), var_I_33(y,x), var_I_34(y,x), var_I_35(y,x), var_I_36(y,x), var_I_37(y,x), var_I_38(y,x), var_I_39(y,x);
                var_I_14(y,x), var_I_24(y,x), var_I_34(y,x), var_I_44(y,x), var_I_45(y,x), var_I_46(y,x), var_I_47(y,x), var_I_48(y,x), var_I_49(y,x);
                var_I_15(y,x), var_I_25(y,x), var_I_35(y,x), var_I_45(y,x), var_I_55(y,x), var_I_56(y,x), var_I_57(y,x), var_I_58(y,x), var_I_59(y,x);
                var_I_16(y,x), var_I_26(y,x), var_I_36(y,x), var_I_46(y,x), var_I_56(y,x), var_I_66(y,x), var_I_67(y,x), var_I_68(y,x), var_I_69(y,x);
                var_I_17(y,x), var_I_27(y,x), var_I_37(y,x), var_I_47(y,x), var_I_57(y,x), var_I_67(y,x), var_I_77(y,x), var_I_78(y,x), var_I_79(y,x);
                var_I_18(y,x), var_I_28(y,x), var_I_38(y,x), var_I_48(y,x), var_I_58(y,x), var_I_68(y,x), var_I_78(y,x), var_I_88(y,x), var_I_89(y,x);
                var_I_19(y,x), var_I_29(y,x), var_I_39(y,x), var_I_49(y,x), var_I_59(y,x), var_I_69(y,x), var_I_79(y,x), var_I_89(y,x), var_I_99(y,x)];
      
       cov_Ip = [cov_Ip_1(y,x), cov_Ip_2(y,x), cov_Ip_3(y,x), cov_Ip_4(y,x), cov_Ip_5(y,x), cov_Ip_6(y,x), cov_Ip_7(y,x), cov_Ip_8(y,x), cov_Ip_9(y,x)];
       a(y, x, :) = cov_Ip * inv(Sigma + eps * eye(9)); % Eqn. (14) in the paper;
   end
   
end

b = mean_p - a(:,:,1) .* mean_I_1 - a(:,:,2) .* mean_I_2 - a(:,:,3) .* mean_I_3 - a(:,:,4) .* mean_I_4 - a(:,:,5) .* mean_I_5 - a(:,:,6) .* mean_I_6 - a(:,:,7) .* mean_I_7 - a(:,:,8) .* mean_I_8 - a(:,:,9) .* mean_I_9;

q = (boxfilter(a(:, :, 1), r).* I(:, :, 1)...
+ boxfilter(a(:, :, 2), r).* I(:, :, 2)...
+ boxfilter(a(:, :, 3), r).* I(:, :, 3)...
+ boxfilter(a(:, :, 4), r).* I(:, :, 4)...
+ boxfilter(a(:, :, 5), r).* I(:, :, 5)...
+ boxfilter(a(:, :, 6), r).* I(:, :, 6)...
+ boxfilter(a(:, :, 7), r).* I(:, :, 7)...
+ boxfilter(a(:, :, 8), r).* I(:, :, 8)...
+ boxfilter(a(:, :, 9), r).* I(:, :, 9)...
+ boxfilter(b, r)) ./ N;  % Eqn. (16) in the paper;
end