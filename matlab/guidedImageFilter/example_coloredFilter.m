% example: colored filtering

close all;
load('.\exampleInput.mat');
I = normals;
p = input;

r = 8;
eps = 0.04;

q = zeros(size(I));
q(:,:,1) = guidedfilter_color(I,p(:,:,1),r,eps);
q(:,:,2) = guidedfilter_color(I,p(:,:,2),r,eps);
q(:,:,3) = guidedfilter_color(I,p(:,:,3),r,eps);

imtool([p,q,I]);
