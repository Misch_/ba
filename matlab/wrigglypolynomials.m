% This script produces the wriggly polynomials shown in Section 3.2.2

usedCoeffs = [1 1 1 1 1 1 1 1];
usedCoeffs = 2*usedCoeffs;

x = [-1:0.01:1];

poly3 = zeros(size(x,1),size(x,2),10);
poly4 = zeros(size(x,1),size(x,2),10);
pass = 0;
for i = 1:3:9
    pass = pass+1;
    i
    coeffs = usedCoeffs./i;
    mean = 5;

    poly1(:,:,pass) = coeffs(1)*x + mean;
    poly2(:,:,pass) = coeffs(1)*x.^2 + coeffs(2)*x + mean;
    poly3(:,:,pass) = coeffs(1)*x.^3 + coeffs(2)*x.^2 + coeffs(3)*x + mean;
    poly4(:,:,pass) = coeffs(1)*x.^4 + coeffs(2)*x.^3 + coeffs(3)*x.^2 + coeffs(4)*x + mean;
    poly5(:,:,pass) = coeffs(1)*x.^5 + coeffs(2)*x.^4 + coeffs(3)*x.^3 + coeffs(4)*x.^2 + coeffs(5)*x + mean;
end

plot(x,poly1(:,:,1),x,poly1(:,:,2),x,poly1(:,:,3)); axis([-1 1 0 15]);
figure;
plot(x,poly2(:,:,1),x,poly2(:,:,2),x,poly2(:,:,3)); axis([-1 1 0 15]);
figure;
plot(x,poly3(:,:,1),x,poly3(:,:,2),x,poly3(:,:,3)); axis([-1 1 0 15]);
figure;
plot(x,poly4(:,:,1),x,poly4(:,:,2),x,poly4(:,:,3)); axis([-1 1 0 15]);
figure;
plot(x,poly5(:,:,1),x,poly5(:,:,2),x,poly5(:,:,3)); axis([-1 1 0 15]);