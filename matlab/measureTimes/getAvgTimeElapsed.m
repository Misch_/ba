clear all;

% Open time file:
% File where the elapsed time is recorded in nano seconds.
% Read out by the ray tracer once a frame - time is measured using
% OpenGL time queries.

% fid = fopen('..\raytracer\build\sample1\timeFile_grayScale.txt');
% fid = fopen('..\raytracer\build\sample1\timeFile_color3.txt');
% fid = fopen('..\raytracer\build\sample1\timeFile_color6.txt');
fid = fopen('..\raytracer\build\sample1\timeFile_color9.txt');

% Scan the different times
A = fscanf(fid, '%d');
fclose(fid);

% Compute the average times
nanoSecs = mean(A);
milliSecs = nanoSecs * 10e-7;
secs = nanoSecs *10e-10;