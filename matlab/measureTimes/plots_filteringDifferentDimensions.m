% Load the file where the measured times are stored
load('.\filterTimes.mat')

% Plot overall filtering times and absolute matrix inversion times
figure;
plot(dimensions,filteringTimes_3_6_9_features, dimensions, matrixInversion_3_6_9_features);
legend('filtering', 'matrix inversion')
legend('filtering', 'matrix inversion','Location','best')
xlabel('Feature dimension','FontSize',12);
ylabel('Time [ms]','FontSize',12);
title('Average measured filtering times','FontSize',14)

% Plot percentage of the time spent with inverting the matrix
figure;
plot(dimensions,percent,':', 'LineWidth', 2);
xlabel('Feature dimension','FontSize',12);
ylabel('Time of matrix inversion [%]','FontSize',12);
title('Matrix inversion','FontSize',14);
axis([3 9 0 50]);