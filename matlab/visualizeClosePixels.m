function [close_white, others_white,orig_pixel_marked] = visualizeClosePixels(image,threshold,refPosX, refPosY)
%% 
% A function that compares the closeness of pixels in an image.
% Parameters:
% image:        the image that will be considered
%
% threshold:    the threshold which determines whether to pixels are close
%               or not. A pixel is close, if its euclidean distance to the reference
%               pixel is smaller than threshold.
% 
% positionOfReference:
%               the position of the reference pixel with which all the other ones will be compared
%% Usage:
% load('castelVecchio')
% [close,notClose,orig] = visualizeClosePixels(castelVecchio,50,250,130); imtool([orig,similar,notsimilar]);


close_white = image;
others_white = image;
orig_pixel_marked = image;

for i=-4:4
orig_pixel_marked(refPosY+i,refPosX,:) = [1 0 0];
orig_pixel_marked(refPosY+i,refPosX-1,:) = [1 0 0];
orig_pixel_marked(refPosY+i,refPosX+1,:) = [1 0 0];
orig_pixel_marked(refPosY,refPosX+i,:) = [1 0 0];
orig_pixel_marked(refPosY-1,refPosX+i,:) = [1 0 0];
orig_pixel_marked(refPosY+1,refPosX+i,:) = [1 0 0];
end

refPos = [refPosY refPosX];
for i = 1:size(image,1)
   for j = 1:size(image,2)
       testPos = [i j];
       difference = testPos - refPos;
       difference = sqrt(difference * difference');
       if(difference < threshold)
          close_white(i,j,:) = [1 1 1]; 
       else
           others_white(i,j,:) = [1 1 1];
       end
   end 
end

% imtool(test);