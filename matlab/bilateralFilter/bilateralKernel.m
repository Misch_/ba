
% load image
load('.\bunt2.mat');
image = bunt2;

% set variables
kernelAtX = 265;
kernelAtY = 120;

sigma = [16.0,0.2];

filterRadius = 16;
window = 2*filterRadius + 1;

% apply bilateral filter and get kernel
[filtered,dom,ran] = bfilter2(image,filterRadius,sigma,kernelAtY,kernelAtX);

figure;
image(kernelAtY,kernelAtX,:) = [1 0 0];
imshow(image);
rectangle('Position',[kernelAtX-(filterRadius+1) kernelAtY-(filterRadius+1) window window], 'LineWidth',2, 'EdgeColor','r');

% show domain kernel
imtool(dom);

% show range kernel
imtool(ran);

% show bilateral filter kernel
imtool(dom.*ran);
