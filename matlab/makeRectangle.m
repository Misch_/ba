function [rect,extract] = makeRectangle(img,radius,x,y,col)
% Makes a rectangle of specified radius and color at the position (x,y) of
% the image img.
%
% Output:
% rect: The image img with a colored rectangle at the specified position
% extract: The extract of the image at the specified rectangle
% 
% Usage:
% [rect,extract] = makeRectangle(noisyConference,50,60,320, [1 0 0]);
% imtool(rect);
% imtool(extract);
% 
% Figure in the thesis:
% [rect, extract] = makeRectangle(conference_gaussian_sigma10,50,60,320,[1 0 0]); imtool(rect)
% [rect2, extract] = makeRectangle(rect,50,280,375,[0 1 0]); imtool(rect2)

rect = im2double(img);

xMin = x-(radius+1);
xMax = x+radius+1;
yMin = y-(radius+1);
yMax = y+radius+1;

% top line
for i = xMin:xMax
rect(yMin,i,:) = col;
end
for i = xMin-1:xMax+1
rect(yMin-1,i,:) = col;
end
for i = xMin-2:xMax+2
rect(yMin-2,i,:) = col;
end

% bottom line
for i = xMin:xMax
rect(yMax,i,:) = col;
end
for i = xMin-1:xMax+1
rect(yMax+1,i,:) = col;
end
for i = xMin-2:xMax+2
rect(yMax+2,i,:) = col;
end

% left line
for i = yMin:yMax
rect(i,xMin,:) = col;
end
for i = yMin-1:yMax+1
rect(i,xMin-1,:) = col;
end
for i = yMin-2:yMax+2
rect(i,xMin-2,:) = col;
end

% right line
for i = yMin:yMax
rect(i,xMax,:) = col;
end
for i = yMin-1:yMax+1
rect(i,xMax+1,:) = col;
end
for i = yMin-2:yMax+2
rect(i,xMax+2,:) = col;
end


extract = img(yMin:yMax,xMin:xMax,:);
