\chapter{Introduction}
In computer graphics there is the ubiquitous goal of generating images that look as real as possible.
However, in some applications we have more constraints than just the quality of the generated images. 
Especially in interactive and real-time applications it is crucial to have short computation times such that a new image can be presented several times a second.
Whereas e.g.\ 3D Monster Maze, one of the first 3D games for home computers (1981), was animated at about 6 frames per second, nowadays real-time applications run at 30 up to 200 or even more frames per second.
At the same time the most scenes are much more complex today than it was the case in 1981, and moreover, we would like to render such complex scenes in a more realistic fashion.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{3D-monster-maze}
	\caption{3D Monster Maze, one of the first 3D games for home computers first appeared in 1981. The image was taken from \url{http://en.wikipedia.org/wiki/3D_Monster_Maze.}}
	\label{fig:3dMonsterMaze}
\end{figure}
Whereas in 3D Monster Maze the dinosaur and the walls were presented in a rather schematic way (Figure~\ref{fig:3dMonsterMaze}), a real-looking image has to concern issues like colors, detailed structures and illumination. 
Since light is actually the only thing that a human eye captures when looking at the world, it is obvious that in order to simulate realistic scenes, we should also try to simulate realistic illumination conditions. 
This may e.g.\ include effects like shadows, reflection, refraction, caustics and many more.

\section{Motivation}
The simulation of light is generally a very challenging task. Different ray tracing algorithms allow more or less realistic simulations of local or even global illumination conditions. One important result from the last 30 years is the so-called \emph{rendering equation}, introduced in 1986 by James T. Kajiya \cite{rendEqKajiya}. 
With this physical-based description of the behavior of light in a scene, it became possible to render computer modeled 3D scenes with much more realistic illumination conditions. One of today's most popular algorithms is the so-called \emph{path tracing} algorithm. 
It is based on Monte Carlo integration which will end up with a realistic simulation because it is directly approximating the rendering equation. 
The slow convergence rate of this numerical method leads to very high computational costs and therefore rendering a high quality image using plain Monte Carlo integration may take several days, even with today's hardware. 
This is why many interactive and real-time applications were (and are) still based on the rasterization algorithm that runs much faster. On the other hand, rasterization does not naturally include any of the effects that characterize real lighting. Instead, separate workarounds are necessary to enable one or more effects.

Now, what can be done to enable realistic illumination with time resources that are so heavily limited as it is the case in real-time applications? One idea to tackle the problem of the path tracing algorithm is to let the algorithm roughly compute the illumination conditions and then post-process the image to increase the quality. Such a post-processing step is called \emph{filtering}, and there exist many different filtering methods. Another approach is to increase the convergence rate of the Monte Carlo approximation using adaptive sampling. Basically, this means that one tries to concentrate more on the image regions that are of bad quality and less on the regions where the approximation is already more exact. Combinations of both methods are also possible, one example is \cite{gem}.

\section{Goal}
The goal of this bachelor's project was to implement the guided image filter \cite{gf} that was introduced by He et al.\ in 2010. The advisors of the project provided an implementation of a real-time ray tracer based on nVidia optiX from a former master's project \cite{masMarco}.

I extended the implementation of the guided image filter in order to use features like e.g.\ a normal map or texture information as a guidance image to detect structures in the image that should be preserved during the filtering process.

The guided image filter was then compared in speed and quality with several other methods, in particular the adaptive sampling and reconstruction approach\footnote{An implementation of the algorithm has already been implemented in the ray tracer by M.\ Manzi during his master's project \cite{masMarco}.} \cite{gem} and the bilateral filter \cite{tomasi}, \cite{petschnigg2004}.

\section{Overview}
Chapter~\ref{chap:background} gives some background information. Its purpose is to clarify the motivation to use the path tracing algorithm, starting from an explanation of the rendering equation. A short description of an earlier ray tracing algorithm and its limitations is given to emphasize the advantages of using path tracing. Further, some statistical terms will be defined and a general explanation of what filtering is, including some important examples, is given. At the end of the chapter the adaptive sampling and reconstruction approach by Rouselle et al.\ \cite{gem} is sketched.

In Chapter~\ref{chap:gf}, the guided image filter, proposed by He et al.\ (\cite{gf}, \cite{gf2012}), is introduced. The arising formulas and their extensions in case of multichannel images are discussed. Furthermore, an explanation of how to make the speed of the guided image filtering process independent of the filter radius is given.

Chapter~\ref{chap:results} provides the obtained results using the guided image filter. It gives some information about the initial situation of this project and, based on it, some comparisons with other methods -- in particular bilateral filtering (Subsection~\ref{subsec:bilateral-filtering}) and adaptive sampling and reconstruction (Section~\ref{sec:adaptive}). Furthermore, some general conclusions are given at the end of the chapter.