\chapter{Guided Image Filtering}
\label{chap:gf}
Like the joint bilateral filter that we have seen in Subsection~\ref{subsec:bilateral-filtering}, the guided image filter can use a guidance image to compute the filter kernel and it is a translation-variant, edge-aware filter. It was first introduced in 2010 by He et al.\ (\cite{gf}). The main advantage compared to other edge-aware filtering methods is, that the computational costs are independent of the filter radius.
This chapter provides a detailed explanation of the ideas and formulas of this filtering method. Further, the extension to multichannel input and guidance images, as well as a radius independent implementation are discussed.

\section{Core idea}
The key assumption of the guided image filter is that the filter output \(q\) is locally a linear transform of the guidance image \(I\). 
In \cite{gf} this assumption was written as follows:
\begin{equation} q_i = a_k I_i + b_k, \quad \forall i \in \omega_k.
\label{eq:local-linear-model}
\end{equation}
Here, \(\omega_k\) is a pixel window centered at pixel \(k\), \(q_i\) is the filter output and \(I_i\) the guidance image at pixel \(i\), and \(a_k\) and \(b_k\) are some coefficients. 
Note that \(a_k\) and \(b_k\) depend on \(k\) but not on \(i\); they are assumed to be constant on the whole window \(\omega_k\). 
This local linear model ensures that the filter output \(q\) takes the information about the edges from the guidance image; \(q\) has an edge only if the guidance image \(I\) has an edge, because \(\nabla q = a \nabla I\).

\section{Technical details}
\label{sec:formulas}
The main task is to find, for each \(k\), the appropriate linear coefficients \(a_k\) and \(b_k\) in equation (\ref{eq:local-linear-model}). This is a typical regression problem and because we are trying to fit a linear model to the data (the noisy input image), linear regression is used. 
\subsection{Cost function}
In most cases linear regression ends up in minimizing the following cost function that consists of the squared errors: 
\[ 
	E(a_k,b_k) = \sum_{i \in \omega_k}{((\underbrace{a_k I_i + b_k}_{= \quad q_i} - p_i)^2 + \epsilon a_k^2).}
\]
Here, \(\epsilon\) is a regularization parameter which will be further discussed in Subsection~\ref{subsec:regularization}. In order to understand how linear regression and least squares minimization are related, we could e.g.\ consider the following probabilistic interpretation: 
Assume that our input data is given by
\[ p_i = a_k I_i + b_k + \varepsilon_i, \]
where \(\varepsilon_i\) is an error term to capture noise or effects that cannot be modeled by our linear model. Assuming that the errors are independently and identically distributed according to a normal distribution with zero mean and variance \(\sigma^2\), \(\varepsilon_i \sim \mathcal{N}(0,\sigma^2)\), we can explicitly write the probability density function of \(\varepsilon_i\) as
\[p(\varepsilon_i) = \frac{1}{\sqrt{2 \pi}\,\sigma}\,\text{exp}\left(- \frac{\varepsilon_i^2}{2 \sigma^2} \right).\]
Further, we see that the distribution of \(p_i\) given \(I_i\) has a mean of
\[
\text{E}[p_i \mid I_i] = \text{E}[a_k I_i + b_k + \varepsilon_i] = \text{E}[a_k I_i + b_k] + \underbrace{\text{E}[\varepsilon_i]}_{= 0} = a_k I_i + b_k,
\]
and the variance of \(p_i\) given \(I_i\) is given by 
\[
	\text{Var}(p_i \mid I_i) = \text{Var}(a_k I_i + b_k + \varepsilon_i) = \text{Var}(\varepsilon_i) = \sigma^2.
\]
Therefore, \(p_i\) given \(I_i\) is normally distributed with mean \(a_k I_i + b_k\) and variance~\(\sigma^2\). We can write this as \(p_i \mid I_i \sim \mathcal{N}(a_k I_i + b_k, \sigma^2)\), and the probability density is given by 
\[p(p_i \mid I_i) = \frac{1}{\sqrt{2 \pi}\,\sigma}\,\text{exp}\left(- \frac{(p_i - a_k I_i - b_k)^2}{2 \sigma^2} \right).\]

If we view this as a function of the parameters \(a_k\) and \(b_k\), then the resulting function
\[L(a_k,b_k) = \prod_{i \in w_k} p(p_i \mid I_i)\]
is called the \emph{likelihood function}.
Our aim is to find \(a_k\) and \(b_k\) that make the probability of reconstructing the right input image \(p\) given some guidance image \(I\) as high as possible. Given the above probabilistic interpretation, this corresponds to maximizing \(L\) with respect to \(a_k\) and \(b_k\). 
Because of the exponential function arising in \(L\), it is easier to find \(a_k\) and \(b_k\) by means of maximizing \(\log L(a_k,b_k)\); this does not change the result because \(\log\) is a strictly increasing function. 
Computing \(\log L(a_k,b_k)\) leads to an expression that is maximal if
\[\sum_{i \in \omega_k}{(a_k I_i + b_k - p_i)^2}\]
is minimal. Note that this is our cost function \(E(a_k,b_k)\) if we assume the regularization term to be zero. A more detailed explanation of this probabilistic interpretation can e.g.\ be found in \cite{likelihood}\footnote{Those lecture notes were also used by Prof.\ Dr.\ P.\ Favaro in the lecture ``Machine Learning'' in fall semester 2012 at the University of Bern.}, \cite{krengel88} or \cite{bayesian}.

The cost function \(E\), including the regularization term, is called the \emph{linear ridge regression model} \cite{regression-analysis}, sometimes also referred to as \emph{Tikhonov regularization}. The solution of the minimization problem is given by the roots of the partial derivatives of the cost function \(E\); it is
\begin{align}
	a_k \quad =& \quad \frac{\frac{1}{|\omega|} \sum_{i \in \omega_k}{I_i p_i - \mu_k \bar{p}_k}}{\sigma_k^2 + \epsilon}, \label{eq:ak}\\
	b_k \quad =& \quad \bar{p}_k - a_k \mu_k. \label{eq:bk}
\end{align}
Here, \(\mu_k\) and \(\sigma_k^2\) are the mean and variance of \(I\) in \(\omega_k\), \(|\omega|\) is the number of pixels in \(\omega_k\) and \(\bar{p}_k = \frac{1}{|w|} \sum_{i \in \omega_k} p_i\) is the mean of \(p\) in \(\omega_k\).

\subsection{Regularization term}
\label{subsec:regularization}
The regularization parameter \(\epsilon\) prevents \(a_k\) from being too large. In order to understand its effects on the filter output, we consider the two cases \(I=p\) and \(I \neq p\).
\subsubsection*{The case of \(I = p\)}
In the case of \(I = p\) we have 
\[E(a_k,b_k) = \sum_{i\in \omega_k} (a_k p_i + b_k-p_i)^2 + \epsilon a_k^2.\]
If \(\epsilon = 0\), then the trivial solution to minimize \(E\) with respect to \(a_k\) and \(b_k\) is \(a_k=1\) and \(b_k = 0\). Our linear model (\ref{eq:local-linear-model}) then says that \(q_i = p_i\) which means that the filtering process does not take any effect at all. Before we discuss the case \(I \neq p\), let us try to better understand the effects of \(\epsilon\) in this simple case. Looking at the equations (\ref{eq:ak}) and (\ref{eq:bk}), the assumption \(I = p\) leads to many simplifications. Since then \(p_i = I_i\) and \(\bar{p}_k = \mu_k,\) we can write
\begin{align}
a_k \quad =& \quad \frac{\frac{1}{|\omega|} \sum_{i \in \omega_k}{I_i p_i} - \mu_k \bar{p}_k}{\sigma_k^2 + \epsilon} \notag\\
					=& \quad \frac{\frac{1}{|\omega|} \sum_{i \in \omega_k}{I_i^2} - \mu_k^2}{\sigma_k^2 + \epsilon} \notag\\
					=& \quad \frac{E_{w_k}[I^2] - E_{w_k}[I]^2}{\sigma_k^2 + \epsilon} \notag\\
					=& \quad \frac{\sigma_k^2}{\sigma_k^2 + \epsilon}. \label{eq:ak-simple}
\end{align}
Here, \(E_{w_k}\) denotes the mean within the window \(\omega_k\). The coefficient \(b_k\) can also be simplified by writing
\begin{align}
b_k ~ = ~ \bar{p}_k - a_k \mu_k ~ = ~ \mu_k - a_k \mu_k ~ = ~ (1-a_k)\mu_k.	\label{eq:bk-simple}	
\end{align}
If \(\epsilon > 0\), then we distinguish the two different cases of the guidance image being flat or changing a lot within the window \(w_k\). This cases can directly be translated as the variance of \(I\) being small or large within \(w_k\). So we observe the following two extremal cases:

\begin{description}
\item[High variance:]
If the guidance image \(I\) changes a lot within \(w_k\), we have \(\sigma_k^2 \gg \epsilon\). 
Because we assume that the guidance image consists of information about which structures should be preserved during the filtering process, in such regions we want that the filter output heavily depends on the guidance image. 
With our setup of the cost function and the given solutions for the parameters \(a_k\) and \(b_k\) from linear ridge regression, this is exactly what is going to happen:
\[ a_k \quad = \quad \frac{\sigma_k^2}{\sigma_k^2 + \epsilon}\ \quad \approx \quad \frac{\sigma_k^2}{\sigma_k^2} \quad = \quad 1.\]
We can neglect \(\epsilon\) because the variance \(\sigma_k^2\) is much bigger and \(\epsilon\) is, compared to \(\sigma_k^2\), almost zero.
Therefore we get \(b_k = (1-a_k)\mu_k \approx \ 0\).

\item[Flat patch:]
If, to the contrary, the guidance image is almost constant within \(w_k\), we have a very small variance and thus \(\sigma_k^2 \ll \epsilon\).
Intuitively, \(I\) being constant means that there is no structure in the image that should be preserved, and the aim in this case is to simply average the pixel values of the pixels within \(\omega_k\) in order to smooth the image. 
We can show that this is what actually happens if we simplify equations (\ref{eq:ak}) and (\ref{eq:bk}) according to \(\sigma_k^2 \ll \epsilon\):
\[ a_k \quad = \quad \frac{\sigma_k^2}{\sigma_k^2 + \epsilon}\ \quad \approx \quad \frac{0}{\epsilon} \quad = \quad 0.\]
This time we neglected \(\sigma_k^2\) because we assumed the variance to be so small that it is almost zero compared to \(\epsilon\). It is easy to see that \(b_k = (1-a_k)\mu_k \approx \mu_k = \bar{p}_k\); this is exactly what was just formulated as an intuitive goal in the case of \(\omega_k\) being a flat patch.
\end{description}
The regularization parameter \(\epsilon\) thus controls the criterion of a window being a ``flat patch'' or a ``high variance'' one -- the regions with a much smaller variance than \(\epsilon\) are smoothed, whereas those with a much larger variance than \(\epsilon\) are preserved (see Figure~\ref{fig:epsilon-in-simple-case}). The effect of \(\epsilon\) in the guided filter is similar to the one of the range variance \(\sigma_r^2\) in the bilateral filter (see Subsection~\ref{subsec:bilateral-filtering}).

% Camera:
%[-37.7379, 55.4996, -35.8877][-37.2799, 54.7688, -32.999][0, 1, 0]45
\begin{figure}[ht]
	\centering
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=\textwidth]{conference_epsilon_0}	
		\caption*{$\epsilon = 0$ (no effect)}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=\textwidth]{conference_epsilon_0-01}	
		\caption*{$\epsilon = 0.01$}
	\end{subfigure}
	
	\vspace{3mm}
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=\textwidth]{conference_epsilon_0-02}	
		\caption*{$\epsilon = 0.02$}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=\textwidth]{conference_epsilon_0-05}	
		\caption*{$\epsilon = 0.05$}
	\end{subfigure}
	\caption{The regularization parameter $\epsilon$ controls which regions are be smoothed or preserved. Regions with a much smaller variance than $\epsilon$ are smoothed, those with a much larger variance than $\epsilon$ are preserved. If $\epsilon = 0$, then nothing happens. As $\epsilon$ grows, more regions have a smaller variance than $\epsilon$ and therefore more regions are smoothed.}
	\label{fig:epsilon-in-simple-case}
\end{figure}
\subsubsection*{The case of \(I \neq p\)}
In the case of \(I \neq p\) the choice of the ridge regression model may not be as well justified because setting \(\epsilon\) to zero does not mean that the filtering process does not take any effect. Instead, we have other issues, especially when it comes to multidimensional guidance images \(I\). In this case the variance \(\sigma_k^2\) is replaced by an \(n \times n\) covariance matrix \(\Sigma_k\) where \(n\) is the dimension of the guidance image (see Section~\ref{sec:colandfeatures}). One reason to choose a ridge regression model instead of simple linear regression is that the covariance matrix \(\Sigma_k\) could be singular; then adding the matrix 
\[ \epsilon U = 
\begin{bmatrix}
\epsilon	& 0 			& \cdots	& 0 			\\
0 				& \ddots	& 				& \vdots 	\\
\vdots 		& 				& \ddots	& 0	 			\\
0					&	\cdots	&	0				& \epsilon
\end{bmatrix}
\]
to \(\Sigma_k\) guarantees the result to be invertible. 
However, the issue of \(\Sigma_k\) being singular may possibly be solved by computing the pseudo-inverse matrix, e.g.\ using singular value decomposition (SVD). 
Unfortunately, the only explanation from He et al.\ of why they chose a ridge regression model is the one that refers to the case of \(I = p\) from above. 
They give the explanation \textit{``\(\epsilon\) is a regularization parameter to prevent \(a_k\) from being too large''} in \cite{gf}, \cite{gf2012}. Choosing a regularized regression model generally means to introduce some bias to the estimator in order to reduce the variance of the estimated values. The variance/bias-tradeoff in the context of Tikhonov regularization was also investigated in \cite{johansen1997tikhonov}.
Intuitively, the reduction of variance can be understood from the fact that a polynomial model with small parameters is in general less wriggly than if the parameters are large; see some examples in Figure~\ref{fig:wriggly}. 
Since in our application of noise reduction reducing the variance is an overall goal, this may justify the choice of a biased estimator. 
\begin{figure}[ht]
	\centering
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=\textwidth]{wriggly1}	
		\caption*{Linear function -- case of guided filter}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=\textwidth]{wriggly2}	
		\caption*{2\textsuperscript{nd} order polynomial}
	\end{subfigure}
	
	\vspace{3mm}
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=\textwidth]{wriggly3}	
		\caption*{3\textsuperscript{rd} order polynomial}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=\textwidth]{wriggly4}	
		\caption*{4\textsuperscript{nd} order polynomial}
	\end{subfigure}
	\caption{The regularization brings an additional term to the cost function that penalizes large values of the parameters of the model. Smaller values of the parameters end up in smoother functions with less variations -- in the images above there are polynomials of different orders, each of them having a constant term $5$. The blue curve stands for a polynomial with all (but the already mentioned one) coefficients being $2$, and the red curve stands for a polynomial with all the coefficients being $\frac{1}{5}$.}
	\label{fig:wriggly}
\end{figure}
It turns out, however, that despite handling the singularity problems, the regularization term has a rather disadvantageous effect. The edge-preserving property heavily suffers from the introduced bias (Figure~\ref{fig:epsilon}).

% Camera position: 
% [153.595, 285.327, -192.503][309.497, 224.206, 589.796][0, 1, 0]45
\begin{figure}[ht]
	\centering
	\begin{subfigure}[h]{0.3\textwidth}
		\includegraphics[width=\textwidth]{cornell_epsilon_input}	
		\caption*{Input (6spp)}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.3\textwidth}
		\includegraphics[width=\textwidth]{cornell_epsilon_1}	
		\caption*{$\epsilon = 1.0$}
	\end{subfigure}
		~
	\begin{subfigure}[h]{0.3\textwidth}
		\includegraphics[width=\textwidth]{cornell_epsilon_0-1}	
		\caption*{$\epsilon = 0.1$}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.3\textwidth}
		\includegraphics[width=\textwidth]{cornell_epsilon_0-01}	
		\caption*{$\epsilon = 0.01$}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.3\textwidth}
		\includegraphics[width=\textwidth]{cornell_epsilon_0-001}	
		\caption*{$\epsilon = 0.001$}
	\end{subfigure}
		~
	\begin{subfigure}[h]{0.3\textwidth}
		\includegraphics[width=\textwidth]{cornell_epsilon_0-0001}	
		\caption*{$\epsilon = 0.0001$}
	\end{subfigure}
	\caption{The regularization parameter controls the accuracy of the edge detection. If it is chosen too small, then the inverse of some of the covariance matrices may become numerically unstable -- in the chosen implementation, this leads to visible artifacts as in the lower right image. A too large value can cause blurred edges (top row). Note that $\epsilon$ does not control the level of smoothing but only where the smoothing takes effect.}
	\label{fig:epsilon}
\end{figure}

For the purpose of this thesis the case of \(I = p\) is not very interesting (see Section~\ref{sec:noisyGuide}), so guided image filtering is applied with a regularization parameter \(\epsilon\) in order to tackle the singularity problem that cannot be accurately handeled by the implemented matrix invertion algorithm\footnote{I chose to adapt the C++-implementation of matrix inversion using LU-factorization by M. Dinolfo \cite{matrixInversion} to use it in an OpenGL fragment shader. The inversion runs in $\mathcal{O}(n^3)$ time.}; it is chosen very small so that the edges are preserved. 

\subsection{Averaging the local linear coefficients}
Until now it was described how, starting with the local linear model from equation (\ref{eq:local-linear-model}), we could find the optimal coefficients \(a_k\) and \(b_k\) to linearly map the guidance image \(I\) onto the desired filter output \(q\) within a window \(\omega_k\). For each pixel \(k\) we have our local coefficients \(a_k\) and \(b_k\) that map the whole window centered at \(k\) to the filter output in an optimal way. Since every pixel lies within multiple windows with different centers, for each pixel there are multiple coefficients that claim to describe the optimal mapping. 
He et al.\ propose to simply average the optimal constants obtained for all the windows that contain the considered pixel. 
This ends up in the following formula for the filter output:
\begin{equation}
	q_i ~ = ~ \frac{1}{|\omega|} \sum_{k \vert i \in \omega_k}{(a_k I_i + b_k)} ~ = ~ \bar{a}_i I_i + \bar{b}_i, 
	\label{eq:gf-final}
\end{equation}

where \(\bar{a}_i = \frac{1}{|\omega|} \sum_{k \in \omega_i}{a_k}\) and \(\bar{b}_i = \frac{1}{|\omega|} \sum_{k \in \omega_i}{b_k}\).

\subsection{Filter kernel}
The previously introduced formulas (\ref{eq:gf-final}), (\ref{eq:ak}) and (\ref{eq:bk}) can be rewritten in the form of a weighted average
\[q_i = \sum_j W_{ij}(I)p_j\]
with the filter kernel \(W_{ij}\) being a function of the guidance image \(I\).
The kernel weights are explicitly given by\footnote{For a detailed derivation please consider \cite[Section 3.3]{gf2012}}
\begin{equation}
W_{ij}(I) = \frac{1}{|w|^2} \sum_{k \vert i,j \in \omega_k} \left( 1+ \frac{(I_i-\mu_k)(I_j - \mu_k)}{\sigma_k^2 + \epsilon}\right). 
\label{eq:kernel}
\end{equation}
If \(I_i \) and \(I_j\) are on the same side of an edge, then \(I_i - \mu_k\) and \(I_j - \mu_k\) have the same sign. If they are on different sides, they have different signs and therefore the product will be a negative number. 
This means that the weights are small for pixels that lie on different sides of an edge, and large for pixels who lie on the same side. Note also that the weights decrease with the distance of the pixels. If \(i\) and \(j\) are spatially far away from each other, then there are fewer windows that contain both \(i\) and \(j\). Therefore there are fewer summation terms in equation (\ref{eq:kernel}) but the normalization factor \(\frac{1}{|w|^2}\) remains the same.

\section{Color filtering and usage of features}
\label{sec:colandfeatures}
Extending the guided image filter to a colored input image \(p\) can be done straightforward by simply applying the filter to each channel independently.

If the guidance image \(I\) is multichannel (and therefore denoted by \(\boldsymbol{I}\)), then the local linear model is reformulated using multidimensional vectors instead of the scalars used so far. The linear model (\ref{eq:local-linear-model}) becomes in this case:
\[q_i = \boldsymbol{a}_k\T \boldsymbol{I}_i + b_k, \quad \forall i \in \omega_k,\]
where \(\boldsymbol{I}_i\) and \(\boldsymbol{a}_k\) are now \(n \times 1 \) vectors (thus \(\boldsymbol{a}_k\T \boldsymbol{I}_i\) is given by a dot product), and \(q_i\) and \(b_k\) are scalars. 
The coefficient vector \(\boldsymbol{a}_k\) is then given by
\[\boldsymbol{a}_k ~ = ~ (\Sigma_k + \epsilon U)^{-1} \left( \frac{1}{|w|} \sum_{i \in \omega_k} \boldsymbol{I}_i p_i - \boldsymbol{\mu_k} \bar{p}_k \right). \label{eq:ak-multi} \]
Here, \(\Sigma_k\) denotes the \(n \times n\) covariance matrix of \(\boldsymbol{I}\) in \(\omega_k\) and \(U\) is the \(n \times n\) identity matrix.

The formulas (\ref{eq:ak}) and (\ref{eq:gf-final}) for \(b_k\) and \(q_i\) have now the following form:
\begin{align*}
	b_k ~ =~& \bar{p}_k - \boldsymbol{a}_k\T \mu_k, \\
	q_i ~ =~& \bar{\boldsymbol{a}}_i\T \boldsymbol{I}_i + \bar{b}_i.
\end{align*}
Note that equation~(\ref{eq:ak-multi}) requires the inversion of a \(d_\mathcal{R} \times d_\mathcal{R}\)-matrix for each pixel, where \(d_\mathcal{R}\) is the dimension of the range of the guidance image. The computational costs of matrix inversion increase cubically with the range dimension \(d_\mathcal{R}\) which heavily influences the overall costs of the filtering process.

Bauszat et al.\ proposed in \cite{features} to apply the guided image filter in the context of ray tracing. They used a 4-dimensional guidance image consisting of a 3-dimensional normal map and a 1-dimensional depth map. In order to filter the indirect illumination, those features are captured and saved at the hitpoint of the first ray that is shot through a pixel. The indirect illumination at this point is estimated using Monte Carlo integration and stored in a separate buffer which is then filtered with the already mentioned features as a guidance. This filtered indirect illumination map can then provide color information for the following pixel samples. This approach includes a reorganization of the rendering pipeline which was not implemented in this bachelor's project.

\section{Implementation}
\subsection{Given framework}
The whole project is based on the nVidia optiX framework.
I used C++, OpenGL 4.3 and GLSL core 1.50. All the indicated computation times were measured on a Intel Xeon 2.5GHz CPU and 8GB RAM. The used GPU is a nVidia GeForce GTX 580. All the mathematical operations were implemented in fragment shaders since every operation has to be computed once per pixel. A ray tracer based on nVidia optiX was implemented by my advisor M.\ Manzi during his master's project \cite{masMarco}. Some information about the initial speed of the application and image quality is provided in Section~\ref{sec:initialPos}.

\subsection{\texorpdfstring{$\mathcal{O}(1)$}{O(1)}-Boxfilter}
Considering the equations~(\ref{eq:ak}) and (\ref{eq:bk}), we observe that almost all the computation steps amount to compute means, i.e.\ averages. 
This is performed by applying a simple boxfilter, as it is shortly mentioned in Section~\ref{sec:filtering}.

In 1984, Franklin C. Crow introduced the concept of the \emph{summed-area table} (SAT), often also referred to as \emph{integral image}, to computer graphics \cite{crow1984summed}. The value at each pixel of a SAT of an image is the sum of all the pixel values at the pixels lying to its lower left. For a schematic explanation of how a SAT looks like, see Figure~\ref{fig:satSchematic}.
\begin{figure}[ht]
\def\arraystretch{1.8}
\centering
	\begin{subfigure}[h]{0.45\textwidth}
		\centering
		\begin{tabular}{ | c | c | c | c | c | c | c | c |}
	 \hline
		0.1 & 0.1 & 0.1 & 0.1 \\ \hline
		0.1 & 0.1 & 0.1 & 0.1 \\ \hline		
		0.1 & 0.1 & 0.1 & 0.1 \\ \hline
		0.1 & 0.1 & 0.1 & 0.1 \\ \hline		
	\end{tabular}
		\caption{Image}
		\label{subfig:imageArea}
	\end{subfigure}
	\begin{subfigure}[h]{0.45\textwidth}
	\centering
		\begin{tabular}{ | c | c | c | c | c | c | c | c |}
	 \hline
		0.4 & 0.8 & 1.2 & 1.6 \\ \hline
		0.3 & 0.6 & 0.9 & 1.2 \\ \hline		
		0.2 & 0.4 & 0.6 & 0.8 \\ \hline
		0.1 & 0.2 & 0.3 & 0.4 \\ \hline		
	\end{tabular}
		\caption{Integral image (SAT)}
		\label{subfig:imageSAT}
	\end{subfigure}
		\caption{Pixel values of a $4 \times 4$ pixel area (\subref{subfig:imageArea}) and SAT of the same pixel area (\subref{subfig:imageSAT}). Each value of the SAT is the sum of the pixel values of the pixels to the lower left.}
		\label{fig:satSchematic}
\end{figure}

Given such a SAT, it is straightforward to calculate the sum of all the pixel values within an arbitrary rectangular region by taking a sum and two differences of values from the table. As an example, assume that we want to sum over an area bounded by xl, xr, yb, and yt (Figure~\ref{fig:sat3operations}). The sum is then given by 
\[ \sum ~ = ~ \text{SAT}[\text{xr,yt}] - \text{SAT}[\text{xr,yb}] - \text{SAT}[\text{xl,yt}] + \text{SAT}[\text{xl,yb}].\]
In order to calculate a pixel of the filter output of a boxfilter, we simply have to calculate this summed area for a square window centered at the pixel of interest and divide by the total number of pixels in the window. Thus the computational costs of the boxfilter do not depend on the filter radius \(r\) since 
\begin{align*} 
q_k =& \sum_{i \in \omega_k} \frac{p_i}{(2 r + 1)^2} ~ = ~ \frac{1}{(2 r + 1)^2} \sum_{i \in \omega_k} p_i \\
 =& ~ \frac{\text{SAT}_p[\text{xr,yt}] - \text{SAT}_p[\text{xr,yb}] - \text{SAT}_p[\text{xl,yt}] + \text{SAT}_p[\text{xl,yb}]}{(2 r + 1)^2},
\end{align*}
where \(q_k\) is the filter output at pixel \(k\), \(p_i\) is the input image at pixel \(i\), \(r\) is the filter radius of the square window \(\omega_k\) centered at pixel \(k\) and \(\text{SAT}_p\) is the summed-area table of \(p\). 
If the center pixel \(k\) lies somewhere in the image such that not the whole window \(\omega_k\) also lies withing the image, then the denominator will obviously be a smaller number -- namely the number of pixels of the window that are still in the image.
\begin{figure}[ht]
\centering
	\includegraphics[width=0.5\textwidth]{satCrow1984}
	\caption{Calculation of summed area from a summed-area table. The sum over the pixels in the brightest region can be computed by subtracting the SAT-values at the upper left position $(xl,yt)$ and the lower right position $(xr,yb)$ from the value at the upper right position $(xr,yt)$. Because the lower left region has then been subtracted twice, it is necessary to add the value at $(xl,yb)$ again. This image is taken from \cite{crow1984summed}.}
	\label{fig:sat3operations}
\end{figure}

The generation of summed-area tables was implemented in OpenGL as described in detail by Hensley et al.\ in \cite{sat-generation}. This method allows constructing a SAT in \(\mathcal{O}(\log n) \) time, \(n\) being the number of pixels in the image. The algorithm proceeds in two phases: first a horizontal phase, then a vertical phase (see Figure~\ref{fig:sat-phases}). 
\begin{figure}[ht]
\centering
	\includegraphics[width=0.8\textwidth]{sat_phases}
	\caption{The algorithm of Hensley et al.\ consists of a horizonal and a vertical phase.}
	\label{fig:sat-phases}
\end{figure}
In both phases, fragment programs are executed to accumulate pixel values in parallel. 

In the first pass of each phase, the values at each pixel and its immediate neighbor are accumulated and stored to a texture. In the horizontal phase, this is each pixel and its immediate left neighbor (Figure~\ref{fig:sat-pass1}).
\begin{figure}[ht]
\centering
	\includegraphics[width=0.7\textwidth]{sat_pass1}
	\caption{In the first pass of the horizontal phase, the values at each pixel and its left neighbor are accumulated and stored to a texture. This image is taken from \cite{sat-generation}.}
	\label{fig:sat-pass1}
\end{figure}
In the next pass, the values of each pixel and its (indirect) neighbor that lies 2 positions to the left are accumulated. 
This procedure continues and in the $i$\textsuperscript{th} pass, the values of each pixel and its neighbor that lies \(2^{(i-1)}\) positions to the left are accumulated (see Figure~\ref{fig:sat-allPasses}).
\begin{figure}[ht]
\centering
	\includegraphics[width=0.7\textwidth]{sat_allPasses}
	\caption{Illustration of all passes of the horizontal phase. During the $i$\textsuperscript{th} pass, the values of each pixel and the pixel that lies $2^{i-1}$ positions to its left are accumulated. The image is taken from \cite{sat-generation}.}
	\label{fig:sat-allPasses}
\end{figure}
The number of passes for the horizontal phase is equal to \(\log_2(\text{width}) \), and \(\log_2(\text{height})\) for the vertical phase. The accumulated values are always stored in a texture -- Hensley et al.\ used two textures and swapped them such that they could alternately read or write to one or the other texture.