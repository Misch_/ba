\section{Filtering}
\label{sec:filtering}
Image filtering is a widely used concept in many applications in computer graphics. Filtering means that we recompute the value of the pixels in an image as a weighted average of some other pixels. For one pixel such a filter can generally be described by the formula
\[
q = \sum_{i}w_i p_i,
\]
where $q$ is the filter output, $p_i$ is the i\textsuperscript{th} pixel of an input image $p$ and $w_i$ is a weighting function that is called the \emph{filter kernel}. The index $i$ can for example take values in the whole image $p$ or in a smaller patch of the image (see Figure~\ref{fig:weighted_avg})

\begin{figure}[ht]
\centering
\includegraphics[width=5.0cm]{filter_general}
\caption{The value of the pixel $q$ is a weighted average of the pixels $p_i$.}
\label{fig:weighted_avg}
\end{figure}		
	
A simple example of a filter is the so-called \emph{boxfilter} that computes the final pixel value as the average of the pixel values in a patch around it. 
The weighting function is therefore constantly $w_i = \frac{1}{n}$ where $n$ is the number of pixels in the patch. 
Going through every pixel of an image and recompute its value as the simple average of the pixels lying in a quadratic window around it yields a blurred image with some typical cross structures along the x- and y-axes (see Figure~\ref{fig:boxfilter}).

\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.3]{yellow_square_on_black}
	\vspace{1cm}
	\includegraphics[scale=0.3]{yellow_square_on_black_filtered_radius40}
	\caption{A yellow square before (left hand side) and after (right hand side) performing a boxfilter with a filter radius of $r = 40$ pixels. Each pixel of the filtered image is the average of $(2 \cdot 40 + 1) \times (2 \cdot 40 + 1)$ pixels that lie within the surrounding window.}
	\label{fig:boxfilter}
\end{figure}

\subsection{Low-pass filtering}
A low-pass filter is a filter that attenuates signals with high frequencies but passes low-frequency signals. In image processing a high frequency region is a region where the pixel values change fast. This includes for example noise but also edges or fine structures and details in an image. A popular example of a low-pass filter is the Gaussian filter where further away pixels have a smaller impact on the result than the ones that are nearby. The weight of a pixel \(p_i\) is given by 
\[ w_i = \frac{1}{2 \pi \sigma^2} e^{- \frac{\|p_i - q \|^2}{\sigma^2}}.\]
The filter kernel has the shape of a Gaussian bell curve with a peak at the center pixel \(q\). 
If a pixel \(p_i\) is close to our inspected pixel \(q\), then \(\|p_i-q\|\) is small and the weight \(w_i\) is large. 
Analogously, further away pixels have smaller weights and therefore a smaller impact on the filtering output at \(q\). The parameter \(\sigma\) is the standard deviation of a normal distribution (with mean \(q\)) and controls the wideness of the Gaussian bell curve which also influences the height of the peak because \(\sum_i w_i = 1\) has to be fulfilled (see Figure~\ref{fig:gaussianFilterKernelDifferentSigmas}).
%
%matlab:
% for i = 12.5:2.5:20
%     gauss = fspecial('gaussian',[100,100],i);
%     figure;
%     surf(gauss,'FaceColor',[0.0,0.0,1.0],'EdgeColor','none');
%     camlight left; lighting phong; grid on; axis on; axis([0 100 0 100 0 0.001]);
% end
%
\begin{figure}[ht]
\centering
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{gaussianKernel_sigma12-5}
	\caption*{\(\sigma = 12.5\)}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{gaussianKernel_sigma15}
	\caption*{\(\sigma = 15\)}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{gaussianKernel_sigma17-5}
	\caption*{\(\sigma = 17.5\)}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{gaussianKernel_sigma20}
	\caption*{\(\sigma = 20\)}
\end{subfigure}
\caption{Gaussian filter kernels with different standard deviations $\sigma$}
\label{fig:gaussianFilterKernelDifferentSigmas}
\end{figure}

Applying the Gaussian filter to an image blurs the image depending on the choice of \(\sigma\) (Figure~\ref{fig:gaussianBlurs}). More exactly, it reduces the image's high-frequency components which leads to a smoothing effect.

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{wohlensee}
		\caption*{Input image}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{wohlensee_gaussFiltered_sigma3}
		\caption*{Gaussian blur, \(\sigma = 3\)}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{wohlensee_gaussFiltered_sigma10}
		\caption*{Gaussian blur, \(\sigma = 10\)}
	\end{subfigure}
\caption{A photography of the Wohlensee in Switzerland, blurred with different Gaussian filter kernels.}
\label{fig:gaussianBlurs}
\end{figure}

If we apply a Gaussian filter to a noisy image, we observe that the high-frequency noise will indeed be reduced which corresponds to a reduced variance. 
This is, in terms of noise reduction, a good thing, e.g.\ it may reduce the mean squared error since \(\text{MSE} = \text{Var} + \text{Bias}^2\) (see Section~\ref{sec:mse}). The problem arises in image regions where structures should be preserved. Because also edges are examples of high-frequency changes in an image, a Gaussian filter recklessly blurs them. This corresponds to an increasing bias which again has a bad influence on the MSE. For a visual impression of the tradeoff problem between variance and bias, see Figure~\ref{fig:gaussianVarianceAndBias}.
\begin{figure}[ht]
	\centering
	\begin{subfigure}[h]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{noisyConference_bothRectangles}
		\caption*{Noisy input image -- 2spp}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{conference_gaussian_bothRectangles}
		\caption*{Gaussian blur, \(\sigma = 10\)}
	\end{subfigure}
	
	\vspace{3mm}
	\begin{subfigure}[h]{0.4\textwidth}
		\centering
		{
			\setlength{\fboxsep}{0pt}%
			\setlength{\fboxrule}{1.0pt}%
			\fcolorbox{red}{white}{\includegraphics[width=0.45\textwidth]{noisyConference_extractAtSmooth}}%
			~
			\fcolorbox{red}{white}{\includegraphics[width=0.45\textwidth]{conference_gaussian_extractAtSmooth}}%
		}
		\caption*{Smooth region}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.4\textwidth}
		\centering
		{
			\setlength{\fboxsep}{0pt}%
			\setlength{\fboxrule}{1.0pt}%
			\fcolorbox{green}{white}{\includegraphics[width=0.45\textwidth]{noisyConference_extractAtEdge}}%
			~
			\fcolorbox{green}{white}{\includegraphics[width=0.45\textwidth]{conference_gaussian_extractAtEdge}}%
		}
		\caption*{Structured region}
	\end{subfigure}
\caption{A noisy image filtered by a Gaussian filter (top row). 
Low-pass filtering suppresses high-frequency details, thus noise but also edges. 
In unstructured regions (bottom left) the results are somewhat convincing, but structures that are important in other regions are not preserved (bottom right).}
\label{fig:gaussianVarianceAndBias}
\end{figure}

\subsection{Bilateral filtering}
\label{subsec:bilateral-filtering}
The bilateral filter, first introduced by Tomasi and Manduchi in \cite{tomasi}, is an edge-aware filter. This means that bilateral filtering smooths images while preserving edges.

So far we have seen examples of filter kernels that are constant in a window (boxfilter) or where a pixel's weight depends on the Euclidean distance of the pixel to the center pixel (Gaussian filter). 
In both cases the filter kernel does not depend on the content of the image. Intuitively, it makes  
sense that such a filter not necessarily preserves edges because it does not consider any information about the image content, especially not about structures that should be preserved.

If we consider as an image just a rectangular region (image domain) and the colors as a value at each pixel (image range), we can refer ``closeness'' to vicinity in the domain and ``similarity'' to vicinity in the range (see Figure~\ref{fig:closeVSsimilar}).

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{castelVecchio_pixelMarked}
		\caption*{Input image with a red cross at the currently considered pixel.}
		\label{subfig:pixelMarked}
	\end{subfigure}
		
	\vspace{3mm}
	\begin{subfigure}[h]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{castelVecchio_close}
		\caption*{Pixels that are close.}
		\label{subfig:close}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{castelVecchio_similar}
		\caption*{Pixels that are similar.}
		\label{subfig:similar}
	\end{subfigure}
\caption{Visualization of the two different concepts of closeness (vicinity in domain) and similarity (vicinity in range).}
\label{fig:closeVSsimilar}
\end{figure}

Until now we concentrated on so-called \emph{domain filtering} where the filter kernel does not depend on the content (the range) of the image. In the exact same manner we can apply pure range filtering. This approach does not ignore the structures in the image but it merely transforms the gray- or color map of the input image and is rather useless in image denoising. For a detailed analysis of the properties of range filtering in this context, see \cite{tomasi}.

The idea of bilateral filtering is to combine domain- and range filtering. The filter kernel consists of two weighting functions (Gaussians are commonly used here), one applied to the image domain and the other one to the image range. In the case of Gaussians, the filter kernel is given by
\[ W_{ij} = \frac{1}{K_i} \text{ exp} \left( -\frac{\|\boldsymbol{x}_i - \boldsymbol{x}_j \|^2}{\sigma_s^2}\right) \text{ exp} \left( -\frac{\|f(\boldsymbol{x}_i) - f(\boldsymbol{x}_j) \|^2}{\sigma_r^2}\right). \]
Here, \(K_i\) is a normalizing parameter to ensure that \(\sum_j W_{ij} = 1\), \(\boldsymbol{x}_i\) denotes the pixel coordinates of pixel \(i\) and \(f(\boldsymbol{x}_i)\) is the color/intensity at the pixel \(i\) (analogously for \(\boldsymbol{x}_j\) and \(f(\boldsymbol{x}_j)\)).
The domain filtering therefore happens with a Gaussian with standard deviation \(\sigma_s\) (``s'' stands for ``spatial domain'') and the range filtering with a Gaussian with standard deviation \(\sigma_r\) (``r'' stands for ``range''). 
The domain part guarantees that pixels spatially far away have a very small weight. 
The range part ensures to give a small weight to pixels far away in the range, i.e.\ pixels that are not similar to the currently filtered one. 
Note that whereas in the above equation we consider the Euclidean distance \(\|f(\boldsymbol{x}_i) - f(\boldsymbol{x}_j)\|^2\) for the range part, we could use any other measure of similarity. 
For color images, Tomasi et al.\ (\cite{tomasi}) proposed to use the Euclidean distance in CIE-Lab color space since in this space a small Euclidean distance corresponds to human perceptible similarity. Therefore the value \(f(q)\) of a pixel \(q\) is the weighted average of the values \(f(p)\) of the pixels \(p\) in a neighborhood of \(q\) that look similar to \(f(q)\).
%Explain the bilateral filter, especially the meaning of the filter kernel. Write about the problem that can occur like gradien reversal artifacs. Introduce the idea of a guidance image by explaining some points of the joint bilateral filter. Mention the problems of large computational times and the efforts to make fast implementations.

\subsubsection*{Joint bilateral filtering}
In 2004, a generalization of bilateral filtering, called \emph{joint bilateral filtering}, was introduced by Petschnigg et al.\ \cite{petschnigg2004}. 
The same concept was called \emph{cross-bilateral filtering} by Eisemann and Durand \cite{eisemann2004}. The idea is to use a \emph{guidance image} that contains more accurate, i.e.\ unnoisy, information about the high-frequency structures of an image. 
As an example, Petschnigg et al.\ used a photography taken with flash light as a guidance image to denoise a no-flash photography (see Figure~\ref{fig:flashNoFlash}). 
\begin{figure}[ht]
	\centering
	\begin{subfigure}[h]{0.3\textwidth}
		%\centering
		\includegraphics[width=\textwidth]{petschnigg_flash}
		\caption*{Flash}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.3\textwidth}
		%\centering
		\includegraphics[width=\textwidth]{petschnigg_no-flash}
		\caption*{No-flash}
	\end{subfigure}
	~
	\begin{subfigure}[h]{0.3\textwidth}
		%\centering
		\includegraphics[width=\textwidth]{petschnigg_filtered}
		\caption*{Detail transfer, denoising}
	\end{subfigure}
\caption{An application of joint bilateral filtering - denoising a no-flash image using the detail information of a flash image. 
The images are taken from \cite{petschnigg2004}.}
\label{fig:flashNoFlash}
\end{figure}
The only difference in the filter kernel is that the range part of the kernel is not applied to the range of the original image but instead it is applied to the range of the guidance image:
\[ W_{ij} = \frac{1}{K_i} \text{ exp} \left( -\frac{\|\boldsymbol{x}_i - \boldsymbol{x}_j \|^2}{\sigma_s^2}\right) \text{ exp} \left( -\frac{\|g(\boldsymbol{x}_i) - g(\boldsymbol{x}_j) \|^2}{\sigma_r^2}\right). \]
Note that instead of the function \(f\) which was the pixel value of the original image at a pixel, we now have the function \(g\) which denotes the pixel value of some guidance image at a pixel.

\subsubsection*{Efficiency}
A brute-force implementation of bilateral filtering needs \(\mathcal{O}(N r^2)\) time with \(N\) being the number of pixels in the image and \(r\) being the filter radius that is implicitly given by the support of the spatial part of the filter kernel. In 2008, F.~Porikli proposed a constant-time implementation of the bilateral filter with box spatial and arbitrary range kernels and also of the bilateral filter constructed by arbitrary spatial and polynomial range kernels \cite{porikli2008constant}. Using Taylor series, he was able to also approximate the Gaussian bilateral filter. Chaudhury et al.\ proposed in 2011 to use trigonometric range kernels instead of polynomial ones in order to approximate the standard Gaussian bilateral filter \cite{chaudhury2011fast}. Other acceleration strategies were e.g.\ proposed by Yang et al.\ \cite{yang2009real} and by Paris and Durand \cite{paris2009fast}. One method that concentrates on the problems caused by high dimensionality was proposed by Gastal et al.\ in 2012 \cite{adaptiveManifolds}.