This chapter gives some of the basic ideas of the problems that occur in simulating illumination. The rendering equation will be roughly explained and also the path tracing algorithm that comes up with a numerical approach to solve it. Furthermore, a few statistical key terms will be defined. We then give some explanations of filtering methods and a short overview of the adaptive sampling and reconstruction approach by Rouselle et al.\ \cite{gem}.

\section{The rendering equation}
In 1986, James T. Kajiya introduced the so-called \emph{rendering equation} (\cite{rendEqKajiya}). 
This equation describes the behavior of light within a 3-dimensional scene in a general way in the sense that it is not restricted to certain kinds of materials and reflection properties. What the equation describes is the total radiance\footnote{Radiance can be intuitively understood as the ``amount of light.'' } leaving a point \(x\) on a surface along a direction \(\omega_0\), e.g.\ the direction from where we look at the surface (see Figure~\ref{fig:radiance_in_direction}).
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{light_in_direction}
	\caption{The rendering equation describes the radiance leaving a point $x$ on a surface along a direction $\omega_0$.}
	\label{fig:radiance_in_direction}
\end{figure}
\subsection{The equation}
The rendering equation says that the leaving radiance is given as the sum of the emitted and the reflected radiance. To give some details, the rendering equation can be written as follows\footnote{This is the formulation of Prof.\ Dr.\ M.\ Zwicker, presented in the lecture ``Computergrafik'' in fall semester 2012 at the University of Bern.}:
\begin{equation}
	L(x,\omega_0) = \underbrace{L_e(x,\omega_0)}_{\text{emitted radiance}} + \underbrace{\int\limits_\Omega f(x,\omega_0,\omega_i)L(x_i,-\omega_i)cos\theta_i d\omega_i}_{\text{reflected radiance}},
\label{eq:rendering_equation}
\end{equation}
where \(x\) and \(\omega_0\) are as above, \( \Omega \) is the hemisphere at \(x\), \( \omega_i \) is the direction of the incoming radiance and \(x_i\) is the point that is visible from \(x\) in direction \( \omega_i \) (see Figure~\ref{rend_eq_illustration}).
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{rendering_equation_illustration}
	\caption{$\omega_0$ is the direction along which we want to know how much radiance is leaving from the point $x$ on the surface. We have to take into account all the possible directions $\omega_i$ of the hemisphere $\Omega$ to calculate the total reflected radiance.}
	\label{rend_eq_illustration}
\end{figure} \newline
The function \(f\) in equation~(\ref{eq:rendering_equation}) is the so-called \emph{bidirectional reflectance distribution function} (BRDF) that describes the appearance and reflection properties of a surface -- for example a white sheet of paper does not reflect incoming light in the same way as a mirror does, which influences the reflected radiance (see Figure~\ref{fig:brdf_illustration}).
\begin{figure}[ht]
	\centering
	\includegraphics[width=10.0cm]{brdf_illustration.jpg}
	\caption{Materials can have different reflection properties. The rendering equation is not restricted to certain kinds of materials -- the BRDF is treated as an arbitrary (integrable) function. The above images are taken from the lecture notes of the lecture ``Computergrafik'' by Prof.\ Dr.\ M.\ Zwicker in fall semester 2012 at the University of Bern, 06\_Shading, p.23.}
	\label{fig:brdf_illustration}
\end{figure}
The rendering equation is a physical-based model to describe the behavior of light. This is why solving it leads to somewhat realistic and plausible-looking illumination conditions in a scene. 
Whereas other approaches, such as e.g.\ the rasterization algorithm, have to apply some hacks to achieve even simple effects, solving the rendering equation already naturally includes many important aspects of light such as soft shadows, caustics, depth of field, reflection, refraction and many others.
\subsection{Limitations}
Since the rendering equation does not include aspects of the wave properties of light, it does not enable rendering effects like e.g.\ polarization, diffraction or fluorescence. From a numerical point of view the rendering equation yields problems because of its high dimensionality. A high-dimensional integral can generally not be solved in closed form. Using quadrature rules to approximate the integral works, but such methods scale badly with the dimensionality of an integral and therefore lead to huge computational costs. Two commonly used approaches from numerical mathematics to approximate the value of a high-dimensional integral are finite element methods and Monte Carlo integration. The latter will be shortly discussed in Section~\ref{sec:monte-carlo}. For a more detailed overview of the methods, see \cite{numerical-integration}.