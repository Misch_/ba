\section{Mean squared error - MSE}
\label{sec:mse}

\subsection*{Estimator}
In statistics, a so-called \emph{estimator} is defined as a rule for calculating an estimate of a given quantity. 
The estimator makes the estimates based on some data that can be observed. More formally, if \(\theta\) is the quantity that we want to estimate, then we call \(\hat{\theta}\) an \emph{estimator} of \(\theta\). 
As already mentioned, \(\hat{\theta}\) performs the estimate based on some observed data, and thus we write \(\hat{\theta}(x)\) to denote an \emph{estimate} of \(\theta\) for an observed dataset \(x\). The observed dataset \(x\) is also called a \emph{sample} and is one of several outcomes of a random variable \(X\).

\subsection*{Absolute and relative error}
For a given sample \(x\), the \emph{absolute error} of an estimator is given by \(|\hat{\theta}(x) - \theta|\). The \emph{relative error} considers the error with respect to the magnitude of the estimated value and is given by \(|\frac{\hat{\theta}(x) - \theta}{\theta}|\). Considering the relative error when comparing images may be a better choice than the absolute error because otherwise some errors may be overemphasized due to the differences in brightness in the image.

\subsection*{Variance, covariance and bias}
The \emph{covariance} of two random variables \(X\) and \(Y\) is defined as
\[\text{Cov}(X,Y) = \text{E}[XY] - \text{E}[X] \text{E}[Y],\]
where \(\text{E}\) denotes the \emph{expected value}.

The \emph{variance} measures how far a set of numbers is spread out. If \(X\) is a random variable, then the variance is defined as
\[\text{Var}(X) = \text{E}[X^2] - \text{E}[X]^2.\]

Extending the variance to a multidimensional random vector ends up in the so-called \emph{covariance matrix}. If \(X = (X_1, ..., X_n)\) is a random vector, then the covariance matrix is given by
\[ \Sigma = 
\begin{bmatrix}
\text{Cov}(X_1,X_1)			& \text{Cov}(X_1,X_2)	& \cdots	&  \text{Cov}(X_1,X_n)	\\
\text{Cov}(X_2,X_1)	& \ddots							& 				& \vdots 								\\
\vdots 							& 										& \ddots	& 	 										\\
\text{Cov}(X_n,X_2)	&	\cdots							&					& \text{Cov}(X_n,X_n)
\end{bmatrix},\]
where 
\[\text{Cov}(X_i,X_i) = \text{E}[X_i X_i] - \text{E}[X_i] \text{E}[X_i] = \text{E}[X_i^2] - \text{E}[X_i]^2 = \text{Var}(X_i), \forall i \in \{1,...,n\}.\]

The \emph{bias} of an estimator measures the expected difference of the estimates and the true value of the estimated parameter:
\[\text{Bias}(\hat{\theta},\theta) = \text{E}[\hat{\theta}-\theta.]\]

\subsection*{Mean squared error (MSE)}
The \emph{mean squared error} of an estimator \(\hat{\theta}\) is defined as the expected value of the squared errors,
\[ \text{MSE}(\hat{\theta}) = \text{E}[(\hat{\theta}(X) - \theta)^2].\]
It is equal to the sum of the variance and the squared bias,
\[ \text{MSE}(\hat{\theta}) = \text{Var}(\hat{\theta}) + \text{Bias}(\hat{\theta},\theta)^2.\]
In this thesis the used error metric is the \emph{relative mean squared error} which, given a reference image \(I\) and another image \(I'\) of the same size, can be computed as 
\[\text{MSE}(I',I) = \frac{1}{n} \sum_{i=1}^n \frac{(I'_i - I_i)^2}{I_i^2 + 10^{-3}},\]
where \(n\) is the number of pixels in the images.

In the case of colored images, \(I'_i\) and \(I_i\) are \(3 \times 1\) vectors. The above average is then computed for each channel separately and the resulting relative MSE is the average of the relative MSE over the channels.
