\section{Ray tracing}
The core concept of ray tracing is to shoot light rays towards a 3D-scene, find their intersections with some objects in the scene and then, with this information, compute the color that should be rendered. A scene consists of geometric primitives such as spheres, cubes, polygons and also more complex objects, e.g.\ parametric patches  or implicit surfaces. 

In general, there is a distinction between \emph{direct} and \emph{indirect illumination} at a given point in a scene. Direct illumination refers to the illumination through some light sources. Indirect illumination concentrates on the illumination that is caused by the interaction of several objects that are not light sources. Some algorithms only perform \emph{ray casting} which means that only the direct illumination is gathered. Other ones, e.g.\ Whitted-style ray tracing, take some reflection and refraction properties into account.

\subsection{Whitted-style or recursive ray tracing}
% From the Whitted1980-paper, Sec.5 Summary:
% While in many cases the model generates very realistic effects, it leaves considerable room for improvement.
% Specifically, it does not provide for diffuse reflection from distributed light sources, nor does it gracefully handle specular reflections from less glossy surfaces.
%
In 1980, Turner Whitted presented in \cite{Whitted1980} a method that is nowadays called \emph{Whitted-style} or \emph{recursive ray tracing}. 
Whitted-style ray tracing refers to a technique that simulates one aspect of indirect illumination, namely the \emph{specular-to-specular light transport} (\cite{ray-tracing}). 
Whereas ray casting always stops after computing the direct illumination of a point on a surface, Whitted-style ray tracing does not necessarily do so. 
Instead, when a ray hits an object whose material is specular, a further ray will be calculated that leaves the surface in the direction of mirror reflection (see Figure~\ref{fig:whitted-style}). 
Such reflected rays are examples of \emph{secondary rays}. 
They can themselves hit objects and produce further reflected rays. 
A maximal number of bounces, often also called \emph{maximal depth}, has to be given to terminate the recursive procedure. 
This method enables the possibility to render some cases of reflection and refraction.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{mirror_reflection}	
	\caption{Whitted-style ray tracing will shoot a ray to the mirror. Because the material is specular, a reflected ray will be computed. The reflected ray may hit another object and again rise a new reflected or refracted ray. The recursive procedure stops if the maximal depth is reached or if a ray hits an object that is non-specular.}
	\label{fig:whitted-style}
\end{figure}

\subsection{Path tracing}
Path tracing is a more sophisticated ray tracing algorithm that approximates the rendering equation (\ref{eq:rendering_equation}) using Monte Carlo integration. 
E.g.\ glossy materials cannot be gracefully handled by Whitted-style ray tracing. T. Whitted proposed (\cite{Whitted1980}) to calculate the mirror direction and then introduce some little perturbations to simulate glossy materials, but this is not a physical correct interpretation of the reflection properties of a glossy surface. The essential difference between Whitted-style ray tracing and path tracing is that reflected rays are not only considered in the case of specular material but instead for all kinds of materials. Thus, the algorithm could be considered as a generalization of Whitted-style ray tracing to all BRDF functions. 
Since a diffuse surface reflects any incoming ray uniformly towards every direction, an outgoing ray could possibly be a reflection from any direction in the hemisphere. Hence this generalization makes the problem much more complex because diffuse materials that were ignored before now theoretically cause infinitely many reflected rays. Why it is important to consider the case of diffuse material for the simulation of realistic illumination conditions can be understood with the following example: 
Suppose that there are two objects in a scene where one of which is red and the other one is gray (see Figure~\ref{fig:path-tracing}).
\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{color-bleeding-schematic}	
	\caption{A scene consisting of two objects where one of which is red and the other one gray. Diffuse reflection causes some color bleeding and therefore the gray surface has a reddish tone.}
	\label{fig:path-tracing}
\end{figure}
Whitted-style ray tracing would only capture the direct illumination since in the case of diffuse materials, no secondary rays are calculated. 
The color that will be rendered is the color that comes only from direct illumination, i.e.\ from the ray that goes to the point directly from the light source. 
But what we see in reality is that the red object (and any other reflective surface in the scene) could reflect some rays from the light source (and any other rays) onto the diffuse surface (see Figure~\ref{fig:path-tracing}). This leads to color bleeding which means that e.g.\ the gray surface has a reddish tone if the two surfaces are near each other. See for example the gray cube on the left side of the rendered scene in Figure~\ref{fig:color-bleeding}.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{color-bleeding}	
	\caption{Cornell box scene. The gray cube on the left has a reddish tone caused by diffuse reflection from the red wall.}
	\label{fig:color-bleeding}
\end{figure}
As a further example, imagine a bright light on a table reflected by a glossy surface\footnote{Note that although theoretically caustics and mirror/glossy reflection onto diffuse surfaces are captured by path tracing, those are still very hard problems to solve and accurately rendering such effects requires a huge amount of samples.}. 

If a ray hits a surface in the scene, path tracing tries to consider reflected rays along every direction of the hemisphere. This is done by randomly shooting many rays towards a scene and at each hit with a surface calculating one further secondary ray towards a random direction. This procedure corresponds to an approximation of the rendering equation through Monte Carlo integration. It suffers from heavy noise if only few sample rays are considered (see e.g.\ Figure~\ref{fig:sppSponza}).